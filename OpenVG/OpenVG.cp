MODULE LibsOpenVG ["openvg"];

	IMPORT SYSTEM;

	CONST 
		OPENVG_VERSION_1_0* = 1;
		OPENVG_VERSION_1_0_1* = 1;
		OPENVG_VERSION_1_1* = 2;
		VG_MAXSHORT* = 7FFFH;
		VG_MAXINT* = 7FFFFFFFH;
		VG_MAX_ENUM* = 7FFFFFFFH;

		VG_INVALID_HANDLE* = 0;

		(* H2D: Enumeration: VGboolean *)
		VG_FALSE* = 0;
		VG_TRUE* = 1;
		VG_BOOLEAN_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGErrorCode *)
		VG_NO_ERROR* = 0;
		VG_BAD_HANDLE_ERROR* = 1000H;
		VG_ILLEGAL_ARGUMENT_ERROR* = 1001H;
		VG_OUT_OF_MEMORY_ERROR* = 1002H;
		VG_PATH_CAPABILITY_ERROR* = 1003H;
		VG_UNSUPPORTED_IMAGE_FORMAT_ERROR* = 1004H;
		VG_UNSUPPORTED_PATH_FORMAT_ERROR* = 1005H;
		VG_IMAGE_IN_USE_ERROR* = 1006H;
		VG_NO_CONTEXT_ERROR* = 1007H;
		VG_ERROR_CODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGParam*)

		(*  Mode settings  *)
		VG_MATRIX_MODE* = 1100H;
		VG_FILL_RULE* = 1101H;
		VG_IMAGE_QUALITY* = 1102H;
		VG_RENDERING_QUALITY* = 1103H;
		VG_BLEND_MODE* = 1104H;
		VG_IMAGE_MODE* = 1105H;

		(*  Scissoring rectangles  *)
		VG_SCISSOR_RECTS* = 1106H;

		(*  Color Transformation  *)
		VG_COLOR_TRANSFORM* = 1170H;
		VG_COLOR_TRANSFORM_VALUES* = 1171H;

		(*  Stroke parameters  *)
		VG_STROKE_LINE_WIDTH* = 1110H;
		VG_STROKE_CAP_STYLE* = 1111H;
		VG_STROKE_JOIN_STYLE* = 1112H;
		VG_STROKE_MITER_LIMIT* = 1113H;
		VG_STROKE_DASH_PATTERN* = 1114H;
		VG_STROKE_DASH_PHASE* = 1115H;
		VG_STROKE_DASH_PHASE_RESET* = 1116H;

		(*  Edge fill color for VG_TILE_FILL tiling mode  *)
		VG_TILE_FILL_COLOR* = 1120H;

		(*  Color for vgClear  *)
		VG_CLEAR_COLOR* = 1121H;

		(*  Glyph origin  *)
		VG_GLYPH_ORIGIN* = 1122H;

		(*  Enable/disable alpha masking and scissoring  *)
		VG_MASKING* = 1130H;
		VG_SCISSORING* = 1131H;

		(*  Pixel layout information  *)
		VG_PIXEL_LAYOUT* = 1140H;
		VG_SCREEN_LAYOUT* = 1141H;


		(*  Source format selection for image filters  *)
		VG_FILTER_FORMAT_LINEAR* = 1150H;
		VG_FILTER_FORMAT_PREMULTIPLIED* = 1151H;

		(*  Destination write enable mask for image filters  *)
		VG_FILTER_CHANNEL_MASK* = 1152H;

		(*  Implementation limits (read-only)  *)
		VG_MAX_SCISSOR_RECTS* = 1160H;
		VG_MAX_DASH_COUNT* = 1161H;
		VG_MAX_KERNEL_SIZE* = 1162H;
		VG_MAX_SEPARABLE_KERNEL_SIZE* = 1163H;
		VG_MAX_COLOR_RAMP_STOPS* = 1164H;
		VG_MAX_IMAGE_WIDTH* = 1165H;
		VG_MAX_IMAGE_HEIGHT* = 1166H;
		VG_MAX_IMAGE_PIXELS* = 1167H;
		VG_MAX_IMAGE_BYTES* = 1168H;
		VG_MAX_FLOAT* = 1169H;
		VG_MAX_GAUSSIAN_STD_DEVIATION* = 116AH;
		VG_PARAM_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGRenderingQuality *)
		VG_RENDERING_QUALITY_NONANTIALIASED* = 1200H;
		VG_RENDERING_QUALITY_FASTER* = 1201H;
		VG_RENDERING_QUALITY_BETTER* = 1202H;   (*  Default  *)
		VG_RENDERING_QUALITY_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPixelLayout *)
		VG_PIXEL_LAYOUT_UNKNOWN* = 1300H;
		VG_PIXEL_LAYOUT_RGB_VERTICAL* = 1301H;
		VG_PIXEL_LAYOUT_BGR_VERTICAL* = 1302H;
		VG_PIXEL_LAYOUT_RGB_HORIZONTAL* = 1303H;
		VG_PIXEL_LAYOUT_BGR_HORIZONTAL* = 1304H;
		VG_PIXEL_LAYOUT_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGMatrixMode *)
		VG_MATRIX_PATH_USER_TO_SURFACE* = 1400H;
		VG_MATRIX_IMAGE_USER_TO_SURFACE* = 1401H;
		VG_MATRIX_FILL_PAINT_TO_USER* = 1402H;
		VG_MATRIX_STROKE_PAINT_TO_USER* = 1403H;
		VG_MATRIX_GLYPH_USER_TO_SURFACE* = 1404H;
		VG_MATRIX_MODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGMaskOperation *)
		VG_CLEAR_MASK* = 1500H;
		VG_FILL_MASK* = 1501H;
		VG_SET_MASK* = 1502H;
		VG_UNION_MASK* = 1503H;
		VG_INTERSECT_MASK* = 1504H;
		VG_SUBTRACT_MASK* = 1505H;
		VG_MASK_OPERATION_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPathDatatype *)
		VG_PATH_FORMAT_STANDARD* = 0;
		VG_PATH_DATATYPE_S_8* = 0;
		VG_PATH_DATATYPE_S_16* = 1;
		VG_PATH_DATATYPE_S_32* = 2;
		VG_PATH_DATATYPE_F* = 3;
		VG_PATH_DATATYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPathAbsRel *)
		VG_ABSOLUTE* = 0;
		VG_RELATIVE* = 1;
		VG_PATH_ABS_REL_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPathSegment *)
		VG_CLOSE_PATH* = 0;
		VG_MOVE_TO* = 2;
		VG_LINE_TO* = 4;
		VG_HLINE_TO* = 6;
		VG_VLINE_TO* = 8;
		VG_QUAD_TO* = 10;
		VG_CUBIC_TO* = 12;
		VG_SQUAD_TO* = 14;
		VG_SCUBIC_TO* = 16;
		VG_SCCWARC_TO* = 18;
		VG_SCWARC_TO* = 20;
		VG_LCCWARC_TO* = 22;
		VG_LCWARC_TO* = 24;
		VG_PATH_SEGMENT_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPathCommand *)
		VG_MOVE_TO_ABS* = 2;
		VG_MOVE_TO_REL* = 3;
		VG_LINE_TO_ABS* = 4;
		VG_LINE_TO_REL* = 5;
		VG_HLINE_TO_ABS* = 6;
		VG_HLINE_TO_REL* = 7;
		VG_VLINE_TO_ABS* = 8;
		VG_VLINE_TO_REL* = 9;
		VG_QUAD_TO_ABS* = 10;
		VG_QUAD_TO_REL* = 11;
		VG_CUBIC_TO_ABS* = 12;
		VG_CUBIC_TO_REL* = 13;
		VG_SQUAD_TO_ABS* = 14;
		VG_SQUAD_TO_REL* = 15;
		VG_SCUBIC_TO_ABS* = 16;
		VG_SCUBIC_TO_REL* = 17;
		VG_SCCWARC_TO_ABS* = 18;
		VG_SCCWARC_TO_REL* = 19;
		VG_SCWARC_TO_ABS* = 20;
		VG_SCWARC_TO_REL* = 21;
		VG_LCCWARC_TO_ABS* = 22;
		VG_LCCWARC_TO_REL* = 23;
		VG_LCWARC_TO_ABS* = 24;
		VG_LCWARC_TO_REL* = 25;
		VG_PATH_COMMAND_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPathCapabilities *)
		VG_PATH_CAPABILITY_APPEND_FROM* = 1;
		VG_PATH_CAPABILITY_APPEND_TO* = 2;
		VG_PATH_CAPABILITY_MODIFY* = 4;
		VG_PATH_CAPABILITY_TRANSFORM_FROM* = 8;
		VG_PATH_CAPABILITY_TRANSFORM_TO* = 16;
		VG_PATH_CAPABILITY_INTERPOLATE_FROM* = 32;
		VG_PATH_CAPABILITY_INTERPOLATE_TO* = 64;
		VG_PATH_CAPABILITY_PATH_LENGTH* = 128;
		VG_PATH_CAPABILITY_POINT_ALONG_PATH* = 256;
		VG_PATH_CAPABILITY_TANGENT_ALONG_PATH* = 512;
		VG_PATH_CAPABILITY_PATH_BOUNDS* = 1024;
		VG_PATH_CAPABILITY_PATH_TRANSFORMED_BOUNDS* = 2048;
		VG_PATH_CAPABILITY_ALL* = 4095;
		VG_PATH_CAPABILITIES_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPathParamType *)
		VG_PATH_FORMAT* = 1600H;
		VG_PATH_DATATYPE* = 1601H;
		VG_PATH_SCALE* = 1602H;
		VG_PATH_BIAS* = 1603H;
		VG_PATH_NUM_SEGMENTS* = 1604H;
		VG_PATH_NUM_COORDS* = 1605H;
		VG_PATH_PARAM_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGCapStyle *)
		VG_CAP_BUTT* = 1700H;
		VG_CAP_ROUND* = 1701H;
		VG_CAP_SQUARE* = 1702H;
		VG_CAP_STYLE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGJoinStyle *)
		VG_JOIN_MITER* = 1800H;
		VG_JOIN_ROUND* = 1801H;
		VG_JOIN_BEVEL* = 1802H;
		VG_JOIN_STYLE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGFillRule *)
		VG_EVEN_ODD* = 1900H;
		VG_NON_ZERO* = 1901H;
		VG_FILL_RULE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPaintMode *)
		VG_STROKE_PATH* = 1;
		VG_FILL_PATH* = 2;
		VG_PAINT_MODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPaintParamType *)

		(*  Color paint parameters  *)
		VG_PAINT_TYPE* = 1A00H;
		VG_PAINT_COLOR* = 1A01H;
		VG_PAINT_COLOR_RAMP_SPREAD_MODE* = 1A02H;
		VG_PAINT_COLOR_RAMP_PREMULTIPLIED* = 1A07H;
		VG_PAINT_COLOR_RAMP_STOPS* = 1A03H;

		(*  Linear gradient paint parameters  *)
		VG_PAINT_LINEAR_GRADIENT* = 1A04H;

		(*  Radial gradient paint parameters  *)
		VG_PAINT_RADIAL_GRADIENT* = 1A05H;

		(*  Pattern paint parameters  *)
		VG_PAINT_PATTERN_TILING_MODE* = 1A06H;
		VG_PAINT_PARAM_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGPaintType *)
		VG_PAINT_TYPE_COLOR* = 1B00H;
		VG_PAINT_TYPE_LINEAR_GRADIENT* = 1B01H;
		VG_PAINT_TYPE_RADIAL_GRADIENT* = 1B02H;
		VG_PAINT_TYPE_PATTERN* = 1B03H;
		VG_PAINT_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGColorRampSpreadMode *)
		VG_COLOR_RAMP_SPREAD_PAD* = 1C00H;
		VG_COLOR_RAMP_SPREAD_REPEAT* = 1C01H;
		VG_COLOR_RAMP_SPREAD_REFLECT* = 1C02H;
		VG_COLOR_RAMP_SPREAD_MODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGTilingMode *)
		VG_TILE_FILL* = 1D00H;
		VG_TILE_PAD* = 1D01H;
		VG_TILE_REPEAT* = 1D02H;
		VG_TILE_REFLECT* = 1D03H;
		VG_TILING_MODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGImageFormat *)

		(*  RGB{A,X} channel ordering  *)
		VG_sRGBX_8888* = 0;
		VG_sRGBA_8888* = 1;
		VG_sRGBA_8888_PRE* = 2;
		VG_sRGB_565* = 3;
		VG_sRGBA_5551* = 4;
		VG_sRGBA_4444* = 5;
		VG_sL_8* = 6;
		VG_lRGBX_8888* = 7;
		VG_lRGBA_8888* = 8;
		VG_lRGBA_8888_PRE* = 9;
		VG_lL_8* = 10;
		VG_A_8* = 11;
		VG_BW_1* = 12;
		VG_A_1* = 13;
		VG_A_4* = 14;

		(*  {A,X}RGB channel ordering  *)
		VG_sXRGB_8888* = 64;
		VG_sARGB_8888* = 65;
		VG_sARGB_8888_PRE* = 66;
		VG_sARGB_1555* = 68;
		VG_sARGB_4444* = 69;
		VG_lXRGB_8888* = 71;
		VG_lARGB_8888* = 72;
		VG_lARGB_8888_PRE* = 73;

		(*  BGR{A,X} channel ordering  *)
		VG_sBGRX_8888* = 128;
		VG_sBGRA_8888* = 129;
		VG_sBGRA_8888_PRE* = 130;
		VG_sBGR_565* = 131;
		VG_sBGRA_5551* = 132;
		VG_sBGRA_4444* = 133;
		VG_lBGRX_8888* = 135;
		VG_lBGRA_8888* = 136;
		VG_lBGRA_8888_PRE* = 137;

		(*  {A,X}BGR channel ordering  *)
		VG_sXBGR_8888* = 192;
		VG_sABGR_8888* = 193;
		VG_sABGR_8888_PRE* = 194;
		VG_sABGR_1555* = 196;
		VG_sABGR_4444* = 197;
		VG_lXBGR_8888* = 199;
		VG_lABGR_8888* = 200;
		VG_lABGR_8888_PRE* = 201;
		VG_IMAGE_FORMAT_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGImageQuality *)
		VG_IMAGE_QUALITY_NONANTIALIASED* = 1;
		VG_IMAGE_QUALITY_FASTER* = 2;
		VG_IMAGE_QUALITY_BETTER* = 4;
		VG_IMAGE_QUALITY_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGImageParamType *)
		VG_IMAGE_FORMAT* = 1E00H;
		VG_IMAGE_WIDTH* = 1E01H;
		VG_IMAGE_HEIGHT* = 1E02H;
		VG_IMAGE_PARAM_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGImageMode *)
		VG_DRAW_IMAGE_NORMAL* = 1F00H;
		VG_DRAW_IMAGE_MULTIPLY* = 1F01H;
		VG_DRAW_IMAGE_STENCIL* = 1F02H;
		VG_IMAGE_MODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGImageChannel *)
		VG_RED* = 8;
		VG_GREEN* = 4;
		VG_BLUE* = 2;
		VG_ALPHA* = 1;
		VG_IMAGE_CHANNEL_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGBlendMode *)
		VG_BLEND_SRC* = 2000H;
		VG_BLEND_SRC_OVER* = 2001H;
		VG_BLEND_DST_OVER* = 2002H;
		VG_BLEND_SRC_IN* = 2003H;
		VG_BLEND_DST_IN* = 2004H;
		VG_BLEND_MULTIPLY* = 2005H;
		VG_BLEND_SCREEN* = 2006H;
		VG_BLEND_DARKEN* = 2007H;
		VG_BLEND_LIGHTEN* = 2008H;
		VG_BLEND_ADDITIVE* = 2009H;
		VG_BLEND_MODE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGFontParamType *)
		VG_FONT_NUM_GLYPHS* = 2F00H;
		VG_FONT_PARAM_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGHardwareQueryType *)
		VG_IMAGE_FORMAT_QUERY* = 2100H;
		VG_PATH_DATATYPE_QUERY* = 2101H;
		VG_HARDWARE_QUERY_TYPE_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGHardwareQueryResult *)
		VG_HARDWARE_ACCELERATED* = 2200H;
		VG_HARDWARE_UNACCELERATED* = 2201H;
		VG_HARDWARE_QUERY_RESULT_FORCE_SIZE* = VG_MAX_ENUM;

		(* H2D: Enumeration: VGStringID *)
		VG_VENDOR* = 2300H;
		VG_RENDERER* = 2301H;
		VG_VERSION* = 2302H;
		VG_EXTENSIONS* = 2303H;
		VG_STRING_ID_FORCE_SIZE* = VG_MAX_ENUM;

	TYPE 
		VGboolean* = INTEGER;
		VGbyte* = SHORTCHAR;
		VGfloat* = REAL;
		VGubyte* = SHORTCHAR;
		VGshort* = SHORTINT;
		VGint* = INTEGER;
		VGuint* = INTEGER;
		VGbitfield* = INTEGER;

		VGHandle* = INTEGER;
		VGPath* = INTEGER;
		VGImage* = INTEGER;
		VGMaskLayer* = INTEGER;
		VGFont* = INTEGER;
		VGErrorCode* = INTEGER;
		VGParamType* = INTEGER;
		VGRenderingQuality* = INTEGER;
		VGPixelLayout* = INTEGER;
		VGMatrixMode* = INTEGER;
		VGMaskOperation* = INTEGER;
		VGPathDatatype* = INTEGER;
		VGPathAbsRel* = INTEGER;
		VGPathSegment* = INTEGER;
		VGPathCommand* = INTEGER;
		VGPathCapabilities* = INTEGER;
		VGPathParamType* = INTEGER;
		VGCapStyle* = INTEGER;
		VGJoinStyle* = INTEGER;
		VGFillRule* = INTEGER;
		VGPaintMode* = INTEGER;
		VGPaintParamType* = INTEGER;
		VGPaint= INTEGER;
		VGColorRampSpreadMode* = INTEGER;
		VGTilingMode* = INTEGER;
		VGImageFormat* = INTEGER;
		VGImageQuality* = INTEGER;
		VGImageParam= INTEGER;
		VGImageMode* = INTEGER;
		VGImageChannel* = INTEGER;
		VGBlendMode* = INTEGER;
		VGFontParamType* = INTEGER;
		VGHardwareQueryType* = INTEGER;
		VGHardwareQueryResult* = INTEGER;
		VGStringID* = INTEGER;

		Pointer* = POINTER TO RECORD [untagged] END;
		VGfloatArr* = POINTER TO ARRAY [untagged] OF VGfloat;
		VGintArr* = POINTER TO ARRAY [untagged] OF VGint;
		Array2VGfloat* = ARRAY [untagged] 2 OF VGfloat;
		VGubyteArr* = POINTER TO ARRAY [untagged] OF VGubyte;
		VGuintArr* = POINTER TO ARRAY [untagged] OF VGuint;
		VGshortArr* = POINTER TO ARRAY [untagged] OF VGshort;

	(*  Function Prototypes  *)

	PROCEDURE vgGetError* (): VGErrorCode;

	PROCEDURE vgFlush*;

	PROCEDURE vgFinish*;

	(*  Getters and Setters  *)

	PROCEDURE vgSetf* ( type: VGParamType; value: VGfloat );

	PROCEDURE vgSeti* ( type: VGParamType; value: VGint );

	PROCEDURE vgSetfv* ( type: VGParamType; count: VGint; values: VGfloatArr );

	PROCEDURE vgSetiv* ( type: VGParamType; count: VGint; values: VGintArr );

	PROCEDURE vgGetf* ( type: VGParamType ): VGfloat;

	PROCEDURE vgGeti* ( type: VGParamType ): VGint;

	PROCEDURE vgGetVectorSize* ( type: VGParamType ): VGint;

	PROCEDURE vgGetfv* ( type: VGParamType; count: VGint; values: VGfloatArr );

	PROCEDURE vgGetiv* ( type: VGParamType; count: VGint; values: VGintArr );

	PROCEDURE vgSetParameterf* ( object: VGHandle; paramType: VGint; value: VGfloat );

	PROCEDURE vgSetParameteri* ( object: VGHandle; paramType: VGint; value: VGint );

	PROCEDURE vgSetParameterfv* ( object: VGHandle; paramType: VGint; count: VGint;
                              values: VGfloatArr );

	PROCEDURE vgSetParameteriv* ( object: VGHandle; paramType: VGint; count: VGint;
                              values: VGintArr );

	PROCEDURE vgGetParameterf* ( object: VGHandle; paramType: VGint ): VGfloat;

	PROCEDURE vgGetParameteri* ( object: VGHandle; paramType: VGint ): VGint;

	PROCEDURE vgGetParameterVectorSize* ( object: VGHandle; paramType: VGint ): VGint;

	PROCEDURE vgGetParameterfv* ( object: VGHandle; paramType: VGint; count: VGint;
                              values: VGfloatArr );

	PROCEDURE vgGetParameteriv* ( object: VGHandle; paramType: VGint; count: VGint;
                              values: VGintArr );

	(*  Matrix Manipulation  *)

	PROCEDURE vgLoadIdentity*;

	PROCEDURE vgLoadMatrix* ( m: VGfloatArr );

	PROCEDURE vgGetMatrix* ( m: VGfloatArr );

	PROCEDURE vgMultMatrix* ( m: VGfloatArr );

	PROCEDURE vgTranslate* ( tx: VGfloat; ty: VGfloat );

	PROCEDURE vgScale* ( sx: VGfloat; sy: VGfloat );

	PROCEDURE vgShear* ( shx: VGfloat; shy: VGfloat );

	PROCEDURE vgRotate* ( angle: VGfloat );

	(*  Masking and Clearing  *)

	PROCEDURE vgMask* ( mask: VGHandle; operation: VGMaskOperation; x: VGint; y: VGint;
                    width: VGint; height: VGint );

	PROCEDURE vgRenderToMask* ( path: VGPath; paintModes: VGbitfield;
                            operation: VGMaskOperation );

	PROCEDURE vgCreateMaskLayer* ( width: VGint; height: VGint ): VGMaskLayer;

	PROCEDURE vgDestroyMaskLayer* ( maskLayer: VGMaskLayer );

	PROCEDURE vgFillMaskLayer* ( maskLayer: VGMaskLayer; x: VGint; y: VGint; width: VGint;
                             height: VGint; value: VGfloat );

	PROCEDURE vgCopyMask* ( maskLayer: VGMaskLayer; dx: VGint; dy: VGint; sx: VGint;
                        sy: VGint; width: VGint; height: VGint );

	PROCEDURE vgClear* ( x: VGint; y: VGint; width: VGint; height: VGint );

	(*  Paths  *)

	PROCEDURE vgCreatePath* ( pathFormat: VGint; datatype: VGPathDatatype; scale: VGfloat;
                          bias: VGfloat; segmentCapacityHint: VGint;
                          coordCapacityHint: VGint; capabilities: VGbitfield ): VGPath;

	PROCEDURE vgClearPath* ( path: VGPath; capabilities: VGbitfield );

	PROCEDURE vgDestroyPath* ( path: VGPath );

	PROCEDURE vgRemovePathCapabilities* ( path: VGPath; capabilities: VGbitfield );

	PROCEDURE vgGetPathCapabilities* ( path: VGPath ): VGbitfield;

	PROCEDURE vgAppendPath* ( dstPath: VGPath; srcPath: VGPath );

	PROCEDURE vgAppendPathData* ( dstPath: VGPath; numSegments: VGint;
                              pathSegments: VGubyteArr; pathData: Pointer );

	PROCEDURE vgModifyPathCoords* ( dstPath: VGPath; startIndex: VGint; numSegments: VGint;
                                pathData: Pointer );

	PROCEDURE vgTransformPath* ( dstPath: VGPath; srcPath: VGPath );

	PROCEDURE vgInterpolatePath* ( dstPath: VGPath; startPath: VGPath; endPath: VGPath;
                               amount: VGfloat ): VGboolean;

	PROCEDURE vgPathLength* ( path: VGPath; startSegment: VGint;
                          numSegments: VGint ): VGfloat;

	PROCEDURE vgPointAlongPath* ( path: VGPath; startSegment: VGint; numSegments: VGint;
                              distance: VGfloat; x: VGfloatArr; y: VGfloatArr;
                              tangentX: VGfloatArr; tangentY: VGfloatArr );

	PROCEDURE vgPathBounds* ( path: VGPath; minX: VGfloatArr; minY: VGfloatArr;
                          width: VGfloatArr; height: VGfloatArr );

	PROCEDURE vgPathTransformedBounds* ( path: VGPath; minX: VGfloatArr;
                                     minY: VGfloatArr; width: VGfloatArr;
                                     height: VGfloatArr );

	PROCEDURE vgDrawPath* ( path: VGPath; paintModes: VGbitfield );

	(*  Paint  *)

	PROCEDURE vgCreatePaint* (): VGPaint;

	PROCEDURE vgDestroyPaint* ( paint: VGPaint );

	PROCEDURE vgSetPaint* ( paint: VGPaint; paintModes: VGbitfield );

	PROCEDURE vgGetPaint* ( paintMode: VGPaintMode ): VGPaint;

	PROCEDURE vgSetColor* ( paint: VGPaint; rgba: VGuint );

	PROCEDURE vgGetColor* ( paint: VGPaint ): VGuint;

	PROCEDURE vgPaintPattern* ( paint: VGPaint; pattern: VGImage );

	(*  Images  *)

	PROCEDURE vgCreateImage* ( format: VGImageFormat; width: VGint; height: VGint;
                           allowedQuality: VGbitfield ): VGImage;

	PROCEDURE vgDestroyImage* ( image: VGImage );

	PROCEDURE vgClearImage* ( image: VGImage; x: VGint; y: VGint; width: VGint;
                          height: VGint );

	PROCEDURE vgImageSubData* ( image: VGImage; data: Pointer; dataStride: VGint;
                            dataFormat: VGImageFormat; x: VGint; y: VGint;
                            width: VGint; height: VGint );

	PROCEDURE vgGetImageSubData* ( image: VGImage; data: Pointer; dataStride: VGint;
                               dataFormat: VGImageFormat; x: VGint; y: VGint;
                               width: VGint; height: VGint );

	PROCEDURE vgChildImage* ( parent: VGImage; x: VGint; y: VGint; width: VGint;
                          height: VGint ): VGImage;

	PROCEDURE vgGetParent* ( image: VGImage ): VGImage;

	PROCEDURE vgCopyImage* ( dst: VGImage; dx: VGint; dy: VGint; src: VGImage; sx: VGint;
                         sy: VGint; width: VGint; height: VGint; dither: VGboolean );

	PROCEDURE vgDrawImage* ( image: VGImage );

	PROCEDURE vgSetPixels* ( dx: VGint; dy: VGint; src: VGImage; sx: VGint; sy: VGint;
                         width: VGint; height: VGint );

	PROCEDURE vgWritePixels* ( data: Pointer; dataStride: VGint;
                           dataFormat: VGImageFormat; dx: VGint; dy: VGint;
                           width: VGint; height: VGint );

	PROCEDURE vgGetPixels* ( dst: VGImage; dx: VGint; dy: VGint; sx: VGint; sy: VGint;
                         width: VGint; height: VGint );

	PROCEDURE vgReadPixels* ( data: Pointer; dataStride: VGint;
                          dataFormat: VGImageFormat; sx: VGint; sy: VGint;
                          width: VGint; height: VGint );

	PROCEDURE vgCopyPixels* ( dx: VGint; dy: VGint; sx: VGint; sy: VGint; width: VGint;
                          height: VGint );

	(*  Text  *)

	PROCEDURE vgCreateFont* ( glyphCapacityHint: VGint ): VGFont;

	PROCEDURE vgDestroyFont* ( font: VGFont );

	PROCEDURE vgSetGlyphToPath* ( font: VGFont; glyphIndex: VGuint; path: VGPath;
                              isHinted: VGboolean; glyphOrigin: Array2VGfloat;
                              escapement: Array2VGfloat );

	PROCEDURE vgSetGlyphToImage* ( font: VGFont; glyphIndex: VGuint; image: VGImage;
                               glyphOrigin: Array2VGfloat;
                               escapement: Array2VGfloat );

	PROCEDURE vgClearGlyph* ( font: VGFont; glyphIndex: VGuint );

	PROCEDURE vgDrawGlyph* ( font: VGFont; glyphIndex: VGuint; paintModes: VGbitfield;
                         allowAutoHinting: VGboolean );

	PROCEDURE vgDrawGlyphs* ( font: VGFont; glyphCount: VGint; glyphIndices: VGuintArr;
                          adjustments_x: VGfloatArr; adjustments_y: VGfloatArr;
                          paintModes: VGbitfield; allowAutoHinting: VGboolean );

	(*  Image Filters  *)

	PROCEDURE vgColorMatrix* ( dst: VGImage; src: VGImage; matrix: VGfloatArr );

	PROCEDURE vgConvolve* ( dst: VGImage; src: VGImage; kernelWidth: VGint;
                        kernelHeight: VGint; shiftX: VGint; shiftY: VGint;
                        kernel: VGshortArr; scale: VGfloat; bias: VGfloat;
                        tilingMode: VGTilingMode );

	PROCEDURE vgSeparableConvolve* ( dst: VGImage; src: VGImage; kernelWidth: VGint;
                                 kernelHeight: VGint; shiftX: VGint; shiftY: VGint;
                                 kernelX: VGshortArr; kernelY: VGshortArr;
                                 scale: VGfloat; bias: VGfloat;
                                 tilingMode: VGTilingMode );

	PROCEDURE vgGaussianBlur* ( dst: VGImage; src: VGImage; stdDeviationX: VGfloat;
                            stdDeviationY: VGfloat; tilingMode: VGTilingMode );

	PROCEDURE vgLookup* ( dst: VGImage; src: VGImage; redLUT: VGubyteArr;
                      greenLUT: VGubyteArr; blueLUT: VGubyteArr;
                      alphaLUT: VGubyteArr; outputLinear: VGboolean;
                      outputPremultiplied: VGboolean );

	PROCEDURE vgLookupSingle* ( dst: VGImage; src: VGImage; lookupTable: VGuintArr;
                            sourceChannel: VGImageChannel; outputLinear: VGboolean;
                            outputPremultiplied: VGboolean );

	(*  Hardware Queries  *)

	PROCEDURE vgHardwareQuery* ( key: VGHardwareQueryType;
                             setting: VGint ): VGHardwareQueryResult;

	(*  Renderer and Extension Information  *)

	PROCEDURE vgGetString* ( name: VGStringID ): VGubyteArr;

END LibsOpenVG.