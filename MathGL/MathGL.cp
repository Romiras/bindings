(* Component Pascal binding for MathGL *)

MODULE LibsMathGL ["mgl"];

	CONST
		MGL_VERSION*	= 8.1;

	TYPE

		Pointer* = POINTER TO RECORD [untagged] END;
		Byte1D* = POINTER TO ARRAY [untagged] OF BYTE;
		String* = POINTER TO ARRAY [untagged] OF SHORTCHAR;
		UnicodeString* = POINTER TO ARRAY [untagged] OF CHAR;

		Single1D* = POINTER TO ARRAY [untagged] OF SHORTREAL;
		Single2D* = POINTER TO ARRAY [untagged] OF ARRAY [untagged] OF SHORTREAL;
		Single3D* = POINTER TO ARRAY [untagged] OF ARRAY [untagged] OF ARRAY [untagged] OF SHORTREAL;
		Double1D* = POINTER TO ARRAY [untagged] OF REAL;
		Double2D* = POINTER TO ARRAY [untagged] OF ARRAY [untagged] OF REAL;
		Double3D* = POINTER TO ARRAY [untagged] OF ARRAY [untagged] OF ARRAY [untagged] OF REAL;

		HMGL* = Pointer;
		HMDT* = Pointer;
		HMPR* = Pointer;

		GSLVector* = Pointer;
		GSLMatrix* = Pointer;

		GLUTDrawFunction* = PROCEDURE [ccall] (gr: HMGL; p: Pointer): INTEGER;

	(*****************************************************************************)

	(* HMGL mgl_create_graph_gl(); *)
	PROCEDURE [ccall] mgl_create_graph_gl* (): HMGL;

	(* HMGL mgl_create_graph_zb(int width, int height); *)
	PROCEDURE [ccall] mgl_create_graph_zb* (width, height: INTEGER): HMGL;

	(* HMGL mgl_create_graph_ps(int width, int height); *)
	PROCEDURE [ccall] mgl_create_graph_ps* (width, height: INTEGER): HMGL;

	(* HMGL mgl_create_graph_glut(int ( *draw)(HMGL gr, void *p), const char *title, void *par); *)
	PROCEDURE [ccall] mgl_create_graph_glut* ["mgl-glut", ""] (draw: GLUTDrawFunction; title: String; par: Pointer): HMGL;

	(* HMGL mgl_create_graph_fltk(int ( *draw)(HMGL gr, void *p), const char *title, void *par); *)
	PROCEDURE [ccall] mgl_create_graph_fltk* ["mgl-fltk", ""] (draw: GLUTDrawFunction; title: String; par: Pointer): HMGL;

	(* HMGL mgl_create_graph_qt(int ( *draw)(HMGL gr, void *p), const char *title, void *par); *)
	PROCEDURE [ccall] mgl_create_graph_qt* ["mgl-qt", ""] (draw: GLUTDrawFunction; title: String; par: Pointer): HMGL;

	(* HMGL mgl_create_graph_idtf(); *)
	PROCEDURE [ccall] mgl_create_graph_idtf* (): HMGL;

	(* void mgl_fltk_run(); *)
	PROCEDURE [ccall] mgl_fltk_run* ["mgl-fltk", ""] ();

	(* void mgl_qt_run(); *)
	PROCEDURE [ccall] mgl_qt_run* ["mgl-qt", ""] ();

	(* void mgl_set_show_mouse_pos(HMGL gr, int enable); *)
	PROCEDURE [ccall] mgl_set_show_mouse_pos* (graph: HMGL; enable: INTEGER);

	(* void mgl_get_last_mouse_pos(HMGL gr, float *x, float *y, float *z); *)
	PROCEDURE [ccall] mgl_get_last_mouse_pos* (gr: HMGL; x: Single1D; y: Single1D; z: Single1D);

	(* void mgl_update(HMGL graph); *)
	PROCEDURE [ccall] mgl_update* (graph: HMGL);

	(* void mgl_delete_graph(HMGL graph); *)
	PROCEDURE [ccall] mgl_delete_graph* (graph: HMGL);

	(*****************************************************************************)
	(* HMDT mgl_create_data(); *)
	PROCEDURE [ccall] mgl_create_data* (): HMDT;

	(* HMDT mgl_create_data_size(int nx, int ny, int nz); *)
	PROCEDURE [ccall] mgl_create_data_size* (nx, ny, nz: INTEGER): HMDT;

	(* HMDT mgl_create_data_file(char *fname); *)
	PROCEDURE [ccall] mgl_create_data_file* (fname: String): HMDT;

	(* void mgl_delete_data(HMDT dat); *)
	PROCEDURE [ccall] mgl_delete_data* (dat: HMDT);

	(*****************************************************************************)
	(* HMPR mgl_create_parser(); *)
	PROCEDURE [ccall] mgl_create_parser* (): HMPR;

	(* void mgl_delete_parser(HMPR p); *)
	PROCEDURE [ccall] mgl_delete_parser* (p: HMPR);

	(* void mgl_add_param(HMPR p, int id, const char *str); *)
	PROCEDURE [ccall] mgl_add_param* (p: HMPR; id: INTEGER; str: String);

	(* void mgl_add_paramw(HMPR p, int id, const wchar_t *str); *)
	PROCEDURE [ccall] mgl_add_paramw* (p: HMPR; id: INTEGER; str: UnicodeString);

	(* ===!!! NOTE !!! You must not delete obtained data arrays !!!=============== *)
	(* HMDT mgl_add_var(HMPR, const char *name); *)
	PROCEDURE [ccall] mgl_add_var* (p: HMPR; name: String): HMDT;

	(* ===!!! NOTE !!! You must not delete obtained data arrays !!!=============== *)
	(* HMDT mgl_find_var(HMPR, const char *name); *)
	PROCEDURE [ccall] mgl_find_var* (p: HMPR; name: String): HMDT;

	(* int mgl_parse(HMGL gr, HMPR p, const char *str, int pos); *)
	PROCEDURE [ccall] mgl_parse* (gr: HMGL; p: HMPR; str: String; pos: INTEGER): INTEGER;

	(* int mgl_parsew(HMGL gr, HMPR p, const wchar_t *str, int pos); *)
	PROCEDURE [ccall] mgl_parsew* (gr: HMGL; p: HMPR; str: UnicodeString; pos: INTEGER): INTEGER;

	(* void mgl_parse_text(HMGL gr, HMPR p, const char *str); *)
	PROCEDURE [ccall] mgl_parse_text* (gr: HMGL; p: HMPR; str: String);

	(* void mgl_parsew_text(HMGL gr, HMPR p, const wchar_t *str); *)
	PROCEDURE [ccall] mgl_parsew_text* (gr: HMGL; p: HMPR; str: UnicodeString);

	(* void mgl_restore_once(HMPR p); *)
	PROCEDURE [ccall] mgl_restore_once* (p: HMPR);

	(* void mgl_parser_allow_setsize(HMPR p, int a); *)
	PROCEDURE [ccall] mgl_parser_allow_setsize* (p: HMPR; a: INTEGER);


	(*****************************************************************************)
	(* 		Setup mglGraph														  *)
	(*****************************************************************************)
	(* void mgl_set_def_param(HMGL gr); *)
	PROCEDURE [ccall] mgl_set_def_param* (graph: HMGL);

	(* void mgl_set_palette(HMGL gr, const char *colors); *)
	PROCEDURE [ccall] mgl_set_palette* (graph: HMGL; colors: String);

	(* void mgl_set_pal_color(HMGL graph, int n, float r, float g, float b); *)
	PROCEDURE [ccall] mgl_set_pal_color* (graph: HMGL; n: INTEGER; r, g, b: SHORTREAL);

	(* void mgl_set_pal_num(HMGL graph, int num); *)
	PROCEDURE [ccall] mgl_set_pal_num* (graph: HMGL; num: INTEGER);

	(* void mgl_set_rotated_text(HMGL graph, int rotated); *)
	PROCEDURE [ccall] mgl_set_rotated_text* (graph: HMGL; rotated: INTEGER);

	(* void mgl_set_cut(HMGL graph, int cut); *)
	PROCEDURE [ccall] mgl_set_cut* (graph: HMGL; cut: INTEGER);

	(* void mgl_set_cut_box(HMGL gr, float x1,float y1,float z1,float x2,float y2,float z2); *)
	PROCEDURE [ccall] mgl_set_cut_box* (gr: HMGL; x1, y1, z1, x2, y2, z2: SHORTREAL);

	(* void mgl_set_tick_len(HMGL graph, float len); *)
	PROCEDURE [ccall] mgl_set_tick_len* (graph: HMGL; len: SHORTREAL);

	(* void mgl_set_bar_width(HMGL graph, float width); *)
	PROCEDURE [ccall] mgl_set_bar_width* (graph: HMGL; width: SHORTREAL);

	(* void mgl_set_base_line_width(HMGL gr, float size); *)
	PROCEDURE [ccall] mgl_set_base_line_width* (gr: HMGL; size: SHORTREAL);

	(* void mgl_set_mark_size(HMGL graph, float size); *)
	PROCEDURE [ccall] mgl_set_mark_size* (graph: HMGL; size: SHORTREAL);

	(* void mgl_set_arrow_size(HMGL graph, float size); *)
	PROCEDURE [ccall] mgl_set_arrow_size* (graph: HMGL; size: SHORTREAL);

	(* void mgl_set_font_size(HMGL graph, float size); *)
	PROCEDURE [ccall] mgl_set_font_size* (graph: HMGL; size: SHORTREAL);

	(* void mgl_set_font_def(HMGL graph, const char *fnt); *)
	PROCEDURE [ccall] mgl_set_font_def* (graph: HMGL; fnt: String);

	(* void mgl_set_alpha_default(HMGL graph, float alpha); *)
	PROCEDURE [ccall] mgl_set_alpha_default* (graph: HMGL; alpha: SHORTREAL);

	(* void mgl_set_size(HMGL graph, int width, int height); *)
	PROCEDURE [ccall] mgl_set_size* (graph: HMGL; width, height: INTEGER);

	(* void mgl_set_axial_dir(HMGL graph, char dir); *)
	PROCEDURE [ccall] mgl_set_axial_dir* (graph: HMGL; dir: SHORTCHAR);

	(* void mgl_set_meshnum(HMGL graph, int num); *)
	PROCEDURE [ccall] mgl_set_meshnum* (graph: HMGL; num: INTEGER);

	(* void mgl_set_zoom(HMGL gr, float x1, float y1, float x2, float y2); *)
	PROCEDURE [ccall] mgl_set_zoom* (gr: HMGL; x1, y1, x2, y2: SHORTREAL);

	(* void mgl_set_plotfactor(HMGL gr, float val); *)
	PROCEDURE [ccall] mgl_set_plotfactor* (gr: HMGL; val: SHORTREAL);

	(* void mgl_set_draw_face(HMGL gr, int enable); *)
	PROCEDURE [ccall] mgl_set_draw_face* (gr: HMGL; enable: INTEGER);

	(* void mgl_set_scheme(HMGL gr, const char *sch); *)
	PROCEDURE [ccall] mgl_set_scheme* (gr: HMGL; sch: String);

	(* void mgl_load_font(HMGL gr, const char *name, const char *path); *)
	PROCEDURE [ccall] mgl_load_font* (gr: HMGL; name: String; path: SHORTCHAR);

	(* void mgl_copy_font(HMGL gr, HMGL gr_from); *)
	PROCEDURE [ccall] mgl_copy_font* (gr, gr_from: HMGL);

	(* void mgl_restore_font(HMGL gr); *)
	PROCEDURE [ccall] mgl_restore_font* (gr: HMGL);

	(*****************************************************************************)
	(* 		Export to file or to memory														  *)
	(*****************************************************************************)
	(* void mgl_show_image(HMGL graph, const char *viewer, int keep); *)
	PROCEDURE [ccall] mgl_show_image* (graph: HMGL; viewer: String; keep: INTEGER);

	(* void mgl_write_frame(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_frame* (graph: HMGL;  fname: String;  descr: String);

	(* void mgl_write_bmp(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_bmp* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_jpg(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_jpg* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_png(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_png* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_png_solid(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_png_solid* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_eps(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_eps* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_svg(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_svg* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_idtf(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_idtf* (graph: HMGL; fname: String; descr: String);

	(* void mgl_write_gif(HMGL graph, const char *fname,const char *descr); *)
	PROCEDURE [ccall] mgl_write_gif* (graph: HMGL; fname: String; descr: String);

	(* void mgl_start_gif(HMGL graph, const char *fname,int ms); *)
	PROCEDURE [ccall] mgl_start_gif* (graph: HMGL; fname: String; ms: INTEGER);

	(* void mgl_close_gif(HMGL graph); *)
	PROCEDURE [ccall] mgl_close_gif* (graph: HMGL);

	(* CONST unsigned char *mgl_get_rgb(HMGL graph); *)
	PROCEDURE [ccall] mgl_get_rgb* (graph: HMGL): Byte1D;

	(* CONST unsigned char *mgl_get_rgba(HMGL graph); *)
	PROCEDURE [ccall] mgl_get_rgba* (graph: HMGL): Byte1D;

	(* int mgl_get_width(HMGL graph); *)
	PROCEDURE [ccall] mgl_get_width* (graph: HMGL): INTEGER;

	(* int mgl_get_height(HMGL graph); *)
	PROCEDURE [ccall] mgl_get_height* (graph: HMGL): INTEGER;

	(*****************************************************************************)
	(* 		Setup frames transparency (alpha) and lightning						  *)
	(*****************************************************************************)
	(* int mgl_new_frame(HMGL graph); *)
	PROCEDURE [ccall] mgl_new_frame* (graph: HMGL): INTEGER;

	(* void mgl_end_frame(HMGL graph); *)
	PROCEDURE [ccall] mgl_end_frame* (graph: HMGL);

	(* int mgl_get_num_frame(HMGL graph); *)
	PROCEDURE [ccall] mgl_get_num_frame* (graph: HMGL): INTEGER;

	(* void mgl_reset_frames(HMGL graph); *)
	PROCEDURE [ccall] mgl_reset_frames* (graph: HMGL);

	(* void mgl_set_transp_type(HMGL graph, int type); *)
	PROCEDURE [ccall] mgl_set_transp_type* (graph: HMGL; ttype: INTEGER);

	(* void mgl_set_transp(HMGL graph, int enable); *)
	PROCEDURE [ccall] mgl_set_transp* (graph: HMGL; enable: INTEGER);

	(* void mgl_set_alpha(HMGL graph, int enable); *)
	PROCEDURE [ccall] mgl_set_alpha* (graph: HMGL; enable: INTEGER);

	(* void mgl_set_fog(HMGL graph, float d, float dz); *)
	PROCEDURE [ccall] mgl_set_fog* (graph: HMGL; d, dz: SHORTREAL);

	(* void mgl_set_light(HMGL graph, int enable); *)
	PROCEDURE [ccall] mgl_set_light* (graph: HMGL; enable: INTEGER);

	(* void mgl_set_light_n(HMGL gr, int n, int enable); *)
	PROCEDURE [ccall] mgl_set_light_n* (graph: HMGL; n: INTEGER; enable: INTEGER);

	(* void mgl_add_light(HMGL graph, int n, float x, float y, float z, char c); *)
	PROCEDURE [ccall] mgl_add_light* (graph: HMGL; n: INTEGER; x, y, z: SHORTREAL; c: SHORTCHAR);

	(* void mgl_add_light_rgb(HMGL graph, int n, float x, float y, float z, int infty, float r, float g, float b, float i); *)
	PROCEDURE [ccall] mgl_add_light_rgb* (graph: HMGL; n: INTEGER; x, y, z: SHORTREAL; infty: INTEGER; r, g, b, i: SHORTREAL);

	(* void mgl_set_ambbr(HMGL gr, float i); *)
	PROCEDURE [ccall] mgl_set_ambbr* (graph: HMGL; i: SHORTREAL);

	(*****************************************************************************)
	(* 		Scale and rotate													  *)
	(*****************************************************************************)
	(* void mgl_identity(HMGL graph); *)
	PROCEDURE [ccall] mgl_identity* (graph: HMGL);

	(* void mgl_clf(HMGL graph); *)
	PROCEDURE [ccall] mgl_clf* (graph: HMGL);

	(* void mgl_flush(HMGL gr); *)
	PROCEDURE [ccall] mgl_flush* (gr: HMGL);

	(* void mgl_clf_rgb(HMGL graph, float r, float g, float b); *)
	PROCEDURE [ccall] mgl_clf_rgb* (graph: HMGL; r, g, b: SHORTREAL);

	(* void mgl_subplot(HMGL graph, int nx,int ny,int m); *)
	PROCEDURE [ccall] mgl_subplot* (graph: HMGL; nx, ny, m: INTEGER);

	(* void mgl_subplot_d(HMGL graph, int nx,int ny,int m, float dx, float dy); *)
	PROCEDURE [ccall] mgl_subplot_d* (graph: HMGL; nx, ny, m: INTEGER; dx, dy: SHORTREAL);

	(* void mgl_inplot(HMGL graph, float x1,float x2,float y1,float y2); *)
	PROCEDURE [ccall] mgl_inplot* (graph: HMGL; x1, x2, y1, y2: SHORTREAL);

	(* void mgl_relplot(HMGL graph, float x1,float x2,float y1,float y2); *)
	PROCEDURE [ccall] mgl_relplot* (graph: HMGL; x1, x2, y1, y2: SHORTREAL);

	(* void mgl_columnplot(HMGL graph, int num, int ind); *)
	PROCEDURE [ccall] mgl_columnplot* (graph: HMGL; num, ind: INTEGER);

	(* void mgl_aspect(HMGL graph, float Ax,float Ay,float Az); *)
	PROCEDURE [ccall] mgl_aspect* (graph: HMGL; Ax, Ay, Az: SHORTREAL);

	(* void mgl_rotate(HMGL graph, float TetX,float TetZ,float TetY); *)
	PROCEDURE [ccall] mgl_rotate* (graph: HMGL; TetX, TetZ, TetY: SHORTREAL);

	(* void mgl_rotate_vector(HMGL graph, float Tet,float x,float y,float z); *)
	PROCEDURE [ccall] mgl_rotate_vector* (graph: HMGL; Tet, x, y, z: SHORTREAL);

	(* void mgl_perspective(HMGL graph, float val); *)
	PROCEDURE [ccall] mgl_perspective* (graph: HMGL; val: SHORTREAL);

	(*****************************************************************************)
	(* 		Axis functions														  *)
	(*****************************************************************************)
	(* void mgl_set_ticks(HMGL graph, float DX, float DY, float DZ); *)
	PROCEDURE [ccall] mgl_set_ticks* (graph: HMGL; DX, DY, DZ: SHORTREAL);

	(* void mgl_set_subticks(HMGL graph, int NX, int NY, int NZ); *)
	PROCEDURE [ccall] mgl_set_subticks* (graph: HMGL; NX, NY, NZ: INTEGER);

	(* void mgl_set_ticks_dir(HMGL graph, char dir, float d, int ns, float org); *)
	PROCEDURE [ccall] mgl_set_ticks_dir* (graph: HMGL; dir: SHORTCHAR; d: SHORTREAL; ns: INTEGER; org:SHORTREAL);

	(* void mgl_set_ticks_val(HMGL graph, char dir, int n, REAL val, const char *lbl, ...); *)
	(*  oops.. variadic paramerers functions are not supported *)

	(* void mgl_set_ticks_vals(HMGL graph, char dir, int n, float *val, const char **lbl); *)
	PROCEDURE [ccall] mgl_set_ticks_vals* (graph: HMGL; dir: SHORTCHAR; n: INTEGER; val: Single1D; lbl: String);

	(* void mgl_set_caxis(HMGL graph, float C1,float C2); *)
	PROCEDURE [ccall] mgl_set_caxis* (graph: HMGL; C1, C2: SHORTREAL);

	(* void mgl_set_axis(HMGL graph, float x1, float y1, float z1, float x2, float y2, float z2, float x0, float y0, float z0); *)
	PROCEDURE [ccall] mgl_set_axis* (graph: HMGL; x1, y1, z1, x2, y2, z2, x0, y0, z0: SHORTREAL);

	(* void mgl_set_axis_3d(HMGL graph, float x1, float y1, float z1, float x2, float y2, float z2); *)
	PROCEDURE [ccall] mgl_set_axis_3d* (graph: HMGL; x1, y1, z1, x2, y2, z2: SHORTREAL);

	(* void mgl_set_axis_2d(HMGL graph, float x1, float y1, float x2, float y2); *)
	PROCEDURE [ccall] mgl_set_axis_2d* (graph: HMGL; x1, y1, x2, y2: SHORTREAL);

	(* void mgl_set_origin(HMGL graph, float x0, float y0, float z0); *)
	PROCEDURE [ccall] mgl_set_origin* (graph: HMGL; x0, y0, z0: SHORTREAL);

	(* void mgl_set_tick_origin(HMGL graph, float x0, float y0, float z0); *)
	PROCEDURE [ccall] mgl_set_tick_origin* (graph: HMGL; x0, y0, z0: SHORTREAL);

	(* void mgl_set_crange(HMGL graph, const HMDT a, int add); *)
	PROCEDURE [ccall] mgl_set_crange* (graph: HMGL; a: HMDT; add: INTEGER);

	(* void mgl_set_xrange(HMGL graph, const HMDT a, int add); *)
	PROCEDURE [ccall] mgl_set_xrange* (graph: HMGL; a: HMDT; add: INTEGER);

	(* void mgl_set_yrange(HMGL graph, const HMDT a, int add); *)
	PROCEDURE [ccall] mgl_set_yrange* (graph: HMGL; a: HMDT; add: INTEGER);

	(* void mgl_set_zrange(HMGL graph, const HMDT a, int add); *)
	PROCEDURE [ccall] mgl_set_zrange* (graph: HMGL; a: HMDT; add: INTEGER);

	(* void mgl_set_auto(HMGL graph, float x1, float x2, float y1, float y2, float z1, float z2); *)
	PROCEDURE [ccall] mgl_set_auto* (graph: HMGL; x1, x2, y1, y2, z1, z2: SHORTREAL);

	(* void mgl_set_func(HMGL graph, const char *EqX,const char *EqY,const char *EqZ); *)
	PROCEDURE [ccall] mgl_set_func* (graph: HMGL; EqX: String; EqY: String; EqZ: String);

	(* void mgl_set_ternary(HMGL gr, int enable); *)
	PROCEDURE [ccall] mgl_set_ternary* (graph: HMGL; enable: INTEGER);

	(* void mgl_set_cutoff(HMGL graph, const char *EqC); *)
	PROCEDURE [ccall] mgl_set_cutoff* (graph: HMGL; EqC: String);

	(* void mgl_box(HMGL graph, int ticks); *)
	PROCEDURE [ccall] mgl_box* (graph: HMGL; ticks: INTEGER);

	(* void mgl_box_str(HMGL graph, const char *col, int ticks); *)
	PROCEDURE [ccall] mgl_box_str* (graph: HMGL; col: String; ticks: INTEGER);

	(* void mgl_box_rgb(HMGL graph, float r, float g, float b, int ticks); *)
	PROCEDURE [ccall] mgl_box_rgb* (graph: HMGL; r, g, b: SHORTREAL; ticks: INTEGER);

	(* void mgl_axis(HMGL graph, const char *dir); *)
	PROCEDURE [ccall] mgl_axis* (graph: HMGL; dir: String);

	(* void mgl_axis_grid(HMGL graph, const char *dir,const char *pen); *)
	PROCEDURE [ccall] mgl_axis_grid* (graph: HMGL; dir: String; pen: String);

	(* void mgl_label(HMGL graph, char dir, const char *text); *)
	PROCEDURE [ccall] mgl_label* (graph: HMGL; dir: SHORTCHAR; text: String);

	(* void mgl_label_ext(HMGL graph, char dir, const char *text, int pos, float size, float shift); *)
	PROCEDURE [ccall] mgl_label_ext* (graph: HMGL; dir: SHORTCHAR; text: String; pos: INTEGER; size: SHORTREAL; shift: SHORTREAL);

	(* void mgl_tune_ticks(HMGL graph, int tune, float fact_pos); *)
	PROCEDURE [ccall] mgl_tune_ticks* (graph: HMGL; tune: INTEGER; fact_pos: SHORTREAL);

	(* void mgl_set_xttw(HMGL graph, const wchar_t *templ); *)
	PROCEDURE [ccall] mgl_set_xttw* (graph: HMGL; templ: UnicodeString);

	(* void mgl_set_yttw(HMGL graph, const wchar_t *templ); *)
	PROCEDURE [ccall] mgl_set_yttw* (graph: HMGL; templ: UnicodeString);

	(* void mgl_set_zttw(HMGL graph, const wchar_t *templ); *)
	PROCEDURE [ccall] mgl_set_zttw* (graph: HMGL; templ: UnicodeString);

	(* void mgl_set_cttw(HMGL graph, const wchar_t *templ); *)
	PROCEDURE [ccall] mgl_set_cttw* (graph: HMGL; templ: UnicodeString);

	(* void mgl_set_xtt(HMGL graph, const char *templ); *)
	PROCEDURE [ccall] mgl_set_xtt* (graph: HMGL; templ: String);

	(* void mgl_set_ytt(HMGL graph, const char *templ); *)
	PROCEDURE [ccall] mgl_set_ytt* (graph: HMGL; templ: String);

	(* void mgl_set_ztt(HMGL graph, const char *templ); *)
	PROCEDURE [ccall] mgl_set_ztt* (graph: HMGL; templ: String);

	(* void mgl_set_ctt(HMGL graph, const char *templ); *)
	PROCEDURE [ccall] mgl_set_ctt* (graph: HMGL; templ: String);

	(*****************************************************************************)
	(* 		Simple drawing														  *)
	(*****************************************************************************)
	(* void mgl_ball(HMGL graph, float x,float y,float z); *)
	PROCEDURE [ccall] mgl_ball* (graph: HMGL; x, y, z: SHORTREAL);

	(* void mgl_ball_rgb(HMGL graph, float x, float y, float z, float r, float g, float b, float alpha); *)
	PROCEDURE [ccall] mgl_ball_rgb* (graph: HMGL; x, y, z, r, g, b, alpha: SHORTREAL);

	(* void mgl_ball_str(HMGL graph, float x, float y, float z, const char *col); *)
	PROCEDURE [ccall] mgl_ball_str* (graph: HMGL; x, y, z: SHORTREAL; col: String);

	(* void mgl_line(HMGL graph, float x1, float y1, float z1, float x2, float y2, float z2, const char *pen,int n); *)
	PROCEDURE [ccall] mgl_line* (graph: HMGL; x1, y1, z1, x2, y2, z2: SHORTREAL; pen: String; n: INTEGER);

	(* void mgl_facex(HMGL graph, float x0, float y0, float z0, float wy, float wz, const char *stl, float dx, float dy); *)
	PROCEDURE [ccall] mgl_facex* (graph: HMGL; x0, y0, z0, wy, wz: SHORTREAL; stl: String; dx, dy: SHORTREAL);

	(* void mgl_facey(HMGL graph, float x0, float y0, float z0, float wx, float wz, const char *stl, float dx, float dy); *)
	PROCEDURE [ccall] mgl_facey* (graph: HMGL; x0, y0, z0, wx, wz: SHORTREAL; stl: String; dx, dy: SHORTREAL);

	(* void mgl_facez(HMGL graph, float x0, float y0, float z0, float wx, float wy, const char *stl, float dx, float dy); *)
	PROCEDURE [ccall] mgl_facez* (graph: HMGL; x0, y0, z0, wx, wy: SHORTREAL; stl: String; dx, dy: SHORTREAL);

	(* void mgl_curve(HMGL graph, float x1, float y1, float z1, float dx1, float dy1, float dz1, float x2, float y2, float z2, float dx2, float dy2, float dz2, const char *pen,int n); *)
	PROCEDURE [ccall] mgl_curve* (graph: HMGL; x1, y1, z1, dx1, dy1, dz1, x2, y2, z2, dx2, dy2, dz2: SHORTREAL; pen: String; n: INTEGER);

	(* void mgl_puts(HMGL graph, float x, float y, float z,const char *text); *)
	PROCEDURE [ccall] mgl_puts* (graph: HMGL; x, y, z: SHORTREAL; text: String);

	(* void mgl_putsw(HMGL graph, float x, float y, float z,const wchar_t *text); *)
	PROCEDURE [ccall] mgl_putsw* (graph: HMGL; x, y, z: SHORTREAL; text: UnicodeString);

	(* void mgl_puts_dir(HMGL graph, float x, float y, float z, float dx, float dy, float dz, const char *text, float size); *)
	PROCEDURE [ccall] mgl_puts_dir* (graph: HMGL; x, y, z, dx, dy, dz: SHORTREAL; text: String; size: SHORTREAL);

	(* void mgl_putsw_dir(HMGL graph, float x, float y, float z, float dx, float dy, float dz, const wchar_t *text, float size); *)
	PROCEDURE [ccall] mgl_putsw_dir* (graph: HMGL; x, y, z, dx, dy, dz: SHORTREAL; text: UnicodeString; size: SHORTREAL);

	(* void mgl_text(HMGL graph, float x, float y, float z,const char *text); *)
	PROCEDURE [ccall] mgl_text* (graph: HMGL; x, y, z: SHORTREAL; text: String);

	(* void mgl_title(HMGL graph, const char *text, const char *fnt, float size); *)
	PROCEDURE [ccall] mgl_title* (graph: HMGL; text: String; fnt: String; size: SHORTREAL);

	(* void mgl_titlew(HMGL graph, const wchar_t *text, const char *fnt, float size); *)
	PROCEDURE [ccall] mgl_titlew* (graph: HMGL; text: UnicodeString; fnt: String; size: SHORTREAL);

	(* void mgl_putsw_ext(HMGL graph, float x, float y, float z,const wchar_t *text,const char *font,float size,char dir); *)
	PROCEDURE [ccall] mgl_putsw_ext* (graph: HMGL; x, y, z: SHORTREAL; text: UnicodeString; font: String; size: SHORTREAL; dir: SHORTCHAR);

	(* void mgl_puts_ext(HMGL graph, float x, float y, float z,const char *text,const char *font,float size,char dir); *)
	PROCEDURE [ccall] mgl_puts_ext* (graph: HMGL; x, y, z: SHORTREAL; text: String; font: String; size: SHORTREAL; dir: SHORTCHAR);

	(* void mgl_text_ext(HMGL graph, float x, float y, float z,const char *text,const char *font,float size,char dir); *)
	PROCEDURE [ccall] mgl_text_ext* (graph: HMGL; x, y, z: SHORTREAL; text: String; font: String; size: SHORTREAL; dir: SHORTCHAR);

	(* void mgl_colorbar(HMGL graph, const char *sch,int where); *)
	PROCEDURE [ccall] mgl_colorbar* (graph: HMGL; sch: String; where: INTEGER);

	(* void mgl_colorbar_ext(HMGL graph, const char *sch, int where, float x, float y, float w, float h); *)
	PROCEDURE [ccall] mgl_colorbar_ext* (graph: HMGL; sch: String; where: INTEGER; x, y, w, h: SHORTREAL);

	(* void mgl_simple_plot(HMGL graph, const HMDT a, int type, const char *stl); *)
	PROCEDURE [ccall] mgl_simple_plot* (graph: HMGL; a: HMDT; ttype: INTEGER; stl: String);

	(* void mgl_add_legend(HMGL graph, const char *text,const char *style); *)
	PROCEDURE [ccall] mgl_add_legend* (graph: HMGL; text: String; style: String);

	(* void mgl_add_legendw(HMGL graph, const wchar_t *text,const char *style); *)
	PROCEDURE [ccall] mgl_add_legendw* (graph: HMGL; text: UnicodeString; style: String);

	(* void mgl_clear_legend(HMGL graph); *)
	PROCEDURE [ccall] mgl_clear_legend* (graph: HMGL);

	(* void mgl_legend_xy(HMGL graph, float x, float y, const char *font, float size, float llen); *)
	PROCEDURE [ccall] mgl_legend_xy* (graph: HMGL; x, y: SHORTREAL; font: String; size: SHORTREAL; llen: SHORTREAL);

	(* void mgl_legend(HMGL graph, int where, const char *font, float size, float llen); *)
	PROCEDURE [ccall] mgl_legend* (graph: HMGL; where: INTEGER; font: String; size: SHORTREAL; llen: SHORTREAL);

	(* void mgl_set_legend_box(HMGL gr, int enable); *)
	PROCEDURE [ccall] mgl_set_legend_box* (graph: HMGL; enable: INTEGER);

	(*****************************************************************************)
	(* 		1D plotting functions												  *)
	(*****************************************************************************)
	(* void mgl_fplot(HMGL graph, const char *fy, const char *stl, int n); *)
	PROCEDURE [ccall] mgl_fplot* (graph: HMGL; fy: String; pen: String; n: INTEGER);

	(* void mgl_fplot_xyz(HMGL graph, const char *fx, const char *fy, const char *fz, const char *stl, int n); *)
	PROCEDURE [ccall] mgl_fplot_xyz* (graph: HMGL; fx: String; fy: String; fz: String; pen: String; n: INTEGER);

	(* void mgl_plot_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *pen); *)
	PROCEDURE [ccall] mgl_plot_xyz* (graph: HMGL; x, y, z: HMDT; pen: String);

	(* void mgl_plot_xy(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_plot_xy* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_plot(HMGL graph, const HMDT y,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_plot* (graph: HMGL; y: HMDT; pen: String);

	(* void mgl_plot_2(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_plot_2* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_plot_3(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_plot_3* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_tens_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT c,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_tens_xyz* (graph: HMGL; x, y, z, c: HMDT; pen: String);

	(* void mgl_tens_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT c,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_tens_xy* (graph: HMGL; x, y, c: HMDT; pen: String);

	(* void mgl_tens(HMGL graph, const HMDT y,	CONST HMDT c,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_tens* (graph: HMGL; y, c: HMDT; pen: String);

	(* void mgl_area_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *pen); *)
	PROCEDURE [ccall] mgl_area_xyz* (graph: HMGL; x, y, z: HMDT; pen: String);

	(* void mgl_area_xy(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_area_xy* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_area_xys(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_area_xys* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_area(HMGL graph, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_area* (graph: HMGL; y: HMDT; pen: String);

	(* void mgl_area_2(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_area_2* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_area_3(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_area_3* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_region_xy(HMGL graph, const HMDT x, const HMDT y1, const HMDT y2, const char *pen, int inside); *)
	PROCEDURE [ccall] mgl_region_xy* (graph: HMGL; x, y1, y2: HMDT; pen: String; inside: INTEGER);

	(* void mgl_region(HMGL graph, const HMDT y1, const HMDT y2, const char *pen, int inside); *)
	PROCEDURE [ccall] mgl_region* (graph: HMGL; y1, y2: HMDT; pen: String; inside: INTEGER);

	(* void mgl_mark(HMGL graph, float x,float y,float z,char mark); *)
	PROCEDURE [ccall] mgl_mark* (graph: HMGL; x, y, z: SHORTREAL; mark: SHORTCHAR);

	(* void mgl_stem_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *pen); *)
	PROCEDURE [ccall] mgl_stem_xyz* (graph: HMGL; x, y, z: HMDT; pen: String);

	(* void mgl_stem_xy(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_stem_xy* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_stem(HMGL graph, const HMDT y,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_stem* (graph: HMGL; y: HMDT; pen: String);

	(* void mgl_stem_2(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_stem_2* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_stem_3(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_stem_3* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_step_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *pen); *)
	PROCEDURE [ccall] mgl_step_xyz* (graph: HMGL; x, y, z: HMDT; pen: String);

	(* void mgl_step_xy(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_step_xy* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_step(HMGL graph, const HMDT y,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_step* (graph: HMGL; y: HMDT; pen: String);

	(* void mgl_step_2(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_step_2* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_step_3(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_step_3* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_bars_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *pen); *)
	PROCEDURE [ccall] mgl_bars_xyz* (graph: HMGL; x, y, z: HMDT; pen: String);

	(* void mgl_bars_xy(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_bars_xy* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_bars(HMGL graph, const HMDT y,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_bars* (graph: HMGL; y: HMDT; pen: String);

	(* void mgl_bars_2(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_bars_2* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_bars_3(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_bars_3* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_barh_xy(HMGL graph, const HMDT x, const HMDT y, const char *pen); *)
	PROCEDURE [ccall] mgl_barh_xy* (graph: HMGL; x, y: HMDT; pen: String);

	(* void mgl_barh_yx(HMGL graph, const HMDT y, const HMDT v, const char *pen); *)
	PROCEDURE [ccall] mgl_barh_yx* (graph: HMGL; y, v: HMDT; pen: String);

	(* void mgl_barh(HMGL graph, const HMDT v,	CONST char *pen); *)
	PROCEDURE [ccall] mgl_barh* (graph: HMGL; v: HMDT; pen: String);

	(*****************************************************************************)
	(* 		Advanced 1D plotting functions												  *)
	(*****************************************************************************)
	(* void mgl_torus(HMGL graph, const HMDT r, const HMDT z, const char *pen); *)
	PROCEDURE [ccall] mgl_torus* (graph: HMGL; r, z: HMDT; pen: String);

	(* void mgl_torus_2(HMGL graph, const HMDT a, const char *pen); *)
	PROCEDURE [ccall] mgl_torus_2* (graph: HMGL; a: HMDT; pen: String);

	(* void mgl_text_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z,const char *text, const char *font, float size); *)
	PROCEDURE [ccall] mgl_text_xyz* (graph: HMGL; x, y, z: HMDT; text: String; font: String; size: SHORTREAL);

	(* void mgl_text_xy(HMGL graph, const HMDT x, const HMDT y, const char *text, const char *font, float size); *)
	PROCEDURE [ccall] mgl_text_xy* (graph: HMGL; x, y: HMDT; text: String; font: String; size: SHORTREAL);

	(* void mgl_text_y(HMGL graph, const HMDT y, const char *text, const char *font, float size); *)
	PROCEDURE [ccall] mgl_text_y* (graph: HMGL; y: HMDT; text: String; font: String; size: SHORTREAL);

	(* void mgl_chart(HMGL graph, const HMDT a, const char *col); *)
	PROCEDURE [ccall] mgl_chart* (graph: HMGL; a: HMDT; col: String);

	(* void mgl_error(HMGL graph, const HMDT y, const HMDT ey, const char *pen); *)
	PROCEDURE [ccall] mgl_error* (graph: HMGL; y, ey: HMDT; pen: String);

	(* void mgl_error_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT ey, const char *pen); *)
	PROCEDURE [ccall] mgl_error_xy* (graph: HMGL; x, y, ey: HMDT; pen: String);

	(* void mgl_error_exy(HMGL graph, const HMDT x, const HMDT y, const HMDT ex, const HMDT ey, const char *pen); *)
	PROCEDURE [ccall] mgl_error_exy* (graph: HMGL; x, y, ex, ey: HMDT; pen: String);

	(* void mgl_mark_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT r, const char *pen); *)
	PROCEDURE [ccall] mgl_mark_xyz* (graph: HMGL; x, y, z, r: HMDT; pen: String);

	(* void mgl_mark_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT r, const char *pen); *)
	PROCEDURE [ccall] mgl_mark_xy* (graph: HMGL; x, y, z: HMDT; pen: String);

	(* void mgl_mark_y(HMGL graph, const HMDT y, const HMDT r, const char *pen); *)
	PROCEDURE [ccall] mgl_mark_y* (graph: HMGL; y, r: HMDT; pen: String);

	(* void mgl_tube_xyzr(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT r, const char *pen); *)
	PROCEDURE [ccall] mgl_tube_xyzr* (graph: HMGL; x, y, z, r: HMDT; pen: String);

	(* void mgl_tube_xyr(HMGL graph, const HMDT x, const HMDT y, const HMDT r, const char *pen); *)
	PROCEDURE [ccall] mgl_tube_xyr* (graph: HMGL; x, y, r: HMDT; pen: String);

	(* void mgl_tube_r(HMGL graph, const HMDT y, const HMDT r, const char *pen); *)
	PROCEDURE [ccall] mgl_tube_r* (graph: HMGL; y, r: HMDT; pen: String);

	(* void mgl_tube_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, float r, const char *pen); *)
	PROCEDURE [ccall] mgl_tube_xyz* (graph: HMGL; x, y, z: HMDT; r: SHORTREAL; pen: String);

	(* void mgl_tube_xy(HMGL graph, const HMDT x, const HMDT y, float r, const char *penl); *)
	PROCEDURE [ccall] mgl_tube_xy* (graph: HMGL; x, y:  HMDT; r: SHORTREAL; penl: String);

	(* void mgl_tube(HMGL graph, const HMDT y, float r, const char *pen); *)
	PROCEDURE [ccall] mgl_tube* (graph: HMGL; y: HMDT; r: SHORTREAL; pen: String);


	(* void mgl_textmark_xyzr(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT r, const char *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmark_xyzr* (graph: HMGL; x, y, z, r: HMDT; text: String; fnt: String);

	(* void mgl_textmark_xyr(HMGL graph, const HMDT x, const HMDT y, const HMDT r, const char *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmark_xyr* (graph: HMGL; x, y, r: HMDT; text: String; fnt: String);

	(* void mgl_textmark_yr(HMGL graph, const HMDT y, const HMDT r, const char *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmark_yr* (graph: HMGL; y, r: HMDT; text: String; fnt: String);

	(* void mgl_textmark(HMGL graph, const HMDT y, const char *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmark* (graph: HMGL; y: HMDT; text: String; fnt: String);

	(* void mgl_textmarkw_xyzr(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT r, const wchar_t *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmarkw_xyzr* (graph: HMGL; x, y, z, r: HMDT; text: UnicodeString; fnt: String);

	(* void mgl_textmarkw_xyr(HMGL graph, const HMDT x, const HMDT y, const HMDT r, const wchar_t *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmarkw_xyr* (graph: HMGL; x, y, r: HMDT; text: UnicodeString; fnt: String);

	(* void mgl_textmarkw_yr(HMGL graph, const HMDT y, const HMDT r, const wchar_t *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmarkw_yr* (graph: HMGL; y, r: HMDT; text: UnicodeString; fnt: String);

	(* void mgl_textmarkw(HMGL graph, const HMDT y, const wchar_t *text, const char *fnt); *)
	PROCEDURE [ccall] mgl_textmarkw* (graph: HMGL; y: HMDT; text: UnicodeString; fnt: String);

	(*****************************************************************************)
	(* 		2D plotting functions												  *)
	(*****************************************************************************)
	(* void mgl_fsurf(HMGL graph, const char *fz, const char *stl, int n); *)
	PROCEDURE [ccall] mgl_fsurf* (graph: HMGL; fz: String; stl: String; n: INTEGER);

	(* void mgl_fsurf_xyz(HMGL graph, const char *fx, const char *fy, const char *fz, const char *stl, int n); *)
	PROCEDURE [ccall] mgl_fsurf_xyz* (graph: HMGL; fx: String; fy: String; fz: String; stl: String; n: INTEGER);

	(* void mgl_grid_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *stl,float zVal); *)
	PROCEDURE [ccall] mgl_grid_xy* (graph: HMGL; x, y, z: HMDT; stl: String; zVal: SHORTREAL);

	(* void mgl_grid(HMGL graph, const HMDT a,const char *stl,float zVal); *)
	PROCEDURE [ccall] mgl_grid* (graph: HMGL; a: HMDT; stl: String; zVal: SHORTREAL);

	(* void mgl_mesh_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_mesh_xy* (graph: HMGL; x, y, z: HMDT; stl: String);

	(* void mgl_mesh(HMGL graph, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_mesh* (graph: HMGL; z: HMDT; stl: String);

	(* void mgl_fall_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_fall_xy* (graph: HMGL; x, y, z: HMDT; stl: String);

	(* void mgl_fall(HMGL graph, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_fall* (graph: HMGL; z: HMDT; stl: String);

	(* void mgl_belt_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_belt_xy* (graph: HMGL; x, y, z: HMDT; stl: String);

	(* void mgl_belt(HMGL graph, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_belt* (graph: HMGL; z: HMDT; sch: String);

	(* void mgl_surf_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_surf_xy* (graph: HMGL; x, y, z: HMDT; sch: String);

	(* void mgl_surf(HMGL graph, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_surf* (graph: HMGL; z: HMDT; sch: String);

	(* void mgl_dens_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_dens_xy* (graph: HMGL; x, y, z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_dens(HMGL graph, const HMDT z, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_dens* (graph: HMGL; z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_boxs_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_boxs_xy* (graph: HMGL; x, y, z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_boxs(HMGL graph, const HMDT z, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_boxs* (graph: HMGL; z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_tile_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_tile_xy* (graph: HMGL; x, y, z: HMDT; sch: String);

	(* void mgl_tile(HMGL graph, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_tile* (graph: HMGL; z: HMDT; sch: String);

	(* void mgl_tiles_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT r, const char *sch); *)
	PROCEDURE [ccall] mgl_tiles_xy* (graph: HMGL; x, y, z, r: HMDT; sch: String);

	(* void mgl_tiles(HMGL graph, const HMDT z, const HMDT r, const char *sch); *)
	PROCEDURE [ccall] mgl_tiles* (graph: HMGL; z, r: HMDT; sch: String);

	(* void mgl_cont_xy_val(HMGL graph, const HMDT v, const HMDT x, const HMDT y, const HMDT z, const char *sch, float zVal); *)
	PROCEDURE [ccall] mgl_cont_xy_val* (graph: HMGL; v, x, y, z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_cont_val(HMGL graph, const HMDT v, const HMDT z, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_cont_val* (graph: HMGL; v, z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_cont_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch, int Num, float zVal); *)
	PROCEDURE [ccall] mgl_cont_xy* (graph: HMGL; x, y, z: HMDT; sch: String; Num: INTEGER; zVal: SHORTREAL);

	(* void mgl_cont(HMGL graph, const HMDT z, const char *sch, int Num, float zVal); *)
	PROCEDURE [ccall] mgl_cont* (graph: HMGL; z: HMDT; sch: String; Num: INTEGER; zVal: SHORTREAL);

	(* void mgl_contf_xy_val(HMGL graph, const HMDT v, const HMDT x, const HMDT y, const HMDT z, const char *sch, float zVal); *)
	PROCEDURE [ccall] mgl_contf_xy_val* (graph: HMGL; v, x, y, z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_contf_val(HMGL graph, const HMDT v, const HMDT z, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_contf_val* (graph: HMGL; v, z: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_contf_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const char *sch, int Num, float zVal); *)
	PROCEDURE [ccall] mgl_contf_xy* (graph: HMGL; x, y, z: HMDT; sch: String; Num: INTEGER; zVal: SHORTREAL);

	(* void mgl_contf(HMGL graph, const HMDT z, const char *sch, int Num, float zVal); *)
	PROCEDURE [ccall] mgl_contf* (graph: HMGL; z: HMDT; sch: String; Num: INTEGER; zVal: SHORTREAL);

	(* void mgl_axial_xy_val(HMGL graph, const HMDT v, const HMDT x, const HMDT y, const HMDT a, const char *sch); *)
	PROCEDURE [ccall] mgl_axial_xy_val* (graph: HMGL; v, x, y, a: HMDT; sch: String);

	(* void mgl_axial_val(HMGL graph, const HMDT v, const HMDT a, const char *sch); *)
	PROCEDURE [ccall] mgl_axial_val* (graph: HMGL; v, a: HMDT; sch: String);

	(* void mgl_axial_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT a, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_axial_xy* (graph: HMGL; x, y, a: HMDT; sch: String);

	(* void mgl_axial(HMGL graph, const HMDT a, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_axial* (graph: HMGL; a: HMDT; sch: String; Num: INTEGER);

	(*****************************************************************************)
	(* 		Dual plotting functions												  *)
	(*****************************************************************************)
	(* void mgl_surfc_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT c, const char *sch); *)
	PROCEDURE [ccall] mgl_surfc_xy* (graph: HMGL; x, y, z, c: HMDT; sch: String);

	(* void mgl_surfc(HMGL graph, const HMDT z, const HMDT c, const char *sch); *)
	PROCEDURE [ccall] mgl_surfc* (graph: HMGL; z, c: HMDT; sch: String);

	(* void mgl_surfa_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT c, const char *sch); *)
	PROCEDURE [ccall] mgl_surfa_xy* (graph: HMGL; x, y, z, c: HMDT; sch: String);

	(* void mgl_surfa(HMGL graph, const HMDT z, const HMDT c, const char *sch); *)
	PROCEDURE [ccall] mgl_surfa* (graph: HMGL; z, c: HMDT; sch: String);

	(* void mgl_stfa_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT re, const HMDT im, int dn, const char *sch, float zVal); *)
	PROCEDURE [ccall] mgl_stfa_xy* (graph: HMGL; x, y, re, im: HMDT; dn: INTEGER; sch: String; zVal: SHORTREAL);

	(* void mgl_stfa(HMGL graph, const HMDT re, const HMDT im, int dn, const char *sch, float zVal); *)
	PROCEDURE [ccall] mgl_stfa* (graph: HMGL; re, im: HMDT; dn: INTEGER; sch: String; zVal: SHORTREAL);

	(* void mgl_vect_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_vect_xy* (graph: HMGL; x, y, z, ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_vect_2d(HMGL graph, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_vect_2d* (graph: HMGL; ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_vectl_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_vectl_xy* (graph: HMGL; x, y, z, ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_vectl_2d(HMGL graph, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_vectl_2d* (graph: HMGL; ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_vectc_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_vectc_xy* (graph: HMGL; x, y, ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_vectc_2d(HMGL graph, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_vectc_2d* (graph: HMGL; ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_vect_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT ax, const HMDT ay, const HMDT az, const char *sch); *)
	PROCEDURE [ccall] mgl_vect_xyz* (graph: HMGL; x, y, z, ax, ay, az: HMDT; sch: String);

	(* void mgl_vect_3d(HMGL graph, const HMDT ax, const HMDT ay, const HMDT az, const char *sch); *)
	PROCEDURE [ccall] mgl_vect_3d* (graph: HMGL; ax, ay, az: HMDT; sch: String);

	(* void mgl_vectl_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT ax, const HMDT ay, const HMDT az, const char *sch); *)
	PROCEDURE [ccall] mgl_vectl_xyz* (graph: HMGL; x, y, z, ax, ay, az: HMDT; sch: String);

	(* void mgl_vectl_3d(HMGL graph, const HMDT ax, const HMDT ay, const HMDT az, const char *sch); *)
	PROCEDURE [ccall] mgl_vectl_3d* (graph: HMGL; ax, ay, az: HMDT; sch: String);

	(* void mgl_vectc_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT ax, const HMDT ay, const HMDT az, const char *sch); *)
	PROCEDURE [ccall] mgl_vectc_xyz* (graph: HMGL; x, y, z, ax, ay, az: HMDT; sch: String);

	(* void mgl_vectc_3d(HMGL graph, const HMDT ax, const HMDT ay, const HMDT az, const char *sch); *)
	PROCEDURE [ccall] mgl_vectc_3d* (graph: HMGL; ax, ay, az: HMDT; sch: String);

	(* void mgl_map_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT a, const HMDT b, const char *sch, int ks, int pnts); *)
	PROCEDURE [ccall] mgl_map_xy* (graph: HMGL; x, y, a, b: HMDT; sch: String; ks: INTEGER; pnts: INTEGER);

	(* void mgl_map(HMGL graph, const HMDT a, const HMDT b, const char *sch, int ks, int pnts); *)
	PROCEDURE [ccall] mgl_map* (graph: HMGL; a, b: HMDT; sch: String; ks: INTEGER; pnts: INTEGER);

	(* void mgl_surf3a_xyz_val(HMGL graph, float Val, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const HMDT b, const char *stl); *)
	PROCEDURE [ccall] mgl_surf3a_xyz_val* (graph: HMGL; Val: SHORTREAL; x, y, z, a, b: HMDT; stla: String);

	(* void mgl_surf3a_val(HMGL graph, float Val, const HMDT a, const HMDT b, const char *stl); *)
	PROCEDURE [ccall] mgl_surf3a_val* (graph: HMGL; Val: SHORTREAL; a, b: HMDT; stl: String);

	(* void mgl_surf3a_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const HMDT b, const char *stl, int num); *)
	PROCEDURE [ccall] mgl_surf3a_xyz* (graph: HMGL; x, y, z, a, b: HMDT; stl: String; num: INTEGER);

	(* void mgl_surf3a(HMGL graph, const HMDT a, const HMDT b, const char *stl, int num); *)
	PROCEDURE [ccall] mgl_surf3a* (graph: HMGL; a, b: HMDT; stl: String; num: INTEGER);

	(* void mgl_surf3c_xyz_val(HMGL graph, float Val, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const HMDT b, const char *stl); *)
	PROCEDURE [ccall] mgl_surf3c_xyz_val* (graph: HMGL; Val: SHORTREAL; x, y, z, a, b: HMDT; stl: String);

	(* void mgl_surf3c_val(HMGL graph, float Val, const HMDT a, const HMDT b, const char *stl); *)
	PROCEDURE [ccall] mgl_surf3c_val* (graph: HMGL; Val: SHORTREAL; a, b: HMDT; stl: String);

	(* void mgl_surf3c_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const HMDT b, *)
	(* 			CONST char *stl, int num); *)
	PROCEDURE [ccall] mgl_surf3c_xyz* (graph: HMGL; x, y, z, a, b: HMDT; stl: String; num: INTEGER);

	(* void mgl_surf3c(HMGL graph, const HMDT a, const HMDT b, const char *stl, int num); *)
	PROCEDURE [ccall] mgl_surf3c* (graph: HMGL; a, b: HMDT; stl: String; num: INTEGER);

	(* void mgl_flow_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT ax, const HMDT ay, const char *sch, int num, int central, float zVal); *)
	PROCEDURE [ccall] mgl_flow_xy* (graph: HMGL; x, y, ax, ay: HMDT; sch: String; num: INTEGER; central: INTEGER; zVal: SHORTREAL);

	(* void mgl_flow_2d(HMGL graph, const HMDT ax, const HMDT ay, const char *sch, int num, int central, float zVal); *)
	PROCEDURE [ccall] mgl_flow_2d* (graph: HMGL; ax, ay: HMDT; sch: String; num: INTEGER; central: INTEGER; zVal: SHORTREAL);

	(* void mgl_flow_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT ax, const HMDT ay, const HMDT az, const char *sch, int num, int central); *)
	PROCEDURE [ccall] mgl_flow_xyz* (graph: HMGL; x, y, z, ax, ay, az: HMDT; sch: String; num: INTEGER; central: INTEGER);

	(* void mgl_flow_3d(HMGL graph, const HMDT ax, const HMDT ay, const HMDT az, const char *sch, int num, int central); *)
	PROCEDURE [ccall] mgl_flow_3d* (graph: HMGL; ax, ay, az: HMDT; sch: String; num: INTEGER; central: INTEGER);

	(* void mgl_pipe_xy(HMGL graph, const HMDT x, const HMDT y, const HMDT ax, const HMDT ay, const char *sch, float r0, int num, int central, float zVal); *)
	PROCEDURE [ccall] mgl_pipe_xy* (graph: HMGL; x, y, ax, ay: HMDT; sch: String; r0: SHORTREAL; num: INTEGER; central: INTEGER; zVal: SHORTREAL);

	(* void mgl_pipe_2d(HMGL graph, const HMDT ax, const HMDT ay, const char *sch, float r0, int num, int central, float zVal); *)
	PROCEDURE [ccall] mgl_pipe_2d* (graph: HMGL; ax, ay: HMDT; sch: String; r0: SHORTREAL; num: INTEGER; central: INTEGER; zVal: SHORTREAL);

	(* void mgl_pipe_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT ax, const HMDT ay, const HMDT az, const char *sch, float r0, int num, int central); *)
	PROCEDURE [ccall] mgl_pipe_xyz* (graph: HMGL; x, y, z, ax, ay, az: HMDT; sch: String; r0: SHORTREAL; num: INTEGER; central: INTEGER);

	(* void mgl_pipe_3d(HMGL graph, const HMDT ax, const HMDT ay, const HMDT az, const char *sch, float r0, int num, int central); *)
	PROCEDURE [ccall] mgl_pipe_3d* (graph: HMGL; ax, ay, az: HMDT; sch: String; r0: SHORTREAL; num: INTEGER; central: INTEGER);

	(* void mgl_dew_xy(HMGL gr, const HMDT x, const HMDT y, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_dew_xy* (gr: HMGL; x, y, z, ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_dew_2d(HMGL gr, const HMDT ax, const HMDT ay, const char *sch,float zVal); *)
	PROCEDURE [ccall] mgl_dew_2d* (gr: HMGL; ax, ay: HMDT; sch: String; zVal: SHORTREAL);

	(*****************************************************************************)
	(* 		3D plotting functions												  *)
	(*****************************************************************************)
	(* void mgl_grid3_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, char dir, int sVal, const char *stl); *)
	PROCEDURE [ccall] mgl_grid3_xyz* (graph: HMGL; x, y, z, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; stl: String);

	(* void mgl_grid3(HMGL graph, const HMDT a, char dir, int sVal, const char *stl); *)
	PROCEDURE [ccall] mgl_grid3* (graph: HMGL; a: HMDT; dir: SHORTCHAR; sVal: INTEGER; stl: String);

	(* void mgl_grid3_all_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *stl); *)
	PROCEDURE [ccall] mgl_grid3_all_xyz* (graph: HMGL; x, y, z, a: HMDT; stl: String);

	(* void mgl_grid3_all(HMGL graph, const HMDT a, const char *stl); *)
	PROCEDURE [ccall] mgl_grid3_all* (graph: HMGL; a: HMDT; stl: String);

	(* void mgl_dens3_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, char dir, int sVal, const char *stl); *)
	PROCEDURE [ccall] mgl_dens3_xyz* (graph: HMGL; x, y, z, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; stl: String);

	(* void mgl_dens3(HMGL graph, const HMDT a, char dir, int sVal, const char *stl); *)
	PROCEDURE [ccall] mgl_dens3* (graph: HMGL; a: HMDT; dir: SHORTCHAR; sVal: INTEGER; stl: String);

	(* void mgl_dens3_all_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *stl); *)
	PROCEDURE [ccall] mgl_dens3_all_xyz* (graph: HMGL; x, y, z, a: HMDT; stl: String);

	(* void mgl_dens3_all(HMGL graph, const HMDT a, const char *stl); *)
	PROCEDURE [ccall] mgl_dens3_all* (graph: HMGL; a: HMDT; stl: String);

	(* void mgl_surf3_xyz_val(HMGL graph, float Val, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *stl); *)
	PROCEDURE [ccall] mgl_surf3_xyz_val* (graph: HMGL; Val: SHORTREAL; x, y, z, a: HMDT; stl: String);

	(* void mgl_surf3_val(HMGL graph, float Val, const HMDT a, const char *stl); *)
	PROCEDURE [ccall] mgl_surf3_val* (graph: HMGL; Val: SHORTREAL; a: HMDT; stl: String);

	(* void mgl_surf3_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *stl, int num); *)
	PROCEDURE [ccall] mgl_surf3_xyz* (graph: HMGL; x, y, z, a: HMDT; stl: String; num: INTEGER);

	(* void mgl_surf3(HMGL graph, const HMDT a, const char *stl, int num); *)
	PROCEDURE [ccall] mgl_surf3* (graph: HMGL; a: HMDT; stl: String; num: INTEGER);

	(* void mgl_cont3_xyz_val(HMGL graph, const HMDT v, const HMDT x, const HMDT y, const HMDT z, const HMDT a, char dir, int sVal, const char *sch); *)
	PROCEDURE [ccall] mgl_cont3_xyz_val* (graph: HMGL; v, x, y, z, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; sch: String);

	(* void mgl_cont3_val(HMGL graph, const HMDT v, const HMDT a, char dir, int sVal, const char *sch); *)
	PROCEDURE [ccall] mgl_cont3_val* (graph: HMGL; v, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; sch: String);

	(* void mgl_cont3_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, char dir, int sVal, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_cont3_xyz* (graph: HMGL; x, y, z, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; sch: String; Num: INTEGER);

	(* void mgl_cont3(HMGL graph, const HMDT a, char dir, int sVal, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_cont3* (graph: HMGL; a: HMDT; dir: SHORTCHAR; sVal: INTEGER; stl: String; Num: INTEGER);

	(* void mgl_cont_all_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_cont_all_xyz* (graph: HMGL; x, y, z, a: HMDT; sch: String; Num: INTEGER);

	(* void mgl_cont_all(HMGL graph, const HMDT a, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_cont_all* (graph: HMGL; a: HMDT; sch: String; Num: INTEGER);

	(* void mgl_cloudp_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *stl, float alpha); *)
	PROCEDURE [ccall] mgl_cloudp_xyz* (graph: HMGL; x, y, z, a: HMDT; stl: String; alpha: SHORTREAL);

	(* void mgl_cloudp(HMGL graph, const HMDT a, const char *stl, float alpha); *)
	PROCEDURE [ccall] mgl_cloudp* (graph: HMGL; a: HMDT; stl: String; alpha: SHORTREAL);

	(* void mgl_cloud_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *stl, float alpha); *)
	PROCEDURE [ccall] mgl_cloud_xyz* (graph: HMGL; x, y, z, a: HMDT; stl: String; alpha: SHORTREAL);

	(* void mgl_cloud(HMGL graph, const HMDT a, const char *stl, float alpha); *)
	PROCEDURE [ccall] mgl_cloud* (graph: HMGL; a: HMDT; stl: String; alpha: SHORTREAL);

	(* void mgl_contf3_xyz_val(HMGL graph, const HMDT v, const HMDT x, const HMDT y, const HMDT z, const HMDT a, char dir, int sVal, const char *sch); *)
	PROCEDURE [ccall] mgl_contf3_xyz_val* (graph: HMGL; v, x, y, z, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; sch: String);

	(* void mgl_contf3_val(HMGL graph, const HMDT v, const HMDT a, char dir, int sVal, const char *sch); *)
	PROCEDURE [ccall] mgl_contf3_val* (graph: HMGL; v, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; stl: String);

	(* void mgl_contf3_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, char dir, int sVal, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_contf3_xyz* (graph: HMGL; x, y, z, a: HMDT; dir: SHORTCHAR; sVal: INTEGER; sch: String; Num: INTEGER);

	(* void mgl_contf3(HMGL graph, const HMDT a, char dir, int sVal, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_contf3* (graph: HMGL; a: HMDT; dir: SHORTCHAR; sVal: INTEGER; sch: String; Num: INTEGER);

	(* void mgl_contf_all_xyz(HMGL graph, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_contf_all_xyz* (graph: HMGL; x, y, z, a: HMDT; sch: String; Num: INTEGER);

	(* void mgl_contf_all(HMGL graph, const HMDT a, const char *sch, int Num); *)
	PROCEDURE [ccall] mgl_contf_all* (graph: HMGL; a: HMDT; sch: String; Num: INTEGER);

	(* void mgl_beam_val(HMGL graph, float Val, const HMDT tr, const HMDT g1, const HMDT g2, const HMDT a, float r, const char *stl, int norm); *)
	PROCEDURE [ccall] mgl_beam_val* (graph: HMGL; Val: SHORTREAL; tr, g1, g2, a: HMDT; r: SHORTREAL; stl: String; norm: INTEGER);

	(* void mgl_beam(HMGL graph, const HMDT tr, const HMDT g1, const HMDT g2, const HMDT a, float r, const char *stl, int norm, int num); *)
	PROCEDURE [ccall] mgl_beam* (graph: HMGL; tr, g1, g2, a: HMDT; r: SHORTREAL; stl: String; norm, num: INTEGER);

	(*****************************************************************************)
	(* 		Triangular plotting functions											  *)
	(*****************************************************************************)
	(* void mgl_triplot_xyzc(HMGL gr, const HMDT nums, const HMDT x, const HMDT y, const HMDT z, const HMDT c, const char *sch); *)
	PROCEDURE [ccall] mgl_triplot_xyzc* (graph: HMGL; nums, x, y, z, c: HMDT; sch: String);

	(* void mgl_triplot_xyz(HMGL gr, const HMDT nums, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_triplot_xyz* (graph: HMGL; nums, x, y, z: HMDT; sch: String);

	(* void mgl_triplot_xy(HMGL gr, const HMDT nums, const HMDT x, const HMDT y, const char *sch, float zVal); *)
	PROCEDURE [ccall] mgl_triplot_xy* (graph: HMGL; nums, x, y: HMDT; sch: String; zVal: SHORTREAL);

	(* void mgl_dots(HMGL gr, const HMDT x, const HMDT y, const HMDT z, const char *sch); *)
	PROCEDURE [ccall] mgl_dots* (graph: HMGL; x, y, z: HMDT; sch: String);

	(* void mgl_dots_tr(HMGL gr, const HMDT tr, const char *sch); *)
	PROCEDURE [ccall] mgl_dots_tr* (graph: HMGL; tr: HMDT; sch: String);

	(* void mgl_crust(HMGL gr, const HMDT x, const HMDT y, const HMDT z, const char *sch, float er); *)
	PROCEDURE [ccall] mgl_crust* (graph: HMGL; x, y, z: HMDT; sch: String; er: SHORTREAL);

	(* void mgl_crust_tr(HMGL gr, const HMDT tr, const char *sch, float er); *)
	PROCEDURE [ccall] mgl_crust_tr* (graph: HMGL; tr: HMDT; sch: String; er: SHORTREAL);

	(*****************************************************************************)
	(* 		Combined plotting functions											  *)
	(*****************************************************************************)
	(* void mgl_dens_x(HMGL graph, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_dens_x* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_dens_y(HMGL graph, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_dens_y* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_dens_z(HMGL graph, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_dens_z* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_cont_x(HMGL graph, const HMDT a, const char *stl, float sVal, int Num); *)
	PROCEDURE [ccall] mgl_cont_x* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL; Num: INTEGER);

	(* void mgl_cont_y(HMGL graph, const HMDT a, const char *stl, float sVal, int Num); *)
	PROCEDURE [ccall] mgl_cont_y* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL; Num: INTEGER);

	(* void mgl_cont_z(HMGL graph, const HMDT a, const char *stl, float sVal, int Num); *)
	PROCEDURE [ccall] mgl_cont_z* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL; Num: INTEGER);

	(* void mgl_cont_x_val(HMGL graph, const HMDT v, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_cont_x_val* (graph: HMGL; v, a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_cont_y_val(HMGL graph, const HMDT v, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_cont_y_val* (graph: HMGL; v, a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_cont_z_val(HMGL graph, const HMDT v, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_cont_z_val* (graph: HMGL; v, a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_contf_x(HMGL graph, const HMDT a, const char *stl, float sVal, int Num); *)
	PROCEDURE [ccall] mgl_contf_x* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL; Num: INTEGER);

	(* void mgl_contf_y(HMGL graph, const HMDT a, const char *stl, float sVal, int Num); *)
	PROCEDURE [ccall] mgl_contf_y* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL; Num: INTEGER);

	(* void mgl_contf_z(HMGL graph, const HMDT a, const char *stl, float sVal, int Num); *)
	PROCEDURE [ccall] mgl_contf_z* (graph: HMGL; a: HMDT; stl: String; sVal: SHORTREAL; Num: INTEGER);

	(* void mgl_contf_x_val(HMGL graph, const HMDT v, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_contf_x_val* (graph: HMGL; v, a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_contf_y_val(HMGL graph, const HMDT v, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_contf_y_val* (graph: HMGL; v, a: HMDT; stl: String; sVal: SHORTREAL);

	(* void mgl_contf_z_val(HMGL graph, const HMDT v, const HMDT a, const char *stl, float sVal); *)
	PROCEDURE [ccall] mgl_contf_z_val* (graph: HMGL; v, a: HMDT; stl: String; sVal: SHORTREAL);

	(*****************************************************************************)
	(* 		Data creation functions												  *)
	(*****************************************************************************)
	(* void mgl_data_rearrange(HMDT dat, int mx, int my, int mz); *)
	PROCEDURE [ccall] mgl_data_rearrange* (dat: HMDT; mx, my, mz: INTEGER);

	(* void mgl_data_set_float(HMDT dat, const float *A,int NX,int NY,int NZ); *)
	PROCEDURE [ccall] mgl_data_set_float* (dat: HMDT; A: Single1D; NX, NY, NZ: INTEGER);

	(* void mgl_data_set_double(HMDT dat, const REAL *A,int NX,int NY,int NZ); *)
	PROCEDURE [ccall] mgl_data_set_double* (dat: HMDT; A: Double1D; NX, NY, NZ: INTEGER);

	(* void mgl_data_set_float2(HMDT d, const float **A,int N1,int N2); *)
	PROCEDURE [ccall] mgl_data_set_float2* (d: HMDT; A: Single2D; N1, N2: INTEGER);

	(* void mgl_data_set_double2(HMDT d, const REAL **A,int N1,int N2); *)
	PROCEDURE [ccall] mgl_data_set_double2* (d: HMDT; A: Double2D; N1, N2: INTEGER);

	(* void mgl_data_set_float3(HMDT d, const float ***A,int N1,int N2,int N3); *)
	PROCEDURE [ccall] mgl_data_set_float3* (d: HMDT; A: Single3D; N1, N2, N3: INTEGER);

	(* void mgl_data_set_double3(HMDT d, const REAL ***A,int N1,int N2,int N3); *)
	PROCEDURE [ccall] mgl_data_set_double3* (d: HMDT; A: Double3D; N1, N2: INTEGER);

	(* void mgl_data_set(HMDT dat, const HMDT a); *)
	PROCEDURE [ccall] mgl_data_set* (dat: HMDT; a: HMDT);


	(* void mgl_data_set_vector(HMDT dat, gsl_vector *v); *)
	PROCEDURE [ccall] mgl_data_set_vector* (dat: HMDT; v: GSLVector);

	(* void mgl_data_set_matrix(HMDT dat, gsl_matrix *m); *)
	PROCEDURE [ccall] mgl_data_set_matrix* (dat: HMDT; m: GSLMatrix);

	(* float mgl_data_get_value(HMDT dat, int i, int j, int k); *)
	PROCEDURE [ccall] mgl_data_get_value* (dat: HMDT; i, j, k: INTEGER): SHORTREAL;

	(* int mgl_data_get_nx(HMDT dat); *)
	PROCEDURE [ccall] mgl_data_get_nx* (dat: HMDT): INTEGER;

	(* int mgl_data_get_ny(HMDT dat); *)
	PROCEDURE [ccall] mgl_data_get_ny* (dat: HMDT): INTEGER;

	(* int mgl_data_get_nz(HMDT dat); *)
	PROCEDURE [ccall] mgl_data_get_nz* (dat: HMDT): INTEGER;

	(* void mgl_data_set_value(HMDT dat, float v, int i, int j, int k); *)
	PROCEDURE [ccall] mgl_data_set_value* (dat: HMDT; v: SHORTREAL; i, j, k: INTEGER);

	(* void mgl_data_set_values(HMDT dat, const char *val, int nx, int ny, int nz); *)
	PROCEDURE [ccall] mgl_data_set_values* (dat: HMDT; val: String; nx, ny, nz: INTEGER);

	(* int mgl_data_read(HMDT dat, const char *fname); *)
	PROCEDURE [ccall] mgl_data_read* (dat: HMDT; fname: String): INTEGER;

	(* int mgl_data_read_mat(HMDT dat, const char *fname, int dim); *)
	PROCEDURE [ccall] mgl_data_read_mat* (dat: HMDT; fname: String; dim: INTEGER): INTEGER;

	(* int mgl_data_read_dim(HMDT dat, const char *fname,int mx,int my,int mz); *)
	PROCEDURE [ccall] mgl_data_read_dim* (dat: HMDT; fname: String; mx, my, mz: INTEGER): INTEGER;

	(* void mgl_data_save(HMDT dat, const char *fname,int ns); *)
	PROCEDURE [ccall] mgl_data_save* (dat: HMDT; fname: String; ns: INTEGER);

	(* void mgl_data_export(HMDT dat, const char *fname, const char *scheme,float v1,float v2,int ns); *)
	PROCEDURE [ccall] mgl_data_export* (dat: HMDT; fname: String; scheme: String; v1, v2: SHORTREAL; ns: INTEGER);

	(* void mgl_data_import(HMDT dat, const char *fname, const char *scheme,float v1,float v2); *)
	PROCEDURE [ccall] mgl_data_import* (dat: HMDT; fname: String; scheme: String; v1, v2: SHORTREAL);

	(* void mgl_data_create(HMDT dat, int nx,int ny,int nz); *)
	PROCEDURE [ccall] mgl_data_create* (dat: HMDT; nx, ny, nz: INTEGER);

	(* void mgl_data_transpose(HMDT dat, const char *dim); *)
	PROCEDURE [ccall] mgl_data_transpose* (dat: HMDT; dim: String);

	(* void mgl_data_norm(HMDT dat, float v1,float v2,int sym,int dim); *)
	PROCEDURE [ccall] mgl_data_norm* (dat: HMDT; v1, v2: SHORTREAL; sym: INTEGER; dim: INTEGER);

	(* void mgl_data_norm_slice(HMDT dat, float v1,float v2,char dir,int keep_en,int sym); *)
	PROCEDURE [ccall] mgl_data_norm_slice* (dat: HMDT; v1, v2: SHORTREAL; dir: SHORTCHAR; keep_en, sym: INTEGER);

	(* HMDT mgl_data_subdata(HMDT dat, int xx,int yy,int zz); *)
	PROCEDURE [ccall] mgl_data_subdata* (dat: HMDT; xx, yy, zz: INTEGER): HMDT;

	(* HMDT mgl_data_column(HMDT dat, const char *eq); *)
	PROCEDURE [ccall] mgl_data_column* (dat: HMDT; eq: String): HMDT;

	(* void mgl_data_set_id(HMDT d, const char *id); *)
	PROCEDURE [ccall] mgl_data_set_id* (d: HMDT; id: String);

	(* void mgl_data_fill(HMDT dat, float x1,float x2,char dir); *)
	PROCEDURE [ccall] mgl_data_fill* (dat: HMDT; x1, x2: SHORTREAL; dir: SHORTCHAR);

	(* void mgl_data_fill_eq(HMGL gr, HMDT dat, const char *eq, const HMDT vdat, const HMDT wdat); *)
	PROCEDURE [ccall] mgl_data_fill_eq* (graph: HMGL; dat: HMDT; eq: String; vdat, wdat: HMDT);

	(* void mgl_data_put_val(HMDT dat, float val, int i, int j, int k); *)
	PROCEDURE [ccall] mgl_data_put_val* (dat: HMDT; val: SHORTREAL; i, j, k: INTEGER);

	(* void mgl_data_put_dat(HMDT dat, const HMDT val, int i, int j, int k); *)
	PROCEDURE [ccall] mgl_data_put_dat* (dat: HMDT; val: HMDT; i, j, k: INTEGER);

	(* void mgl_data_modify(HMDT dat, const char *eq,int dim); *)
	PROCEDURE [ccall] mgl_data_modify* (dat: HMDT; eq: String; dim: INTEGER);

	(* void mgl_data_modify_vw(HMDT dat, const char *eq,const HMDT vdat,const HMDT wdat); *)
	PROCEDURE [ccall] mgl_data_modify_vw* (dat: HMDT; eq: String; vdat, wdat: HMDT);

	(* void mgl_data_squeeze(HMDT dat, int rx,int ry,int rz,int smooth); *)
	PROCEDURE [ccall] mgl_data_squeeze* (dat: HMDT; rx, ry, rz, smooth: INTEGER);

	(* float mgl_data_max(HMDT dat); *)
	PROCEDURE [ccall] mgl_data_max* (dat: HMDT): SHORTREAL;

	(* float mgl_data_min(HMDT dat); *)
	PROCEDURE [ccall] mgl_data_min* (dat: HMDT): SHORTREAL;

	(* float *mgl_data_value(HMDT dat, int i,int j,int k); *)
	PROCEDURE [ccall] mgl_data_value* (dat: HMDT; i, j, k: INTEGER): Single1D;

	(* CONST float *mgl_data_data(HMDT dat); *)
	PROCEDURE [ccall] mgl_data_data* (dat: HMDT): Single1D;

	(* HMDT mgl_data_combine(HMDT dat1, const HMDT dat2); *)
	PROCEDURE [ccall] mgl_data_combine* (dat1, dat2: HMDT): HMDT;

	(* void mgl_data_extend(HMDT dat, int n1, int n2); *)
	PROCEDURE [ccall] mgl_data_extend* (dat: HMDT; n1, n2: INTEGER);

	(* void mgl_data_insert(HMDT dat, char dir, int at, int num); *)
	PROCEDURE [ccall] mgl_data_insert* (dat: HMDT; dir: SHORTCHAR; pos, num: INTEGER);

	(* void mgl_data_delete(HMDT dat, char dir, int at, int num); *)
	PROCEDURE [ccall] mgl_data_delete* (dat: HMDT; dir: SHORTCHAR; pos, num: INTEGER);

	(*****************************************************************************)
	(* 		Data manipulation functions											  *)
	(*****************************************************************************)
	(* void mgl_data_smooth(HMDT dat, int type,float delta,const char *dirs); *)
	PROCEDURE [ccall] mgl_data_smooth* (dat: HMDT; tType: INTEGER; delpa: SHORTREAL; dirs: String);

	(* HMDT mgl_data_sum(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_sum* (dat: HMDT; dir: String): HMDT;

	(* HMDT mgl_data_max_dir(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_max_dir* (dat: HMDT; dir: String): HMDT;

	(* HMDT mgl_data_min_dir(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_min_dir* (dat: HMDT; dir: String): HMDT;

	(* void mgl_data_cumsum(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_cumsum* (dat: HMDT; dir: String);

	(* void mgl_data_integral(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_integral* (dat: HMDT; dir: String);

	(* void mgl_data_diff(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_diff* (dat: HMDT; dir: String);

	(* void mgl_data_diff_par(HMDT dat, const HMDT v1, const HMDT v2, const HMDT v3); *)
	PROCEDURE [ccall] mgl_data_diff_par* (dat, v1, v2, v3: HMDT);

	(* void mgl_data_diff2(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_diff2* (dat: HMDT; dir: String);

	(* void mgl_data_swap(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_swap* (dat: HMDT; dir: String);

	(* void mgl_data_mirror(HMDT dat, const char *dir); *)
	PROCEDURE [ccall] mgl_data_mirror* (dat: HMDT; dir: String);

	(* float mgl_data_spline(HMDT dat, float x,float y,float z); *)
	PROCEDURE [ccall] mgl_data_spline* (dat: HMDT; x, y, z: SHORTREAL): SHORTREAL;

	(* float mgl_data_spline1(HMDT dat, float x,float y,float z); *)
	PROCEDURE [ccall] mgl_data_spline1* (dat: HMDT; x, y, z: SHORTREAL): SHORTREAL;

	(* float mgl_data_linear(HMDT dat, float x,float y,float z); *)
	PROCEDURE [ccall] mgl_data_linear* (dat: HMDT; x, y, z: SHORTREAL): SHORTREAL;

	(* float mgl_data_linear1(HMDT dat, float x,float y,float z); *)
	PROCEDURE [ccall] mgl_data_linear1* (dat: HMDT; x, y, z: SHORTREAL): SHORTREAL;

	(* HMDT mgl_data_resize(HMDT dat, int mx,int my,int mz); *)
	PROCEDURE [ccall] mgl_data_resize* (dat: HMDT; mx, my, mz: INTEGER): HMDT;

	(* HMDT mgl_data_resize_box(HMDT dat, int mx,int my,int mz,float x1,float x2, *)
	(* 		float y1,float y2,float z1,float z2); *)
	PROCEDURE [ccall] mgl_data_resize_box* (dat: HMDT; mx, my, mz: INTEGER; x1, x2, y1, y2, z1, z2: SHORTREAL): HMDT;

	(* HMDT mgl_data_hist(HMDT dat, int n, float v1, float v2, int nsub); *)
	PROCEDURE [ccall] mgl_data_hist* (dat: HMDT; n: INTEGER; v1, v2: SHORTREAL; nsub: INTEGER): HMDT;

	(* HMDT mgl_data_hist_w(HMDT dat, const HMDT weight, int n, float v1, float v2, int nsub); *)
	PROCEDURE [ccall] mgl_data_hist_w* (dat, weight: HMDT; n: INTEGER; v1, v2: SHORTREAL; nsub: INTEGER): HMDT;

	(* HMDT mgl_data_momentum(HMDT dat, char dir, const char *how); *)
	PROCEDURE [ccall] mgl_data_momentum* (dat: HMDT; dir: SHORTCHAR; how: String): HMDT;

	(* HMDT mgl_data_evaluate_i(HMDT dat, const HMDT idat, int norm); *)
	PROCEDURE [ccall] mgl_data_evaluate_i* (dat, idat: HMDT; norm: SHORTCHAR): HMDT;

	(* HMDT mgl_data_evaluate_ij(HMDT dat, const HMDT idat, const HMDT jdat, int norm); *)
	PROCEDURE [ccall] mgl_data_evaluate_ij* (dat, idat, jdat: HMDT; norm: SHORTCHAR): HMDT;

	(* HMDT mgl_data_evaluate_ijk(HMDT dat, const HMDT idat, const HMDT jdat, const HMDT kdat, int norm); *)
	PROCEDURE [ccall] mgl_data_evaluate_ijk* (dat, idat, jdat, kdat: HMDT; norm: SHORTCHAR): HMDT;

	(* void mgl_data_envelop(HMDT dat, char dir); *)
	PROCEDURE [ccall] mgl_data_envelop* (dat: HMDT; dir: SHORTCHAR);

	(* void mgl_data_sew(HMDT dat, const char *dirs, float da); *)
	PROCEDURE [ccall] mgl_data_sew* (dat: HMDT; dirs: String; da: SHORTREAL);

	(* void mgl_data_crop(HMDT dat, int n1, int n2, char dir); *)
	PROCEDURE [ccall] mgl_data_crop* (dat: HMDT; n1, n2: INTEGER; dir: SHORTCHAR);

	(*****************************************************************************)
	(* 		Data operations														  *)
	(*****************************************************************************)
	(* void mgl_data_mul_dat(HMDT dat, const HMDT d); *)
	PROCEDURE [ccall] mgl_data_mul_dat* (dat: HMDT; d: HMDT);

	(* void mgl_data_div_dat(HMDT dat, const HMDT d); *)
	PROCEDURE [ccall] mgl_data_div_dat* (dat: HMDT; d: HMDT);

	(* void mgl_data_add_dat(HMDT dat, const HMDT d); *)
	PROCEDURE [ccall] mgl_data_add_dat* (dat: HMDT; d: HMDT);

	(* void mgl_data_sub_dat(HMDT dat, const HMDT d); *)
	PROCEDURE [ccall] mgl_data_sub_dat* (dat: HMDT; d: HMDT);

	(* void mgl_data_mul_num(HMDT dat, float d); *)
	PROCEDURE [ccall] mgl_data_mul_num* (dat: HMDT; d: SHORTREAL);

	(* void mgl_data_div_num(HMDT dat, float d); *)
	PROCEDURE [ccall] mgl_data_div_num* (dat: HMDT; d: SHORTREAL);

	(* void mgl_data_add_num(HMDT dat, float d); *)
	PROCEDURE [ccall] mgl_data_add_num* (dat: HMDT; d: SHORTREAL);

	(* void mgl_data_sub_num(HMDT dat, float d); *)
	PROCEDURE [ccall] mgl_data_sub_num* (dat: HMDT; d: SHORTREAL);

	(*****************************************************************************)
	(* 		Nonlinear fitting													  *)
	(*****************************************************************************)
	(* float mgl_fit_1(HMGL gr, HMDT fit, const HMDT y, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_1* (gr: HMGL; fit: HMDT; y: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_2(HMGL gr, HMDT fit, const HMDT z, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_2* (gr: HMGL; fit: HMDT; z: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_3(HMGL gr, HMDT fit, const HMDT a, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_3* (gr: HMGL; fit: HMDT; a: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xy(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_xy* (gr: HMGL; fit: HMDT; x, y: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyz(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_xyz* (gr: HMGL; fit: HMDT; x, y, z: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyza(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_xyza* (gr: HMGL; fit: HMDT; x, y, z, a: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_ys(HMGL gr, HMDT fit, const HMDT y, const HMDT s, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_ys* (gr: HMGL; fit: HMDT; y, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xys(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT s, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_xys* (gr: HMGL; fit: HMDT; x, y, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyzs(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const HMDT s, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_xyzs* (gr: HMGL; fit: HMDT; x, y, z, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyzas(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const HMDT s, const char *eq, const char *var, float *ini); *)
	PROCEDURE [ccall] mgl_fit_xyzas* (gr: HMGL; fit: HMDT; x, y, z, a, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_1_d(HMGL gr, HMDT fit, const HMDT y, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_1_d* (gr: HMGL; fit: HMDT; y: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_2_d(HMGL gr, HMDT fit, const HMDT z, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_2_d* (gr: HMGL; fit: HMDT; z: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_3_d(HMGL gr, HMDT fit, const HMDT a, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_3_d* (gr: HMGL; fit: HMDT; a: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xy_d(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_xy_d* (gr: HMGL; fit: HMDT; x, y: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyz_d(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_xyz_d* (gr: HMGL; fit: HMDT; x, y, z: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyza_d(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_xyza_d* (gr: HMGL; fit: HMDT; x, y, z, a: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_ys_d(HMGL gr, HMDT fit, const HMDT y, const HMDT s, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_ys_d* (gr: HMGL; fit: HMDT; y, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xys_d(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT s, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_xys_d* (gr: HMGL; fit: HMDT; x, y, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyzs_d(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const HMDT s, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_xyzs_d* (gr: HMGL; fit: HMDT; x, y, z, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* float mgl_fit_xyzas_d(HMGL gr, HMDT fit, const HMDT x, const HMDT y, const HMDT z, const HMDT a, const HMDT s, const char *eq, const char *var, HMDT ini); *)
	PROCEDURE [ccall] mgl_fit_xyzas_d* (gr: HMGL; fit: HMDT; x, y, z, a, s: HMDT; eq: String; vvar: String; ini: Single1D): SHORTREAL;

	(* void mgl_puts_fit(HMGL gr, float x, float y, float z, const char *prefix, const char *font, float size); *)
	PROCEDURE [ccall] mgl_puts_fit* (gr: HMGL; x, y, z: SHORTREAL; prefix: String; font: String; size: SHORTREAL);

	(*****************************************************************************)
	(* void mgl_sphere(HMGL graph, float x, float y, float z, float r, const char *stl); *)
	PROCEDURE [ccall] mgl_sphere* (gr: HMGL; x, y, z, r: SHORTREAL; stl: String);
	(* void mgl_drop(HMGL graph, float x1, float y1, float z1, float x2, float y2, float z2, float r, const char *stl, float shift, float ap); *)
	PROCEDURE [ccall] mgl_drop* (gr: HMGL; x1, y1, z1, x2, y2, z2, r: SHORTREAL; stl: String; shift, ap: SHORTREAL);
	(* void mgl_cone(HMGL graph, float x1, float y1, float z1, float x2, float y2, float z2, float r1, float r2, const char *stl, int edge); *)
	PROCEDURE [ccall] mgl_cone* (gr: HMGL; x1, y1, z1, x2, y2, z2, r1, r2: SHORTREAL; stl: String; edge: INTEGER);

	(* HMDT mgl_pde_solve(HMGL gr, const char *ham, const HMDT ini_re, const HMDT ini_im, float dz, float k0); *)
	PROCEDURE [ccall] mgl_pde_solve* (gr: HMGL; ham: String; ini_re, ini_im: HMDT; dz, k0: SHORTREAL): HMDT;
	(* HMDT mgl_qo2d_solve(char *ham, const HMDT ini_re, const HMDT ini_im, const HMDT ray, float r, float k0, HMDT xx, HMDT yy); *)
	PROCEDURE [ccall] mgl_qo2d_solve* (ham: String; ini_re, ini_im, ray: HMDT; r, k0: SHORTREAL; xx, yy: HMDT): HMDT;
	(* HMDT mgl_af2d_solve(char *ham, const HMDT ini_re, const HMDT ini_im, const HMDT ray, float r, float k0, HMDT xx, HMDT yy); *)
	PROCEDURE [ccall] mgl_af2d_solve* (ham: String; ini_re, ini_im, ray: HMDT; r, k0: SHORTREAL; xx, yy: HMDT): HMDT;
	(* HMDT mgl_ray_trace(char *ham, float x0, float y0, float z0, float px, float py, float pz, float dt, float tmax); *)
	PROCEDURE [ccall] mgl_ray_trace* (ham: String; x0, y0, z0, px, py, pz, dt, tmax: SHORTREAL): HMDT;
	(* HMDT mgl_jacobian_2d(HMDT x, const HMDT y); *)
	PROCEDURE [ccall] mgl_jacobian_2d* (x, y: HMDT): HMDT;
	(* HMDT mgl_jacobian_3d(HMDT x, const HMDT y, const HMDT z); *)
	PROCEDURE [ccall] mgl_jacobian_3d* (x, y, z: HMDT): HMDT;
	(* HMDT mgl_transform_a(HMDT am, const HMDT ph, const char *tr); *)
	PROCEDURE [ccall] mgl_transform_a* (am, ph: HMDT; tr: String): HMDT;
	(* HMDT mgl_transform(HMDT re, const HMDT im, const char *tr); *)
	PROCEDURE [ccall] mgl_transform* (re, im: HMDT; tr: String): HMDT;
	(* HMDT mgl_data_stfa(HMDT re, const HMDT im, int dn, char dir); *)
	PROCEDURE [ccall] mgl_data_stfa* (re, im: HMDT; dn: INTEGER; dir: SHORTCHAR): HMDT;

	(*****************************************************************************)

END LibsMathGL.