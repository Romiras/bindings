MODULE LibsFreeImageWin32 ['FreeImage.dll'];

	IMPORT SYSTEM, WinApi, lib := LibsFreeImage;
	
TYPE

	PtrBITMAPINFOHEADER* = WinApi.PtrBITMAPINFOHEADER;

(*		PtrBITMAPINFOHEADER* = POINTER TO BITMAPINFOHEADER;
		BITMAPINFOHEADER* = RECORD [untagged]
			biSize*: INTEGER;
			biWidth*: INTEGER;
			biHeight*: INTEGER;
			biPlanes*: SHORTINT;
			biBitCount*: SHORTINT;
			biCompression*: INTEGER;
			biSizeImage*: INTEGER;
			biXPelsPerMeter*: INTEGER;
			biYPelsPerMeter*: INTEGER;
			biClrUsed*: INTEGER;
			biClrImportant*: INTEGER;
		END;
*)
PtrBITMAPINFO* = WinApi.PtrBITMAPINFO;

(*		PtrBITMAPINFO* = POINTER TO BITMAPINFO;
		BITMAPINFO* = RECORD [untagged]
			bmiHeader*: BITMAPINFOHEADER;
			bmiColors*: ARRAY [untagged] 1 OF RGBQUAD;
		END;
*)
PROCEDURE GetInfoHeader*['_FreeImage_GetInfoHeader@4'] (dib: lib.FIBITMAP): PtrBITMAPINFOHEADER;
PROCEDURE GetInfo*['_FreeImage_GetInfo@4'] (dib: lib.FIBITMAP): PtrBITMAPINFO;

END LibsFreeImageWin32.