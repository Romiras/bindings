MODULE LibsFreeImage ['FreeImage.dll'];
	(* Version: 3.11 *)
	(* Date: 5 Dec. 2008 *)
	(* Author: Necati Ecevit   *)

   (*  Handle, Memory I/O,  Zlib Compression routines are not tested , and, line conversion routines  are not translated yet. *)

	IMPORT SYSTEM;

	CONST

		(* Indexes for byte arrays, masks and shifts for treating pixels as words
		These coincide with the order of RGBQUAD and RGBTRIPLE
		Little Endian (x86 / MS Windows, Linux) : BGR(A) order *)

		FI_RGBA_RED* = 2;
		FI_RGBA_GREEN* = 1;
		FI_RGBA_BLUE* = 0;
		FI_RGBA_ALPHA* = 3;
		FI_RGBA_RED_MASK* = 00FF0000H;
		FI_RGBA_GREEN_MASK* = 0000FF00H;
		FI_RGBA_BLUE_MASK* = 000000FFH;
		FI_RGBA_ALPHA_MASK* = 0FF000000H;
		FI_RGBA_RED_SHIFT* = 16;
		FI_RGBA_GREEN_SHIFT* = 8;
		FI_RGBA_BLUE_SHIFT* = 0;
		FI_RGBA_ALPHA_SHIFT* = 24;

		(* --------------------------------------------------------------------------
		The 16bit macros only include masks and shifts,
		since each color element is not byte aligned
		--------------------------------------------------------------------------*)

	CONST
		FI16_555_RED_MASK* = 7C00H;
		FI16_555_GREEN_MASK* = 03E0H;
		FI16_555_BLUE_MASK* = 001FH;
		FI16_555_RED_SHIFT* = 10;
		FI16_555_GREEN_SHIFT* = 5;
		FI16_555_BLUE_SHIFT* = 0;
		FI16_565_RED_MASK* = 0F800H;
		FI16_565_GREEN_MASK* = 07E0H;
		FI16_565_BLUE_MASK* = 001FH;
		FI16_565_RED_SHIFT* = 11;
		FI16_565_GREEN_SHIFT* = 5;
		FI16_565_BLUE_SHIFT* = 0;

	(*	ICC profile support *)
	CONST
		FIICC_DEFAULT* = 0H;
		FIICC_COLOR_IS_CMYK*	 = 1H;

	CONST
		(* I/O image format identifiers. *)
	TYPE IMAGE_FORMAT* = INTEGER;
	CONST
		FIF_UNKNOWN* = -1;
		FIF_BMP* = 0;
		FIF_ICO* = 1;
		FIF_JPEG* = 2;
		FIF_JNG* = 3;
		FIF_KOALA* = 4;
		FIF_LBM* = 5;
		FIF_IFF* = FIF_LBM;
		FIF_MNG* = 6;
		FIF_PBM* = 7;
		FIF_PBMRAW* = 8;
		FIF_PCD* = 9;
		FIF_PCX* = 10;
		FIF_PGM* = 11;
		FIF_PGMRAW* = 12;
		FIF_PNG* = 13;
		FIF_PPM* = 14;
		FIF_PPMRAW* = 15;
		FIF_RAS* = 16;
		FIF_TARGA* = 17;
		FIF_TIFF* = 18;
		FIF_WBMP* = 19;
		FIF_PSD* = 20;
		FIF_CUT* = 21;
		FIF_XBM* = 22;
		FIF_XPM* = 23;
		FIF_DDS* = 24;
		FIF_GIF* = 25;
		FIF_HDR* = 26;
		FIF_FAXG3* = 27;
		FIF_SGI* = 28;
		FIF_EXR* = 29;
		FIF_J2K* = 30;
		FIF_JP2* = 31;
		(* Image type used in FreeImage.*)
	TYPE IMAGE_TYPE* = INTEGER;
	CONST
		FIT_UNKNOWN* = 0; 	(* unknown type *)
		FIT_BITMAP* = 1; 	(* standard image			: 1-, 4-, 8-, 16-, 24-, 32-bit *)
		FIT_UINT16*	 = 2; 	(* array of unsigned short	: unsigned 16-bit *)
		FIT_INT16*	 = 3; 	(* array of short			: signed 16-bit *)
		FIT_UINT32*	 = 4; 	(* array of unsigned long	: unsigned 32-bit *)
		FIT_INT32*	 = 5; 	(* array of long			: signed 32-bit *)
		FIT_FLOAT*	 = 6; 	(* array of float			: 32-bit IEEE floating point *)
		FIT_DOUBLE*	 = 7; 	(* array of double			: 64-bit IEEE floating point *)
		FIT_COMPLEX*	 = 8; 	(* array of FICOMPLEX		: 2 x 64-bit IEEE  floating point *)
		FIT_RGB16*	 = 9; 	(* 48-bit RGB image			: 3 x 16-bit *)
		FIT_RGBA16*	 = 10; 	(* 64-bit RGBA image		: 4 x 16-bit *)
		FIT_RGBF*	 = 11; 	(* 96-bit RGB float image	: 3 x 32-bit IEEE floating point *)
		FIT_RGBAF*	 = 12; 	(* 128-bit RGBA float image	: 4 x 32-bit IEEE floating point *)

		(* Image color type used in FreeImage. *)
	TYPE IMAGE_COLOR_TYPE* = INTEGER;
	CONST
		FIC_MINISWHITE* = 0; 		(* min value is white *)
		FIC_MINISBLACK* = 1; 		(* min value is black *)
		FIC_RGB* = 2; 		(* RGB color model *)
		FIC_PALETTE* = 3; 		(* color map indexed *)
		FIC_RGBALPHA* = 4; 		(* RGB color model with alpha channel *)
		FIC_CMYK* = 5; 		(* CMYK color model *)

		(* Color quantization algorithms. Constants used in FreeImage_ColorQuantize. *)
	TYPE IMAGE_QUANTIZE* = INTEGER;
	CONST
		FIQ_WUQUANT* = 0; 		(* Xiaolin Wu color quantization algorithm *)
		FIQ_NNQUANT* = 1; 			(* NeuQuant neural-net quantization algorithm by Anthony Dekker *)

		(* Dithering algorithms. Constants used FreeImage_Dither. *)
	TYPE IMAGE_DITHER* = INTEGER;
	CONST
		FID_FS* = 0; 	(* Floyd & Steinberg error diffusion *)
		FID_BAYER4x4* = 1; 	(* Bayer ordered dispersed dot dithering (order 2 dithering matrix)*)
		FID_BAYER8x8* = 2; 	(* Bayer ordered dispersed dot dithering (order 3 dithering matrix)*)
		FID_CLUSTER6x6* = 3; 	(* Ordered clustered dot dithering (order 3 - 6x6 matrix)*)
		FID_CLUSTER8x8* = 4; 	(* Ordered clustered dot dithering (order 4 - 8x8 matrix)*)
		FID_CLUSTER16x16* = 5; (* Ordered clustered dot dithering (order 8 - 16x16 matrix)*)
		FID_BAYER16x16* = 6; (* Bayer ordered dispersed dot dithering (order 4 dithering matrix)*)

		(* Lossless JPEG transformations Constants used in FreeImage_JPEGTransform	*)
	TYPE IMAGE_JPEG_OPERATION* = INTEGER;
	CONST
		FIJPEG_OP_NONE*			 = 0; 	(* no transformation *)
		FIJPEG_OP_FLIP_H*		 = 1; 	(* horizontal flip *)
		FIJPEG_OP_FLIP_V*		 = 2; 	(* vertical flip *)
		FIJPEG_OP_TRANSPOSE*		 = 3; 	(* transpose across UL-to-LR axis *)
		FIJPEG_OP_TRANSVERSE*	 = 4; 	(* transpose across UR-to-LL axis *)
		FIJPEG_OP_ROTATE_90*		 = 5; 	(* 90-degree clockwise rotation *)
		FIJPEG_OP_ROTATE_180*	 = 6; 	 (* 180-degree rotation *)
		FIJPEG_OP_ROTATE_270*	 = 7; 	(* 270-degree clockwise (or 90 ccw) *)

(* Tone mapping operators. Constants used in FreeImage_ToneMapping. *)
 TYPE IMAGE_TMO* = INTEGER;
CONST
    FITMO_DRAGO03* = 0; (* Adaptive logarithmic mapping (F. Drago, 2003) *)
	FITMO_REINHARD05* = 1; (* Dynamic range reduction inspired by photoreceptor physiology (E. Reinhard, 2005) *)
    FITMO_FATTAL02*  = 2;	(* Gradient domain high dynamic range compression (R. Fattal, 2002) *)
		(* Upsampling / downsampling filters. Constants used in FreeImage_Rescale. *)
	TYPE IMAGE_FILTER* = INTEGER;
	CONST
		FILTER_BOX*	 = 0; 	(* Box, pulse, Fourier window, 1st order (constant) b-spline *)
		FILTER_BICUBIC* = 1; 	(* Mitchell & Netravali's two-param cubic filter *)
		FILTER_BILINEAR* = 2; 	(* Bilinear filter *)
		FILTER_BSPLINE* = 3; 	(* 4th order (cubic) b-spline *)
		FILTER_CATMULLROM* = 4; 	(* Catmull-Rom spline, Overhauser spline *)
		FILTER_LANCZOS3* = 5; 	 (* Lanczos3 filter *)

		(* Color channels. Constants used in color manipulation routines. *)
	TYPE IMAGE_COLOR_CHANNEL* = INTEGER;
	CONST
		FICC_RGB* = 0; (* Use red, green and blue channels *)
		FICC_RED* = 1; (* Use red channel *)
		FICC_GREEN* = 2; (* Use green channel *)
		FICC_BLUE* = 3; (* Use blue channel *)
		FICC_ALPHA* = 4; (* Use alpha channel *)
		FICC_BLACK* = 5; (* Use black channel *)
		FICC_REAL* = 6; (* Complex images: use real part *)
		FICC_IMAG* = 7; (* Complex images: use imaginary part *)
		FICC_MAG* = 8; (* Complex images: use magnitude *)
		FICC_PHASE* = 9; 	 (* Complex images: use phase *)

(*  Tag data type information (based on TIFF specifications)
		Note: RATIONALs are the ratio of two 32-bit integer values. *)
		
	TYPE IMAGE_MDTYPE* = INTEGER;
	CONST
		FIDT_NOTYPE* = 0; 	(* placeholder *)
		FIDT_BYTE* = 1; 	(* 8-bit unsigned integer *)
		FIDT_ASCII* = 2; 	(* 8-bit bytes w/ last byte null *)
		FIDT_SHORT* = 3; 	(* 16-bit unsigned integer *)
		FIDT_LONG* = 4; 	(* 32-bit unsigned integer *)
		FIDT_RATIONAL* = 5; 	(* 64-bit unsigned fraction *)
		FIDT_SBYTE* = 6; 	(* 8-bit signed integer *)
		FIDT_UNDEFINED* = 7; 	(* 8-bit untyped data *)
		FIDT_SSHORT* = 8; 	(* 16-bit signed integer *)
		FIDT_SLONG* = 9; 	(* 32-bit signed integer *)
		FIDT_SRATIONAL* = 10; 	(* 64-bit signed fraction *)
		FIDT_FLOAT* = 11; 	(* 32-bit IEEE floating point *)
		FIDT_DOUBLE* = 12; 	(* 64-bit IEEE floating point *)
		FIDT_IFD* = 13; 	(* 32-bit unsigned integer (offset)*)
		FIDT_PALETTE* = 14; 	(* 32-bit RGBQUAD *)

		(*  Metadata models supported by FreeImage *)
	TYPE IMAGE_MDMODEL* = INTEGER;
	CONST
		FIMD_NODATA* = -1;
		FIMD_COMMENTS* = 0; 	(* single comment or keywords *)
		FIMD_EXIF_MAIN* = 1; 	(* Exif-TIFF metadata *)
		FIMD_EXIF_EXIF* = 2; 	(* Exif-specific metadata *)
		FIMD_EXIF_GPS* = 3; 	(* Exif GPS metadata *)
		FIMD_EXIF_MAKERNOTE* = 4; 	(* Exif maker note metadata *)
		FIMD_EXIF_INTEROP* = 5; 	(* Exif interoperability metadata *)
		FIMD_IPTC* = 6; 	(* IPTC/NAA metadata *)
		FIMD_XMP* = 7; 	(* Abobe XMP metadata *)
		FIMD_GEOTIFF* = 8; 	(* GeoTIFF metadata *)
		FIMD_ANIMATION* = 9; 	(* Animation metadata *)
		FIMD_CUSTOM* = 10; (* Used to attach other metadata types to a dib *)

		(*  Load / Save flag constants  *)
	TYPE  IMAGE_FLAGS*=INTEGER;
	CONST
		BMP_DEFAULT* = 0;
		BMP_SAVE_RLE* = 1;
		CUT_DEFAULT* = 0;
		DDS_DEFAULT* = 0;
		EXR_DEFAULT*=	0;		(* save data as half with piz-based wavelet compression *)
		EXR_FLOAT*=0001H;	(* save data as float instead of as half (not recommended) *)
		EXR_NONE*=0002H;	(* save with no compression *) 
		EXR_ZIP*=0004H;	(* save with zlib compression, in blocks of 16 scan lines *)
		EXR_PIZ*=0008H;	(* save with piz-based wavelet compression *)
		EXR_PXR24*=0010H;	(* save with lossy 24-bit float compression *)
		EXR_B44*=0020H;	(* save with lossy 44% float compression - goes to 22% when combined with EXR_LC *)
		EXR_LC*=0040H;	(* save images with one luminance and two chroma channels, rather than as RGB (lossy compression) *)
		
		FAXG3_DEFAULT* = 0;
		GIF_DEFAULT* = 0;
		GIF_LOAD256* = 1; 		(*  Load the image as a 256 color image with ununsed palette entries, if it's 16 or 2 color *)
		GIF_PLAYBACK* = 2; 		(* 'Play' the GIF to generate each frame (as 32bpp) instead of returning raw frame data when loading *)
		HDR_DEFAULT* = 0;
		ICO_DEFAULT* = 0;
		ICO_MAKEALPHA* = 1; 		(*  convert to 32bpp and create an alpha channel from the AND-mask when loading *)
		IFF_DEFAULT* = 0;
		J2K_DEFAULT* = 0;		(* save with a 16:1 rate *)
		JP2_DEFAULT* = 0;		(* save with a 16:1 rate *)	 	
		JPEG_DEFAULT* = 0;
		JPEG_FAST* = 1;
		JPEG_ACCURATE* = 2;
		JPEG_CMYK* = 01000H; 	(*  load separated CMYK "as is" (use | to combine with other flags) *)		
		JPEG_QUALITYSUPERB* = 080X;
		JPEG_QUALITYGOOD* = 0100H;
		JPEG_QUALITYNORMAL* = 0200H;
		JPEG_QUALITYAVERAGE* = 0400H;
		JPEG_QUALITYBAD* = 0800H;
		JPEG_PROGRESSIVE* = 02000H; (*save as a progressive-JPEG (use | to combine with other flags) *)
		JPEG_SUBSAMPLING_411=1000H;		(* save with high 4x1 chroma subsampling (4:1:1)  *)
		JPEG_SUBSAMPLING_420*= 4000H;		(* save with medium 2x2 medium chroma subsampling (4:2:0) - default value *)
		JPEG_SUBSAMPLING_422* = 8000H;		(* save with low 2x1 chroma subsampling (4:2:2)  *)
		JPEG_SUBSAMPLING_444*= 10000H;	(* save with no chroma subsampling (4:4:4) *)		
		KOALA_DEFAULT* = 0;
		LBM_DEFAULT* = 0;
		MNG_DEFAULT* = 0;
		PCD_DEFAULT* = 0;
		PCD_BASE* = 1; 		(* load the bitmap sized 768 x 512 *)
		PCD_BASEDIV4* = 2; 		(*  load the bitmap sized 384 x 256 *)
		PCD_BASEDIV16* = 3; 		(*  load the bitmap sized 192 x 128 *)
		PCX_DEFAULT* = 0;
		PNG_DEFAULT* = 0;
		PNG_IGNOREGAMMA* = 1; 		(*  avoid gamma correction *)
		PNG_Z_BEST_SPEED* =	0001H;	(* save using ZLib level 1 compression flag (default value is 6) *)
		PNG_Z_DEFAULT_COMPRESSION*=	0006H;	(* save using ZLib level 6 compression flag (default recommended value) *)
		PNG_Z_BEST_COMPRESSION*=0009H;	(* save using ZLib level 9 compression flag (default value is 6) *)
		PNG_Z_NO_COMPRESSION*=	0100H;	(* save without ZLib compression *)
		PNG_INTERLACED*=			0200H;	(* save using Adam7 interlacing (use | to combine with other save flags)*)		
		PNM_DEFAULT* = 0;
		PNM_SAVE_RAW* = 0; (*  If set the writer saves in RAW format (i.e. P4, P5 or P6) *)
		PNM_SAVE_ASCII* = 1; (* If set the writer saves in ASCII format (i.e. P1, P2 or P3) *)
		PSD_DEFAULT* = 0;
		RAS_DEFAULT* = 0;
		SGI_DEFAULT* = 0;
		TARGA_DEFAULT* = 0;
		TARGA_LOAD_RGB888* = 1; (*  If set the loader converts RGB555 and ARGB8888 -> RGB888. *)
		TIFF_DEFAULT* = 0;
		TIFF_CMYK* = 00001H; 	(*  reads/stores tags for separated CMYK (use | to combine with compression flags) *)
		TIFF_PACKBITS* = 00100H; (*  save using PACKBITS compression *)
		TIFF_DEFLATE* = 00200H; (*  save using DEFLATE compression (a.k.a. ZLIB compression) *)
		TIFF_ADOBE_DEFLATE* = 00400H; (*  save using ADOBE DEFLATE compression *)
		TIFF_NONE* = 00800H; (*  save without any compression *)
		TIFF_CCITTFAX3* = 01000H; (*  save using CCITT Group 3 fax encoding *)
		TIFF_CCITTFAX4* = 02000H; (*  save using CCITT Group 4 fax encoding *)
		TIFF_LZW* = 04000H; 	(* save using LZW compression *)
		TIFF_JPEG* = 08000H; 	(*  save using JPEG compression *)
		WBMP_DEFAULT* = 0;
		XBM_DEFAULT* = 0;
		XPM_DEFAULT* = 0;

	TYPE
	 RGBQUAD* = RECORD[untagged]
			 rgbBlue*: SHORTCHAR;
			 rgbGreen*: SHORTCHAR;
			 rgbRed*: SHORTCHAR;
			 rgbReserved*: SHORTCHAR
		 END;

		RGBTRIPLE* = RECORD[untagged]
			rgbtBlue*: SHORTCHAR;
			rgbtGreen*: SHORTCHAR;
			rgbtRed*: SHORTCHAR
		END;

(* 48-bit RGB *)
	 FIRGB16* = RECORD[untagged]
	 	red*: SHORTINT;
		 green*: SHORTINT;
		 blue*: SHORTINT
  	END;

	(* 64-bit RGBA *)
	 FIRGBA16* = RECORD[untagged]
	 	red*: SHORTINT;
   	green*: SHORTINT;
 		blue*: SHORTINT;
 		alpha*: SHORTINT
 	END;

	(* 96-bit RGB Float *)
	 FIRGBF* = RECORD[untagged]
  		red*: SHORTREAL;
  		green*: SHORTREAL;
  		blue*: SHORTREAL
  END;

	(* 128-bit RGBA Float	*)
	 FIRGBAF* = RECORD[untagged]
	 	red*: SHORTREAL;
		 green*: SHORTREAL;
		 blue*: SHORTREAL;
		 alpha*: SHORTREAL
	  END;

		(* Data structure for COMPLEX type (complex number) *)
		FICOMPLEX* = RECORD[untagged]
		 r*: REAL; (* real part *)
		 i*: REAL; (* imaginary part *)
		END;

		PChar* = POINTER TO ARRAY[untagged] OF SHORTCHAR;
		PCharU* = POINTER TO ARRAY[untagged] OF CHAR;
		LUT* = ARRAY [untagged] 256 OF BYTE; (* aded for compatibility to v3.10.0, GetAdjustColorsLookupTable, etc. *)
		(* used for easy accessing to pixels of various image types, see Scanline procs. ( Necati) *)
		PtrLineRGB* = POINTER TO ARRAY[untagged] OF RGBTRIPLE; (* for accessing 24 bit lines, RGB *)
		PtrLineRGBA* = POINTER TO ARRAY[untagged] OF RGBQUAD; (* for accessing 32 bit lines, RGBA *)
	    PtrFISINT16* = POINTER TO ARRAY[untagged] OF SHORTINT; (* for 16 bit integers *)
	    PtrFIINT32* = POINTER TO ARRAY[untagged] OF INTEGER; (* for 32 bit integers *)
	    PtrFIFLOAT* = POINTER TO ARRAY[untagged] OF SHORTREAL; (* for 32 bit real *)
	    PtrFIDOUBLE* = POINTER TO ARRAY[untagged] OF REAL; (* for 64 bit real *)

		PtrFICOMPLEX* = POINTER TO ARRAY[untagged] OF FICOMPLEX; (* 2x64 bits real, Complex *)
		PtrFIRGB16* = POINTER TO ARRAY[untagged] OF FIRGB16; (* for 48-bit RGB, 3x16 bits *)
		PtrFIRGBA16* = POINTER TO ARRAY[untagged] OF FIRGBA16; (* 64-bit RGBA, 4x16 bits *)
		PtrFIRGBF* = POINTER TO ARRAY[untagged] OF FIRGBF; (* 96-bit RGB Float, 3*32 bits *)
		PtrFIRGBAF* = POINTER TO ARRAY[untagged] OF FIRGBAF; (* 128-bit RGBA Float, 4x32 bits *)
		
		Palette* = ARRAY[untagged] 256 OF RGBQUAD;
		PtrPALETTE* = POINTER TO Palette;

	   (* File IO routines *)
		PSint* = POINTER TO ARRAY[untagged] OF SHORTINT;
		Pint* = POINTER TO ARRAY[untagged] OF INTEGER;
		FI_ReadProc* = PROCEDURE (buffer: INTEGER; size: INTEGER; count: INTEGER; handle: INTEGER): INTEGER;
		FI_WriteProc* = PROCEDURE (buffer: INTEGER; size: INTEGER; count: INTEGER; handle: INTEGER): INTEGER;
		FI_SeekProc* = PROCEDURE (handle: INTEGER; offset: INTEGER; origin: INTEGER): PSint;
		FI_TellProc* = PROCEDURE (handle: INTEGER): Pint;

	FreeImageIO* = POINTER TO RECORD[untagged]
		read_proc *: FI_ReadProc; (* pointer to the function used to read data *)
		write_proc*: FI_WriteProc; (* pointer to the function used to write data *)
		seek_proc*: FI_SeekProc; (* pointer to the function used to seek *)
		tell_proc*: FI_TellProc; (* pointer to the function used to aquire the current position *)
	END;

		FIMEMORY* = POINTER TO RECORD[untagged] data: INTEGER END;(* Handle to a memory I/O stream *)
		FIBITMAP* = POINTER TO RECORD[untagged] dib: INTEGER END; (* Handle to a FreeImage bitmap *)
		FIMULTIBITMAP* = POINTER TO RECORD [untagged] mdib: INTEGER END; (* handle to a multibitmap *)
		FIMETADATA* = POINTER TO RECORD[untagged] metadata: INTEGER END; (*Handle to a metadata model*)
		FITAG* = POINTER TO RECORD[untagged] tag: INTEGER END; (*Handle to a FreeImage tag *)
		FIICCPROFILE* = POINTER TO RECORD[untagged]
				flags*: SHORTINT; (* info flag *)
				size*: INTEGER; (* profile's size measured in bytes *)
				data*: INTEGER; (* points to a block of contiguous memory containing the profile*)
		END;

	  OutputMessageFunction* = PROCEDURE (*["ccall"]*) (fif: IMAGE_FORMAT; msg: PChar);
	  OutputMessageFunctionStdCall* = PROCEDURE (fif: IMAGE_FORMAT;  msg: PChar);
	

CONST
(* constants used in FreeImage_Seek for Origin parameter *)
	  SEEK_SET* = 0;
	  SEEK_CUR* = 1;
	  SEEK_END* = 2;

	(* Init/Error routines *)
PROCEDURE Initialise*['_FreeImage_Initialise@4'] (load_local_plugins_only: INTEGER);
PROCEDURE DeInitialise*['_FreeImage_DeInitialise@0'];


	(* Version routines *)
PROCEDURE GetVersion*['_FreeImage_GetVersion@0'] (): PChar;
PROCEDURE GetCopyrightMessage*["_FreeImage_GetCopyrightMessage@0"] (): PChar;

	(* Message output functions *)
PROCEDURE SetOutputMessage*["_FreeImage_SetOutputMessage@4"] (omf: OutputMessageFunction);
PROCEDURE SetOutputMessageStdCall*["_FreeImage_SetOutputMessageStdCall@4"] (omf: OutputMessageFunctionStdCall);

	(* Allocate/Clone/Unload routines *)

PROCEDURE Allocate*["_FreeImage_Allocate@24"] (width, height: INTEGER; bpp: INTEGER; red_mask, green_mask, blue_mask: INTEGER): FIBITMAP;
PROCEDURE AllocateT*["_FreeImage_AllocateT@28"] (type: IMAGE_TYPE; width, height: INTEGER; bpp: INTEGER; red_mask, green_mask, blue_mask: INTEGER): FIBITMAP;
PROCEDURE Clone*["_FreeImage_Clone@4"] (dib: FIBITMAP): FIBITMAP;
PROCEDURE Unload*["_FreeImage_Unload@4"] (dib: FIBITMAP);

	(* Load/Save routines *)
PROCEDURE Load*["_FreeImage_Load@12"] (fif: IMAGE_FORMAT; filename: PChar; flags: IMAGE_FLAGS): FIBITMAP;
PROCEDURE LoadU*["_FreeImage_LoadU@12"] (fif: IMAGE_FORMAT; filename: PCharU; flags: IMAGE_FLAGS): FIBITMAP;
PROCEDURE Save*["_FreeImage_Save@16"] (fif: IMAGE_FORMAT; dib: FIBITMAP; filename: PChar; flags: IMAGE_FLAGS): BOOLEAN;
PROCEDURE SaveU*["_FreeImage_SaveU@16"] (fif: IMAGE_FORMAT; dib: FIBITMAP; filename: PCharU; flags: IMAGE_FLAGS): BOOLEAN;
(****
DLL_API FIBITMAP *DLL_CALLCONV FreeImage_LoadFromHandle(FREE_IMAGE_FORMAT fif, FreeImageIO *io, fi_handle handle, int flags FI_DEFAULT(0));
DLL_API BOOL DLL_CALLCONV FreeImage_SaveToHandle(FREE_IMAGE_FORMAT fif, FIBITMAP *dib, FreeImageIO *io, fi_handle handle, int flags FI_DEFAULT(0));
****)

(* Memory I/O stream routines NOT READY*)
PROCEDURE OpenMemory*["_FreeImage_OpenMemory@8"] (data: INTEGER; size_in_bytes: INTEGER): FIMEMORY;
PROCEDURE CloseMemory*["_FreeImage_CloseMemory@4"] (stream: FIMEMORY);
PROCEDURE LoadFromMemory*["_FreeImage_LoadFromMemory@12"] (fif: IMAGE_FORMAT; stream: FIMEMORY; flags: INTEGER): FIBITMAP;
PROCEDURE SaveToMemory*["_FreeImage_SaveToMemory@16"] (fif: IMAGE_FORMAT; dib: FIBITMAP; stream: FIMEMORY; flags: INTEGER): BOOLEAN;
PROCEDURE TellMemory*["_FreeImage_TellMemory@4"] (stream: FIMEMORY): INTEGER;
PROCEDURE SeekMemory*["_FreeImage_SeekMemory@12"] (stream: FIMEMORY; offset: INTEGER; origin: INTEGER): BOOLEAN;
PROCEDURE AcquireMemory*["_FreeImage_AcquireMemory@12"] (stream: FIMEMORY;  data: INTEGER; VAR size_in_bytes: INTEGER): BOOLEAN;
PROCEDURE ReadMemory*["_FreeImage_ReadMemory@16"] (buffer: INTEGER; size: INTEGER; count: INTEGER; stream: FIMEMORY): INTEGER;
PROCEDURE WriteMemory*["_FreeImage_WriteMemory@16"] (buffer: INTEGER; size: INTEGER; count: INTEGER; stream: FIMEMORY): INTEGER;
(****
PROCEDURE LoadMultiBitmapFromMemory* ["_FreeImage_LoadMultiBitmapFromMemory"] (fif: IMAGE_FORMAT; stream: FIMEMORY; flags: INTEGER): FIMULTIBITMAP;
******)


	(* Plugin Interface *)
PROCEDURE GetFIFCount*['_FreeImage_GetFIFCount@0'] (): INTEGER;
PROCEDURE SetPluginEnabled*["_FreeImage_SetPluginEnabled@8"] (fif: IMAGE_FORMAT; enable: BOOLEAN): INTEGER;
PROCEDURE IsPluginEnabled*['_FreeImage_IsPluginEnabled@4'] (fif: IMAGE_FORMAT): INTEGER;
PROCEDURE GetFIFFromFormat*['_FreeImage_GetFIFFromFormat@4'] (format: PChar): IMAGE_FORMAT;
PROCEDURE GetFIFFromMime*['_FreeImage_GetFIFFromMime@4'] (format: PChar): IMAGE_FORMAT;
PROCEDURE GetFormatFromFIF*['_FreeImage_GetFormatFromFIF@4'] (fif: IMAGE_FORMAT): PChar;
PROCEDURE GetFIFExtensionList*['_FreeImage_GetFIFExtensionList@4'] (fif: IMAGE_FORMAT): PChar;
PROCEDURE GetFIFDescription*['_FreeImage_GetFIFDescription@4'] (fif: IMAGE_FORMAT): PChar;
PROCEDURE GetFIFRegExpr*['_FreeImage_GetFIFRegExpr@4'] (fif: IMAGE_FORMAT): PChar;
PROCEDURE GetFIFFromFilename*['_FreeImage_GetFIFFromFilename@4'] (fname: PChar): IMAGE_FORMAT;
PROCEDURE GetFIFFromFilenameU*['_FreeImage_GetFIFFromFilenameU@4'] (fname: PCharU): IMAGE_FORMAT;
PROCEDURE FIFSupportsReading*['_FreeImage_FIFSupportsReading@4'] (fif: IMAGE_FORMAT): BOOLEAN;
PROCEDURE FIFSupportsWriting*['_FreeImage_FIFSupportsWriting@4'] (fif: IMAGE_FORMAT): BOOLEAN;
PROCEDURE FIFSupportsExportBPP*['_FreeImage_FIFSupportsExportBPP@8'] (fif: IMAGE_FORMAT; bpp: INTEGER): BOOLEAN;
PROCEDURE FIFSupportsExportType*['_FreeImage_FIFSupportsExportType@8'] (fif: IMAGE_FORMAT; image_type: IMAGE_TYPE): BOOLEAN;
PROCEDURE FIFSupportsICCProfiles*['_FreeImage_FIFSupportsICCProfiles@4'] (fif: IMAGE_FORMAT): BOOLEAN;

(* Multipaging interface *)
PROCEDURE OpenMultiBitmap*['_FreeImage_OpenMultiBitmap@24'] (fif: IMAGE_FORMAT; filename: PChar; create_new, read_only, keep_cache_in_memory: BOOLEAN): FIMULTIBITMAP;
PROCEDURE CloseMultiBitmap*['_FreeImage_CloseMultiBitmap@8'] (bitmap: FIMULTIBITMAP; flags: INTEGER): BOOLEAN;
PROCEDURE GetPageCount*['_FreeImage_GetPageCount@4'] (bitmap: FIMULTIBITMAP): INTEGER;
PROCEDURE AppendPage*['_FreeImage_AppendPage@8'] (bitmap: FIMULTIBITMAP; data: FIBITMAP);
PROCEDURE InsertPage*['_FreeImage_InsertPage@12'] (bitmap: FIMULTIBITMAP; page: INTEGER; data: FIBITMAP);
PROCEDURE DeletePage*['_FreeImage_DeletePage@8'] (bitmap: FIMULTIBITMAP; page: INTEGER);
PROCEDURE LockPage*['_FreeImage_LockPage@8'] (bitmap: FIMULTIBITMAP; page: INTEGER): FIBITMAP;
PROCEDURE UnlockPage*['_FreeImage_UnlockPage@12'] (bitmap: FIMULTIBITMAP; page: FIBITMAP; changed: BOOLEAN);
PROCEDURE MovePage*['_FreeImage_MovePage@12'] (bitmap: FIMULTIBITMAP; target, source: INTEGER): BOOLEAN;
PROCEDURE GetLockedPageNumbers*['_FreeImage_GetLockedPageNumbers@12'] (bitmap: FIMULTIBITMAP; VAR pages: INTEGER; VAR count: INTEGER): BOOLEAN;


	(*Filetype request routines *)
PROCEDURE GetFileType*["_FreeImage_GetFileType@8"] (filename: PChar; size: INTEGER): IMAGE_FORMAT;
PROCEDURE GetFileTypeU*["_FreeImage_GetFileTypeU@8"] (filename: PCharU; size: INTEGER): IMAGE_FORMAT;
(***
PROCEDURE GetFileTypeFromHandle* ["_FreeImage_GetFileTypeFromHandle@12"] (FreeImageIO *io, fi_handle handle, int size FI_DEFAULT(0)):  IMAGE_FORMAT;
*)
PROCEDURE GetFileTypeFromMemory* ["_FreeImage_GetFileTypeFromMemory@8"] (stream: FIMEMORY; size: INTEGER ): IMAGE_FORMAT;

	(* ImageType request routine*)
PROCEDURE GetImageType*["_FreeImage_GetImageType@4"] (dib: FIBITMAP): IMAGE_TYPE;


	(* FreeImage helper routines *)
PROCEDURE IsLittleEndian*['_FreeImage_IsLittleEndian@0'] (): BOOLEAN;
PROCEDURE LookupX11Color*['_FreeImage_LookupX11Color@16'] (szColor: PChar; VAR nRed, nGreen, nBlue: INTEGER): BOOLEAN;
PROCEDURE LookupSVGColor*['_FreeImage_LookupSVGColor@16'] (szColor: PChar; VAR nRed, nGreen, nBlue: INTEGER): BOOLEAN;

	(* Pixels access routines *)
PROCEDURE GetBits*["_FreeImage_GetBits@4"] (dib: FIBITMAP): INTEGER;
PROCEDURE GetScanLine*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PChar;
PROCEDURE GetPixelColor*["_FreeImage_GetPixelColor@16"] (dib: FIBITMAP; x, y: INTEGER; VAR[nil] value: RGBQUAD);
PROCEDURE SetPixelColor*["_FreeImage_SetPixelColor@16"] (dib: FIBITMAP; x, y: INTEGER; VAR[nil]value: RGBQUAD);
PROCEDURE GetPixelIndex*["_FreeImage_GetPixelIndex@16"] (dib: FIBITMAP; x, y: INTEGER; VAR value: BYTE): BOOLEAN;
PROCEDURE SetPixelIndex*["_FreeImage_SetPixelIndex@16"] (dib: FIBITMAP; x, y: INTEGER; VAR value: BYTE): BOOLEAN;
(*********************************)
    (* utilitiy procedures for different type of image line pixels, necati *)
PROCEDURE GetScanLineRGB*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrLineRGB;
PROCEDURE GetScanLineRGBA*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrLineRGBA;
PROCEDURE GetScanLine16*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER):PtrFISINT16;
PROCEDURE GetScanLine32*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIINT32;

PROCEDURE GetScanLineFloat*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIFLOAT;
PROCEDURE GetScanLineDouble*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIDOUBLE;
PROCEDURE GetScanLineComplex*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER):PtrFICOMPLEX;
PROCEDURE GetScanLineFRGB16*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIRGB16;
PROCEDURE GetScanLineFRGBA16*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIRGBA16;
PROCEDURE GetScanLineFRGBF*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIRGBF;
PROCEDURE GetScanLineFRGBAF*["_FreeImage_GetScanLine@8"] (dib: FIBITMAP; scanline: INTEGER): PtrFIRGBAF;
 (*********************************)

	(* DIB info routines *)
PROCEDURE GetColorsUsed*["_FreeImage_GetColorsUsed@4"] (dib: FIBITMAP): INTEGER;
PROCEDURE GetBPP*["_FreeImage_GetBPP@4"] (dib: FIBITMAP): INTEGER;
PROCEDURE GetWidth*["_FreeImage_GetWidth@4"] (dib: FIBITMAP): INTEGER;
PROCEDURE GetHeight*["_FreeImage_GetHeight@4"] (dib: FIBITMAP): INTEGER;
PROCEDURE GetLine*['_FreeImage_GetLine@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetPitch*['_FreeImage_GetPitch@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetDIBSize*['_FreeImage_GetDIBSize@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetPalette*['_FreeImage_GetPalette@4'] (dib: FIBITMAP): PtrPALETTE;

PROCEDURE GetDotsPerMeterX*['_FreeImage_GetDotsPerMeterX@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetDotsPerMeterY*['_FreeImage_GetDotsPerMeterY@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE SetDotsPerMeterX*['_FreeImage_SetDotsPerMeterX@8'] (dib: FIBITMAP; res: INTEGER);
PROCEDURE SetDotsPerMeterY*['_FreeImage_SetDotsPerMeterY@8'] (dib: FIBITMAP; res: INTEGER);
PROCEDURE GetColorType*['_FreeImage_GetColorType@4'] (dib: INTEGER): INTEGER;
PROCEDURE GetRedMask*['_FreeImage_GetRedMask@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetGreenMask*['_FreeImage_GetGreenMask@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetBlueMask*['_FreeImage_GetBlueMask@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetTransparencyCount*['_FreeImage_GetTransparencyCount@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE GetTransparencyTable*['_FreeImage_GetTransparencyTable@4'] (dib: FIBITMAP): INTEGER;
PROCEDURE SetTransparent*['_FreeImage_SetTransparent@8'] (dib: FIBITMAP; enabled: BOOLEAN);
PROCEDURE SetTransparencyTable*['_FreeImage_SetTransparencyTable@12'] (dib: FIBITMAP; table: INTEGER; count: INTEGER);
PROCEDURE IsTransparent*['_FreeImage_IsTransparent@4'] (dib: FIBITMAP): BOOLEAN;
PROCEDURE SetTransparentIndex*["_FreeImage_SetTransparentIndex@8"](dib:FIBITMAP; index: INTEGER);
PROCEDURE GetTransparentIndex*["_FreeImage_GetTransparentIndex@4"](dib:FIBITMAP): INTEGER;
PROCEDURE HasBackgroundColor*['_FreeImage_HasBackgroundColor@4'] (dib: FIBITMAP): BOOLEAN;
PROCEDURE GetBackgroundColor*['_FreeImage_GetBackgroundColor@8'] (dib: FIBITMAP; VAR[nil] bkcolor: RGBQUAD): BOOLEAN;
PROCEDURE SetBackgroundColor*['_FreeImage_SetBackgroundColor@8'] (dib: FIBITMAP; VAR[nil] bkcolor: RGBQUAD): BOOLEAN;

	(* ICC profile routines *)

PROCEDURE GetICCProfile*["FreeImage_GetICCProfile@4"] (dib: FIBITMAP): FIICCPROFILE;
PROCEDURE CreateICCProfile*["FreeImage_CreateICCProfile@12"] (dib: FIBITMAP; data: INTEGER; size: INTEGER): FIICCPROFILE;
PROCEDURE DestroyICCProfile*["FreeImage_DestroyICCProfile@4"] (dib: FIBITMAP);

	(* Line Conversion Routines : not translated yet *)

	(*  Smart conversion routines *)
PROCEDURE ConvertTo4Bits*['_FreeImage_ConvertTo4Bits@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertTo8Bits*['_FreeImage_ConvertTo8Bits@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertToGreyscale*['_FreeImage_ConvertToGreyscale@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertTo16Bits555*['_FreeImage_ConvertTo16Bits555@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertTo16Bits565*['_FreeImage_ConvertTo16Bits565@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertTo24Bits*['_FreeImage_ConvertTo24Bits@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertTo32Bits*['_FreeImage_ConvertTo32Bits@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ColorQuantize*['_FreeImage_ColorQuantize@8'] (dib: FIBITMAP; quantize: IMAGE_QUANTIZE): FIBITMAP;
PROCEDURE ColorQuantizeEx*["_FreeImage_ColorQuantizeEx@20"] (dib: FIBITMAP; quantize: IMAGE_QUANTIZE; PaletteSize: INTEGER; ReserveSize: INTEGER; ReservePalette: RGBQUAD): FIBITMAP;
PROCEDURE Threshold*['_FreeImage_Threshold@8'] (dib: FIBITMAP; T: BYTE): FIBITMAP;
PROCEDURE Dither*['_FreeImage_Dither@8'] (dib: FIBITMAP; algorithm: IMAGE_DITHER): FIBITMAP;
PROCEDURE ConvertFromRawBits*['_FreeImage_ConvertFromRawBits@36'] (bits: INTEGER; width, height, pitch: INTEGER; bpp, red_mask, green_mask, blue_mask: INTEGER; topdown: BOOLEAN): FIBITMAP;
PROCEDURE ConvertToRawBits*['_FreeImage_ConvertToRawBits@32'] (bits: INTEGER; dib: FIBITMAP; pitch: INTEGER; bpp, red_mask, green_mask, blue_mask: INTEGER; topdown: BOOLEAN);
PROCEDURE ConvertToRGBF*['_FreeImage_ConvertToRGBF@4'] (dib: FIBITMAP): FIBITMAP;
PROCEDURE ConvertToStandardType*['_FreeImage_ConvertToStandardType@8'] (src: FIBITMAP; scale_linear: BOOLEAN): FIBITMAP;
PROCEDURE ConvertToType*['_FreeImage_ConvertToType@12'] (src: FIBITMAP; dst_type: IMAGE_TYPE; scale_linear: BOOLEAN): FIBITMAP;

(* tone mapping operators *)
PROCEDURE ToneMapping*["_FreeImage_ToneMapping@24"] (dib: FIBITMAP; tmo: IMAGE_TMO; first_param, second_param: REAL): FIBITMAP;
PROCEDURE TmoDrago03*["_FreeImage_TmoDrago03@20"] (src: FIBITMAP; gamma, exposure: REAL): FIBITMAP;

PROCEDURE TmoReinhard05*["_FreeImage_TmoReinhard05@20"] (src: FIBITMAP; intensity, contrast: REAL): FIBITMAP;


(************************************)
(*******    ZLib interface   ********)
(************************************)
(** NOT TESTED *)
PROCEDURE ZLibCompress*["_FreeImage_ZLibCompress@16"] (target: INTEGER; target_size: INTEGER; source: INTEGER; source_size: INTEGER): INTEGER;
PROCEDURE ZLibUncompress*["_FreeImage_ZLibUncompress@16"] (target: INTEGER; target_size: INTEGER; source: INTEGER; source_size: INTEGER): INTEGER;
PROCEDURE ZLibGZip*["_FreeImage_ZLibGZip@16"] (target: INTEGER; target_size: INTEGER; source: INTEGER; source_size: INTEGER): INTEGER;
PROCEDURE ZLibGUnzip*["_FreeImage_ZLibGUnzip@16"] (target: INTEGER; target_size: INTEGER; source: INTEGER; source_size: INTEGER): INTEGER;
PROCEDURE ZLibCRC32*["_FreeImage_ZLibCRC32@12"] (crc: INTEGER; source: INTEGER; source_size: INTEGER): INTEGER;

(***************************************************)
(*************   Metadata routines           *******)
(***************************************************)
(* tag creation / destruction*)
PROCEDURE CreateTag*['_FreeImage_CreateTag@0'] (): FITAG;
PROCEDURE DeleteTag*['_FreeImage_DeleteTag@4'] (tag: FITAG);
PROCEDURE CloneTag*['_FreeImage_CloneTag@4'] (tag: FITAG): FITAG;

 (* tag getters and setters *)
PROCEDURE GetTagKey*['_FreeImage_GetTagKey@4'] (tag: FITAG): PChar;
PROCEDURE GetTagDescription*['_FreeImage_GetTagDescription@4'] (tag: FITAG): PChar;
PROCEDURE GetTagID*['_FreeImage_GetTagID@4'] (tag: FITAG): INTEGER;
PROCEDURE GetTagType*['_FreeImage_GetTagType@4'] (tag: FITAG): IMAGE_MDTYPE;
PROCEDURE GetTagCount*['_FreeImage_GetTagCount@4'] (tag: FITAG): INTEGER;
PROCEDURE GetTagLength*['_FreeImage_GetTagLength@4'] (tag: FITAG): INTEGER;
PROCEDURE GetTagValue*['_FreeImage_GetTagValue@4'] (tag: FITAG): PChar;
PROCEDURE SetTagKey*['_FreeImage_SetTagKey@8'] (tag: FITAG; key: PChar): BOOLEAN;
PROCEDURE SetTagDescription*['_FreeImage_SetTagDescription@8'] (tag: FITAG; description: PChar): BOOLEAN;
PROCEDURE SetTagID*['_FreeImage_SetTagID@8'] (tag: FITAG; id: INTEGER): BOOLEAN;
PROCEDURE SetTagType*['_FreeImage_SetTagType@8'] (tag: FITAG; atype: IMAGE_MDTYPE): BOOLEAN;
PROCEDURE SetTagCount*['_FreeImage_SetTagCount@8'] (tag: FITAG; count: INTEGER): BOOLEAN;
PROCEDURE SetTagLength*['_FreeImage_SetTagLength@8'] (tag: FITAG; length: INTEGER): BOOLEAN;
PROCEDURE SetTagValue*['_FreeImage_SetTagValue@8'] (tag: FITAG; value: INTEGER): BOOLEAN;

(* iterator*)
PROCEDURE FindFirstMetadata*['_FreeImage_FindFirstMetadata@12'] (model: IMAGE_MDMODEL; dib: FIBITMAP; VAR tag: FITAG): FIMETADATA;
PROCEDURE FindNextMetadata*['_FreeImage_FindNextMetadata@8'] (mdhandle: FIMETADATA; VAR tag: FITAG): BOOLEAN;
PROCEDURE FindCloseMetadata*['_FreeImage_FindCloseMetadata@4'] (mdhandle: FIMETADATA);

 (* metadata setter and getter *)
PROCEDURE SetMetadata*['_FreeImage_SetMetadata@16'] (model: IMAGE_MDMODEL; dib: FIBITMAP; key: PChar; tag: FITAG): BOOLEAN;
PROCEDURE GetMetadata*['_FreeImage_GetMetadata@16'] (model: IMAGE_MDMODEL; dib: FIBITMAP; key: PChar; VAR tag: FITAG): BOOLEAN;

(* helpers *)
PROCEDURE GetMetadataCount*['_FreeImage_GetMetadataCount@8'] (model: IMAGE_MDMODEL; dib: FIBITMAP): INTEGER;
PROCEDURE CloneMetadata* ['_FreeImage_CloneMetadata@8'] ( dst, src: FIBITMAP): BOOLEAN;
 (* tag to C string conversion *)
PROCEDURE TagToString*['_FreeImage_TagToString@12'] (model: IMAGE_MDMODEL; tag: FITAG; Make: PChar): PChar;

(*********************************************************************)
(******  Image manipulation toolkit    *****)
(**********************************************)
	(* rotation and flipping *)
PROCEDURE RotateClassic*["_FreeImage_RotateClassic@12"] (dib: FIBITMAP; angle: REAL): FIBITMAP;
PROCEDURE RotateEx*["_FreeImage_RotateEx@48"] (dib: FIBITMAP; angle: REAL; x_shift, y_shift, x_origin, y_origin: REAL; use_mask: BOOLEAN): FIBITMAP;
PROCEDURE FlipHorizontal*["_FreeImage_FlipHorizontal@4"] (dib: FIBITMAP): BOOLEAN;
PROCEDURE FlipVertical*["_FreeImage_FlipVertical@4"] (dib: FIBITMAP): BOOLEAN;
PROCEDURE JPEGTransform*["_FreeImage_JPEGTransform@16"] (src_file: PChar; dst_file: PChar; operation: IMAGE_JPEG_OPERATION; perfect: BOOLEAN): BOOLEAN;

	(* upsampling / downsampling *)
PROCEDURE Rescale*["_FreeImage_Rescale@16"] (dib: FIBITMAP; dst_width, dst_height: INTEGER; filter: IMAGE_FILTER): FIBITMAP;
 PROCEDURE MakeThumbnail*["_FreeImage_MakeThumbnail@12"] (dib: FIBITMAP; max_pixel_size: INTEGER; convert: BOOLEAN): FIBITMAP;

	(* color manipulation routines (point operations)*)
PROCEDURE AdjustCurve*["_FreeImage_AdjustCurve@12"] (dib: FIBITMAP; VAR[nil] LUT: ARRAY OF BYTE; channel: IMAGE_COLOR_CHANNEL): BOOLEAN;
PROCEDURE AdjustGamma*["_FreeImage_AdjustGamma@12"] (dib: FIBITMAP; gamma: REAL): BOOLEAN;
PROCEDURE AdjustBrightness*["_FreeImage_AdjustBrightness@12"] (dib: FIBITMAP; percentage: REAL): BOOLEAN;
PROCEDURE AdjustContrast*["_FreeImage_AdjustContrast@12"] (dib: FIBITMAP; percentage: REAL): BOOLEAN;
PROCEDURE Invert*["_FreeImage_Invert@4"] (dib: FIBITMAP): BOOLEAN;
PROCEDURE GetHistogram*["_FreeImage_GetHistogram@12"] (dib: FIBITMAP; VAR[nil] histo: ARRAY[untagged] OF INTEGER; channel: IMAGE_COLOR_CHANNEL): BOOLEAN;
PROCEDURE GetAdjustColorsLookupTable* ["_FreeImage_GetAdjustColorsLookupTable@32"](VAR [nil] LUT: Palette; brightness,  contrast,  gamma: REAL; invert: BOOLEAN): INTEGER;
PROCEDURE AdjustColors*["_FreeImage_AdjustColors@32"](dib: FIBITMAP; brightness, contrast, gamma: REAL; invert: BOOLEAN);
PROCEDURE ApplyColorMapping*["_FreeImage_ApplyColorMapping@24"](dib: FIBITMAP; VAR [nil] srccolors, dstcolors: Palette; count: INTEGER;  ignore_alpha, swap: INTEGER): INTEGER;
PROCEDURE SwapColors*["_FreeImage_SwapColors@16"](dib: FIBITMAP ;VAR [nil] color_a, color_b: Palette ; ignore_alpha: BOOLEAN): INTEGER;
PROCEDURE ApplyPaletteIndexMapping*["_FreeImage_ApplyPaletteIndexMapping@20"](dib: FIBITMAP; VAR [nil] srcindices,	dstindices: LUT; count: INTEGER;swap: BOOLEAN): INTEGER;
PROCEDURE SwapPaletteIndices*["_FreeImage_SwapPaletteIndices@12"](dib: FIBITMAP; VAR [nil] index_a, index_b: LUT): INTEGER;


	(* channel processing routines*)
PROCEDURE GetChannel*["_FreeImage_GetChannel@8"] (dib: FIBITMAP; channel: IMAGE_COLOR_CHANNEL): FIBITMAP;
PROCEDURE SetChannel*["_FreeImage_SetChannel@12"] (dib: FIBITMAP; dib8: FIBITMAP; channel: IMAGE_COLOR_CHANNEL): BOOLEAN;
PROCEDURE GetComplexChannel*['_FreeImage_GetComplexChannel@8'] (dib: FIBITMAP; channel: IMAGE_COLOR_CHANNEL): FIBITMAP;
PROCEDURE SetComplexChannel*['_FreeImage_SetComplexChannel@12'] (dst: FIBITMAP; src: FIBITMAP; channel: IMAGE_COLOR_CHANNEL): BOOLEAN;

	(* copy / paste / composite routines*)
PROCEDURE Copy*["_FreeImage_Copy@20"] (dib: FIBITMAP; left, top, right, bottom: INTEGER): FIBITMAP;
PROCEDURE Paste*["_FreeImage_Paste@20"] (dst: FIBITMAP; src: FIBITMAP; left, top, alpha: INTEGER): BOOLEAN;
PROCEDURE Composite*["_FreeImage_Composite@16"] (fg: FIBITMAP; useFileBkg: BOOLEAN; appBkColor: RGBQUAD; bg: FIBITMAP): FIBITMAP;
PROCEDURE JPEGCrop*["_FreeImage_JPEGCrop@24"] (src_file, dst_file: PChar; left, top, right, bottom: INTEGER): BOOLEAN;
PROCEDURE PreMultiplyWithAlpha*["_FreeImage_PreMultiplyWithAlpha@4"](dib: FIBITMAP): BOOLEAN;

(* miscellaneous algorithms*)
PROCEDURE MultigridPoissonSolver* ["_FreeImage_MultigridPoissonSolver@8"](Laplacian: FIBITMAP; ncycle:INTEGER): FIBITMAP;
BEGIN

END LibsFreeImage.
