MODULE LibsAtk ["libatk-1.0-0.dll"];
(*MODULE LibsAtk ["libatk-1.0.so"];*)

IMPORT
	SYSTEM,
	lg := LibsGlib,
	lo := LibsGObject;

(* ==================================================================== ENUMS *)
TYPE
	AtkCoordType* = lg.gint32;
CONST
	ATK_XY_SCREEN* = 0;
	ATK_XY_WINDOW* = 1;

TYPE
	AtkHyperlinkStateFlags* = SET;
CONST
	ATK_HYPERLINK_IS_INLINE* = {0};

TYPE
	AtkKeyEventType* = lg.gint32;
CONST
	ATK_KEY_EVENT_PRESS*= 0;
	ATK_KEY_EVENT_RELEASE*= 1;
	ATK_KEY_EVENT_LAST_DEFINED* = 2;

TYPE
	AtkLayer* = lg.gint32;
CONST
	ATK_LAYER_INVALID*= 0;
	ATK_LAYER_BACKGROUND* = 1;
	ATK_LAYER_CANVAS* = 2;
	ATK_LAYER_WIDGET* = 3;
	ATK_LAYER_MDI*= 4;
	ATK_LAYER_POPUP*= 5;
	ATK_LAYER_OVERLAY*= 6;
	ATK_LAYER_WINDOW* = 7;

TYPE
	AtkRelationType* = lg.gint32;
CONST
	ATK_RELATION_NULL* = 0;
	ATK_RELATION_CONTROLLED_BY*= 1;
	ATK_RELATION_CONTROLLER_FOR* = 2;
	ATK_RELATION_LABEL_FOR*= 3;
	ATK_RELATION_LABELLED_BY*= 4;
	ATK_RELATION_MEMBER_OF*= 5;
	ATK_RELATION_NODE_CHILD_OF*= 6;
	ATK_RELATION_FLOWS_TO* = 7;
	ATK_RELATION_FLOWS_FROM* = 8;
	ATK_RELATION_SUBWINDOW_OF* = 9;
	ATK_RELATION_EMBEDS* = 10;
	ATK_RELATION_EMBEDDED_BY*= 11;
	ATK_RELATION_POPUP_FOR*= 12;
	ATK_RELATION_PARENT_WINDOW_OF* = 13;
	ATK_RELATION_LAST_DEFINED* = 14;

TYPE
	AtkRole* = lg.gint32;

CONST
	ATK_ROLE_INVALID* = 0;
	ATK_ROLE_ACCEL_LABEL* = 1;
	ATK_ROLE_ALERT* = 2;
	ATK_ROLE_ANIMATION* = 3;
	ATK_ROLE_ARROW* = 4;
	ATK_ROLE_CALENDAR*= 5;
	ATK_ROLE_CANVAS* = 6;
	ATK_ROLE_CHECK_BOX* = 7;
	ATK_ROLE_CHECK_MENU_ITEM* = 8;
	ATK_ROLE_COLOR_CHOOSER* = 9;
	ATK_ROLE_COLUMN_HEADER* = 10;
	ATK_ROLE_COMBO_BOX* = 11;
	ATK_ROLE_DATE_EDITOR* = 12;
	ATK_ROLE_DESKTOP_ICON*= 13;
	ATK_ROLE_DESKTOP_FRAME* = 14;
	ATK_ROLE_DIAL*= 15;
	ATK_ROLE_DIALOG* = 16;
	ATK_ROLE_DIRECTORY_PANE*= 17;
	ATK_ROLE_DRAWING_AREA*= 18;
	ATK_ROLE_FILE_CHOOSER*= 19;
	ATK_ROLE_FILLER* = 20;
	ATK_ROLE_FONT_CHOOSER*= 21;
	ATK_ROLE_FRAME* = 22;
	ATK_ROLE_GLASS_PANE*= 23;
	ATK_ROLE_HTML_CONTAINER*= 24;
	ATK_ROLE_ICON*= 25;
	ATK_ROLE_IMAGE* = 26;
	ATK_ROLE_INTERNAL_FRAME*= 27;
	ATK_ROLE_LABEL* = 28;
	ATK_ROLE_LAYERED_PANE*= 29;
	ATK_ROLE_LIST*= 30;
	ATK_ROLE_LIST_ITEM* = 31;
	ATK_ROLE_MENU*= 32;
	ATK_ROLE_MENU_BAR*= 33;
	ATK_ROLE_MENU_ITEM* = 34;
	ATK_ROLE_OPTION_PANE* = 35;
	ATK_ROLE_PAGE_TAB*= 36;
	ATK_ROLE_PAGE_TAB_LIST* = 37;
	ATK_ROLE_PANEL* = 38;
	ATK_ROLE_PASSWORD_TEXT* = 39;
	ATK_ROLE_POPUP_MENU*= 40;
	ATK_ROLE_PROGRESS_BAR*= 41;
	ATK_ROLE_PUSH_BUTTON* = 42;
	ATK_ROLE_RADIO_BUTTON*= 43;
	ATK_ROLE_RADIO_MENU_ITEM* = 44;
	ATK_ROLE_ROOT_PANE* = 45;
	ATK_ROLE_ROW_HEADER*= 46;
	ATK_ROLE_SCROLL_BAR*= 47;
	ATK_ROLE_SCROLL_PANE* = 48;
	ATK_ROLE_SEPARATOR* = 49;
	ATK_ROLE_SLIDER* = 50;
	ATK_ROLE_SPLIT_PANE*= 51;
	ATK_ROLE_SPIN_BUTTON* = 52;
	ATK_ROLE_STATUSBAR* = 53;
	ATK_ROLE_TABLE* = 54;
	ATK_ROLE_TABLE_CELL*= 55;
	ATK_ROLE_TABLE_COLUMN_HEADER* = 56;
	ATK_ROLE_TABLE_ROW_HEADER*= 57;
	ATK_ROLE_TEAR_OFF_MENU_ITEM* = 58;
	ATK_ROLE_TERMINAL*= 59;
	ATK_ROLE_TEXT*= 60;
	ATK_ROLE_TOGGLE_BUTTON* = 61;
	ATK_ROLE_TOOL_BAR*= 62;
	ATK_ROLE_TOOL_TIP*= 63;
	ATK_ROLE_TREE*= 64;
	ATK_ROLE_TREE_TABLE*= 65;
	ATK_ROLE_UNKNOWN* = 66;
	ATK_ROLE_VIEWPORT*= 67;
	ATK_ROLE_WINDOW* = 68;
	ATK_ROLE_HEADER* = 69;
	ATK_ROLE_FOOTER* = 70;
	ATK_ROLE_PARAGRAPH* = 71;
	ATK_ROLE_RULER* = 72;
	ATK_ROLE_APPLICATION* = 73;
	ATK_ROLE_AUTOCOMPLETE*= 74;
	ATK_ROLE_EDITBAR* = 75;
	ATK_ROLE_EMBEDDED*= 76;
	ATK_ROLE_LAST_DEFINED*= 77;

TYPE
	AtkStateType* = lg.gint32;

CONST
	ATK_STATE_INVALID* = 0;
	ATK_STATE_ACTIVE* = 1;
	ATK_STATE_ARMED* = 2;
	ATK_STATE_BUSY*= 3;
	ATK_STATE_CHECKED* = 4;
	ATK_STATE_DEFUNCT* = 5;
	ATK_STATE_EDITABLE*= 6;
	ATK_STATE_ENABLED* = 7;
	ATK_STATE_EXPANDABLE*= 8;
	ATK_STATE_EXPANDED*= 9;
	ATK_STATE_FOCUSABLE* = 10;
	ATK_STATE_FOCUSED* = 11;
	ATK_STATE_HORIZONTAL*= 12;
	ATK_STATE_ICONIFIED* = 13;
	ATK_STATE_MODAL* = 14;
	ATK_STATE_MULTI_LINE*= 15;
	ATK_STATE_MULTISELECTABLE* = 16;
	ATK_STATE_OPAQUE* = 17;
	ATK_STATE_PRESSED* = 18;
	ATK_STATE_RESIZABLE* = 19;
	ATK_STATE_SELECTABLE*= 20;
	ATK_STATE_SELECTED*= 21;
	ATK_STATE_SENSITIVE* = 22;
	ATK_STATE_SHOWING* = 23;
	ATK_STATE_SINGLE_LINE* = 24;
	ATK_STATE_STALE* = 25;
	ATK_STATE_TRANSIENT* = 26;
	ATK_STATE_VERTICAL*= 27;
	ATK_STATE_VISIBLE* = 28;
	ATK_STATE_MANAGES_DESCENDANTS* = 29;
	ATK_STATE_INDETERMINATE* = 30;
	ATK_STATE_TRUNCATED* = 31;
	ATK_STATE_REQUIRED*= 32;
	ATK_STATE_LAST_DEFINED*= 33;

TYPE
	AtkTextAttribute* = lg.gint32;

CONST
	ATK_TEXT_ATTR_INVALID*= 0;
	ATK_TEXT_ATTR_LEFT_MARGIN*= 1;
	ATK_TEXT_ATTR_RIGHT_MARGIN* = 2;
	ATK_TEXT_ATTR_INDENT* = 3;
	ATK_TEXT_ATTR_INVISIBLE*= 4;
	ATK_TEXT_ATTR_EDITABLE* = 5;
	ATK_TEXT_ATTR_PIXELS_ABOVE_LINES* = 6;
	ATK_TEXT_ATTR_PIXELS_BELOW_LINES* = 7;
	ATK_TEXT_ATTR_PIXELS_INSIDE_WRAP* = 8;
	ATK_TEXT_ATTR_BG_FULL_HEIGHT* = 9;
	ATK_TEXT_ATTR_RISE* = 10;
	ATK_TEXT_ATTR_UNDERLINE*= 11;
	ATK_TEXT_ATTR_STRIKETHROUGH*= 12;
	ATK_TEXT_ATTR_SIZE* = 13;
	ATK_TEXT_ATTR_SCALE* = 14;
	ATK_TEXT_ATTR_WEIGHT* = 15;
	ATK_TEXT_ATTR_LANGUAGE* = 16;
	ATK_TEXT_ATTR_FAMILY_NAME*= 17;
	ATK_TEXT_ATTR_BG_COLOR* = 18;
	ATK_TEXT_ATTR_FG_COLOR* = 19;
	ATK_TEXT_ATTR_BG_STIPPLE* = 20;
	ATK_TEXT_ATTR_FG_STIPPLE* = 21;
	ATK_TEXT_ATTR_WRAP_MODE*= 22;
	ATK_TEXT_ATTR_DIRECTION*= 23;
	ATK_TEXT_ATTR_JUSTIFICATION*= 24;
	ATK_TEXT_ATTR_STRETCH*= 25;
	ATK_TEXT_ATTR_VARIANT*= 26;
	ATK_TEXT_ATTR_STYLE* = 27;
	ATK_TEXT_ATTR_LAST_DEFINED* = 28;

TYPE
	AtkTextBoundary* = lg.gint32;

CONST
	ATK_TEXT_BOUNDARY_CHAR* = 0;
	ATK_TEXT_BOUNDARY_WORD_START* = 1;
	ATK_TEXT_BOUNDARY_WORD_END* = 2;
	ATK_TEXT_BOUNDARY_SENTENCE_START* = 3;
	ATK_TEXT_BOUNDARY_SENTENCE_END* = 4;
	ATK_TEXT_BOUNDARY_LINE_START* = 5;
	ATK_TEXT_BOUNDARY_LINE_END* = 6;

TYPE
	AtkTextClipType* = lg.gint32;

CONST
	ATK_TEXT_CLIP_NONE* = 0;
	ATK_TEXT_CLIP_MIN* = 1;
	ATK_TEXT_CLIP_MAX* = 2;
	ATK_TEXT_CLIP_BOTH* = 3;

(* ====================================================== POINTERS TO OBJECTS *)
TYPE
	PAtkGObjectAccessible* = POINTER TO AtkGObjectAccessible;
	PAtkHyperlink* = POINTER TO AtkHyperlink;
	PAtkNoOpObject*= POINTER TO AtkNoOpObject;
	PAtkNoOpObjectFactory* = POINTER TO AtkNoOpObjectFactory;
	PAtkObject*= POINTER TO AtkObject;
	PAtkObjectFactory* = POINTER TO AtkObjectFactory;
	PAtkRegistry*= POINTER TO AtkRegistry;
	PAtkRelation*= POINTER TO AtkRelation;
	PAtkRelationSet* = POINTER TO AtkRelationSet;
	PAtkStateSet*= POINTER TO AtkStateSet;
	PAtkUtil* = POINTER TO AtkUtil;

(* ====================================================== POINTERS TO STRUCTS *)
TYPE
	PAtkAttribute*= POINTER TO AtkAttribute;
	PAtkKeyEventStruct* = POINTER TO AtkKeyEventStruct;
	PAtkPropertyValues* = POINTER TO AtkPropertyValues;
	PAtkTextRange*= POINTER TO AtkTextRange;
	PAtkTextRectangle* = POINTER TO AtkTextRectangle;

(* ====================================================== POINTERS TO ALIASES *)
TYPE
	PAtkAttributeSet* = POINTER TO AtkAttributeSet;

(* ======================================================= POINTERS TO BOXEDS *)
TYPE
	PAtkRectangle* = POINTER TO AtkRectangle;

(* =================================================== POINTERS TO INTERFACES *)
TYPE
	PAtkAction*= POINTER TO AtkAction;
	PAtkComponent* = POINTER TO AtkComponent;
	PAtkDocument*= POINTER TO AtkDocument;
	PAtkEditableText*= POINTER TO AtkEditableText;
	PAtkHypertext* = POINTER TO AtkHypertext;
	PAtkImage* = POINTER TO AtkImage;
	PAtkImplementor* = POINTER TO AtkImplementor;
	PAtkSelection* = POINTER TO AtkSelection;
	PAtkStreamableContent* = POINTER TO AtkStreamableContent;
	PAtkTable* = POINTER TO AtkTable;
	PAtkText* = POINTER TO AtkText;
	PAtkValue* = POINTER TO AtkValue;

(* ================================================================== ALIASES *)
TYPE
	AtkAttributeSet* = lg.GSList;
	AtkState*= lg.guint64;

(* ================================================================ CALLBACKS *)
TYPE
	Callback* = lo.GCallback;

	AtkEventListener* = RECORD (Callback)
		callback*: PROCEDURE [ccall] (arg1: PAtkObject);
	END;

	AtkEventListenerInit* = RECORD (Callback)
		callback*: PROCEDURE [ccall] ();
	END;

	AtkFocusHandler*= RECORD (Callback)
		callback*: PROCEDURE [ccall]
		(
		arg1: PAtkObject;
		arg2: lg.gboolean
		);
	END;

	AtkFunction* = RECORD (Callback)
		callback*: PROCEDURE [ccall]
		(
		data: lg.gpointer
		): lg.gboolean;
	END;

	AtkKeySnoopFunc*= RECORD (Callback)
		callback*: PROCEDURE [ccall]
		(
		event: PAtkKeyEventStruct;
		func_data: lg.gpointer
		): lg.gint;
	END;

	AtkPropertyChangeHandler* = RECORD (Callback)
		callback*: PROCEDURE [ccall]
		(
		arg1: PAtkObject;
		arg2: PAtkPropertyValues
		);
	END;

(* ================================================================== STRUCTS *)
TYPE

(* AtkAttribute struct *)

	AtkAttribute* = RECORD [noalign]
		name* : lg.Pgchar;
		value*: lg.Pgchar;
	END;

(* AtkKeyEventStruct struct *)

	AtkKeyEventStruct* = RECORD [noalign]
		type* : lg.gint;
		state*: lg.guint;
		keyval* : lg.guint;
		length* : lg.gint;
		string* : lg.Pgchar;
		keycode*	: lg.guint16;
		timestamp*: lg.guint32;
	END;

(* AtkPropertyValues struct *)

	AtkPropertyValues* = RECORD [noalign]
		property_name*: lg.Pgchar;
		old_value*: lo.GValue;
		new_value*: lo.GValue;
	END;

(* AtkTextRange struct *)

	AtkTextRange* = RECORD [noalign]
		bounds*: AtkTextRectangle;
		start_offset*: lg.gint;
		end_offset*	: lg.gint;
		content* : lg.Pgchar;
	END;

(* AtkTextRectangle struct *)

	AtkTextRectangle* = RECORD [noalign]
		x* : lg.gint;
		y* : lg.gint;
		width* : lg.gint;
		height*: lg.gint;
	END;

(* =================================================================== BOXEDS *)
TYPE

(* AtkRectangle boxed *)

	AtkRectangle* = RECORD [noalign]
		x* : lg.gint;
		y* : lg.gint;
		width* : lg.gint;
		height*: lg.gint;
	END;

(* ================================================================= OBJECTS *)
TYPE

(* AtkObject object *)

	AtkObject* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		description*: lg.Pgchar;
		name* : lg.Pgchar;
		accessible_parent*: PAtkObject;
		role* : AtkRole;
		relation_set* : PAtkRelationSet;
		layer*: AtkLayer;
	END;

(* AtkObjectFactory object *)

	AtkObjectFactory* = EXTENSIBLE RECORD [noalign] (lo.GObject) END;

(* AtkHyperlink object *)

	AtkHyperlink* = EXTENSIBLE RECORD [noalign] (lo.GObject) END;

(* AtkRegistry object *)

	AtkRegistry* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		factory_type_registry*	: lg.PGHashTable;
		factory_singleton_cache*: lg.PGHashTable;
	END;

(* AtkRelation object *)

	AtkRelation* = EXTENSIBLE RECORD [noalign] (lo.GObject)
target* : lg.PGPtrArray;
relationship* : AtkRelationType;
	END;

(* AtkRelationSet object *)

	AtkRelationSet* = EXTENSIBLE RECORD [noalign] (lo.GObject)
relations* : lg.PGPtrArray;
	END;

(* AtkStateSet object *)

	AtkStateSet* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* AtkUtil object *)

	AtkUtil* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* AtkGObjectAccessible object *)

	AtkGObjectAccessible* = EXTENSIBLE RECORD [noalign] (AtkObject)
	END;

(* AtkNoOpObject object *)

	AtkNoOpObject* = EXTENSIBLE RECORD [noalign] (AtkObject)
	END;

(* AtkNoOpObjectFactory object *)

	AtkNoOpObjectFactory* = EXTENSIBLE RECORD [noalign] (AtkObjectFactory)
	END;

(* ============================================================= INTERFACES *)
TYPE

(* AtkAction interface *)

	AtkAction* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkComponent interface *)

	AtkComponent* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkDocument interface *)

	AtkDocument* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkEditableText interface *)

	AtkEditableText* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkHypertext interface *)

	AtkHypertext* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkImage interface *)

	AtkImage* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkImplementor interface *)

	AtkImplementor* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkSelection interface *)

	AtkSelection* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkStreamableContent interface *)

	AtkStreamableContent* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkTable interface *)

	AtkTable* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkText interface *)

	AtkText* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkValue interface *)

	AtkValue* = EXTENSIBLE RECORD [noalign] 
	END;

(* AtkObject methods *)
	PROCEDURE [ccall] atk_object_add_relationship*(object: PAtkObject; relationship: AtkRelationType; target: PAtkObject): lg.gboolean;
	PROCEDURE [ccall] atk_object_connect_property_change_handler*(object: PAtkObject; VAR handler: AtkPropertyChangeHandler): lg.guint;
	PROCEDURE [ccall] atk_object_get_description*(object: PAtkObject): lg.Pgchar;
	PROCEDURE [ccall] atk_object_get_index_in_parent*(object: PAtkObject): lg.gint;
	PROCEDURE [ccall] atk_object_get_layer*(object: PAtkObject): AtkLayer;
	PROCEDURE [ccall] atk_object_get_mdi_zorder*(object: PAtkObject): lg.gint;
	PROCEDURE [ccall] atk_object_get_n_accessible_children*(object: PAtkObject): lg.gint;
	PROCEDURE [ccall] atk_object_get_name*(object: PAtkObject): lg.Pgchar;
	PROCEDURE [ccall] atk_object_get_parent*(object: PAtkObject): PAtkObject;
	PROCEDURE [ccall] atk_object_get_role*(object: PAtkObject): AtkRole;
	PROCEDURE [ccall] atk_object_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_object_initialize*(object: PAtkObject; data: lg.gpointer);
	PROCEDURE [ccall] atk_object_notify_state_change*(object: PAtkObject; state: AtkState; value: lg.gboolean);
	PROCEDURE [ccall] atk_object_ref_accessible_child*(object: PAtkObject; i: lg.gint): PAtkObject;
	PROCEDURE [ccall] atk_object_ref_relation_set*(object: PAtkObject): PAtkRelationSet;
	PROCEDURE [ccall] atk_object_ref_state_set*(object: PAtkObject): PAtkStateSet;
	PROCEDURE [ccall] atk_object_remove_property_change_handler*(object: PAtkObject; handler_id: lg.guint);
	PROCEDURE [ccall] atk_object_remove_relationship*(object: PAtkObject; relationship: AtkRelationType; target: PAtkObject): lg.gboolean;
	PROCEDURE [ccall] atk_object_set_description*(object: PAtkObject; description: lg.Pgchar);
	PROCEDURE [ccall] atk_object_set_name*(object: PAtkObject; name: lg.Pgchar);
	PROCEDURE [ccall] atk_object_set_parent*(object: PAtkObject; parent: PAtkObject);
	PROCEDURE [ccall] atk_object_set_role*(object: PAtkObject; role: AtkRole);

(* AtkObjectFactory methods *)
	PROCEDURE [ccall] atk_object_factory_create_accessible*(objectfactory: PAtkObjectFactory; obj: lo.PGObject): PAtkObject;
	PROCEDURE [ccall] atk_object_factory_get_accessible_type*(objectfactory: PAtkObjectFactory): lo.GType;
	PROCEDURE [ccall] atk_object_factory_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_object_factory_invalidate*(objectfactory: PAtkObjectFactory);

(* AtkHyperlink methods *)
	PROCEDURE [ccall] atk_hyperlink_get_end_index*(hyperlink: PAtkHyperlink): lg.gint;
	PROCEDURE [ccall] atk_hyperlink_get_n_anchors*(hyperlink: PAtkHyperlink): lg.gint;
	PROCEDURE [ccall] atk_hyperlink_get_object*(hyperlink: PAtkHyperlink; i: lg.gint): PAtkObject;
	PROCEDURE [ccall] atk_hyperlink_get_start_index*(hyperlink: PAtkHyperlink): lg.gint;
	PROCEDURE [ccall] atk_hyperlink_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_hyperlink_get_uri*(hyperlink: PAtkHyperlink; i: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_hyperlink_is_inline*(hyperlink: PAtkHyperlink): lg.gboolean;
	PROCEDURE [ccall] atk_hyperlink_is_selected_link*(hyperlink: PAtkHyperlink): lg.gboolean;
	PROCEDURE [ccall] atk_hyperlink_is_valid*(hyperlink: PAtkHyperlink): lg.gboolean;

(* AtkRegistry methods *)
	PROCEDURE [ccall] atk_registry_get_factory*(registry: PAtkRegistry; type: lo.GType): PAtkObjectFactory;
	PROCEDURE [ccall] atk_registry_get_factory_type*(registry: PAtkRegistry; type: lo.GType): lo.GType;
	PROCEDURE [ccall] atk_registry_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_registry_set_factory_type*(registry: PAtkRegistry; type: lo.GType; factory_type: lo.GType);

(* AtkRelation methods *)
	PROCEDURE [ccall] atk_relation_add_target*(relation: PAtkRelation; target: PAtkObject);
	PROCEDURE [ccall] atk_relation_get_relation_type*(relation: PAtkRelation): AtkRelationType;
	PROCEDURE [ccall] atk_relation_get_target*(relation: PAtkRelation): lg.PGPtrArray;
	PROCEDURE [ccall] atk_relation_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_relation_type_for_name*(name: lg.Pgchar): AtkRelationType;
	PROCEDURE [ccall] atk_relation_type_get_name*(type: AtkRelationType): lg.Pgchar;
	PROCEDURE [ccall] atk_relation_type_register*(name: lg.Pgchar): AtkRelationType;

(* AtkRelation constructors *)
	PROCEDURE [ccall] atk_relation_new*(targets: POINTER TO ARRAY [untagged] OF PAtkObject; n_targets: lg.gint; relationship: AtkRelationType): PAtkRelation;

(* AtkRelationSet methods *)
	PROCEDURE [ccall] atk_relation_set_add*(relationset: PAtkRelationSet; relation: PAtkRelation);
	PROCEDURE [ccall] atk_relation_set_add_relation_by_type*(relationset: PAtkRelationSet; relationship: AtkRelationType; target: PAtkObject);
	PROCEDURE [ccall] atk_relation_set_contains*(relationset: PAtkRelationSet; relationship: AtkRelationType): lg.gboolean;
	PROCEDURE [ccall] atk_relation_set_get_n_relations*(relationset: PAtkRelationSet): lg.gint;
	PROCEDURE [ccall] atk_relation_set_get_relation*(relationset: PAtkRelationSet; i: lg.gint): PAtkRelation;
	PROCEDURE [ccall] atk_relation_set_get_relation_by_type*(relationset: PAtkRelationSet; relationship: AtkRelationType): PAtkRelation;
	PROCEDURE [ccall] atk_relation_set_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_relation_set_remove*(relationset: PAtkRelationSet; relation: PAtkRelation);

(* AtkRelationSet constructors *)
	PROCEDURE [ccall] atk_relation_set_new*(): PAtkRelationSet;

(* AtkStateSet methods *)
	PROCEDURE [ccall] atk_state_set_add_state*(stateset: PAtkStateSet; type: AtkStateType): lg.gboolean;
	PROCEDURE [ccall] atk_state_set_add_states*(stateset: PAtkStateSet; types: POINTER TO ARRAY [untagged] OF AtkStateType; n_types: lg.gint);
	PROCEDURE [ccall] atk_state_set_and_sets*(stateset: PAtkStateSet; compare_set: PAtkStateSet): PAtkStateSet;
	PROCEDURE [ccall] atk_state_set_clear_states*(stateset: PAtkStateSet);
	PROCEDURE [ccall] atk_state_set_contains_state*(stateset: PAtkStateSet; type: AtkStateType): lg.gboolean;
	PROCEDURE [ccall] atk_state_set_contains_states*(stateset: PAtkStateSet; types: POINTER TO ARRAY [untagged] OF AtkStateType; n_types: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_state_set_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_state_set_is_empty*(stateset: PAtkStateSet): lg.gboolean;
	PROCEDURE [ccall] atk_state_set_or_sets*(stateset: PAtkStateSet; compare_set: PAtkStateSet): PAtkStateSet;
	PROCEDURE [ccall] atk_state_set_remove_state*(stateset: PAtkStateSet; type: AtkStateType): lg.gboolean;
	PROCEDURE [ccall] atk_state_set_xor_sets*(stateset: PAtkStateSet; compare_set: PAtkStateSet): PAtkStateSet;

(* AtkStateSet constructors *)
	PROCEDURE [ccall] atk_state_set_new*(): PAtkStateSet;

(* AtkUtil methods *)
	PROCEDURE [ccall] atk_util_get_type*(): lo.GType;

(* AtkGObjectAccessible methods *)
	PROCEDURE [ccall] atk_gobject_accessible_for_object*(obj: lo.PGObject): PAtkObject;
	PROCEDURE [ccall] atk_gobject_accessible_get_object*(gobjectaccessible: PAtkGObjectAccessible): lo.PGObject;
	PROCEDURE [ccall] atk_gobject_accessible_get_type*(): lo.GType;

(* AtkNoOpObject methods *)
	PROCEDURE [ccall] atk_no_op_object_get_type*(): lo.GType;

(* AtkNoOpObject constructors *)
	PROCEDURE [ccall] atk_no_op_object_new*(obj: lo.PGObject): PAtkNoOpObject;

(* AtkNoOpObjectFactory methods *)
	PROCEDURE [ccall] atk_no_op_object_factory_get_type*(): lo.GType;

(* AtkNoOpObjectFactory constructors *)
	PROCEDURE [ccall] atk_no_op_object_factory_new*(): PAtkNoOpObjectFactory;

(* AtkAction methods *)
	PROCEDURE [ccall] atk_action_do_action*(action: PAtkAction; i: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_action_get_description*(action: PAtkAction; i: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_action_get_keybinding*(action: PAtkAction; i: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_action_get_localized_name*(action: PAtkAction; i: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_action_get_n_actions*(action: PAtkAction): lg.gint;
	PROCEDURE [ccall] atk_action_get_name*(action: PAtkAction; i: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_action_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_action_set_description*(action: PAtkAction; i: lg.gint; desc: lg.Pgchar): lg.gboolean;

(* AtkComponent methods *)
	PROCEDURE [ccall] atk_component_add_focus_handler*(component: PAtkComponent; handler: AtkFocusHandler): lg.guint;
	PROCEDURE [ccall] atk_component_contains*(component: PAtkComponent; x: lg.gint; y: lg.gint; coord_type: AtkCoordType): lg.gboolean;
	PROCEDURE [ccall] atk_component_get_extents*(component: PAtkComponent; x: lg.Pgint; y: lg.Pgint; width: lg.Pgint; height: lg.Pgint; coord_type: AtkCoordType);
	PROCEDURE [ccall] atk_component_get_layer*(component: PAtkComponent): AtkLayer;
	PROCEDURE [ccall] atk_component_get_mdi_zorder*(component: PAtkComponent): lg.gint;
	PROCEDURE [ccall] atk_component_get_position*(component: PAtkComponent; x: lg.Pgint; y: lg.Pgint; coord_type: AtkCoordType);
	PROCEDURE [ccall] atk_component_get_size*(component: PAtkComponent; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] atk_component_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_component_grab_focus*(component: PAtkComponent): lg.gboolean;
	PROCEDURE [ccall] atk_component_ref_accessible_at_point*(component: PAtkComponent; x: lg.gint; y: lg.gint; coord_type: AtkCoordType): PAtkObject;
	PROCEDURE [ccall] atk_component_remove_focus_handler*(component: PAtkComponent; handler_id: lg.guint);
	PROCEDURE [ccall] atk_component_set_extents*(component: PAtkComponent; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; coord_type: AtkCoordType): lg.gboolean;
	PROCEDURE [ccall] atk_component_set_position*(component: PAtkComponent; x: lg.gint; y: lg.gint; coord_type: AtkCoordType): lg.gboolean;
	PROCEDURE [ccall] atk_component_set_size*(component: PAtkComponent; width: lg.gint; height: lg.gint): lg.gboolean;

(* AtkDocument methods *)
	PROCEDURE [ccall] atk_document_get_document*(document: PAtkDocument): lg.gpointer;
	PROCEDURE [ccall] atk_document_get_document_type*(document: PAtkDocument): lg.Pgchar;
	PROCEDURE [ccall] atk_document_get_type*(): lo.GType;

(* AtkEditableText methods *)
	PROCEDURE [ccall] atk_editable_text_copy_text*(editabletext: PAtkEditableText; start_pos: lg.gint; end_pos: lg.gint);
	PROCEDURE [ccall] atk_editable_text_cut_text*(editabletext: PAtkEditableText; start_pos: lg.gint; end_pos: lg.gint);
	PROCEDURE [ccall] atk_editable_text_delete_text*(editabletext: PAtkEditableText; start_pos: lg.gint; end_pos: lg.gint);
	PROCEDURE [ccall] atk_editable_text_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_editable_text_insert_text*(editabletext: PAtkEditableText; string: lg.Pgchar; length: lg.gint; position: lg.Pgint);
	PROCEDURE [ccall] atk_editable_text_paste_text*(editabletext: PAtkEditableText; position: lg.gint);
	PROCEDURE [ccall] atk_editable_text_set_run_attributes*(editabletext: PAtkEditableText; attrib_set: PAtkAttributeSet; start_offset: lg.gint; end_offset: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_editable_text_set_text_contents*(editabletext: PAtkEditableText; string: lg.Pgchar);

(* AtkHypertext methods *)
	PROCEDURE [ccall] atk_hypertext_get_link*(hypertext: PAtkHypertext; link_index: lg.gint): PAtkHyperlink;
	PROCEDURE [ccall] atk_hypertext_get_link_index*(hypertext: PAtkHypertext; char_index: lg.gint): lg.gint;
	PROCEDURE [ccall] atk_hypertext_get_n_links*(hypertext: PAtkHypertext): lg.gint;
	PROCEDURE [ccall] atk_hypertext_get_type*(): lo.GType;

(* AtkImage methods *)
	PROCEDURE [ccall] atk_image_get_image_description*(image: PAtkImage): lg.Pgchar;
	PROCEDURE [ccall] atk_image_get_image_position*(image: PAtkImage; x: lg.Pgint; y: lg.Pgint; coord_type: AtkCoordType);
	PROCEDURE [ccall] atk_image_get_image_size*(image: PAtkImage; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] atk_image_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_image_set_image_description*(image: PAtkImage; description: lg.Pgchar): lg.gboolean;

(* AtkImplementor methods *)
	PROCEDURE [ccall] atk_implementor_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_implementor_ref_accessible*(implementor: PAtkImplementor): PAtkObject;

(* AtkSelection methods *)
	PROCEDURE [ccall] atk_selection_add_selection*(selection: PAtkSelection; i: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_selection_clear_selection*(selection: PAtkSelection): lg.gboolean;
	PROCEDURE [ccall] atk_selection_get_selection_count*(selection: PAtkSelection): lg.gint;
	PROCEDURE [ccall] atk_selection_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_selection_is_child_selected*(selection: PAtkSelection; i: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_selection_ref_selection*(selection: PAtkSelection; i: lg.gint): PAtkObject;
	PROCEDURE [ccall] atk_selection_remove_selection*(selection: PAtkSelection; i: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_selection_select_all_selection*(selection: PAtkSelection): lg.gboolean;

(* AtkStreamableContent methods *)
	PROCEDURE [ccall] atk_streamable_content_get_mime_type*(streamablecontent: PAtkStreamableContent; i: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_streamable_content_get_n_mime_types*(streamablecontent: PAtkStreamableContent): lg.gint;
	PROCEDURE [ccall] atk_streamable_content_get_stream*(streamablecontent: PAtkStreamableContent; mime_type: lg.Pgchar): lg.PGIOChannel;
	PROCEDURE [ccall] atk_streamable_content_get_type*(): lo.GType;

(* AtkTable methods *)
	PROCEDURE [ccall] atk_table_add_column_selection*(table: PAtkTable; column: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_add_row_selection*(table: PAtkTable; row: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_get_caption*(table: PAtkTable): PAtkObject;
	PROCEDURE [ccall] atk_table_get_column_at_index*(table: PAtkTable; index_: lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_column_description*(table: PAtkTable; column: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_table_get_column_extent_at*(table: PAtkTable; row: lg.gint; column: lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_column_header*(table: PAtkTable; column: lg.gint): PAtkObject;
	PROCEDURE [ccall] atk_table_get_index_at*(table: PAtkTable; row: lg.gint; column: lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_n_columns*(table: PAtkTable): lg.gint;
	PROCEDURE [ccall] atk_table_get_n_rows*(table: PAtkTable): lg.gint;
	PROCEDURE [ccall] atk_table_get_row_at_index*(table: PAtkTable; index_: lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_row_description*(table: PAtkTable; row: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_table_get_row_extent_at*(table: PAtkTable; row: lg.gint; column: lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_row_header*(table: PAtkTable; row: lg.gint): PAtkObject;
	PROCEDURE [ccall] atk_table_get_selected_columns*(table: PAtkTable; VAR selected: POINTER TO ARRAY [untagged] OF lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_selected_rows*(table: PAtkTable; VAR selected: POINTER TO ARRAY [untagged] OF lg.gint): lg.gint;
	PROCEDURE [ccall] atk_table_get_summary*(table: PAtkTable): PAtkObject;
	PROCEDURE [ccall] atk_table_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_table_is_column_selected*(table: PAtkTable; column: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_is_row_selected*(table: PAtkTable; row: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_is_selected*(table: PAtkTable; row: lg.gint; column: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_ref_at*(table: PAtkTable; row: lg.gint; column: lg.gint): PAtkObject;
	PROCEDURE [ccall] atk_table_remove_column_selection*(table: PAtkTable; column: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_remove_row_selection*(table: PAtkTable; row: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_table_set_caption*(table: PAtkTable; caption: PAtkObject);
	PROCEDURE [ccall] atk_table_set_column_description*(table: PAtkTable; column: lg.gint; description: lg.Pgchar);
	PROCEDURE [ccall] atk_table_set_column_header*(table: PAtkTable; column: lg.gint; header: PAtkObject);
	PROCEDURE [ccall] atk_table_set_row_description*(table: PAtkTable; row: lg.gint; description: lg.Pgchar);
	PROCEDURE [ccall] atk_table_set_row_header*(table: PAtkTable; row: lg.gint; header: PAtkObject);
	PROCEDURE [ccall] atk_table_set_summary*(table: PAtkTable; accessible: PAtkObject);

(* AtkText methods *)
	PROCEDURE [ccall] atk_text_add_selection*(text: PAtkText; start_offset: lg.gint; end_offset: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_text_attribute_for_name*(name: lg.Pgchar): AtkTextAttribute;
	PROCEDURE [ccall] atk_text_attribute_get_name*(attr: AtkTextAttribute): lg.Pgchar;
	PROCEDURE [ccall] atk_text_attribute_get_value*(attr: AtkTextAttribute; index_: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_text_attribute_register*(name: lg.Pgchar): AtkTextAttribute;
	PROCEDURE [ccall] atk_text_free_ranges*(VAR ranges: POINTER TO ARRAY [untagged] OF AtkTextRange);
	PROCEDURE [ccall] atk_text_get_bounded_ranges*(text: PAtkText; rect: PAtkTextRectangle; coord_type: AtkCoordType; x_clip_type: AtkTextClipType; y_clip_type: AtkTextClipType): POINTER TO ARRAY [untagged] OF AtkTextRange;
	PROCEDURE [ccall] atk_text_get_caret_offset*(text: PAtkText): lg.gint;
	PROCEDURE [ccall] atk_text_get_character_at_offset*(text: PAtkText; offset: lg.gint): lg.gunichar;
	PROCEDURE [ccall] atk_text_get_character_count*(text: PAtkText): lg.gint;
	PROCEDURE [ccall] atk_text_get_character_extents*(text: PAtkText; offset: lg.gint; x: lg.Pgint; y: lg.Pgint; width: lg.Pgint; height: lg.Pgint; coords: AtkCoordType);
	PROCEDURE [ccall] atk_text_get_default_attributes*(text: PAtkText): PAtkAttributeSet;
	PROCEDURE [ccall] atk_text_get_n_selections*(text: PAtkText): lg.gint;
	PROCEDURE [ccall] atk_text_get_offset_at_point*(text: PAtkText; x: lg.gint; y: lg.gint; coords: AtkCoordType): lg.gint;
	PROCEDURE [ccall] atk_text_get_range_extents*(text: PAtkText; start_offset: lg.gint; end_offset: lg.gint; coord_type: AtkCoordType; rect: PAtkTextRectangle);
	PROCEDURE [ccall] atk_text_get_run_attributes*(text: PAtkText; offset: lg.gint; start_offset: lg.Pgint; end_offset: lg.Pgint): PAtkAttributeSet;
	PROCEDURE [ccall] atk_text_get_selection*(text: PAtkText; selection_num: lg.gint; start_offset: lg.Pgint; end_offset: lg.Pgint): lg.Pgchar;
	PROCEDURE [ccall] atk_text_get_text*(text: PAtkText; start_offset: lg.gint; end_offset: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] atk_text_get_text_after_offset*(text: PAtkText; offset: lg.gint; boundary_type: AtkTextBoundary; start_offset: lg.Pgint; end_offset: lg.Pgint): lg.Pgchar;
	PROCEDURE [ccall] atk_text_get_text_at_offset*(text: PAtkText; offset: lg.gint; boundary_type: AtkTextBoundary; start_offset: lg.Pgint; end_offset: lg.Pgint): lg.Pgchar;
	PROCEDURE [ccall] atk_text_get_text_before_offset*(text: PAtkText; offset: lg.gint; boundary_type: AtkTextBoundary; start_offset: lg.Pgint; end_offset: lg.Pgint): lg.Pgchar;
	PROCEDURE [ccall] atk_text_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_text_remove_selection*(text: PAtkText; selection_num: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_text_set_caret_offset*(text: PAtkText; offset: lg.gint): lg.gboolean;
	PROCEDURE [ccall] atk_text_set_selection*(text: PAtkText; selection_num: lg.gint; start_offset: lg.gint; end_offset: lg.gint): lg.gboolean;

(* AtkValue methods *)
	PROCEDURE [ccall] atk_value_get_current_value*(value: PAtkValue; v: lo.PGValue);
	PROCEDURE [ccall] atk_value_get_maximum_value*(value: PAtkValue; v: lo.PGValue);
	PROCEDURE [ccall] atk_value_get_minimum_value*(value: PAtkValue; v: lo.PGValue);
	PROCEDURE [ccall] atk_value_get_type*(): lo.GType;
	PROCEDURE [ccall] atk_value_set_current_value*(value: PAtkValue; v: lo.PGValue): lg.gboolean;
END LibsAtk.
