MODULE LibsGlib ["libglib-2.0-0.dll"];
(*MODULE LibsGlib ["libglib-2.0.so"];*)

	IMPORT SYSTEM;

	CONST
		FALSE* = 0; TRUE* = 1;
		
		G_SIGNAL_RUN_FIRST* = {0};
		G_SIGNAL_RUN_LAST* = {1};
		G_SIGNAL_RUN_CLEANUP* = {2};
		G_SIGNAL_NO_RECURSE* = {3};
		G_SIGNAL_DETAILED* = {4};
		G_SIGNAL_ACTION* = {5};
		G_SIGNAL_NO_HOOKS* = {6};
		G_SIGNAL_FLAGS_MASK* = {0..6};

		G_PARAM_READABLE* = {0};
		G_PARAM_WRITABLE* = {1};
		G_PARAM_CONSTRUCT* = {2};
		G_PARAM_CONSTRUCT_ONLY* = {3};
		G_PARAM_LAX_VALIDATION* = {4};
		G_PARAM_STATIC_NAME* = {5};
		G_PARAM_PRIVATE* = G_PARAM_STATIC_NAME;
		G_PARAM_STATIC_NICK* = {6};
		G_PARAM_STATIC_BLURB* = {7};

		G_PARAM_READWRITE* = G_PARAM_READABLE + G_PARAM_WRITABLE;
		G_PARAM_STATIC_STRINGS* = G_PARAM_STATIC_NAME + G_PARAM_STATIC_NICK +G_PARAM_STATIC_BLURB;
		G_PARAM_MASK* = {0..7};
		G_PARAM_USER_SHIFT* = 8;

		G_TOKEN_EOF* =   0;

		G_TOKEN_LEFT_PAREN* = ORD('(');
		G_TOKEN_RIGHT_PAREN* = ORD(')');
		G_TOKEN_LEFT_CURLY* = ORD('{');
		G_TOKEN_RIGHT_CURLY* = ORD('}');
		G_TOKEN_LEFT_BRACE* = ORD('[');
		G_TOKEN_RIGHT_BRACE* = ORD(']');
		G_TOKEN_EQUAL_SIGN* = ORD('=');
		G_TOKEN_COMMA* = ORD(',');

		G_TOKEN_NONE* = 256;

		G_TOKEN_ERROR* = 257;

		G_TOKEN_CHAR* = 257;
		G_TOKEN_BINARY* = 258;
		G_TOKEN_OCTAL* = 259;
		G_TOKEN_INT* = 260;
		G_TOKEN_HEX* = 261;
		G_TOKEN_FLOAT* = 262;
		G_TOKEN_STRING* = 263;

		G_TOKEN_SYMBOL* = 264;
		G_TOKEN_IDENTIFIER* = 265;
		G_TOKEN_IDENTIFIER_NULL* = 266;

		G_TOKEN_COMMENT_SINGLE* = 267;
		G_TOKEN_COMMENT_MULTI* = 268;
		G_TOKEN_LAST* = 269;
	
	TYPE
		GBase* = EXTENSIBLE RECORD [untagged] END;
		gpointer* = POINTER TO GBase;

		gchar* = SHORTCHAR;
		gshort* = SHORTINT;
		glong* = INTEGER;
		gint* = INTEGER;
		int* = gint;
		gboolean* = gint;

		guchar* = SHORTCHAR;
		gushort* = CHAR;
		gulong* = INTEGER;
		guint* = INTEGER;

		gfloat* = SHORTREAL;
		gdouble* = REAL;

		gint8* = BYTE;
		guint8* = SHORTCHAR;
		gint16* = SHORTINT;
		guint16* = SHORTINT;
		gint32* = INTEGER;
		guint32* = INTEGER;
		gint64* = LONGINT;
		guint64* = LONGINT;

		gunichar* = guint32;

		gsize* = INTEGER;
		gssize* = INTEGER;

		void* = gpointer;
		double* = gdouble;
		GQuark* = guint32;

		pgdoublearray* = POINTER TO ARRAY [untagged] OF double;
		pgchararray* = POINTER TO ARRAY [untagged] OF gchar;
		pguchararray* = POINTER TO ARRAY [untagged] OF guchar;
		pguint8array* = POINTER TO ARRAY [untagged] OF guint8;
		pintarray* = POINTER TO ARRAY [untagged] OF int;
		pgunichararray* = POINTER TO ARRAY [untagged] OF gunichar;
		pguint16array* = POINTER TO ARRAY [untagged] OF guint16;

		Pguchar* = pguchararray;
		Pgchar* = pgchararray;
		Pguint8* = pguint8array;
		Pint* = pintarray;
		Pgunichar* = pgunichararray;
		Pgint* = Pint;
		Pgdouble* = pgdoublearray;
		Pguint16* = pguint16array;

		PGError* = POINTER TO GError;
		GError* = RECORD (GBase)
			gint*   : GQuark;
			code*   : gint;
			message*: Pgchar;
		END;

		PGSList* = POINTER TO GSList;
		GSList* = RECORD (GBase)
			data*: gpointer;
			next*: PGSList;
		END;

		PGList* = POINTER TO GList;
		GList* = RECORD (GBase)
			data*: gpointer;
			next*: PGList;
			prev*: PGList;
		END;

		PGTimeVal* = POINTER TO GTimeVal;
		GTimeVal* = RECORD (GBase)
			tv_sec* : glong;
			tv_usec*: glong;
		END;

		PGArray* = POINTER TO GArray;
		GArray* = RECORD (GBase)
			data*: POINTER TO ARRAY [untagged] OF gchar;
			len*: guint;
		END;

		PGByteArray* = POINTER TO GByteArray;
		GByteArray* = RECORD (GBase)
			data*: POINTER TO ARRAY [untagged] OF guint8;
			len*: guint;
		END;

		PGPtrArray* = POINTER TO GPtrArray;
		GPtrArray* = RECORD (GBase)
			pdata*: POINTER TO ARRAY [untagged] OF gpointer;
			len*: guint;
		END;


		PGHashTable* = POINTER TO GHashTable;
		GHashTable* = RECORD (GBase) END;

		GSignalFlags* = SET;
		GParamFlags* = SET;
		GTokenType* = gint32;

		PGIOChannel* = POINTER TO GIOChannel;
		GIOChannel* = RECORD (GBase) END;

		PGString* = POINTER TO GString;
		GString* = RECORD (GBase)
			str*: POINTER TO ARRAY [untagged] OF gchar;
			len*: gsize;
			allocated_len*: gsize;
		END;


		PGIcon* = POINTER TO GIcon;
		GIcon* = RECORD (GBase) END;

		PGScanner* = POINTER TO GScanner;
		GScanner* = RECORD (GBase) END;

END LibsGlib.
