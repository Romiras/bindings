MODULE LibsGdk ["libgdk-win32-2.0-0.dll"];
(*MODULE LibsGdk ["libgdk-x11-2.0.so"];*)

IMPORT
	SYSTEM,
	lg := LibsGlib,
	lo := LibsGObject,
	gpb := LibsGdkPixbuf,
	pan := LibsPango;

TYPE
	PInt* = POINTER TO ARRAY [untagged] 1 OF INTEGER;
	PStrList* = POINTER TO ARRAY [untagged] OF ARRAY OF SHORTCHAR;

(* ==================================================================== ENUMS *)
TYPE
	GdkAxisUse* = lg.gint32;
CONST
	GDK_AXIS_IGNORE* = 0;
	GDK_AXIS_X*  = 1;
	GDK_AXIS_Y*  = 2;
	GDK_AXIS_PRESSURE* = 3;
	GDK_AXIS_XTILT* = 4;
	GDK_AXIS_YTILT* = 5;
	GDK_AXIS_WHEEL* = 6;
	GDK_AXIS_LAST* = 7;

TYPE
	GdkByteOrder* = lg.gint32;
CONST
	GDK_LSB_FIRST* = 0;
	GDK_MSB_FIRST* = 1;

TYPE
	GdkCapStyle* = lg.gint32;
CONST
	GDK_CAP_NOT_LAST* = 0;
	GDK_CAP_BUTT* = 1;
	GDK_CAP_ROUND* = 2;
	GDK_CAP_PROJECTING* = 3;

TYPE
	GdkCrossingMode* = lg.gint32;
CONST
	GDK_CROSSING_NORMAL* = 0;
	GDK_CROSSING_GRAB* = 1;
	GDK_CROSSING_UNGRAB* = 2;

TYPE
	GdkCursorType* = lg.gint32;
CONST
	GDK_X_CURSOR* = 0;
	GDK_ARROW* = 2;
	GDK_BASED_ARROW_DOWN* = 4;
	GDK_BASED_ARROW_UP* = 6;
	GDK_BOAT*  = 8;
	GDK_BOGOSITY* = 10;
	GDK_BOTTOM_LEFT_CORNER*	= 12;
	GDK_BOTTOM_RIGHT_CORNER* = 14;
	GDK_BOTTOM_SIDE*  = 16;
	GDK_BOTTOM_TEE*  = 18;
	GDK_BOX_SPIRAL*  = 20;
	GDK_CENTER_PTR*  = 22;
	GDK_CIRCLE* = 24;
	GDK_CLOCK* = 26;
	GDK_COFFEE_MUG*  = 28;
	GDK_CROSS* = 30;
	GDK_CROSS_REVERSE* = 32;
	GDK_CROSSHAIR* 	 = 34;
	GDK_DIAMOND_CROSS* = 36;
	GDK_DOT*  = 38;
	GDK_DOTBOX* = 40;
	GDK_DOUBLE_ARROW*  = 42;
	GDK_DRAFT_LARGE*  = 44;
	GDK_DRAFT_SMALL*  = 46;
	GDK_DRAPED_BOX*  = 48;
	GDK_EXCHANGE* = 50;
	GDK_FLEUR* = 52;
	GDK_GOBBLER* = 54;
	GDK_GUMBY* = 56;
	GDK_HAND1* = 58;
	GDK_HAND2* = 60;
	GDK_HEART* = 62;
	GDK_ICON*  = 64;
	GDK_IRON_CROSS*  = 66;
	GDK_LEFT_PTR* = 68;
	GDK_LEFT_SIDE* 	 = 70;
	GDK_LEFT_TEE* = 72;
	GDK_LEFTBUTTON*  = 74;
	GDK_LL_ANGLE* = 76;
	GDK_LR_ANGLE* = 78;
	GDK_MAN*  = 80;
	GDK_MIDDLEBUTTON*  = 82;
	GDK_MOUSE* = 84;
	GDK_PENCIL* = 86;
	GDK_PIRATE* = 88;
	GDK_PLUS*  = 90;
	GDK_QUESTION_ARROW* = 92;
	GDK_RIGHT_PTR* 	 = 94;
	GDK_RIGHT_SIDE*  = 96;
	GDK_RIGHT_TEE* 	 = 98;
	GDK_RIGHTBUTTON*  = 100;
	GDK_RTL_LOGO* = 102;
	GDK_SAILBOAT* = 104;
	GDK_SB_DOWN_ARROW* = 106;
	GDK_SB_H_DOUBLE_ARROW* = 108;
	GDK_SB_LEFT_ARROW* = 110;
	GDK_SB_RIGHT_ARROW* = 112;
	GDK_SB_UP_ARROW*  = 114;
	GDK_SB_V_DOUBLE_ARROW* = 116;
	GDK_SHUTTLE* = 118;
	GDK_SIZING* = 120;
	GDK_SPIDER* = 122;
	GDK_SPRAYCAN* = 124;
	GDK_STAR*  = 126;
	GDK_TARGET* = 128;
	GDK_TCROSS* = 130;
	GDK_TOP_LEFT_ARROW* = 132;
	GDK_TOP_LEFT_CORNER* = 134;
	GDK_TOP_RIGHT_CORNER* = 136;
	GDK_TOP_SIDE* = 138;
	GDK_TOP_TEE* = 140;
	GDK_TREK*  = 142;
	GDK_UL_ANGLE* = 144;
	GDK_UMBRELLA* = 146;
	GDK_UR_ANGLE* = 148;
	GDK_WATCH* = 150;
	GDK_XTERM* = 152;
	GDK_LAST_CURSOR*  = 152+1;
	GDK_CURSOR_IS_PIXMAP* = -1;

TYPE
	GdkDragAction* = SET;
CONST
	GDK_ACTION_DEFAULT* = 0;
	GDK_ACTION_COPY* = 1;
	GDK_ACTION_MOVE* = 2;
	GDK_ACTION_LINK* = 3;
	GDK_ACTION_PRIVATE* = 4;
	GDK_ACTION_ASK* = 5;

TYPE
	GdkDragProtocol* = lg.gint32;
CONST
	GDK_DRAG_PROTO_MOTIF* = 0;
	GDK_DRAG_PROTO_XDND* = 1;
	GDK_DRAG_PROTO_ROOTWIN*  = 2;
	GDK_DRAG_PROTO_NONE* = 3;
	GDK_DRAG_PROTO_WIN32_DROPFILES* = 4;
	GDK_DRAG_PROTO_OLE2* = 5;
	GDK_DRAG_PROTO_LOCAL* = 6;

TYPE
	GdkEventMask* = SET;
CONST
	GDK_EXPOSURE_MASK* = 1;
	GDK_POINTER_MOTION_MASK* = 2;
	GDK_POINTER_MOTION_HINT_MASK* = 3;
	GDK_BUTTON_MOTION_MASK* = 4;
	GDK_BUTTON1_MOTION_MASK* = 5;
	GDK_BUTTON2_MOTION_MASK* = 6;
	GDK_BUTTON3_MOTION_MASK* = 7;
	GDK_BUTTON_PRESS_MASK*  = 8;
	GDK_BUTTON_RELEASE_MASK* = 9;
	GDK_KEY_PRESS_MASK* 	 = 10;
	GDK_KEY_RELEASE_MASK*  = 11;
	GDK_ENTER_NOTIFY_MASK*  = 12;
	GDK_LEAVE_NOTIFY_MASK*  = 13;
	GDK_FOCUS_CHANGE_MASK*  = 14;
	GDK_STRUCTURE_MASK* 	 = 15;
	GDK_PROPERTY_CHANGE_MASK* = 16;
	GDK_VISIBILITY_NOTIFY_MASK* = 17;
	GDK_PROXIMITY_IN_MASK*  = 18;
	GDK_PROXIMITY_OUT_MASK* = 19;
	GDK_SUBSTRUCTURE_MASK*  = 20;
	GDK_SCROLL_MASK* = 21;
	GDK_ALL_EVENTS_MASK*  = {1..22};

TYPE
	GdkEventType* = lg.gint32;
CONST
	GDK_NOTHING* 	 = -1;
	GDK_DELETE* = -1+1;
	GDK_DESTROY* 	 = 1;
	GDK_EXPOSE* = 2;
	GDK_MOTION_NOTIFY* = 3;
	GDK_BUTTON_PRESS* = 4;
	GDK_2BUTTON_PRESS* = 5;
	GDK_3BUTTON_PRESS* = 6;
	GDK_BUTTON_RELEASE* = 7;
	GDK_KEY_PRESS*  = 8;
	GDK_KEY_RELEASE* = 9;
	GDK_ENTER_NOTIFY* = 10;
	GDK_LEAVE_NOTIFY* = 11;
	GDK_FOCUS_CHANGE* = 12;
	GDK_CONFIGURE*  = 13;
	GDK_MAP* = 14;
	GDK_UNMAP* = 15;
	GDK_PROPERTY_NOTIFY* = 16;
	GDK_SELECTION_CLEAR* = 17;
	GDK_SELECTION_REQUEST* = 18;
	GDK_SELECTION_NOTIFY*	= 19;
	GDK_PROXIMITY_IN* = 20;
	GDK_PROXIMITY_OUT* = 21;
	GDK_DRAG_ENTER*  = 22;
	GDK_DRAG_LEAVE*  = 23;
	GDK_DRAG_MOTION* = 24;
	GDK_DRAG_STATUS* = 25;
	GDK_DROP_START*  = 26;
	GDK_DROP_FINISHED* = 27;
	GDK_CLIENT_EVENT* = 28;
	GDK_VISIBILITY_NOTIFY* = 29;
	GDK_NO_EXPOSE*  = 30;
	GDK_SCROLL* = 31;
	GDK_WINDOW_STATE* = 32;
	GDK_SETTING* 	 = 33;
	GDK_OWNER_CHANGE* = 34;
	GDK_GRAB_BROKEN* = 35;

TYPE
	GdkExtensionMode* = lg.gint32;
CONST
	GDK_EXTENSION_EVENTS_NONE* = 0;
	GDK_EXTENSION_EVENTS_ALL* = 1;
	GDK_EXTENSION_EVENTS_CURSOR* = 2;

TYPE
	GdkFill* = lg.gint32;
CONST
	GDK_SOLID* 	 = 0;
	GDK_TILED* 	 = 1;
	GDK_STIPPLED*  = 2;
	GDK_OPAQUE_STIPPLED* = 3;

TYPE
	GdkFillRule* = lg.gint32;
CONST
	GDK_EVEN_ODD_RULE* = 0;
	GDK_WINDING_RULE*	= 1;

TYPE
	GdkFilterReturn* = lg.gint32;
CONST
	GDK_FILTER_CONTINUE*	= 0;
	GDK_FILTER_TRANSLATE* = 1;
	GDK_FILTER_REMOVE* = 2;

TYPE
	GdkFontType* = lg.gint32;
CONST
	GDK_FONT_FONT* = 0;
	GDK_FONT_FONTSET* = 1;

TYPE
	GdkFunction* = lg.gint32;
CONST
	GDK_COPY*  = 0;
	GDK_INVERT* = 1;
	GDK_XOR*  = 2;
	GDK_CLEAR* = 3;
	GDK_AND*  = 4;
	GDK_AND_REVERSE* = 5;
	GDK_AND_INVERT*	= 6;
	GDK_NOOP*  = 7;
	GDK_OR*  = 8;
	GDK_EQUIV* = 9;
	GDK_OR_REVERSE*	= 10;
	GDK_COPY_INVERT* = 11;
	GDK_OR_INVERT* = 12;
	GDK_NAND*  = 13;
	GDK_NOR*  = 14;
	GDK_SET*  = 15;

TYPE
	GdkGCValuesMask* = SET;
CONST
	GDK_GC_FOREGROUND* = 0;
	GDK_GC_BACKGROUND* = 1;
	GDK_GC_FONT*  = 2;
	GDK_GC_FUNCTION* = 3;
	GDK_GC_FILL*  = 4;
	GDK_GC_TILE*  = 5;
	GDK_GC_STIPPLE* = 6;
	GDK_GC_CLIP_MASK* = 7;
	GDK_GC_SUBWINDOW* = 8;
	GDK_GC_TS_X_ORIGIN* = 9;
	GDK_GC_TS_Y_ORIGIN* = 10;
	GDK_GC_CLIP_X_ORIGIN* = 11;
	GDK_GC_CLIP_Y_ORIGIN* = 12;
	GDK_GC_EXPOSURES* = 13;
	GDK_GC_LINE_WIDTH* = 14;
	GDK_GC_LINE_STYLE* = 15;
	GDK_GC_CAP_STYLE* = 16;
	GDK_GC_JOIN_STYLE* = 17;

TYPE
	GdkGrabStatus* = lg.gint32;
CONST
	GDK_GRAB_SUCCESS*  = 0;
	GDK_GRAB_ALREADY_GRABBED* = 1;
	GDK_GRAB_INVALID_TIME* = 2;
	GDK_GRAB_NOT_VIEWABLE* = 3;
	GDK_GRAB_FROZEN*  = 4;

TYPE
	GdkGravity* = lg.gint32;
CONST
	GDK_GRAVITY_NORTH_WEST* = 1;
	GDK_GRAVITY_NORTH* = 1+1;
	GDK_GRAVITY_NORTH_EAST* = 1+2;
	GDK_GRAVITY_WEST* = 1+3;
	GDK_GRAVITY_CENTER* = 1+4;
	GDK_GRAVITY_EAST* = 1+5;
	GDK_GRAVITY_SOUTH_WEST* = 1+6;
	GDK_GRAVITY_SOUTH* = 1+7;
	GDK_GRAVITY_SOUTH_EAST* = 1+8;
	GDK_GRAVITY_STATIC* = 1+9;

TYPE
	GdkImageType* = lg.gint32;
CONST
	GDK_IMAGE_NORMAL*	= 0;
	GDK_IMAGE_SHARED*	= 1;
	GDK_IMAGE_FASTEST* = 2;

TYPE
	GdkInputCondition* = SET;
CONST
	GDK_INPUT_READ* = 0;
	GDK_INPUT_WRITE* = 1;
	GDK_INPUT_EXCEPTION* = 2;

TYPE
	GdkInputMode* = lg.gint32;
CONST
	GDK_MODE_DISABLED* = 0;
	GDK_MODE_SCREEN* = 1;
	GDK_MODE_WINDOW* = 2;

TYPE
	GdkInputSource* = lg.gint32;
CONST
	GDK_SOURCE_MOUSE*	= 0;
	GDK_SOURCE_PEN* = 1;
	GDK_SOURCE_ERASER* = 2;
	GDK_SOURCE_CURSOR* = 3;

TYPE
	GdkJoinStyle* = lg.gint32;
CONST
	GDK_JOIN_MITER* = 0;
	GDK_JOIN_ROUND* = 1;
	GDK_JOIN_BEVEL* = 2;

TYPE
	GdkLineStyle* = lg.gint32;
CONST
	GDK_LINE_SOLID* = 0;
	GDK_LINE_ON_OFF_DASH* = 1;
	GDK_LINE_DOUBLE_DASH* = 2;

TYPE
	GdkModifierType* = SET;
CONST
	GDK_SHIFT_MASK* = 0;
	GDK_LOCK_MASK* = 1;
	GDK_CONTROL_MASK* = 2;
	GDK_MOD1_MASK* = 3;
	GDK_MOD2_MASK* = 4;
	GDK_MOD3_MASK* = 5;
	GDK_MOD4_MASK* = 6;
	GDK_MOD5_MASK* = 7;
	GDK_BUTTON1_MASK* = 8;
	GDK_BUTTON2_MASK* = 9;
	GDK_BUTTON3_MASK* = 10;
	GDK_BUTTON4_MASK* = 11;
	GDK_BUTTON5_MASK* = 12;
	GDK_RELEASE_MASK* = 30;
	GDK_MODIFIER_MASK* = {GDK_RELEASE_MASK} + {0..12};

TYPE
	GdkNotifyType* = lg.gint32;
CONST
	GDK_NOTIFY_ANCESTOR*  = 0;
	GDK_NOTIFY_VIRTUAL* 	 = 1;
	GDK_NOTIFY_INFERIOR*  = 2;
	GDK_NOTIFY_NONLINEAR*  = 3;
	GDK_NOTIFY_NONLINEAR_VIRTUAL* = 4;
	GDK_NOTIFY_UNKNOWN* 	 = 5;

TYPE
	GdkOverlapType* = lg.gint32;
CONST
	GDK_OVERLAP_RECTANGLE_IN* = 0;
	GDK_OVERLAP_RECTANGLE_OUT*	= 1;
	GDK_OVERLAP_RECTANGLE_PART* = 2;

TYPE
	GdkOwnerChange* = lg.gint32;
CONST
	GDK_OWNER_CHANGE_NEW_OWNER* = 0;
	GDK_OWNER_CHANGE_DESTROY* = 1;
	GDK_OWNER_CHANGE_CLOSE* = 2;

TYPE
	GdkPropMode* = lg.gint32;
CONST
	GDK_PROP_MODE_REPLACE* = 0;
	GDK_PROP_MODE_PREPEND* = 1;
	GDK_PROP_MODE_APPEND*	= 2;

TYPE
	GdkPropertyState* = lg.gint32;
CONST
	GDK_PROPERTY_NEW_VALUE* = 0;
	GDK_PROPERTY_DELETE* = 1;

TYPE
	GdkRgbDither* = lg.gint32;
CONST
	GDK_RGB_DITHER_NONE* = 0;
	GDK_RGB_DITHER_NORMAL* = 1;
	GDK_RGB_DITHER_MAX* = 2;

TYPE
	GdkScrollDirection* = lg.gint32;
CONST
	GDK_SCROLL_UP* = 0;
	GDK_SCROLL_DOWN*	= 1;
	GDK_SCROLL_LEFT*	= 2;
	GDK_SCROLL_RIGHT* = 3;

TYPE
	GdkSettingAction* = lg.gint32;
CONST
	GDK_SETTING_ACTION_NEW* = 0;
	GDK_SETTING_ACTION_CHANGED* = 1;
	GDK_SETTING_ACTION_DELETED* = 2;

TYPE
	GdkStatus* = lg.gint32;
CONST
	GDK_OK*  = 0;
	GDK_ERROR* = -1;
	GDK_ERROR_PARAM* = -2;
	GDK_ERROR_FILE*	= -3;
	GDK_ERROR_MEM* = -4;

TYPE
	GdkSubwindowMode* = lg.gint32;
CONST
	GDK_CLIP_BY_CHILDREN*	= 0;
	GDK_INCLUDE_INFERIORS* = 1;

TYPE
	GdkVisibilityState* = lg.gint32;
CONST
	GDK_VISIBILITY_UNOBSCURED* = 0;
	GDK_VISIBILITY_PARTIAL*  = 1;
	GDK_VISIBILITY_FULLY_OBSCURED* = 2;

TYPE
	GdkVisualType* = lg.gint32;
CONST
	GDK_VISUAL_STATIC_GRAY*	= 0;
	GDK_VISUAL_GRAYSCALE* = 1;
	GDK_VISUAL_STATIC_COLOR* = 2;
	GDK_VISUAL_PSEUDO_COLOR* = 3;
	GDK_VISUAL_TRUE_COLOR* = 4;
	GDK_VISUAL_DIRECT_COLOR* = 5;

TYPE
	GdkWMDecoration* = SET;
CONST
	GDK_DECOR_ALL* = 0;
	GDK_DECOR_BORDER* = 1;
	GDK_DECOR_RESIZEH*	= 2;
	GDK_DECOR_TITLE* = 3;
	GDK_DECOR_MENU* = 4;
	GDK_DECOR_MINIMIZE* = 5;
	GDK_DECOR_MAXIMIZE* = 6;

TYPE
	GdkWMFunction* = SET;
CONST
	GDK_FUNC_ALL* = 0;
	GDK_FUNC_RESIZE* = 1;
	GDK_FUNC_MOVE* = 2;
	GDK_FUNC_MINIMIZE* = 3;
	GDK_FUNC_MAXIMIZE* = 4;
	GDK_FUNC_CLOSE* = 5;

TYPE
	GdkWindowAttributesType* = SET;
CONST
	GDK_WA_TITLE* = 1;
	GDK_WA_X*  = 2;
	GDK_WA_Y*  = 3;
	GDK_WA_CURSOR* = 4;
	GDK_WA_COLORMAP* = 5;
	GDK_WA_VISUAL* = 6;
	GDK_WA_WMCLASS*	= 7;
	GDK_WA_NOREDIR*	= 8;

TYPE
	GdkWindowClass* = lg.gint32;
CONST
	GDK_INPUT_OUTPUT* = 0;
	GDK_INPUT_ONLY* = 1;

TYPE
	GdkWindowEdge* = lg.gint32;
CONST
	GDK_WINDOW_EDGE_NORTH_WEST* = 0;
	GDK_WINDOW_EDGE_NORTH* = 1;
	GDK_WINDOW_EDGE_NORTH_EAST* = 2;
	GDK_WINDOW_EDGE_WEST* = 3;
	GDK_WINDOW_EDGE_EAST* = 4;
	GDK_WINDOW_EDGE_SOUTH_WEST* = 5;
	GDK_WINDOW_EDGE_SOUTH* = 6;
	GDK_WINDOW_EDGE_SOUTH_EAST* = 7;

TYPE
	GdkWindowHints* = SET;
CONST
	GDK_HINT_POS*  = 0;
	GDK_HINT_MIN_SIZE* = 1;
	GDK_HINT_MAX_SIZE* = 2;
	GDK_HINT_BASE_SIZE* = 3;
	GDK_HINT_ASPECT* = 4;
	GDK_HINT_RESIZE_INC*	= 5;
	GDK_HINT_WIN_GRAVITY* = 6;
	GDK_HINT_USER_POS* = 7;
	GDK_HINT_USER_SIZE* = 8;

TYPE
	GdkWindowState* = SET;
CONST
	GDK_WINDOW_STATE_WITHDRAWN*	= 0;
	GDK_WINDOW_STATE_ICONIFIED*	= 1;
	GDK_WINDOW_STATE_MAXIMIZED*	= 2;
	GDK_WINDOW_STATE_STICKY* = 3;
	GDK_WINDOW_STATE_FULLSCREEN* = 4;
	GDK_WINDOW_STATE_ABOVE* = 5;
	GDK_WINDOW_STATE_BELOW* = 6;

TYPE
	GdkWindowType* = lg.gint32;
CONST
	GDK_WINDOW_ROOT* = 0;
	GDK_WINDOW_TOPLEVEL* = 1;
	GDK_WINDOW_CHILD* = 2;
	GDK_WINDOW_DIALOG* = 3;
	GDK_WINDOW_TEMP* = 4;
	GDK_WINDOW_FOREIGN*	= 5;

TYPE
	GdkWindowTypeHint* = lg.gint32;
CONST
	GDK_WINDOW_TYPE_HINT_NORMAL* = 0;
	GDK_WINDOW_TYPE_HINT_DIALOG* = 1;
	GDK_WINDOW_TYPE_HINT_MENU*  = 2;
	GDK_WINDOW_TYPE_HINT_TOOLBAR* = 3;
	GDK_WINDOW_TYPE_HINT_SPLASHSCREEN* = 4;
	GDK_WINDOW_TYPE_HINT_UTILITY* = 5;
	GDK_WINDOW_TYPE_HINT_DOCK*  = 6;
	GDK_WINDOW_TYPE_HINT_DESKTOP* = 7;

(* ====================================================== POINTERS TO OBJECTS *)
TYPE
	PGdkColormap* = POINTER TO GdkColormap;
	PGdkDevice*  = POINTER TO GdkDevice;
	PGdkDisplay*  = POINTER TO GdkDisplay;
	PGdkDisplayManager* = POINTER TO GdkDisplayManager;
	PGdkDragContext* = POINTER TO GdkDragContext;
	PGdkDrawable* = POINTER TO GdkDrawable;
	PGdkGC* = POINTER TO GdkGC;
	PGdkImage*  = POINTER TO GdkImage;
	PGdkKeymap*  = POINTER TO GdkKeymap;
	PGdkPangoRenderer*	= POINTER TO GdkPangoRenderer;
	PGdkPixmap*  = POINTER TO GdkPixmap;
	PGdkScreen*  = POINTER TO GdkScreen;
	PGdkVisual*  = POINTER TO GdkVisual;
	PGdkWindow*  = POINTER TO GdkWindow;
	PGdkBitmap*  = POINTER TO GdkBitmap;

(* ====================================================== POINTERS TO STRUCTS *)
TYPE
	PBRESINFO* = POINTER TO BRESINFO;
	PEdgeTable* = POINTER TO EdgeTable;
	PEdgeTableEntry*  = POINTER TO EdgeTableEntry;
	PGdkDeviceAxis*  = POINTER TO GdkDeviceAxis;
	PGdkDeviceKey* 	 = POINTER TO GdkDeviceKey;
	PGdkDisplayPointerHooks* = POINTER TO GdkDisplayPointerHooks;
	PGdkEvent* = POINTER TO GdkEvent;
	PGdkEventAny* = POINTER TO GdkEventAny;
	PGdkEventButton*  = POINTER TO GdkEventButton;
	PGdkEventClient*  = POINTER TO GdkEventClient;
	PGdkEventConfigure* = POINTER TO GdkEventConfigure;
	PGdkEventCrossing* = POINTER TO GdkEventCrossing;
	PGdkEventDND* = POINTER TO GdkEventDND;
	PGdkEventExpose*  = POINTER TO GdkEventExpose;
	PGdkEventFocus*  = POINTER TO GdkEventFocus;
	PGdkEventGrabBroken* = POINTER TO GdkEventGrabBroken;
	PGdkEventKey* = POINTER TO GdkEventKey;
	PGdkEventMotion*  = POINTER TO GdkEventMotion;
	PGdkEventNoExpose* = POINTER TO GdkEventNoExpose;
	PGdkEventOwnerChange* = POINTER TO GdkEventOwnerChange;
	PGdkEventProperty* = POINTER TO GdkEventProperty;
	PGdkEventProximity* = POINTER TO GdkEventProximity;
	PGdkEventScroll*  = POINTER TO GdkEventScroll;
	PGdkEventSelection* = POINTER TO GdkEventSelection;
	PGdkEventSetting*  = POINTER TO GdkEventSetting;
	PGdkEventVisibility* = POINTER TO GdkEventVisibility;
	PGdkEventWindowState* = POINTER TO GdkEventWindowState;
	PGdkGCValues* = POINTER TO GdkGCValues;
	PGdkGeometry* = POINTER TO GdkGeometry;
	PGdkKeymapKey* 	 = POINTER TO GdkKeymapKey;
	PGdkPangoAttrEmbossed* = POINTER TO GdkPangoAttrEmbossed;
	PGdkPangoAttrStipple* = POINTER TO GdkPangoAttrStipple;
	PGdkPixmapObject*  = POINTER TO GdkPixmapObject;
	PGdkPoint* = POINTER TO GdkPoint;
	PGdkPointerHooks*  = POINTER TO GdkPointerHooks;
	PGdkRegion* = POINTER TO GdkRegion;
	PGdkRegionBox* 	 = POINTER TO GdkRegionBox;
	PGdkRgbCmap* = POINTER TO GdkRgbCmap;
	PGdkSegment* = POINTER TO GdkSegment;
	PGdkSpan*  = POINTER TO GdkSpan;
	PGdkTimeCoord* 	 = POINTER TO GdkTimeCoord;
	PGdkTrapezoid* 	 = POINTER TO GdkTrapezoid;
	PGdkWindowAttr*  = POINTER TO GdkWindowAttr;
	PGdkWindowObject*  = POINTER TO GdkWindowObject;
	PPOINTBLOCK* = POINTER TO POINTBLOCK;
	PScanLineList* 	 = POINTER TO ScanLineList;
	PScanLineListBlock* = POINTER TO ScanLineListBlock;
	PGdkAtom*  = POINTER TO GdkAtom;
	PGdkXEvent* = lg.void;

(* ====================================================== POINTERS TO ALIASES *)
TYPE
	PGdkSelection* = POINTER TO GdkSelection;
	PGdkSelectionType* = POINTER TO GdkSelectionType;
	PGdkTarget*  = POINTER TO GdkTarget;

(* ======================================================= POINTERS TO BOXEDS *)
TYPE
	PGdkColor* = POINTER TO GdkColor;
	PGdkCursor* = POINTER TO GdkCursor;
	PGdkFont* = POINTER TO GdkFont;
	PGdkRectangle* = POINTER TO GdkRectangle;

(* ================================================================== ALIASES *)
TYPE
	GdkNativeWindow*	= lg.guint32;
	GdkSelection* = GdkAtom;
	GdkSelectionType* = GdkAtom;
	GdkTarget*  = GdkAtom;
	GdkWChar*  = lg.guint32;
	GdkXEvent*  = lg.void;

(* ================================================================ CALLBACKS *)
TYPE
	Callback* = lo.GCallback;

	GdkDestroyNotify*  = RECORD (Callback)
 callback*: PROCEDURE [ccall]
 (
 	data: lg.gpointer
 );
	END;

	GdkEventFunc*   = RECORD (Callback)
 callback*: PROCEDURE [ccall]
 (
 	event: PGdkEvent;
 	data : lg.gpointer
 );
	END;

	GdkFilterFunc* 	 = RECORD (Callback)
 callback*: PROCEDURE [ccall]
 (
 	xevent: PGdkXEvent;
 	event : PGdkEvent;
 	data : lg.gpointer
 ): GdkFilterReturn;
	END;

	GdkInputFunction*  = RECORD (Callback)
 callback*: PROCEDURE [ccall]
 (
 	data : lg.gpointer;
 	source	 : lg.gint;
 	condition: GdkInputCondition
 );
	END;

	GdkSpanFunc*   = RECORD (Callback)
 callback*: PROCEDURE [ccall]
 (
 	span: PGdkSpan;
 	data: lg.gpointer
 );
	END;

	InvalidateMaybeRecurseChildFunc* = RECORD (Callback)
 callback*: PROCEDURE [ccall]
 (
 	arg1: PGdkWindow;
 	data: lg.gpointer
 ): lg.gboolean;
	END;

(* GdkEvent struct *)

	GdkEvent* = EXTENSIBLE RECORD [noalign]
	END;

(* ================================================================== STRUCTS *)
TYPE

(* BRESINFO struct *)

	BRESINFO* = RECORD [noalign]
		minor_axis*: lg.int;
		d* : lg.int;
		m* : lg.int;
		m1*  : lg.int;
		incr1* : lg.int;
		incr2* : lg.int;
	END;

(* EdgeTable struct *)

	EdgeTable* = RECORD [noalign]
 ymax* : lg.int;
 ymin* : lg.int;
 scanlines*: ScanLineList;
	END;

(* EdgeTableEntry struct *)

	EdgeTableEntry* = RECORD [noalign]
	END;

(* GdkDeviceAxis struct *)

	GdkDeviceAxis* = RECORD [noalign]
		use*: GdkAxisUse;
		min*: lg.gdouble;
		max*: lg.gdouble;
	END;

(* GdkDeviceKey struct *)

	GdkDeviceKey* = RECORD [noalign]
		keyval* : lg.guint;
		modifiers*: GdkModifierType;
	END;

(* GdkDisplayPointerHooks struct *)

	GdkDisplayPointerHooks* = RECORD [noalign]
		get_pointer* : PROCEDURE [ccall] (display: PGdkDisplay; VAR screen: PGdkScreen; VAR x: lg.gint; y: lg.gint; VAR mask: GdkModifierType);
		window_get_pointer*: PROCEDURE [ccall] (display: PGdkDisplay; window: PGdkWindow; VAR x: lg.gint; VAR y: lg.gint; VAR mask: GdkModifierType): PGdkWindow;
		window_at_pointer* : PROCEDURE [ccall] (display: PGdkDisplay; VAR win_x: lg.gint; VAR win_y: lg.gint): PGdkWindow;
	END;

(* GdkEventAny struct *)

	GdkEventAny* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
	END;

(* GdkEventButton struct *)

	GdkEventButton* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		time*  : lg.guint32;
		x* : lg.gdouble;
		y* : lg.gdouble;
		axes*  : lg.Pgdouble;
		state* : lg.guint;
		button* : lg.guint;
		device* : PGdkDevice;
		x_root* : lg.gdouble;
		y_root* : lg.gdouble;
	END;

(* GdkEventClient struct *)

	GdkEventClient* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window*  : PGdkWindow;
		send_event* : lg.gint8;
		message_type*: GdkAtom;
		data_format* : lg.gushort;
		b* : lg.gchar;
	END;

(* GdkEventConfigure struct *)

	GdkEventConfigure* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		x* : lg.gint;
		y* : lg.gint;
		width* : lg.gint;
		height* : lg.gint;
	END;

(* GdkEventCrossing struct *)

	GdkEventCrossing* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		subwindow* : PGdkWindow;
		time*  : lg.guint32;
		x* : lg.gdouble;
		y* : lg.gdouble;
		x_root* : lg.gdouble;
		y_root* : lg.gdouble;
		mode*  : GdkCrossingMode;
		detail* : GdkNotifyType;
		focus* : lg.gboolean;
		state* : lg.guint;
	END;

(* GdkEventDND struct *)

	GdkEventDND* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		context* : PGdkDragContext;
		time*  : lg.guint32;
		x_root* : lg.gshort;
		y_root* : lg.gshort;
	END;

(* GdkEventExpose struct *)

	GdkEventExpose* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		area*  : GdkRectangle;
		region* : PGdkRegion;
		count* : lg.gint;
	END;

(* GdkEventFocus struct *)

	GdkEventFocus* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		in*  : lg.gint16;
	END;

(* GdkEventGrabBroken struct *)

	GdkEventGrabBroken* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type* : GdkEventType;
		window* : PGdkWindow;
		send_event* : lg.gint8;
		keyboard* : lg.gboolean;
		implicit* : lg.gboolean;
		grab_window*: PGdkWindow;
	END;

(* GdkEventKey struct *)

	GdkEventKey* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type* : GdkEventType;
		window* : PGdkWindow;
		send_event*  : lg.gint8;
		time* : lg.guint32;
		state* : lg.guint;
		keyval* : lg.guint;
		length* : lg.gint;
		string* : lg.Pgchar;
		hardware_keycode*: lg.guint16;
		group* : lg.guint8;
	END;

(* GdkEventMotion struct *)

	GdkEventMotion* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		time*  : lg.guint32;
		x* : lg.gdouble;
		y* : lg.gdouble;
		axes*  : lg.Pgdouble;
		state* : lg.guint;
		is_hint* : lg.gint16;
		device* : PGdkDevice;
		x_root* : lg.gdouble;
		y_root* : lg.gdouble;
	END;

(* GdkEventNoExpose struct *)

	GdkEventNoExpose* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
	END;

(* GdkEventOwnerChange struct *)

	GdkEventOwnerChange* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type* : GdkEventType;
		window*  : PGdkWindow;
		send_event* : lg.gint8;
		owner* : GdkNativeWindow;
		reason*  : GdkOwnerChange;
		selection* : GdkAtom;
		time* : lg.guint32;
		selection_time*: lg.guint32;
	END;

(* GdkEventProperty struct *)

	GdkEventProperty* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		atom*  : GdkAtom;
		time*  : lg.guint32;
		state* : lg.guint;
	END;

(* GdkEventProximity struct *)

	GdkEventProximity* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		time*  : lg.guint32;
		device* : PGdkDevice;
	END;

(* GdkEventScroll struct *)

	GdkEventScroll* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		time*  : lg.guint32;
		x* : lg.gdouble;
		y* : lg.gdouble;
		state* : lg.guint;
		direction* : GdkScrollDirection;
		device* : PGdkDevice;
		x_root* : lg.gdouble;
		y_root* : lg.gdouble;
	END;

(* GdkEventSelection struct *)

	GdkEventSelection* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		selection* : GdkAtom;
		target* : GdkAtom;
		property* : GdkAtom;
		time*  : lg.guint32;
		requestor* : GdkNativeWindow;
	END;

(* GdkEventSetting struct *)

	GdkEventSetting* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		action* : GdkSettingAction;
		name*  : lg.Pgchar;
	END;

(* GdkEventVisibility struct *)

	GdkEventVisibility* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type*  : GdkEventType;
		window* : PGdkWindow;
		send_event*: lg.gint8;
		state* : GdkVisibilityState;
	END;

(* GdkEventWindowState struct *)

	GdkEventWindowState* = EXTENSIBLE RECORD [noalign] (GdkEvent)
		type* : GdkEventType;
		window* : PGdkWindow;
		send_event*  : lg.gint8;
		changed_mask* : GdkWindowState;
		new_window_state*: GdkWindowState;
	END;

(* GdkGCValues struct *)

	GdkGCValues* = RECORD [noalign]
		foreground*  : GdkColor;
		background*  : GdkColor;
		font*  : PGdkFont;
		function* : GdkFunction;
		fill*  : GdkFill;
		tile*  : PGdkPixmap;
		stipple* : PGdkPixmap;
		clip_mask* : PGdkPixmap;
		subwindow_mode* : GdkSubwindowMode;
		ts_x_origin* : lg.gint;
		ts_y_origin* : lg.gint;
		clip_x_origin* : lg.gint;
		clip_y_origin* : lg.gint;
		graphics_exposures*: lg.gint;
		line_width*  : lg.gint;
		line_style*  : GdkLineStyle;
		cap_style* : GdkCapStyle;
		join_style*  : GdkJoinStyle;
	END;

(* GdkGeometry struct *)

	GdkGeometry* = RECORD [noalign]
		min_width* : lg.gint;
		min_height* : lg.gint;
		max_width* : lg.gint;
		max_height* : lg.gint;
		base_width* : lg.gint;
		base_height*: lg.gint;
		width_inc* : lg.gint;
		height_inc* : lg.gint;
		min_aspect* : lg.gdouble;
		max_aspect* : lg.gdouble;
		win_gravity*: GdkGravity;
	END;

(* GdkKeymapKey struct *)

	GdkKeymapKey* = RECORD [noalign]
		keycode*: lg.guint;
		group* : lg.gint;
		level* : lg.gint;
	END;

(* GdkPangoAttrEmbossed struct *)

	GdkPangoAttrEmbossed* = RECORD [noalign]
		attr* : pan.PangoAttribute;
		embossed*: lg.gboolean;
	END;

(* GdkPangoAttrStipple struct *)

	GdkPangoAttrStipple* = RECORD [noalign]
 attr* : pan.PangoAttribute;
 stipple*: PGdkBitmap;
	END;

(* GdkPixmapObject struct *)

	GdkPixmapObject* = RECORD [noalign]
		parent_instance*: GdkDrawable;
		impl* : PGdkDrawable;
		depth* : lg.gint;
	END;

(* GdkPoint struct *)

	GdkPoint* = RECORD [noalign]
		x*: lg.gint;
		y*: lg.gint;
	END;

(* GdkPointerHooks struct *)

	GdkPointerHooks* = RECORD [noalign]
		get_pointer*  : PROCEDURE [ccall] (window: PGdkWindow; VAR x, y: lg.gint; VAR mask: GdkModifierType): PGdkWindow;
		window_at_pointer*: PROCEDURE [ccall] (screen: PGdkScreen; VAR win_x, win_y: lg.gint): PGdkWindow;
	END;

(* GdkRegion struct *)

	GdkRegion* = RECORD [noalign]
		size* : lg.glong;
		numRects*: lg.glong;
		rects* : PGdkRegionBox;
		extents* : GdkRegionBox;
	END;

(* GdkRegionBox struct *)

	GdkRegionBox* = RECORD [noalign]
		x1*: lg.gint;
		y1*: lg.gint;
		x2*: lg.gint;
		y2*: lg.gint;
	END;

(* GdkRgbCmap struct *)

	GdkRgbCmap* = RECORD [noalign]
		colors* : lg.guint32;
		n_colors* : lg.gint;
		info_list*: lg.PGSList;
	END;

(* GdkSegment struct *)

	GdkSegment* = RECORD [noalign]
		x1*: lg.gint;
		y1*: lg.gint;
		x2*: lg.gint;
		y2*: lg.gint;
	END;

(* GdkSpan struct *)

	GdkSpan* = RECORD [noalign]
		x* : lg.gint;
		y* : lg.gint;
		width*: lg.gint;
	END;

(* GdkTimeCoord struct *)

	GdkTimeCoord* = RECORD [noalign]
		time*: lg.guint32;
		axes*: lg.gdouble;
	END;

(* GdkTrapezoid struct *)

	GdkTrapezoid* = RECORD [noalign]
		y1* : lg.gdouble;
		x11*: lg.gdouble;
		x21*: lg.gdouble;
		y2* : lg.gdouble;
		x12*: lg.gdouble;
		x22*: lg.gdouble;
	END;

(* GdkWindowAttr struct *)

	GdkWindowAttr* = RECORD [noalign]
		title* : lg.Pgchar;
		event_mask* : lg.gint;
		x*  : lg.gint;
		y*  : lg.gint;
		width* : lg.gint;
		height* : lg.gint;
		wclass* : GdkWindowClass;
		visual* : PGdkVisual;
		colormap* : PGdkColormap;
		window_type*  : GdkWindowType;
		cursor* : PGdkCursor;
		wmclass_name* : lg.Pgchar;
		wmclass_class* : lg.Pgchar;
		override_redirect*: lg.gboolean;
	END;

(* Flags of GdkWindowObject struct *)

CONST
	GdkWindowObject_flag0_guffaw_gravity_Mask* = {0};
	GdkWindowObject_flag0_input_only_Mask* = {1};
	GdkWindowObject_flag0_modal_hint_Mask* = {2};
	GdkWindowObject_flag0_destroyed_Mask* = {3..4};
	GdkWindowObject_flag0_accept_focus_Mask* = {5};
	GdkWindowObject_flag0_focus_on_map_Mask* = {6};
	GdkWindowObject_flag0_shaped_Mask*  = {7};

TYPE

(* GdkWindowObject struct *)

	GdkWindowObject* = RECORD [noalign]
		parent_instance* : GdkDrawable;
		impl* : PGdkDrawable;
		parent* : PGdkWindowObject;
		user_data* : lg.gpointer;
		x* : lg.gint;
		y* : lg.gint;
		extension_events* : lg.gint;
		filters* : lg.PGList;
		children* : lg.PGList;
		bg_color* : GdkColor;
		bg_pixmap* : PGdkPixmap;
		paint_stack*  : lg.PGSList;
		update_area*  : PGdkRegion;
		update_freeze_count*: lg.guint;
		window_type*  : lg.guint8;
		depth*  : lg.guint8;
		resize_count* : lg.guint8;
		state*  : GdkWindowState;
		flag0*  : SET;
	END;

(* POINTBLOCK struct *)

	POINTBLOCK* = RECORD [noalign]
	END;

(* ScanLineList struct *)

	ScanLineList* = RECORD [noalign]
	END;

(* ScanLineListBlock struct *)

	ScanLineListBlock* = RECORD [noalign]
	END;

(* GdkAtom struct *)

	GdkAtom* = RECORD [noalign]
	END;

(* =================================================================== BOXEDS *)
TYPE

(* GdkColor boxed *)

	GdkColor* = RECORD [noalign]
		pixel*: lg.guint32;
		red* : lg.guint16;
		green*: lg.guint16;
		blue* : lg.guint16;
	END;

(* GdkCursor boxed *)

	GdkCursor* = RECORD [noalign]
		type* : GdkCursorType;
		ref_count*: lg.guint;
	END;

(* GdkFont boxed *)

	GdkFont* = RECORD [noalign]
		type* : GdkFontType;
		ascent* : lg.gint;
		descent*: lg.gint;
	END;

(* GdkRectangle boxed *)

	GdkRectangle* = RECORD [noalign]
		x* : lg.gint;
		y* : lg.gint;
		width* : lg.gint;
		height*: lg.gint;
	END;

(*	================================================================= OBJECTS *)
TYPE

(* GdkDrawable object *)

	GdkDrawable* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* GdkColormap object *)

	GdkColormap* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		size* : lg.gint;
		colors* : POINTER TO ARRAY [untagged] OF PGdkColor;
		visual* : PGdkVisual;
		windowing_data* : lg.gpointer;
	END;

(* GdkDevice object *)

	GdkDevice* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		name* : lg.Pgchar;
		source* : GdkInputSource;
		mode* : GdkInputMode;
		has_cursor* : lg.gboolean;
		num_axes* : lg.gint;
		axes* : PGdkDeviceAxis;
		num_keys* : lg.gint;
		keys* : PGdkDeviceKey;
	END;

(* Flags of GdkDisplay object *)

CONST
	GdkDisplay_flag0_closed_Mask* = {0};

TYPE

(* GdkDisplay object *)

	GdkDisplay* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		queued_events*  : lg.PGList;
		queued_tail* : lg.PGList;
		button_click_time* : lg.guint32;
		button_window*  : PGdkWindow;
		button_number*  : lg.gint;
		double_click_time* : lg.guint;
		core_pointer* : PGdkDevice;
		pointer_hooks*  : PGdkDisplayPointerHooks;
		GdkDisplay_flag0* : SET;
		button_x* : lg.gint;
		button_y* : lg.gint;
	END;

(* GdkDisplayManager object *)

	GdkDisplayManager* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* GdkDragContext object *)

	GdkDragContext* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		protocol* : GdkDragProtocol;
		is_source* : lg.gboolean;
		source_window* : PGdkWindow;
		dest_window* : PGdkWindow;
		targets* : lg.PGList;
		actions* : GdkDragAction;
		suggested_action* : GdkDragAction;
		action*  : GdkDragAction;
		start_time* : lg.guint32;
		windowing_data*  : lg.gpointer;
	END;

(* GdkGC object *)

	GdkGC* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		clip_x_origin*: lg.gint;
		clip_y_origin*: lg.gint;
		ts_x_origin* : lg.gint;
		ts_y_origin* : lg.gint;
		colormap* : PGdkColormap;
	END;

(* GdkImage object *)

	GdkImage* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		type* : GdkImageType;
		visual*  : PGdkVisual;
		byte_order* : GdkByteOrder;
		width* : lg.gint;
		height*  : lg.gint;
		depth* : lg.guint16;
		bpp* : lg.guint16;
		bpl* : lg.guint16;
		bits_per_pixel*: lg.guint16;
		mem* : lg.gpointer;
		colormap*  : PGdkColormap;
		windowing_data*: lg.gpointer;
	END;

(* GdkKeymap object *)

	GdkKeymap* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		display*  : PGdkDisplay;
	END;

(* GdkPangoRenderer object *)

	GdkPangoRenderer* = EXTENSIBLE RECORD [noalign] (pan.PangoRenderer)
		GdkPangoRenderer_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GdkScreen object *)

CONST
	GdkScreen_flag0_closed_Mask* = {0};

TYPE

(* GdkScreen object *)

	GdkScreen* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		GdkScreen_flag0*: SET;
		exposure_gcs* : PGdkGC;
		font_options*: ANYPTR (* cairo_font_options_t *);
		resolution* : lg.gdouble;
	END;

(* GdkVisual object *)

	GdkVisual* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		type* : GdkVisualType;
		depth* : lg.gint;
		byte_order* : GdkByteOrder;
		colormap_size* : lg.gint;
		bits_per_rgb*: lg.gint;
		red_mask*: lg.guint32;
		red_shift*: lg.gint;
		red_prec* : lg.gint;
		green_mask*: lg.guint32;
		green_shift*: lg.gint;
		green_prec*: lg.gint;
		blue_mask* : lg.guint32;
		blue_shift*: lg.gint;
		blue_prec*: lg.gint;
	END;

(* GdkBitmap object *)

	GdkBitmap* = EXTENSIBLE RECORD [noalign] 
 parent_instance*: lo.GObject;
	END;

(* GdkPixmap object *)

	GdkPixmap* = EXTENSIBLE RECORD [noalign] (GdkDrawable)
	END;

(* GdkWindow object *)

	GdkWindow* = EXTENSIBLE RECORD [noalign] (GdkDrawable)
	END;

(* GdkDrawable methods *)
	PROCEDURE [ccall] gdk_draw_arc*(drawable: PGdkDrawable; gc: PGdkGC; filled: lg.gboolean; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; angle1: lg.gint; angle2: lg.gint);
	PROCEDURE [ccall] gdk_draw_drawable*(drawable: PGdkDrawable; gc: PGdkGC; src: PGdkDrawable; xsrc: lg.gint; ysrc: lg.gint; xdest: lg.gint; ydest: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_draw_glyphs*(drawable: PGdkDrawable; gc: PGdkGC; font: pan.PPangoFont; x: lg.gint; y: lg.gint; glyphs: pan.PPangoGlyphString);
	PROCEDURE [ccall] gdk_draw_glyphs_transformed*(drawable: PGdkDrawable; gc: PGdkGC; matrix: pan.PPangoMatrix; font: pan.PPangoFont; x: lg.gint; y: lg.gint; glyphs: pan.PPangoGlyphString);
	PROCEDURE [ccall] gdk_draw_gray_image*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; dith: GdkRgbDither; buf: lg.Pguchar; rowstride: lg.gint);
	PROCEDURE [ccall] gdk_draw_image*(drawable: PGdkDrawable; gc: PGdkGC; image: PGdkImage; xsrc: lg.gint; ysrc: lg.gint; xdest: lg.gint; ydest: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_draw_indexed_image*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; dith: GdkRgbDither; buf: lg.Pguchar; rowstride: lg.gint; cmap: PGdkRgbCmap);
	PROCEDURE [ccall] gdk_draw_layout*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; layout: pan.PPangoLayout);
	PROCEDURE [ccall] gdk_draw_layout_line*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; line: pan.PPangoLayoutLine);
	PROCEDURE [ccall] gdk_draw_layout_line_with_colors*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; line: pan.PPangoLayoutLine; foreground: PGdkColor; background: PGdkColor);
	PROCEDURE [ccall] gdk_draw_layout_with_colors*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; layout: pan.PPangoLayout; foreground: PGdkColor; background: PGdkColor);
	PROCEDURE [ccall] gdk_draw_line*(drawable: PGdkDrawable; gc: PGdkGC; x1_: lg.gint; y1_: lg.gint; x2_: lg.gint; y2_: lg.gint);
	PROCEDURE [ccall] gdk_draw_lines*(drawable: PGdkDrawable; gc: PGdkGC; points: PGdkPoint; npoints: lg.gint);
	PROCEDURE [ccall] gdk_draw_pixbuf*(drawable: PGdkDrawable; gc: PGdkGC; pixbuf: gpb.PGdkPixbuf; src_x: lg.gint; src_y: lg.gint; dest_x: lg.gint; dest_y: lg.gint; width: lg.gint; height: lg.gint; dither: GdkRgbDither; x_dither: lg.gint; y_dither: lg.gint);
	PROCEDURE [ccall] gdk_draw_point*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gdk_draw_points*(drawable: PGdkDrawable; gc: PGdkGC; points: PGdkPoint; n_points: lg.gint);
	PROCEDURE [ccall] gdk_draw_polygon*(drawable: PGdkDrawable; gc: PGdkGC; filled: lg.gboolean; points: PGdkPoint; npoints: lg.gint);
	PROCEDURE [ccall] gdk_draw_rectangle*(drawable: PGdkDrawable; gc: PGdkGC; filled: lg.gboolean; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_draw_rgb_32_image*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; dith: GdkRgbDither; buf: lg.Pguchar; rowstride: lg.gint);
	PROCEDURE [ccall] gdk_draw_rgb_32_image_dithalign*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; dith: GdkRgbDither; buf: lg.Pguchar; rowstride: lg.gint; xdith: lg.gint; ydith: lg.gint);
	PROCEDURE [ccall] gdk_draw_rgb_image*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; dith: GdkRgbDither; rgb_buf: lg.Pguchar; rowstride: lg.gint);
	PROCEDURE [ccall] gdk_draw_rgb_image_dithalign*(drawable: PGdkDrawable; gc: PGdkGC; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; dith: GdkRgbDither; rgb_buf: lg.Pguchar; rowstride: lg.gint; xdith: lg.gint; ydith: lg.gint);
	PROCEDURE [ccall] gdk_draw_segments*(drawable: PGdkDrawable; gc: PGdkGC; segs: PGdkSegment; nsegs: lg.gint);
	PROCEDURE [ccall] gdk_draw_trapezoids*(drawable: PGdkDrawable; gc: PGdkGC; trapezoids: PGdkTrapezoid; n_trapezoids: lg.gint);
	PROCEDURE [ccall] gdk_drawable_copy_to_image*(drawable: PGdkDrawable; image: PGdkImage; src_x: lg.gint; src_y: lg.gint; dest_x: lg.gint; dest_y: lg.gint; width: lg.gint; height: lg.gint): PGdkImage;
	PROCEDURE [ccall] gdk_drawable_get_clip_region*(drawable: PGdkDrawable): PGdkRegion;
	PROCEDURE [ccall] gdk_drawable_get_colormap*(drawable: PGdkDrawable): PGdkColormap;
	PROCEDURE [ccall] gdk_drawable_get_depth*(drawable: PGdkDrawable): lg.gint;
	PROCEDURE [ccall] gdk_drawable_get_display*(drawable: PGdkDrawable): PGdkDisplay;
	PROCEDURE [ccall] gdk_drawable_get_image*(drawable: PGdkDrawable; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint): PGdkImage;
	PROCEDURE [ccall] gdk_drawable_get_screen*(drawable: PGdkDrawable): PGdkScreen;
	PROCEDURE [ccall] gdk_drawable_get_size*(drawable: PGdkDrawable; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gdk_drawable_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_drawable_get_visible_region*(drawable: PGdkDrawable): PGdkRegion;
	PROCEDURE [ccall] gdk_drawable_get_visual*(drawable: PGdkDrawable): PGdkVisual;
	PROCEDURE [ccall] gdk_drawable_set_colormap*(drawable: PGdkDrawable; colormap: PGdkColormap);

(* GdkColormap methods *)
	PROCEDURE [ccall] gdk_colormap_alloc_color*(colormap: PGdkColormap; color: PGdkColor; writeable: lg.gboolean; best_match: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gdk_colormap_alloc_colors*(colormap: PGdkColormap; colors: PGdkColor; ncolors: lg.gint; writeable: lg.gboolean; best_match: lg.gboolean; VAR success: lg.gboolean): lg.gint;
	PROCEDURE [ccall] gdk_colormap_free_colors*(colormap: PGdkColormap; colors: PGdkColor; ncolors: lg.gint);
	PROCEDURE [ccall] gdk_colormap_get_screen*(colormap: PGdkColormap): PGdkScreen;
	PROCEDURE [ccall] gdk_colormap_get_system*(): PGdkColormap;
	PROCEDURE [ccall] gdk_colormap_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_colormap_get_visual*(colormap: PGdkColormap): PGdkVisual;
	PROCEDURE [ccall] gdk_colormap_query_color*(colormap: PGdkColormap; pixel: lg.gulong; result: PGdkColor);

(* GdkColormap constructors *)
	PROCEDURE [ccall] gdk_colormap_new*(visual: PGdkVisual; allocate: lg.gboolean): PGdkColormap;

(* GdkDevice methods *)
	PROCEDURE [ccall] gdk_device_free_history*(events: POINTER TO ARRAY [untagged] OF PGdkTimeCoord; n_events: lg.gint);
	PROCEDURE [ccall] gdk_device_get_axis*(device: PGdkDevice; axes: lg.Pgdouble; use: GdkAxisUse; value: lg.Pgdouble): lg.gboolean;
	PROCEDURE [ccall] gdk_device_get_core_pointer*(): PGdkDevice;
	PROCEDURE [ccall] gdk_device_get_history*(device: PGdkDevice; window: PGdkWindow; start: lg.guint32; stop: lg.guint32; VAR events: POINTER TO ARRAY [untagged] OF PGdkTimeCoord; n_events: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gdk_device_get_state*(device: PGdkDevice; window: PGdkWindow; axes: lg.Pgdouble; VAR mask: GdkModifierType);
	PROCEDURE [ccall] gdk_device_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_device_set_axis_use*(device: PGdkDevice; index_: lg.guint; use: GdkAxisUse);
	PROCEDURE [ccall] gdk_device_set_key*(device: PGdkDevice; index_: lg.guint; keyval: lg.guint; modifiers: GdkModifierType);
	PROCEDURE [ccall] gdk_device_set_mode*(device: PGdkDevice; mode: GdkInputMode): lg.gboolean;
	PROCEDURE [ccall] gdk_device_set_source*(device: PGdkDevice; source: GdkInputSource);

(* GdkDisplay methods *)
	PROCEDURE [ccall] gdk_display_add_client_message_filter*(display: PGdkDisplay; message_type: GdkAtom; func: GdkFilterFunc; data: lg.gpointer);
	PROCEDURE [ccall] gdk_display_beep*(display: PGdkDisplay);
	PROCEDURE [ccall] gdk_display_close*(display: PGdkDisplay);
	PROCEDURE [ccall] gdk_display_flush*(display: PGdkDisplay);
	PROCEDURE [ccall] gdk_display_get_core_pointer*(display: PGdkDisplay): PGdkDevice;
	PROCEDURE [ccall] gdk_display_get_default*(): PGdkDisplay;
	PROCEDURE [ccall] gdk_display_get_default_cursor_size*(display: PGdkDisplay): lg.guint;
	PROCEDURE [ccall] gdk_display_get_default_group*(display: PGdkDisplay): PGdkWindow;
	PROCEDURE [ccall] gdk_display_get_default_screen*(display: PGdkDisplay): PGdkScreen;
	PROCEDURE [ccall] gdk_display_get_event*(display: PGdkDisplay): PGdkEvent;
	PROCEDURE [ccall] gdk_display_get_maximal_cursor_size*(display: PGdkDisplay; VAR width, height: lg.guint);
	PROCEDURE [ccall] gdk_display_get_n_screens*(display: PGdkDisplay): lg.gint;
	PROCEDURE [ccall] gdk_display_get_name*(display: PGdkDisplay): lg.Pgchar;
	PROCEDURE [ccall] gdk_display_get_pointer*(display: PGdkDisplay; VAR screen: PGdkScreen; VAR x, y: lg.gint; VAR mask: GdkModifierType);
	PROCEDURE [ccall] gdk_display_get_screen*(display: PGdkDisplay; screen_num: lg.gint): PGdkScreen;
	PROCEDURE [ccall] gdk_display_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_display_get_window_at_pointer*(display: PGdkDisplay; win_x: lg.Pgint; win_y: lg.Pgint): PGdkWindow;
	PROCEDURE [ccall] gdk_display_keyboard_ungrab*(display: PGdkDisplay; time_: lg.guint32);
	PROCEDURE [ccall] gdk_display_list_devices*(display: PGdkDisplay): lg.PGList;
	PROCEDURE [ccall] gdk_display_open*(display_name: lg.Pgchar): PGdkDisplay;
	PROCEDURE [ccall] gdk_display_open_default_libgtk_only*(): PGdkDisplay;
	PROCEDURE [ccall] gdk_display_peek_event*(display: PGdkDisplay): PGdkEvent;
	PROCEDURE [ccall] gdk_display_pointer_is_grabbed*(display: PGdkDisplay): lg.gboolean;
	PROCEDURE [ccall] gdk_display_pointer_ungrab*(display: PGdkDisplay; time_: lg.guint32);
	PROCEDURE [ccall] gdk_display_put_event*(display: PGdkDisplay; event: PGdkEvent);
	PROCEDURE [ccall] gdk_display_request_selection_notification*(display: PGdkDisplay; selection: GdkAtom): lg.gboolean;
	PROCEDURE [ccall] gdk_display_set_double_click_distance*(display: PGdkDisplay; distance: lg.guint);
	PROCEDURE [ccall] gdk_display_set_double_click_time*(display: PGdkDisplay; msec: lg.guint);
	PROCEDURE [ccall] gdk_display_set_pointer_hooks*(display: PGdkDisplay; new_hooks: PGdkDisplayPointerHooks): PGdkDisplayPointerHooks;
	PROCEDURE [ccall] gdk_display_store_clipboard*(display: PGdkDisplay; clipboard_window: PGdkWindow; time_: lg.guint32; targets: PGdkAtom; n_targets: lg.gint);
	PROCEDURE [ccall] gdk_display_supports_clipboard_persistence*(display: PGdkDisplay): lg.gboolean;
	PROCEDURE [ccall] gdk_display_supports_cursor_alpha*(display: PGdkDisplay): lg.gboolean;
	PROCEDURE [ccall] gdk_display_supports_cursor_color*(display: PGdkDisplay): lg.gboolean;
	PROCEDURE [ccall] gdk_display_supports_selection_notification*(display: PGdkDisplay): lg.gboolean;
	PROCEDURE [ccall] gdk_display_sync*(display: PGdkDisplay);
	PROCEDURE [ccall] gdk_display_warp_pointer*(display: PGdkDisplay; screen: PGdkScreen; x: lg.gint; y: lg.gint);

(* GdkDisplayManager methods *)
	PROCEDURE [ccall] gdk_display_manager_get*(): PGdkDisplayManager;
	PROCEDURE [ccall] gdk_display_manager_get_default_display*(displaymanager: PGdkDisplayManager): PGdkDisplay;
	PROCEDURE [ccall] gdk_display_manager_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_display_manager_list_displays*(displaymanager: PGdkDisplayManager): lg.PGSList;
	PROCEDURE [ccall] gdk_display_manager_set_default_display*(displaymanager: PGdkDisplayManager; display: PGdkDisplay);

(* GdkDragContext methods *)
	PROCEDURE [ccall] gdk_drag_context_get_type*(): lo.GType;

(* GdkDragContext constructors *)
	PROCEDURE [ccall] gdk_drag_context_new*(): PGdkDragContext;

(* GdkGC methods *)
	PROCEDURE [ccall] gdk_gc_copy*(gc: PGdkGC; src_gc: PGdkGC);
	PROCEDURE [ccall] gdk_gc_get_colormap*(gc: PGdkGC): PGdkColormap;
	PROCEDURE [ccall] gdk_gc_get_screen*(gc: PGdkGC): PGdkScreen;
	PROCEDURE [ccall] gdk_gc_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_gc_get_values*(gc: PGdkGC; values: PGdkGCValues);
	PROCEDURE [ccall] gdk_gc_offset*(gc: PGdkGC; x_offset: lg.gint; y_offset: lg.gint);
	PROCEDURE [ccall] gdk_gc_set_background*(gc: PGdkGC; color: PGdkColor);
	PROCEDURE [ccall] gdk_gc_set_clip_mask*(gc: PGdkGC; mask: PGdkBitmap);
	PROCEDURE [ccall] gdk_gc_set_clip_origin*(gc: PGdkGC; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gdk_gc_set_clip_rectangle*(gc: PGdkGC; rectangle: PGdkRectangle);
	PROCEDURE [ccall] gdk_gc_set_clip_region*(gc: PGdkGC; region: PGdkRegion);
	PROCEDURE [ccall] gdk_gc_set_colormap*(gc: PGdkGC; colormap: PGdkColormap);
	PROCEDURE [ccall] gdk_gc_set_dashes*(gc: PGdkGC; dash_offset: lg.gint; dash_list: lg.gint8; n: lg.gint);
	PROCEDURE [ccall] gdk_gc_set_exposures*(gc: PGdkGC; exposures: lg.gboolean);
	PROCEDURE [ccall] gdk_gc_set_fill*(gc: PGdkGC; fill: GdkFill);
	PROCEDURE [ccall] gdk_gc_set_foreground*(gc: PGdkGC; color: PGdkColor);
	PROCEDURE [ccall] gdk_gc_set_function*(gc: PGdkGC; function: GdkFunction);
	PROCEDURE [ccall] gdk_gc_set_line_attributes*(gc: PGdkGC; line_width: lg.gint; line_style: GdkLineStyle; cap_style: GdkCapStyle; join_style: GdkJoinStyle);
	PROCEDURE [ccall] gdk_gc_set_rgb_bg_color*(gc: PGdkGC; color: PGdkColor);
	PROCEDURE [ccall] gdk_gc_set_rgb_fg_color*(gc: PGdkGC; color: PGdkColor);
	PROCEDURE [ccall] gdk_gc_set_stipple*(gc: PGdkGC; stipple: PGdkPixmap);
	PROCEDURE [ccall] gdk_gc_set_subwindow*(gc: PGdkGC; mode: GdkSubwindowMode);
	PROCEDURE [ccall] gdk_gc_set_tile*(gc: PGdkGC; tile: PGdkPixmap);
	PROCEDURE [ccall] gdk_gc_set_ts_origin*(gc: PGdkGC; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gdk_gc_set_values*(gc: PGdkGC; values: PGdkGCValues; values_mask: GdkGCValuesMask);

(* GdkGC constructors *)
	PROCEDURE [ccall] gdk_gc_new*(drawable: PGdkDrawable): PGdkGC;
	PROCEDURE [ccall] gdk_gc_new_with_values*(drawable: PGdkDrawable; values: PGdkGCValues; values_mask: GdkGCValuesMask): PGdkGC;

(* GdkImage methods *)
	PROCEDURE [ccall] gdk_image_get_colormap*(image: PGdkImage): PGdkColormap;
	PROCEDURE [ccall] gdk_image_get_pixel*(image: PGdkImage; x: lg.gint; y: lg.gint): lg.guint32;
	PROCEDURE [ccall] gdk_image_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_image_put_pixel*(image: PGdkImage; x: lg.gint; y: lg.gint; pixel: lg.guint32);
	PROCEDURE [ccall] gdk_image_set_colormap*(image: PGdkImage; colormap: PGdkColormap);

(* GdkImage constructors *)
	PROCEDURE [ccall] gdk_image_new*(type: GdkImageType; visual: PGdkVisual; width: lg.gint; height: lg.gint): PGdkImage;

(* GdkKeymap methods *)
	PROCEDURE [ccall] gdk_keymap_get_default*(): PGdkKeymap;
	PROCEDURE [ccall] gdk_keymap_get_direction*(keymap: PGdkKeymap): pan.PangoDirection;
	PROCEDURE [ccall] gdk_keymap_get_entries_for_keycode*(keymap: PGdkKeymap; hardware_keycode: lg.guint; VAR keys: POINTER TO ARRAY [untagged] OF GdkKeymapKey; VAR keyvals: POINTER TO ARRAY [untagged] OF lg.guint; n_entries: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gdk_keymap_get_entries_for_keyval*(keymap: PGdkKeymap; keyval: lg.guint; VAR keys: POINTER TO ARRAY [untagged] OF GdkKeymapKey; n_keys: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gdk_keymap_get_for_display*(display: PGdkDisplay): PGdkKeymap;
	PROCEDURE [ccall] gdk_keymap_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_keymap_lookup_key*(keymap: PGdkKeymap; key: PGdkKeymapKey): lg.guint;
	PROCEDURE [ccall] gdk_keymap_translate_keyboard_state*(keymap: PGdkKeymap; hardware_keycode: lg.guint; state: GdkModifierType; group: lg.gint; VAR keyval: lg.guint; VAR effective_group: lg.gint; VAR level: lg.gint; VAR consumed_modifiers: GdkModifierType): lg.gboolean;

(* GdkPangoRenderer methods *)
	PROCEDURE [ccall] gdk_pango_renderer_get_default*(screen: PGdkScreen): PGdkPangoRenderer;
	PROCEDURE [ccall] gdk_pango_renderer_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_pango_renderer_set_drawable*(pangorenderer: PGdkPangoRenderer; drawable: PGdkDrawable);
	PROCEDURE [ccall] gdk_pango_renderer_set_gc*(pangorenderer: PGdkPangoRenderer; gc: PGdkGC);
	PROCEDURE [ccall] gdk_pango_renderer_set_override_color*(pangorenderer: PGdkPangoRenderer; part: pan.PangoRenderPart; color: PGdkColor);
	PROCEDURE [ccall] gdk_pango_renderer_set_stipple*(pangorenderer: PGdkPangoRenderer; part: pan.PangoRenderPart; stipple: PGdkBitmap);

(* GdkPangoRenderer constructors *)
	PROCEDURE [ccall] gdk_pango_renderer_new*(screen: PGdkScreen): PGdkPangoRenderer;

(* GdkScreen methods *)
	PROCEDURE [ccall] gdk_screen_broadcast_client_message*(screen: PGdkScreen; event: PGdkEvent);
	PROCEDURE [ccall] gdk_screen_get_default*(): PGdkScreen;
	PROCEDURE [ccall] gdk_screen_get_default_colormap*(screen: PGdkScreen): PGdkColormap;
	PROCEDURE [ccall] gdk_screen_get_display*(screen: PGdkScreen): PGdkDisplay;
	PROCEDURE [ccall] gdk_screen_get_font_options_libgtk_only*(screen: PGdkScreen): ANYPTR (* cairo_font_options_t *);
	PROCEDURE [ccall] gdk_screen_get_height*(screen: PGdkScreen): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_height_mm*(screen: PGdkScreen): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_monitor_at_point*(screen: PGdkScreen; x: lg.gint; y: lg.gint): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_monitor_at_window*(screen: PGdkScreen; window: PGdkWindow): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_monitor_geometry*(screen: PGdkScreen; monitor_num: lg.gint; dest: PGdkRectangle);
	PROCEDURE [ccall] gdk_screen_get_n_monitors*(screen: PGdkScreen): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_number*(screen: PGdkScreen): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_resolution_libgtk_only*(screen: PGdkScreen): lg.gdouble;
	PROCEDURE [ccall] gdk_screen_get_rgb_colormap*(screen: PGdkScreen): PGdkColormap;
	PROCEDURE [ccall] gdk_screen_get_rgb_visual*(screen: PGdkScreen): PGdkVisual;
	PROCEDURE [ccall] gdk_screen_get_rgba_colormap*(screen: PGdkScreen): PGdkColormap;
	PROCEDURE [ccall] gdk_screen_get_rgba_visual*(screen: PGdkScreen): PGdkVisual;
	PROCEDURE [ccall] gdk_screen_get_root_window*(screen: PGdkScreen): PGdkWindow;
	PROCEDURE [ccall] gdk_screen_get_setting*(screen: PGdkScreen; name: lg.Pgchar; value: lo.PGValue): lg.gboolean;
	PROCEDURE [ccall] gdk_screen_get_system_colormap*(screen: PGdkScreen): PGdkColormap;
	PROCEDURE [ccall] gdk_screen_get_system_visual*(screen: PGdkScreen): PGdkVisual;
	PROCEDURE [ccall] gdk_screen_get_toplevel_windows*(screen: PGdkScreen): lg.PGList;
	PROCEDURE [ccall] gdk_screen_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_screen_get_width*(screen: PGdkScreen): lg.gint;
	PROCEDURE [ccall] gdk_screen_get_width_mm*(screen: PGdkScreen): lg.gint;
	PROCEDURE [ccall] gdk_screen_height*(): lg.gint;
	PROCEDURE [ccall] gdk_screen_height_mm*(): lg.gint;
	PROCEDURE [ccall] gdk_screen_list_visuals*(screen: PGdkScreen): lg.PGList;
	PROCEDURE [ccall] gdk_screen_make_display_name*(screen: PGdkScreen): lg.Pgchar;
	PROCEDURE [ccall] gdk_screen_set_default_colormap*(screen: PGdkScreen; colormap: PGdkColormap);
	PROCEDURE [ccall] gdk_screen_set_font_options_libgtk_only*(screen: PGdkScreen; options: ANYPTR (* cairo_font_options_t *));
	PROCEDURE [ccall] gdk_screen_set_resolution_libgtk_only*(screen: PGdkScreen; dpi: lg.gdouble);
	PROCEDURE [ccall] gdk_screen_width*(): lg.gint;
	PROCEDURE [ccall] gdk_screen_width_mm*(): lg.gint;

(* GdkVisual methods *)
	PROCEDURE [ccall] gdk_visual_get_best*(): PGdkVisual;
	PROCEDURE [ccall] gdk_visual_get_best_depth*(): lg.gint;
	PROCEDURE [ccall] gdk_visual_get_best_type*(): GdkVisualType;
	PROCEDURE [ccall] gdk_visual_get_best_with_both*(depth: lg.gint; visual_type: GdkVisualType): PGdkVisual;
	PROCEDURE [ccall] gdk_visual_get_best_with_depth*(depth: lg.gint): PGdkVisual;
	PROCEDURE [ccall] gdk_visual_get_best_with_type*(visual_type: GdkVisualType): PGdkVisual;
	PROCEDURE [ccall] gdk_visual_get_screen*(visual: PGdkVisual): PGdkScreen;
	PROCEDURE [ccall] gdk_visual_get_system*(): PGdkVisual;
	PROCEDURE [ccall] gdk_visual_get_type*(): lo.GType;

(* GdkPixmap methods *)
	PROCEDURE [ccall] gdk_pixmap_colormap_create_from_xpm*(drawable: PGdkDrawable; colormap: PGdkColormap; VAR mask: PGdkBitmap; transparent_color: PGdkColor; filename: lg.Pgchar): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_colormap_create_from_xpm_d*(drawable: PGdkDrawable; colormap: PGdkColormap; VAR mask: PGdkBitmap; transparent_color: PGdkColor; VAR data: lg.Pgchar): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_create_from_data*(drawable: PGdkDrawable; data: lg.Pgchar; width: lg.gint; height: lg.gint; depth: lg.gint; fg: PGdkColor; bg: PGdkColor): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_create_from_xpm*(drawable: PGdkDrawable; VAR mask: PGdkBitmap; transparent_color: PGdkColor; filename: lg.Pgchar): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_create_from_xpm_d*(drawable: PGdkDrawable; VAR mask: PGdkBitmap; transparent_color: PGdkColor; VAR data: lg.Pgchar): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_foreign_new*(anid: GdkNativeWindow): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_foreign_new_for_display*(display: PGdkDisplay; anid: GdkNativeWindow): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_get_type*(): lo.GType;
	PROCEDURE [ccall] gdk_pixmap_lookup*(anid: GdkNativeWindow): PGdkPixmap;
	PROCEDURE [ccall] gdk_pixmap_lookup_for_display*(display: PGdkDisplay; anid: GdkNativeWindow): PGdkPixmap;
	PROCEDURE [ccall] gdk_bitmap_create_from_data*(drawable: PGdkDrawable; data: lg.Pgchar; width: lg.gint; height: lg.gint): PGdkBitmap;

(* GdkPixmap constructors *)
	PROCEDURE [ccall] gdk_pixmap_new*(drawable: PGdkDrawable; width: lg.gint; height: lg.gint; depth: lg.gint): PGdkPixmap;

(* GdkWindow methods *)
	PROCEDURE [ccall] gdk_window_add_filter*(window: PGdkWindow; function: GdkFilterFunc; data: lg.gpointer);
	PROCEDURE [ccall] gdk_window_at_pointer*(win_x: lg.Pgint; win_y: lg.Pgint): PGdkWindow;
	PROCEDURE [ccall] gdk_window_begin_move_drag*(window: PGdkWindow; button: lg.gint; root_x: lg.gint; root_y: lg.gint; timestamp: lg.guint32);
	PROCEDURE [ccall] gdk_window_begin_paint_rect*(window: PGdkWindow; rectangle: PGdkRectangle);
	PROCEDURE [ccall] gdk_window_begin_paint_region*(window: PGdkWindow; region: PGdkRegion);
	PROCEDURE [ccall] gdk_window_begin_resize_drag*(window: PGdkWindow; edge: GdkWindowEdge; button: lg.gint; root_x: lg.gint; root_y: lg.gint; timestamp: lg.guint32);
	PROCEDURE [ccall] gdk_window_clear*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_clear_area*(window: PGdkWindow; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_window_clear_area_e*(window: PGdkWindow; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_window_configure_finished*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_constrain_size*(geometry: PGdkGeometry; flags: lg.guint; width: lg.gint; height: lg.gint; new_width: lg.Pgint; new_height: lg.Pgint);
	PROCEDURE [ccall] gdk_window_deiconify*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_destroy*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_enable_synchronized_configure*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_end_paint*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_focus*(window: PGdkWindow; timestamp: lg.guint32);
	PROCEDURE [ccall] gdk_window_foreign_new*(anid: GdkNativeWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_foreign_new_for_display*(display: PGdkDisplay; anid: GdkNativeWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_freeze_updates*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_fullscreen*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_get_children*(window: PGdkWindow): lg.PGList;
	PROCEDURE [ccall] gdk_window_get_decorations*(window: PGdkWindow; VAR decorations: GdkWMDecoration): lg.gboolean;
	PROCEDURE [ccall] gdk_window_get_events*(window: PGdkWindow): GdkEventMask;
	PROCEDURE [ccall] gdk_window_get_frame_extents*(window: PGdkWindow; rect: PGdkRectangle);
	PROCEDURE [ccall] gdk_window_get_geometry*(window: PGdkWindow; x: lg.Pgint; y: lg.Pgint; width: lg.Pgint; height: lg.Pgint; depth: lg.Pgint);
	PROCEDURE [ccall] gdk_window_get_group*(window: PGdkWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_get_internal_paint_info*(window: PGdkWindow; VAR real_drawable: PGdkDrawable; VAR x_offset, y_offset: lg.gint);
	PROCEDURE [ccall] gdk_window_get_origin*(window: PGdkWindow; x: lg.Pgint; y: lg.Pgint): lg.gint;
	PROCEDURE [ccall] gdk_window_get_parent*(window: PGdkWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_get_pointer*(window: PGdkWindow; x: lg.Pgint; y: lg.Pgint; VAR mask: GdkModifierType): PGdkWindow;
	PROCEDURE [ccall] gdk_window_get_position*(window: PGdkWindow; x: lg.Pgint; y: lg.Pgint);
	PROCEDURE [ccall] gdk_window_get_root_origin*(window: PGdkWindow; x: lg.Pgint; y: lg.Pgint);
	PROCEDURE [ccall] gdk_window_get_state*(window: PGdkWindow): GdkWindowState;
	PROCEDURE [ccall] gdk_window_get_toplevel*(window: PGdkWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_get_toplevels*(): lg.PGList;
	PROCEDURE [ccall] gdk_window_get_update_area*(window: PGdkWindow): PGdkRegion;
	PROCEDURE [ccall] gdk_window_get_user_data*(window: PGdkWindow; VAR data: lg.gpointer);
	PROCEDURE [ccall] gdk_window_get_window_type*(window: PGdkWindow): GdkWindowType;
	PROCEDURE [ccall] gdk_window_hide*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_iconify*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_invalidate_maybe_recurse*(window: PGdkWindow; region: PGdkRegion; child_func: InvalidateMaybeRecurseChildFunc; user_data: lg.gpointer);
	PROCEDURE [ccall] gdk_window_invalidate_rect*(window: PGdkWindow; rect: PGdkRectangle; invalidate_children: lg.gboolean);
	PROCEDURE [ccall] gdk_window_invalidate_region*(window: PGdkWindow; region: PGdkRegion; invalidate_children: lg.gboolean);
	PROCEDURE [ccall] gdk_window_is_viewable*(window: PGdkWindow): lg.gboolean;
	PROCEDURE [ccall] gdk_window_is_visible*(window: PGdkWindow): lg.gboolean;
	PROCEDURE [ccall] gdk_window_lookup*(anid: GdkNativeWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_lookup_for_display*(display: PGdkDisplay; anid: GdkNativeWindow): PGdkWindow;
	PROCEDURE [ccall] gdk_window_lower*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_maximize*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_merge_child_shapes*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_move*(window: PGdkWindow; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gdk_window_move_region*(window: PGdkWindow; region: PGdkRegion; dx: lg.gint; dy: lg.gint);
	PROCEDURE [ccall] gdk_window_move_resize*(window: PGdkWindow; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_window_peek_children*(window: PGdkWindow): lg.PGList;
	PROCEDURE [ccall] gdk_window_process_all_updates*();
	PROCEDURE [ccall] gdk_window_process_updates*(window: PGdkWindow; update_children: lg.gboolean);
	PROCEDURE [ccall] gdk_window_raise*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_register_dnd*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_remove_filter*(window: PGdkWindow; function: GdkFilterFunc; data: lg.gpointer);
	PROCEDURE [ccall] gdk_window_reparent*(window: PGdkWindow; new_parent: PGdkWindow; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gdk_window_resize*(window: PGdkWindow; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gdk_window_scroll*(window: PGdkWindow; dx: lg.gint; dy: lg.gint);
	PROCEDURE [ccall] gdk_window_set_accept_focus*(window: PGdkWindow; accept_focus: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_back_pixmap*(window: PGdkWindow; pixmap: PGdkPixmap; parent_relative: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_background*(window: PGdkWindow; color: PGdkColor);
	PROCEDURE [ccall] gdk_window_set_child_shapes*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_set_cursor*(window: PGdkWindow; cursor: PGdkCursor);
	PROCEDURE [ccall] gdk_window_set_debug_updates*(setting: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_decorations*(window: PGdkWindow; decorations: GdkWMDecoration);
	PROCEDURE [ccall] gdk_window_set_events*(window: PGdkWindow; event_mask: GdkEventMask);
	PROCEDURE [ccall] gdk_window_set_focus_on_map*(window: PGdkWindow; focus_on_map: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_functions*(window: PGdkWindow; functions: GdkWMFunction);
	PROCEDURE [ccall] gdk_window_set_geometry_hints*(window: PGdkWindow; geometry: PGdkGeometry; geom_mask: GdkWindowHints);
	PROCEDURE [ccall] gdk_window_set_group*(window: PGdkWindow; leader: PGdkWindow);
	PROCEDURE [ccall] gdk_window_set_icon*(window: PGdkWindow; icon_window: PGdkWindow; pixmap: PGdkPixmap; mask: PGdkBitmap);
	PROCEDURE [ccall] gdk_window_set_icon_list*(window: PGdkWindow; pixbufs: lg.PGList);
	PROCEDURE [ccall] gdk_window_set_icon_name*(window: PGdkWindow; name: lg.Pgchar);
	PROCEDURE [ccall] gdk_window_set_keep_above*(window: PGdkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_keep_below*(window: PGdkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_modal_hint*(window: PGdkWindow; modal: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_override_redirect*(window: PGdkWindow; override_redirect: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_role*(window: PGdkWindow; role: lg.Pgchar);
	PROCEDURE [ccall] gdk_window_set_skip_pager_hint*(window: PGdkWindow; skips_pager: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_skip_taskbar_hint*(window: PGdkWindow; skips_taskbar: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_static_gravities*(window: PGdkWindow; use_static: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gdk_window_set_title*(window: PGdkWindow; title: lg.Pgchar);
	PROCEDURE [ccall] gdk_window_set_transient_for*(window: PGdkWindow; parent: PGdkWindow);
	PROCEDURE [ccall] gdk_window_set_type_hint*(window: PGdkWindow; hint: GdkWindowTypeHint);
	PROCEDURE [ccall] gdk_window_set_urgency_hint*(window: PGdkWindow; urgent: lg.gboolean);
	PROCEDURE [ccall] gdk_window_set_user_data*(window: PGdkWindow; user_data: lg.gpointer);
	PROCEDURE [ccall] gdk_window_shape_combine_mask*(window: PGdkWindow; mask: PGdkBitmap; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gdk_window_shape_combine_region*(window: PGdkWindow; shape_region: PGdkRegion; offset_x: lg.gint; offset_y: lg.gint);
	PROCEDURE [ccall] gdk_window_show*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_show_unraised*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_stick*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_thaw_updates*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_unfullscreen*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_unmaximize*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_unstick*(window: PGdkWindow);
	PROCEDURE [ccall] gdk_window_withdraw*(window: PGdkWindow);

	(* GdkWindow constructors *)
	PROCEDURE [ccall] gdk_window_new*(parent: PGdkWindow; attributes: PGdkWindowAttr; attributes_mask: lg.gint): PGdkWindow;
	PROCEDURE [ccall] gtk_init*(argc: PInt; argv: PStrList);
END LibsGdk.
