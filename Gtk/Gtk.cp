MODULE LibsGtk ["libgtk-win32-2.0-0.dll"];
(*MODULE LibsGtk ["libgtk-x11-2.0.so"];*)

IMPORT
	SYSTEM,
	lg := LibsGlib,
	lo := LibsGObject,
	atk := LibsAtk,
	pan := LibsPango,
	gpb := LibsGdkPixbuf,
	gdk := LibsGdk;
	 
TYPE
	PInt* = POINTER TO ARRAY [untagged] 1 OF INTEGER;
	PStrList* = POINTER TO ARRAY [untagged] OF ARRAY OF SHORTCHAR;

(* ==================================================================== ENUMS *)
TYPE
	GtkAccelFlags* = SET;
CONST
	GTK_ACCEL_VISIBLE* = 0;
	GTK_ACCEL_LOCKED*  = 1;
	GTK_ACCEL_MASK* = {0..2};

TYPE
	GtkAnchorType* = lg.gint32;
CONST
	GTK_ANCHOR_CENTER*  = 0;
	GTK_ANCHOR_NORTH* = 1;
	GTK_ANCHOR_NORTH_WEST* = 2;
	GTK_ANCHOR_NORTH_EAST* = 3;
	GTK_ANCHOR_SOUTH* = 4;
	GTK_ANCHOR_SOUTH_WEST* = 5;
	GTK_ANCHOR_SOUTH_EAST* = 6;
	GTK_ANCHOR_WEST* = 7;
	GTK_ANCHOR_EAST* = 8;
	GTK_ANCHOR_N* = GTK_ANCHOR_NORTH;
	GTK_ANCHOR_NW* = GTK_ANCHOR_NORTH_WEST;
	GTK_ANCHOR_NE* = GTK_ANCHOR_NORTH_EAST;
	GTK_ANCHOR_S* = GTK_ANCHOR_SOUTH;
	GTK_ANCHOR_SW* = GTK_ANCHOR_SOUTH_WEST;
	GTK_ANCHOR_SE* = GTK_ANCHOR_SOUTH_EAST;
	GTK_ANCHOR_W* = GTK_ANCHOR_WEST;
	GTK_ANCHOR_E* = GTK_ANCHOR_EAST;

TYPE
	GtkArgFlags* = SET;
CONST
	GTK_ARG_READABLE* = lg.G_PARAM_READABLE;
	GTK_ARG_WRITABLE* = lg.G_PARAM_WRITABLE;
	GTK_ARG_CONSTRUCT* = lg.G_PARAM_CONSTRUCT;
	GTK_ARG_CONSTRUCT_ONLY* = lg.G_PARAM_CONSTRUCT_ONLY;
	GTK_ARG_CHILD_ARG* = {4};

TYPE
	GtkArrowType* = lg.gint32;
CONST
	GTK_ARROW_UP* = 0;
	GTK_ARROW_DOWN*  = 1;
	GTK_ARROW_LEFT*  = 2;
	GTK_ARROW_RIGHT* = 3;

TYPE
	GtkAttachOptions* = SET;
CONST
	GTK_EXPAND* = 0;
	GTK_SHRINK* = 1;
	GTK_FILL* = 2;

TYPE
	GtkButtonBoxStyle* = lg.gint32;
CONST
	GTK_BUTTONBOX_DEFAULT_STYLE* = 0;
	GTK_BUTTONBOX_SPREAD*  = 1;
	GTK_BUTTONBOX_EDGE* = 2;
	GTK_BUTTONBOX_START* = 3;
	GTK_BUTTONBOX_END*  = 4;

TYPE
	GtkButtonsType* = lg.gint32;
CONST
	GTK_BUTTONS_NONE* = 0;
	GTK_BUTTONS_OK*  = 1;
	GTK_BUTTONS_CLOSE*  = 2;
	GTK_BUTTONS_CANCEL* = 3;
	GTK_BUTTONS_YES_NO* = 4;
	GTK_BUTTONS_OK_CANCEL* = 5;

TYPE
	GtkCalendarDisplayOptions* = SET;
CONST
	GTK_CALENDAR_SHOW_HEADING* = 0;
	GTK_CALENDAR_SHOW_DAY_NAMES* = 1;
	GTK_CALENDAR_NO_MONTH_CHANGE* = 2;
	GTK_CALENDAR_SHOW_WEEK_NUMBERS* = 3;
	GTK_CALENDAR_WEEK_START_MONDAY* = 4;

TYPE
	GtkCellRendererMode* = lg.gint32;
CONST
	GTK_CELL_RENDERER_MODE_INERT* = 0;
	GTK_CELL_RENDERER_MODE_ACTIVATABLE* = 1;
	GTK_CELL_RENDERER_MODE_EDITABLE* = 2;

TYPE
	GtkCellRendererState* = SET;
CONST
	GTK_CELL_RENDERER_SELECTED* = 0;
	GTK_CELL_RENDERER_PRELIT* = 1;
	GTK_CELL_RENDERER_INSENSITIVE* = 2;
	GTK_CELL_RENDERER_SORTED* = 3;
	GTK_CELL_RENDERER_FOCUSED*  = 4;

TYPE
	GtkCornerType* = lg.gint32;
CONST
	GTK_CORNER_TOP_LEFT*  = 0;
	GTK_CORNER_BOTTOM_LEFT*  = 1;
	GTK_CORNER_TOP_RIGHT* = 2;
	GTK_CORNER_BOTTOM_RIGHT* = 3;

TYPE
	GtkCurveType* = lg.gint32;
CONST
	GTK_CURVE_TYPE_LINEAR* = 0;
	GTK_CURVE_TYPE_SPLINE* = 1;
	GTK_CURVE_TYPE_FREE* = 2;

TYPE
	GtkDebugFlag* = SET;
CONST
	GTK_DEBUG_MISC*  = 0;
	GTK_DEBUG_PLUGSOCKET*  = 1;
	GTK_DEBUG_TEXT*  = 2;
	GTK_DEBUG_TREE*  = 3;
	GTK_DEBUG_UPDATES*  = 4;
	GTK_DEBUG_KEYBINDINGS* = 5;
	GTK_DEBUG_MULTIHEAD* = 6;
	GTK_DEBUG_MODULES*  = 7;
	GTK_DEBUG_GEOMETRY* = 8;
	GTK_DEBUG_ICONTHEME* = 9;

TYPE
	GtkDeleteType* = lg.gint32;
CONST
	GTK_DELETE_CHARS* = 0;
	GTK_DELETE_WORD_ENDS* = 1;
	GTK_DELETE_WORDS* = 2;
	GTK_DELETE_DISPLAY_LINES*  = 3;
	GTK_DELETE_DISPLAY_LINE_ENDS* = 4;
	GTK_DELETE_PARAGRAPH_ENDS* = 5;
	GTK_DELETE_PARAGRAPHS*  = 6;
	GTK_DELETE_WHITESPACE*  = 7;

TYPE
	GtkDestDefaults* = SET;
CONST
	GTK_DEST_DEFAULT_MOTION* = 0;
	GTK_DEST_DEFAULT_HIGHLIGHT* = 1;
	GTK_DEST_DEFAULT_DROP* = 2;
	GTK_DEST_DEFAULT_ALL* = {0..2};

TYPE
	GtkDialogFlags* = SET;
CONST
	GTK_DIALOG_MODAL* = 0;
	GTK_DIALOG_DESTROY_WITH_PARENT* = 1;
	GTK_DIALOG_NO_SEPARATOR*  = 2;

TYPE
	GtkDirectionType* = lg.gint32;
CONST
	GTK_DIR_TAB_FORWARD*  = 0;
	GTK_DIR_TAB_BACKWARD* = 1;
	GTK_DIR_UP*  = 2;
	GTK_DIR_DOWN* = 3;
	GTK_DIR_LEFT* = 4;
	GTK_DIR_RIGHT*  = 5;

TYPE
	GtkExpanderStyle* = lg.gint32;
CONST
	GTK_EXPANDER_COLLAPSED* = 0;
	GTK_EXPANDER_SEMI_COLLAPSED* = 1;
	GTK_EXPANDER_SEMI_EXPANDED*  = 2;
	GTK_EXPANDER_EXPANDED* = 3;

TYPE
	GtkFileChooserAction* = lg.gint32;
CONST
	GTK_FILE_CHOOSER_ACTION_OPEN* = 0;
	GTK_FILE_CHOOSER_ACTION_SAVE* = 1;
	GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER* = 2;
	GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER* = 3;

TYPE
	GtkFileChooserConfirmation* = lg.gint32;
CONST
	GTK_FILE_CHOOSER_CONFIRMATION_CONFIRM* = 0;
	GTK_FILE_CHOOSER_CONFIRMATION_ACCEPT_FILENAME* = 1;
	GTK_FILE_CHOOSER_CONFIRMATION_SELECT_AGAIN* = 2;

TYPE
	GtkFileChooserError* = lg.gint32;
CONST
	GTK_FILE_CHOOSER_ERROR_NONEXISTENT*  = 0;
	GTK_FILE_CHOOSER_ERROR_BAD_FILENAME* = 1;

TYPE
	GtkFileFilterFlags* = SET;
CONST
	GTK_FILE_FILTER_FILENAME*  = 0;
	GTK_FILE_FILTER_URI* = 1;
	GTK_FILE_FILTER_DISPLAY_NAME* = 2;
	GTK_FILE_FILTER_MIME_TYPE* = 3;

TYPE
	GtkIMPreeditStyle* = lg.gint32;
CONST
	GTK_IM_PREEDIT_NOTHING*  = 0;
	GTK_IM_PREEDIT_CALLBACK* = 1;
	GTK_IM_PREEDIT_NONE*  = 2;

TYPE
	GtkIMStatusStyle* = lg.gint32;
CONST
	GTK_IM_STATUS_NOTHING*  = 0;
	GTK_IM_STATUS_CALLBACK* = 1;
	GTK_IM_STATUS_NONE*  = 2;

TYPE
	GtkIconLookupFlags* = SET;
CONST
	GTK_ICON_LOOKUP_NO_SVG* = 0;
	GTK_ICON_LOOKUP_FORCE_SVG* = 1;
	GTK_ICON_LOOKUP_USE_BUILTIN* = 2;

TYPE
	GtkIconSize* = lg.gint32;
CONST
	GTK_ICON_SIZE_INVALID* = 0;
	GTK_ICON_SIZE_MENU* = 1;
	GTK_ICON_SIZE_SMALL_TOOLBAR* = 2;
	GTK_ICON_SIZE_LARGE_TOOLBAR* = 3;
	GTK_ICON_SIZE_BUTTON*  = 4;
	GTK_ICON_SIZE_DND*  = 5;
	GTK_ICON_SIZE_DIALOG*  = 6;

TYPE
	GtkIconThemeError* = lg.gint32;
CONST
	GTK_ICON_THEME_NOT_FOUND* = 0;
	GTK_ICON_THEME_FAILED* = 1;

TYPE
	GtkIconViewDropPosition* = lg.gint32;
CONST
	GTK_ICON_VIEW_NO_DROP* = 0;
	GTK_ICON_VIEW_DROP_INTO*  = 1;
	GTK_ICON_VIEW_DROP_LEFT*  = 2;
	GTK_ICON_VIEW_DROP_RIGHT* = 3;
	GTK_ICON_VIEW_DROP_ABOVE* = 4;
	GTK_ICON_VIEW_DROP_BELOW* = 5;

TYPE
	GtkImageType* = lg.gint32;
CONST
	GTK_IMAGE_EMPTY*  = 0;
	GTK_IMAGE_PIXMAP* = 1;
	GTK_IMAGE_IMAGE*  = 2;
	GTK_IMAGE_PIXBUF* = 3;
	GTK_IMAGE_STOCK*  = 4;
	GTK_IMAGE_ICON_SET*  = 5;
	GTK_IMAGE_ANIMATION* = 6;
	GTK_IMAGE_ICON_NAME* = 7;

TYPE
	GtkJustification* = lg.gint32;
CONST
	GTK_JUSTIFY_LEFT* = 0;
	GTK_JUSTIFY_RIGHT*  = 1;
	GTK_JUSTIFY_CENTER* = 2;
	GTK_JUSTIFY_FILL* = 3;

TYPE
	GtkMatchType* = lg.gint32;
CONST
	GTK_MATCH_ALL* = 0;
	GTK_MATCH_ALL_TAIL* = 1;
	GTK_MATCH_HEAD*  = 2;
	GTK_MATCH_TAIL*  = 3;
	GTK_MATCH_EXACT* = 4;
	GTK_MATCH_LAST*  = 5;

TYPE
	GtkMenuDirectionType* = lg.gint32;
CONST
	GTK_MENU_DIR_PARENT* = 0;
	GTK_MENU_DIR_CHILD*  = 1;
	GTK_MENU_DIR_NEXT* = 2;
	GTK_MENU_DIR_PREV* = 3;

TYPE
	GtkMessageType* = lg.gint32;
CONST
	GTK_MESSAGE_INFO*  = 0;
	GTK_MESSAGE_WARNING*  = 1;
	GTK_MESSAGE_QUESTION* = 2;
	GTK_MESSAGE_ERROR* = 3;

TYPE
	GtkMetricType* = lg.gint32;
CONST
	GTK_PIXELS* = 0;
	GTK_INCHES* = 1;
	GTK_CENTIMETERS* = 2;

TYPE
	GtkMovementStep* = lg.gint32;
CONST
	GTK_MOVEMENT_LOGICAL_POSITIONS* = 0;
	GTK_MOVEMENT_VISUAL_POSITIONS*  = 1;
	GTK_MOVEMENT_WORDS* = 2;
	GTK_MOVEMENT_DISPLAY_LINES*  = 3;
	GTK_MOVEMENT_DISPLAY_LINE_ENDS* = 4;
	GTK_MOVEMENT_PARAGRAPHS*  = 5;
	GTK_MOVEMENT_PARAGRAPH_ENDS* = 6;
	GTK_MOVEMENT_PAGES* = 7;
	GTK_MOVEMENT_BUFFER_ENDS* = 8;
	GTK_MOVEMENT_HORIZONTAL_PAGES*  = 9;

TYPE
	GtkNotebookTab* = lg.gint32;
CONST
	GTK_NOTEBOOK_TAB_FIRST* = 0;
	GTK_NOTEBOOK_TAB_LAST*  = 1;

TYPE
	GtkObjectFlags* = SET;
CONST
	GTK_IN_DESTRUCTION* = 0;
	GTK_FLOATING* = 1;
	GTK_RESERVED_1*  = 2;
	GTK_RESERVED_2*  = 3;

TYPE
	GtkOrientation* = lg.gint32;
CONST
	GTK_ORIENTATION_HORIZONTAL* = 0;
	GTK_ORIENTATION_VERTICAL* = 1;

TYPE
	GtkPackDirection* = lg.gint32;
CONST
	GTK_PACK_DIRECTION_LTR* = 0;
	GTK_PACK_DIRECTION_RTL* = 1;
	GTK_PACK_DIRECTION_TTB* = 2;
	GTK_PACK_DIRECTION_BTT* = 3;

TYPE
	GtkPackType* = lg.gint32;
CONST
	GTK_PACK_START* = 0;
	GTK_PACK_END* = 1;

TYPE
	GtkPathPriorityType* = lg.gint32;
CONST
	GTK_PATH_PRIO_LOWEST* = 0;
	GTK_PATH_PRIO_GTK* = 4;
	GTK_PATH_PRIO_APPLICATION* = 8;
	GTK_PATH_PRIO_THEME* = 10;
	GTK_PATH_PRIO_RC* = 12;
	GTK_PATH_PRIO_HIGHEST*  = 15;

TYPE
	GtkPathType* = lg.gint32;
CONST
	GTK_PATH_WIDGET* = 0;
	GTK_PATH_WIDGET_CLASS* = 1;
	GTK_PATH_CLASS*  = 2;

TYPE
	GtkPolicyType* = lg.gint32;
CONST
	GTK_POLICY_ALWAYS* = 0;
	GTK_POLICY_AUTOMATIC* = 1;
	GTK_POLICY_NEVER*  = 2;

TYPE
	GtkPositionType* = lg.gint32;
CONST
	GTK_POS_LEFT* = 0;
	GTK_POS_RIGHT*  = 1;
	GTK_POS_TOP* = 2;
	GTK_POS_BOTTOM* = 3;

TYPE
	GtkPreviewType* = lg.gint32;
CONST
	GTK_PREVIEW_COLOR*  = 0;
	GTK_PREVIEW_GRAYSCALE* = 1;

TYPE
	GtkProgressBarOrientation* = lg.gint32;
CONST
	GTK_PROGRESS_LEFT_TO_RIGHT* = 0;
	GTK_PROGRESS_RIGHT_TO_LEFT* = 1;
	GTK_PROGRESS_BOTTOM_TO_TOP* = 2;
	GTK_PROGRESS_TOP_TO_BOTTOM* = 3;

TYPE
	GtkProgressBarStyle* = lg.gint32;
CONST
	GTK_PROGRESS_CONTINUOUS* = 0;
	GTK_PROGRESS_DISCRETE* = 1;

TYPE
	GtkRcFlags* = SET;
CONST
	GTK_RC_FG* = 0;
	GTK_RC_BG* = 1;
	GTK_RC_TEXT* = 2;
	GTK_RC_BASE* = 3;

TYPE
	GtkRcTokenType* = lg.gint32;
CONST
	GTK_RC_TOKEN_INVALID*  = lg.G_TOKEN_LAST;
	GTK_RC_TOKEN_INCLUDE*  = lg.G_TOKEN_LAST+1;
	GTK_RC_TOKEN_NORMAL* = lg.G_TOKEN_LAST+2;
	GTK_RC_TOKEN_ACTIVE* = lg.G_TOKEN_LAST+3;
	GTK_RC_TOKEN_PRELIGHT* = lg.G_TOKEN_LAST+4;
	GTK_RC_TOKEN_SELECTED* = lg.G_TOKEN_LAST+5;
	GTK_RC_TOKEN_INSENSITIVE* = lg.G_TOKEN_LAST+6;
	GTK_RC_TOKEN_FG* = lg.G_TOKEN_LAST+7;
	GTK_RC_TOKEN_BG* = lg.G_TOKEN_LAST+8;
	GTK_RC_TOKEN_TEXT*  = lg.G_TOKEN_LAST+9;
	GTK_RC_TOKEN_BASE*  = lg.G_TOKEN_LAST+10;
	GTK_RC_TOKEN_XTHICKNESS*  = lg.G_TOKEN_LAST+11;
	GTK_RC_TOKEN_YTHICKNESS*  = lg.G_TOKEN_LAST+12;
	GTK_RC_TOKEN_FONT*  = lg.G_TOKEN_LAST+13;
	GTK_RC_TOKEN_FONTSET*  = lg.G_TOKEN_LAST+14;
	GTK_RC_TOKEN_FONT_NAME* = lg.G_TOKEN_LAST+15;
	GTK_RC_TOKEN_BG_PIXMAP* = lg.G_TOKEN_LAST+16;
	GTK_RC_TOKEN_PIXMAP_PATH* = lg.G_TOKEN_LAST+17;
	GTK_RC_TOKEN_STYLE* = lg.G_TOKEN_LAST+18;
	GTK_RC_TOKEN_BINDING*  = lg.G_TOKEN_LAST+19;
	GTK_RC_TOKEN_BIND*  = lg.G_TOKEN_LAST+20;
	GTK_RC_TOKEN_WIDGET* = lg.G_TOKEN_LAST+21;
	GTK_RC_TOKEN_WIDGET_CLASS* = lg.G_TOKEN_LAST+22;
	GTK_RC_TOKEN_CLASS* = lg.G_TOKEN_LAST+23;
	GTK_RC_TOKEN_LOWEST* = lg.G_TOKEN_LAST+24;
	GTK_RC_TOKEN_GTK* = lg.G_TOKEN_LAST+25;
	GTK_RC_TOKEN_APPLICATION* = lg.G_TOKEN_LAST+26;
	GTK_RC_TOKEN_THEME* = lg.G_TOKEN_LAST+27;
	GTK_RC_TOKEN_RC* = lg.G_TOKEN_LAST+28;
	GTK_RC_TOKEN_HIGHEST*  = lg.G_TOKEN_LAST+29;
	GTK_RC_TOKEN_ENGINE* = lg.G_TOKEN_LAST+30;
	GTK_RC_TOKEN_MODULE_PATH* = lg.G_TOKEN_LAST+31;
	GTK_RC_TOKEN_IM_MODULE_PATH* = lg.G_TOKEN_LAST+32;
	GTK_RC_TOKEN_IM_MODULE_FILE* = lg.G_TOKEN_LAST+33;
	GTK_RC_TOKEN_STOCK* = lg.G_TOKEN_LAST+34;
	GTK_RC_TOKEN_LTR* = lg.G_TOKEN_LAST+35;
	GTK_RC_TOKEN_RTL* = lg.G_TOKEN_LAST+36;
	GTK_RC_TOKEN_LAST*  = lg.G_TOKEN_LAST+37;

TYPE
	GtkReliefStyle* = lg.gint32;
CONST
	GTK_RELIEF_NORMAL* = 0;
	GTK_RELIEF_HALF* = 1;
	GTK_RELIEF_NONE* = 2;

TYPE
	GtkResizeMode* = lg.gint32;
CONST
	GTK_RESIZE_PARENT* = 0;
	GTK_RESIZE_QUEUE*  = 1;
	GTK_RESIZE_IMMEDIATE* = 2;

TYPE
	GtkResponseType* = lg.gint32;
CONST
	GTK_RESPONSE_NONE* = -1;
	GTK_RESPONSE_REJECT* = -2;
	GTK_RESPONSE_ACCEPT* = -3;
	GTK_RESPONSE_DELETE_EVENT* = -4;
	GTK_RESPONSE_OK*  = -5;
	GTK_RESPONSE_CANCEL* = -6;
	GTK_RESPONSE_CLOSE*  = -7;
	GTK_RESPONSE_YES* = -8;
	GTK_RESPONSE_NO*  = -9;
	GTK_RESPONSE_APPLY*  = -10;
	GTK_RESPONSE_HELP* = -11;

TYPE
	GtkScrollStep* = lg.gint32;
CONST
	GTK_SCROLL_STEPS* = 0;
	GTK_SCROLL_PAGES* = 1;
	GTK_SCROLL_ENDS* = 2;
	GTK_SCROLL_HORIZONTAL_STEPS* = 3;
	GTK_SCROLL_HORIZONTAL_PAGES* = 4;
	GTK_SCROLL_HORIZONTAL_ENDS*  = 5;

TYPE
	GtkScrollType* = lg.gint32;
CONST
	GTK_SCROLL_NONE* = 0;
	GTK_SCROLL_JUMP* = 1;
	GTK_SCROLL_STEP_BACKWARD* = 2;
	GTK_SCROLL_STEP_FORWARD*  = 3;
	GTK_SCROLL_PAGE_BACKWARD* = 4;
	GTK_SCROLL_PAGE_FORWARD*  = 5;
	GTK_SCROLL_STEP_UP* = 6;
	GTK_SCROLL_STEP_DOWN*  = 7;
	GTK_SCROLL_PAGE_UP* = 8;
	GTK_SCROLL_PAGE_DOWN*  = 9;
	GTK_SCROLL_STEP_LEFT*  = 10;
	GTK_SCROLL_STEP_RIGHT* = 11;
	GTK_SCROLL_PAGE_LEFT*  = 12;
	GTK_SCROLL_PAGE_RIGHT* = 13;
	GTK_SCROLL_START* = 14;
	GTK_SCROLL_END*  = 15;

TYPE
	GtkSelectionMode* = lg.gint32;
CONST
	GTK_SELECTION_NONE*  = 0;
	GTK_SELECTION_SINGLE* = 1;
	GTK_SELECTION_BROWSE* = 2;
	GTK_SELECTION_MULTIPLE* = 3;
	GTK_SELECTION_EXTENDED* = GTK_SELECTION_MULTIPLE;

TYPE
	GtkShadowType* = lg.gint32;
CONST
	GTK_SHADOW_NONE* = 0;
	GTK_SHADOW_IN* = 1;
	GTK_SHADOW_OUT*  = 2;
	GTK_SHADOW_ETCHED_IN*  = 3;
	GTK_SHADOW_ETCHED_OUT* = 4;

TYPE
	GtkSideType* = lg.gint32;
CONST
	GTK_SIDE_TOP* = 0;
	GTK_SIDE_BOTTOM* = 1;
	GTK_SIDE_LEFT* = 2;
	GTK_SIDE_RIGHT*  = 3;

TYPE
	GtkSignalRunType* = lg.gint32;
CONST
	GTK_RUN_FIRST* = lg.G_SIGNAL_RUN_FIRST;
	GTK_RUN_LAST* = lg.G_SIGNAL_RUN_LAST;
	GTK_RUN_BOTH* = GTK_RUN_FIRST + GTK_RUN_LAST;
	GTK_RUN_NO_RECURSE* = lg.G_SIGNAL_NO_RECURSE;
	GTK_RUN_ACTION*  = lg.G_SIGNAL_ACTION;
	GTK_RUN_NO_HOOKS* = lg.G_SIGNAL_NO_HOOKS;

TYPE
	GtkSizeGroupMode* = lg.gint32;
CONST
	GTK_SIZE_GROUP_NONE* = 0;
	GTK_SIZE_GROUP_HORIZONTAL* = 1;
	GTK_SIZE_GROUP_VERTICAL* = 2;
	GTK_SIZE_GROUP_BOTH* = 3;

TYPE
	GtkSortType* = lg.gint32;
CONST
	GTK_SORT_ASCENDING*  = 0;
	GTK_SORT_DESCENDING* = 1;

TYPE
	GtkSpinButtonUpdatePolicy* = lg.gint32;
CONST
	GTK_UPDATE_ALWAYS* = 0;
	GTK_UPDATE_IF_VALID* = 1;

TYPE
	GtkSpinType* = lg.gint32;
CONST
	GTK_SPIN_STEP_FORWARD*  = 0;
	GTK_SPIN_STEP_BACKWARD* = 1;
	GTK_SPIN_PAGE_FORWARD*  = 2;
	GTK_SPIN_PAGE_BACKWARD* = 3;
	GTK_SPIN_HOME* = 4;
	GTK_SPIN_END*  = 5;
	GTK_SPIN_USER_DEFINED*  = 6;

TYPE
	GtkStateType* = lg.gint32;
CONST
	GTK_STATE_NORMAL* = 0;
	GTK_STATE_ACTIVE* = 1;
	GTK_STATE_PRELIGHT* = 2;
	GTK_STATE_SELECTED* = 3;
	GTK_STATE_INSENSITIVE* = 4;

TYPE
	GtkSubmenuDirection* = lg.gint32;
CONST
	GTK_DIRECTION_LEFT*  = 0;
	GTK_DIRECTION_RIGHT* = 1;

TYPE
	GtkSubmenuPlacement* = lg.gint32;
CONST
	GTK_TOP_BOTTOM* = 0;
	GTK_LEFT_RIGHT* = 1;

TYPE
	GtkTargetFlags* = SET;
CONST
	GTK_TARGET_SAME_APP* = 0;
	GTK_TARGET_SAME_WIDGET* = 1;

TYPE
	GtkTextDirection* = lg.gint32;
CONST
	GTK_TEXT_DIR_NONE* = 0;
	GTK_TEXT_DIR_LTR*  = 1;
	GTK_TEXT_DIR_RTL*  = 2;

TYPE
	GtkTextSearchFlags* = SET;
CONST
	GTK_TEXT_SEARCH_VISIBLE_ONLY* = 0;
	GTK_TEXT_SEARCH_TEXT_ONLY* = 1;

TYPE
	GtkTextWindowType* = lg.gint32;
CONST
	GTK_TEXT_WINDOW_PRIVATE* = 0;
	GTK_TEXT_WINDOW_WIDGET*  = 1;
	GTK_TEXT_WINDOW_TEXT* = 2;
	GTK_TEXT_WINDOW_LEFT* = 3;
	GTK_TEXT_WINDOW_RIGHT* = 4;
	GTK_TEXT_WINDOW_TOP*  = 5;
	GTK_TEXT_WINDOW_BOTTOM*  = 6;

TYPE
	GtkToolbarChildType* = lg.gint32;
CONST
	GTK_TOOLBAR_CHILD_SPACE*  = 0;
	GTK_TOOLBAR_CHILD_BUTTON* = 1;
	GTK_TOOLBAR_CHILD_TOGGLEBUTTON* = 2;
	GTK_TOOLBAR_CHILD_RADIOBUTTON*  = 3;
	GTK_TOOLBAR_CHILD_WIDGET* = 4;

TYPE
	GtkToolbarSpaceStyle* = lg.gint32;
CONST
	GTK_TOOLBAR_SPACE_EMPTY* = 0;
	GTK_TOOLBAR_SPACE_LINE*  = 1;

TYPE
	GtkToolbarStyle* = lg.gint32;
CONST
	GTK_TOOLBAR_ICONS* = 0;
	GTK_TOOLBAR_TEXT* = 1;
	GTK_TOOLBAR_BOTH* = 2;
	GTK_TOOLBAR_BOTH_HORIZ* = 3;

TYPE
	GtkTreeModelFlags* = SET;
CONST
	GTK_TREE_MODEL_ITERS_PERSIST* = 0;
	GTK_TREE_MODEL_LIST_ONLY*  = 1;

TYPE
	GtkTreeViewColumnSizing* = lg.gint32;
CONST
	GTK_TREE_VIEW_COLUMN_GROW_ONLY* = 0;
	GTK_TREE_VIEW_COLUMN_AUTOSIZE*  = 1;
	GTK_TREE_VIEW_COLUMN_FIXED*  = 2;

TYPE
	GtkTreeViewDropPosition* = lg.gint32;
CONST
	GTK_TREE_VIEW_DROP_BEFORE* = 0;
	GTK_TREE_VIEW_DROP_AFTER* = 1;
	GTK_TREE_VIEW_DROP_INTO_OR_BEFORE* = 2;
	GTK_TREE_VIEW_DROP_INTO_OR_AFTER*  = 3;

TYPE
	GtkUIManagerItemType* = SET;
CONST
	GTK_UI_MANAGER_AUTO*  = {};
	GTK_UI_MANAGER_MENUBAR*  = 0;
	GTK_UI_MANAGER_MENU*  = 1;
	GTK_UI_MANAGER_TOOLBAR*  = 2;
	GTK_UI_MANAGER_PLACEHOLDER* = 3;
	GTK_UI_MANAGER_POPUP* = 4;
	GTK_UI_MANAGER_MENUITEM* = 5;
	GTK_UI_MANAGER_TOOLITEM* = 6;
	GTK_UI_MANAGER_SEPARATOR* = 7;
	GTK_UI_MANAGER_ACCELERATOR* = 8;

TYPE
	GtkUpdateType* = lg.gint32;
CONST
	GTK_UPDATE_CONTINUOUS* = 0;
	GTK_UPDATE_DISCONTINUOUS* = 1;
	GTK_UPDATE_DELAYED* = 2;

TYPE
	GtkVisibility* = lg.gint32;
CONST
	GTK_VISIBILITY_NONE* = 0;
	GTK_VISIBILITY_PARTIAL* = 1;
	GTK_VISIBILITY_FULL* = 2;

TYPE
	GtkWidgetFlags* = SET;
CONST
	GTK_TOPLEVEL* = 4;
	GTK_NO_WINDOW*  = 5;
	GTK_REALIZED* = 6;
	GTK_MAPPED*  = 7;
	GTK_VISIBLE* = 8;
	GTK_SENSITIVE*  = 9;
	GTK_PARENT_SENSITIVE* = 10;
	GTK_CAN_FOCUS*  = 11;
	GTK_HAS_FOCUS*  = 12;
	GTK_CAN_DEFAULT* = 13;
	GTK_HAS_DEFAULT* = 14;
	GTK_HAS_GRAB* = 15;
	GTK_RC_STYLE* = 16;
	GTK_COMPOSITE_CHILD*  = 17;
	GTK_NO_REPARENT* = 18;
	GTK_APP_PAINTABLE* = 19;
	GTK_RECEIVES_DEFAULT* = 20;
	GTK_DOUBLE_BUFFERED*  = 21;
	GTK_NO_SHOW_ALL* = 22;

TYPE
	GtkWidgetHelpType* = lg.gint32;
CONST
	GTK_WIDGET_HELP_TOOLTIP* = 0;
	GTK_WIDGET_HELP_WHATS_THIS* = 1;

TYPE
	GtkWin32EmbedMessageType* = lg.gint32;
CONST
	GTK_WIN32_EMBED_WINDOW_ACTIVATE* = 0;
	GTK_WIN32_EMBED_WINDOW_DEACTIVATE* = 1;
	GTK_WIN32_EMBED_FOCUS_IN* = 2;
	GTK_WIN32_EMBED_FOCUS_OUT* = 3;
	GTK_WIN32_EMBED_MODALITY_ON* = 4;
	GTK_WIN32_EMBED_MODALITY_OFF* = 5;
	GTK_WIN32_EMBED_PARENT_NOTIFY*  = 6;
	GTK_WIN32_EMBED_EVENT_PLUG_MAPPED* = 7;
	GTK_WIN32_EMBED_PLUG_RESIZED* = 8;
	GTK_WIN32_EMBED_REQUEST_FOCUS*  = 9;
	GTK_WIN32_EMBED_FOCUS_NEXT*  = 10;
	GTK_WIN32_EMBED_FOCUS_PREV*  = 11;
	GTK_WIN32_EMBED_GRAB_KEY* = 12;
	GTK_WIN32_EMBED_UNGRAB_KEY*  = 13;
	GTK_WIN32_EMBED_LAST*  = 14;

TYPE
	GtkWindowPosition* = lg.gint32;
CONST
	GTK_WIN_POS_NONE* = 0;
	GTK_WIN_POS_CENTER*  = 1;
	GTK_WIN_POS_MOUSE* = 2;
	GTK_WIN_POS_CENTER_ALWAYS* = 3;
	GTK_WIN_POS_CENTER_ON_PARENT* = 4;

TYPE
	GtkWindowType* = lg.gint32;
CONST
	GTK_WINDOW_TOPLEVEL* = 0;
	GTK_WINDOW_POPUP* = 1;

TYPE
	GtkWrapMode* = lg.gint32;
CONST
	GTK_WRAP_NONE* = 0;
	GTK_WRAP_CHAR* = 1;
	GTK_WRAP_WORD* = 2;
	GTK_WRAP_WORD_CHAR* = 3;

(* ====================================================== POINTERS TO OBJECTS *)
TYPE
	PGtkAboutDialog* = POINTER TO GtkAboutDialog;
	PGtkAccelGroup*  = POINTER TO GtkAccelGroup;
	PGtkAccelLabel*  = POINTER TO GtkAccelLabel;
	PGtkAccelMap* = POINTER TO GtkAccelMap;
	PGtkAccessible*  = POINTER TO GtkAccessible;
	PGtkAction* = POINTER TO GtkAction;
	PGtkActionGroup* = POINTER TO GtkActionGroup;
	PGtkAdjustment*  = POINTER TO GtkAdjustment;
	PGtkAlignment* = POINTER TO GtkAlignment;
	PGtkArrow* = POINTER TO GtkArrow;
	PGtkAspectFrame* = POINTER TO GtkAspectFrame;
	PGtkBin* = POINTER TO GtkBin;
	PGtkBox* = POINTER TO GtkBox;
	PGtkButton* = POINTER TO GtkButton;
	PGtkButtonBox* = POINTER TO GtkButtonBox;
	PGtkCalendar* = POINTER TO GtkCalendar;
	PGtkCellRenderer* = POINTER TO GtkCellRenderer;
	PGtkCellRendererCombo* = POINTER TO GtkCellRendererCombo;
	PGtkCellRendererPixbuf* = POINTER TO GtkCellRendererPixbuf;
	PGtkCellRendererProgress* = POINTER TO GtkCellRendererProgress;
	PGtkCellRendererText*  = POINTER TO GtkCellRendererText;
	PGtkCellRendererToggle* = POINTER TO GtkCellRendererToggle;
	PGtkCellView* = POINTER TO GtkCellView;
	PGtkCheckButton* = POINTER TO GtkCheckButton;
	PGtkCheckMenuItem*  = POINTER TO GtkCheckMenuItem;
	PGtkClipboard* = POINTER TO GtkClipboard;
	PGtkColorButton* = POINTER TO GtkColorButton;
	PGtkColorSelection* = POINTER TO GtkColorSelection;
	PGtkColorSelectionDialog* = POINTER TO GtkColorSelectionDialog;
	PGtkCombo* = POINTER TO GtkCombo;
	PGtkComboBox* = POINTER TO GtkComboBox;
	PGtkComboBoxEntry*  = POINTER TO GtkComboBoxEntry;
	PGtkContainer* = POINTER TO GtkContainer;
	PGtkCurve* = POINTER TO GtkCurve;
	PGtkDialog* = POINTER TO GtkDialog;
	PGtkDrawingArea* = POINTER TO GtkDrawingArea;
	PGtkEntry* = POINTER TO GtkEntry;
	PGtkEntryCompletion* = POINTER TO GtkEntryCompletion;
	PGtkEventBox* = POINTER TO GtkEventBox;
	PGtkExpander* = POINTER TO GtkExpander;
	PGtkFileChooserButton* = POINTER TO GtkFileChooserButton;
	PGtkFileChooserDialog* = POINTER TO GtkFileChooserDialog;
	PGtkFileChooserWidget* = POINTER TO GtkFileChooserWidget;
	PGtkFileFilter*  = POINTER TO GtkFileFilter;
	PGtkFileSelection*  = POINTER TO GtkFileSelection;
	PGtkFixed* = POINTER TO GtkFixed;
	PGtkFontButton*  = POINTER TO GtkFontButton;
	PGtkFontSelection*  = POINTER TO GtkFontSelection;
	PGtkFontSelectionDialog*  = POINTER TO GtkFontSelectionDialog;
	PGtkFrame* = POINTER TO GtkFrame;
	PGtkGammaCurve*  = POINTER TO GtkGammaCurve;
	PGtkHandleBox* = POINTER TO GtkHandleBox;
	PGtkHBox*  = POINTER TO GtkHBox;
	PGtkHButtonBox*  = POINTER TO GtkHButtonBox;
	PGtkHPaned* = POINTER TO GtkHPaned;
	PGtkHRuler* = POINTER TO GtkHRuler;
	PGtkHScale* = POINTER TO GtkHScale;
	PGtkHScrollbar*  = POINTER TO GtkHScrollbar;
	PGtkHSeparator*  = POINTER TO GtkHSeparator;
	PGtkHSV* = POINTER TO GtkHSV;
	PGtkIconFactory* = POINTER TO GtkIconFactory;
	PGtkIconTheme* = POINTER TO GtkIconTheme;
	PGtkIconView* = POINTER TO GtkIconView;
	PGtkImage* = POINTER TO GtkImage;
	PGtkImageMenuItem*  = POINTER TO GtkImageMenuItem;
	PGtkIMContext* = POINTER TO GtkIMContext;
	PGtkIMContextSimple* = POINTER TO GtkIMContextSimple;
	PGtkIMMulticontext* = POINTER TO GtkIMMulticontext;
	PGtkInputDialog* = POINTER TO GtkInputDialog;
	PGtkInvisible* = POINTER TO GtkInvisible;
	PGtkItem*  = POINTER TO GtkItem;
	PGtkItemFactory* = POINTER TO GtkItemFactory;
	PGtkLabel* = POINTER TO GtkLabel;
	PGtkLayout* = POINTER TO GtkLayout;
	PGtkListStore* = POINTER TO GtkListStore;
	PGtkMenu*  = POINTER TO GtkMenu;
	PGtkMenuBar*  = POINTER TO GtkMenuBar;
	PGtkMenuItem* = POINTER TO GtkMenuItem;
	PGtkMenuShell* = POINTER TO GtkMenuShell;
	PGtkMenuToolButton* = POINTER TO GtkMenuToolButton;
	PGtkMessageDialog*  = POINTER TO GtkMessageDialog;
	PGtkMisc*  = POINTER TO GtkMisc;
	PGtkNotebook* = POINTER TO GtkNotebook;
	PGtkObject* = POINTER TO GtkObject;
	PGtkOptionMenu*  = POINTER TO GtkOptionMenu;
	PGtkPaned* = POINTER TO GtkPaned;
	PGtkPlug*  = POINTER TO GtkPlug;
	PGtkProgressBar* = POINTER TO GtkProgressBar;
	PGtkRadioAction* = POINTER TO GtkRadioAction;
	PGtkRadioButton* = POINTER TO GtkRadioButton;
	PGtkRadioMenuItem*  = POINTER TO GtkRadioMenuItem;
	PGtkRadioToolButton* = POINTER TO GtkRadioToolButton;
	PGtkRange* = POINTER TO GtkRange;
	PGtkRcStyle*  = POINTER TO GtkRcStyle;
	PGtkRuler* = POINTER TO GtkRuler;
	PGtkScale* = POINTER TO GtkScale;
	PGtkScrollbar* = POINTER TO GtkScrollbar;
	PGtkScrolledWindow* = POINTER TO GtkScrolledWindow;
	PGtkSeparator* = POINTER TO GtkSeparator;
	PGtkSeparatorMenuItem* = POINTER TO GtkSeparatorMenuItem;
	PGtkSeparatorToolItem* = POINTER TO GtkSeparatorToolItem;
	PGtkSettings* = POINTER TO GtkSettings;
	PGtkSizeGroup* = POINTER TO GtkSizeGroup;
	PGtkSocket* = POINTER TO GtkSocket;
	PGtkSpinButton*  = POINTER TO GtkSpinButton;
	PGtkStatusbar* = POINTER TO GtkStatusbar;
	PGtkStyle* = POINTER TO GtkStyle;
	PGtkTable* = POINTER TO GtkTable;
	PGtkTearoffMenuItem* = POINTER TO GtkTearoffMenuItem;
	PGtkTextBuffer*  = POINTER TO GtkTextBuffer;
	PGtkTextChildAnchor* = POINTER TO GtkTextChildAnchor;
	PGtkTextMark* = POINTER TO GtkTextMark;
	PGtkTextTag*  = POINTER TO GtkTextTag;
	PGtkTextTagTable* = POINTER TO GtkTextTagTable;
	PGtkTextView* = POINTER TO GtkTextView;
	PGtkToggleAction* = POINTER TO GtkToggleAction;
	PGtkToggleButton* = POINTER TO GtkToggleButton;
	PGtkToggleToolButton*  = POINTER TO GtkToggleToolButton;
	PGtkToolbar*  = POINTER TO GtkToolbar;
	PGtkTooltips* = POINTER TO GtkTooltips;
	PGtkToolButton*  = POINTER TO GtkToolButton;
	PGtkToolItem* = POINTER TO GtkToolItem;
	PGtkTreeModelFilter* = POINTER TO GtkTreeModelFilter;
	PGtkTreeModelSort*  = POINTER TO GtkTreeModelSort;
	PGtkTreeSelection*  = POINTER TO GtkTreeSelection;
	PGtkTreeStore* = POINTER TO GtkTreeStore;
	PGtkTreeView* = POINTER TO GtkTreeView;
	PGtkTreeViewColumn* = POINTER TO GtkTreeViewColumn;
	PGtkUIManager* = POINTER TO GtkUIManager;
	PGtkVBox*  = POINTER TO GtkVBox;
	PGtkVButtonBox*  = POINTER TO GtkVButtonBox;
	PGtkViewport* = POINTER TO GtkViewport;
	PGtkVPaned* = POINTER TO GtkVPaned;
	PGtkVRuler* = POINTER TO GtkVRuler;
	PGtkVScale* = POINTER TO GtkVScale;
	PGtkVScrollbar*  = POINTER TO GtkVScrollbar;
	PGtkVSeparator*  = POINTER TO GtkVSeparator;
	PGtkWidget* = POINTER TO GtkWidget;
	PGtkWindow* = POINTER TO GtkWindow;
	PGtkWindowGroup* = POINTER TO GtkWindowGroup;
	PGtkStock* = POINTER TO GtkStock;

(* ====================================================== POINTERS TO STRUCTS *)
TYPE
	PGtkAccelGroupEntry* = POINTER TO GtkAccelGroupEntry;
	PGtkAccelKey*  = POINTER TO GtkAccelKey;
	PGtkActionEntry*  = POINTER TO GtkActionEntry;
	PGtkArg* = POINTER TO GtkArg;
	PGtkBindingArg* = POINTER TO GtkBindingArg;
	PGtkBindingEntry* = POINTER TO GtkBindingEntry;
	PGtkBindingSet* = POINTER TO GtkBindingSet;
	PGtkBindingSignal* = POINTER TO GtkBindingSignal;
	PGtkBoxChild*  = POINTER TO GtkBoxChild;
	PGtkFileFilterInfo*  = POINTER TO GtkFileFilterInfo;
	PGtkFixedChild* = POINTER TO GtkFixedChild;
	PGtkIMContextInfo* = POINTER TO GtkIMContextInfo;
	PGtkImageAnimationData* = POINTER TO GtkImageAnimationData;
	PGtkImageIconNameData*  = POINTER TO GtkImageIconNameData;
	PGtkImageIconSetData* = POINTER TO GtkImageIconSetData;
	PGtkImageImageData*  = POINTER TO GtkImageImageData;
	PGtkImagePixbufData* = POINTER TO GtkImagePixbufData;
	PGtkImagePixmapData* = POINTER TO GtkImagePixmapData;
	PGtkImageStockData*  = POINTER TO GtkImageStockData;
	PGtkImageGIconData*  = POINTER TO GtkImageGIconData;
	PGtkItemFactoryEntry* = POINTER TO GtkItemFactoryEntry;
	PGtkItemFactoryItem* = POINTER TO GtkItemFactoryItem;
	PGtkKeyHash* = POINTER TO GtkKeyHash;
	PGtkLabelSelectionInfo* = POINTER TO GtkLabelSelectionInfo;
	PGtkMenuEntry* = POINTER TO GtkMenuEntry;
	PGtkMnemonicHash* = POINTER TO GtkMnemonicHash;
	PGtkNotebookPage* = POINTER TO GtkNotebookPage;
	PGtkRadioActionEntry* = POINTER TO GtkRadioActionEntry;
	PGtkRangeLayout*  = POINTER TO GtkRangeLayout;
	PGtkRangeStepTimer*  = POINTER TO GtkRangeStepTimer;
	PGtkRcContext* = POINTER TO GtkRcContext;
	PGtkRcProperty* = POINTER TO GtkRcProperty;
	PGtkRulerMetric*  = POINTER TO GtkRulerMetric;
	PGtkSettingsPropertyValue* = POINTER TO GtkSettingsPropertyValue;
	PGtkSettingsValue* = POINTER TO GtkSettingsValue;
	PGtkStockItem* = POINTER TO GtkStockItem;
	PGtkTableChild* = POINTER TO GtkTableChild;
	PGtkTableRowCol*  = POINTER TO GtkTableRowCol;
	PGtkTargetEntry*  = POINTER TO GtkTargetEntry;
	PGtkTargetList* = POINTER TO GtkTargetList;
	PGtkTargetPair* = POINTER TO GtkTargetPair;
	PGtkTextAppearance*  = POINTER TO GtkTextAppearance;
	PGtkTextBTree* = POINTER TO GtkTextBTree;
	PGtkTextLogAttrCache* = POINTER TO GtkTextLogAttrCache;
	PGtkTextPendingScroll*  = POINTER TO GtkTextPendingScroll;
	PGtkTextWindow* = POINTER TO GtkTextWindow;
	PGtkThemeEngine*  = POINTER TO GtkThemeEngine;
	PGtkToggleActionEntry*  = POINTER TO GtkToggleActionEntry;
	PGtkToolbarChild* = POINTER TO GtkToolbarChild;
	PGtkTooltipsData* = POINTER TO GtkTooltipsData;
	PGtkTypeInfo*  = POINTER TO GtkTypeInfo;
	PGtkWidgetAuxInfo* = POINTER TO GtkWidgetAuxInfo;
	PGtkWidgetShapeInfo* = POINTER TO GtkWidgetShapeInfo;
	PGtkWindowGeometryInfo* = POINTER TO GtkWindowGeometryInfo;
	PGtkTreeModelFilterPrivate*= ANYPTR;

(* ====================================================== POINTERS TO ALIASES *)
TYPE
	PGtkAllocation* = POINTER TO GtkAllocation;
	PGtkEnumValue*  = POINTER TO GtkEnumValue;
	PGtkFlagValue*  = POINTER TO GtkFlagValue;
	PGtkTypeClass*  = POINTER TO GtkTypeClass;
	PGtkTypeObject* = POINTER TO GtkTypeObject;

(* ======================================================= POINTERS TO BOXEDS *)
TYPE
	PGtkBorder*  = POINTER TO GtkBorder;
	PGtkIconInfo* = POINTER TO GtkIconInfo;
	PGtkIconSet* = POINTER TO GtkIconSet;
	PGtkIconSource* = POINTER TO GtkIconSource;
	PGtkRequisition* = POINTER TO GtkRequisition;
	PGtkSelectionData* = POINTER TO GtkSelectionData;
	PGtkTextAttributes* = POINTER TO GtkTextAttributes;
	PGtkTextIter* = POINTER TO GtkTextIter;
	PGtkTreeIter* = POINTER TO GtkTreeIter;
	PGtkTreePath* = POINTER TO GtkTreePath;
	PGtkTreeRowReference* = POINTER TO GtkTreeRowReference;

(* =================================================== POINTERS TO INTERFACES *)
TYPE
	PGtkCellEditable* = POINTER TO GtkCellEditable;
	PGtkCellLayout*  = POINTER TO GtkCellLayout;
	PGtkEditable* = POINTER TO GtkEditable;
	PGtkFileChooser* = POINTER TO GtkFileChooser;
	PGtkTreeDragDest* = POINTER TO GtkTreeDragDest;
	PGtkTreeDragSource* = POINTER TO GtkTreeDragSource;
	PGtkTreeModel* = POINTER TO GtkTreeModel;
	PGtkTreeSortable* = POINTER TO GtkTreeSortable;

(* ================================================================== ALIASES *)
TYPE
	GtkAllocation* = gdk.GdkRectangle;
	GtkClassInitFunc* = lo.GBaseInitFunc;
	GtkEnumValue*  = lo.GEnumValue;
	GtkFlagValue*  = lo.GFlagsValue;
	GtkFundamentalType*  = lo.GType;
	GtkObjectInitFunc* = lo.GInstanceInitFunc;
	GtkSignalMarshaller* = lo.GSignalCMarshaller;
	GtkType* = lo.GType;
	GtkTypeClass*  = lo.GTypeClass;
	GtkTypeObject* = lo.GTypeInstance;

(* ================================================================ CALLBACKS *)
TYPE
	Callback* = lo.GCallback;

	GtkAboutDialogActivateLinkFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  about: PGtkAboutDialog;
	  link : lg.Pgchar;
	  data : lg.gpointer
	);
	END;

	GtkAccelGroupActivate* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  accel_group : PGtkAccelGroup;
	  acceleratable: lo.PGObject;
	  keyval : lg.guint;
	  modifier : gdk.GdkModifierType
	): lg.gboolean;
	END;

	GtkAccelGroupFindFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  key : PGtkAccelKey;
	  closure: lo.PGClosure;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkAccelMapForeach* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  data : lg.gpointer;
	  accel_path: lg.Pgchar;
	  accel_key : lg.guint;
	  accel_mods: gdk.GdkModifierType;
	  changed : lg.gboolean
	);
	END;

	GtkCallback* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  widget: PGtkWidget;
	  data : lg.gpointer
	);
	END;

	GUserFunction* = RECORD (lo.GCallback) 
	callback*: PROCEDURE [ccall] 
	(
	   widget: PGtkWidget;
	   event: gdk.PGdkEvent;
	   user_data: lg.gpointer
	);
	END;

	GtkCallbackMarshal* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  object: PGtkObject;
	  data : lg.gpointer;
	  n_args: lg.guint;
	  args : PGtkArg
	);
	END;

	GtkCellLayoutDataFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  cell_layout: PGtkCellLayout;
	  cell : PGtkCellRenderer;
	  tree_model : PGtkTreeModel;
	  iter : PGtkTreeIter;
	  data : lg.gpointer
	);
	END;

	GtkClipboardClearFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  clipboard : PGtkClipboard;
	  user_data_or_owner: lg.gpointer
	);
	END;

	GtkClipboardGetFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  clipboard : PGtkClipboard;
	  selection_data : PGtkSelectionData;
	  info : lg.guint;
	  user_data_or_owner: lg.gpointer
	);
	END;

	GtkClipboardImageReceivedFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  clipboard: PGtkClipboard;
	  pixbuf : gpb.PGdkPixbuf;
	  data : lg.gpointer
	);
	END;

	GtkClipboardReceivedFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  clipboard : PGtkClipboard;
	  selection_data: PGtkSelectionData;
	  data : lg.gpointer
	);
	END;

	GtkClipboardTargetsReceivedFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  clipboard: PGtkClipboard;
	  atoms : gdk.PGdkAtom;
	  n_atoms : lg.gint;
	  data : lg.gpointer
	);
	END;

	GtkClipboardTextReceivedFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  clipboard: PGtkClipboard;
	  text : lg.Pgchar;
	  data : lg.gpointer
	);
	END;

	GtkColorSelectionChangePaletteFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  colors : gdk.PGdkColor;
	  n_colors: lg.gint
	);
	END;

	GtkColorSelectionChangePaletteWithScreenFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  screen : gdk.PGdkScreen;
	  colors : gdk.PGdkColor;
	  n_colors: lg.gint
	);
	END;

	GtkDestroyNotify*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  data: lg.gpointer
	);
	END;

	GtkEntryCompletionMatchFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  completion: PGtkEntryCompletion;
	  key : lg.Pgchar;
	  iter : PGtkTreeIter;
	  user_data : lg.gpointer
	): lg.gboolean;
	END;

	GtkFileFilterFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  filter_info: PGtkFileFilterInfo;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkFunction* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  data: lg.gpointer
	): lg.gboolean;
	END;

	GtkIconViewForeachFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  icon_view: PGtkIconView;
	  path : PGtkTreePath;
	  data : lg.gpointer
	);
	END;

	GtkItemFactoryCallback*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	();
	END;

	GtkItemFactoryCallback1* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  callback_data : lg.gpointer;
	  callback_action: lg.guint;
	  widget : PGtkWidget
	);
	END;

	GtkItemFactoryCallback2* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	();
	END;

	GtkKeySnoopFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  grab_widget: PGtkWidget;
	  event : gdk.PGdkEventKey;
	  func_data : lg.gpointer
	): lg.gint;
	END;

	GtkMenuCallback* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  widget : PGtkWidget;
	  user_data: lg.gpointer
	);
	END;

	GtkMenuDetachFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  attach_widget: PGtkWidget;
	  menu : PGtkMenu
	);
	END;

	GtkMenuPositionFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  menu : PGtkMenu;
	  x : lg.Pgint;
	  y : lg.Pgint;
	  VAR push_in : lg.gboolean;
	  user_data: lg.gpointer
	);
	END;

	GtkMnemonicHashForeach*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  keyval : lg.guint;
	  targets: lg.PGSList;
	  data : lg.gpointer
	);
	END;

	GtkModuleDisplayInitFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  display: gdk.PGdkDisplay
	);
	END;

	GtkModuleInitFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  VAR argc: lg.gint;
	  VAR argv: POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar
	);
	END;

	GtkPrintFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  func_data: lg.gpointer;
	  str : lg.Pgchar
	);
	END;

	GtkRcPropertyParser*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  pspec : lo.PGParamSpec;
	  rc_string : lg.PGString;
	  property_value: lo.PGValue
	): lg.gboolean;
	END;

	GtkSignalFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	();
	END;

	GtkTextCharPredicate* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  ch : lg.gunichar;
	  user_data: lg.gpointer
	): lg.gboolean;
	END;

	GtkTextTagTableForeach*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  tag : PGtkTextTag;
	  data: lg.gpointer
	);
	END;

	GtkTranslateFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  path : lg.Pgchar;
	  func_data: lg.gpointer
	): lg.Pgchar;
	END;

	GtkTreeCellDataFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  tree_column: PGtkTreeViewColumn;
	  cell : PGtkCellRenderer;
	  tree_model : PGtkTreeModel;
	  iter : PGtkTreeIter;
	  data : lg.gpointer
	);
	END;

	GtkTreeDestroyCountFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  tree_view: PGtkTreeView;
	  path : PGtkTreePath;
	  children : lg.gint;
	  user_data: lg.gpointer
	);
	END;

	GtkTreeIterCompareFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model : PGtkTreeModel;
	  a : PGtkTreeIter;
	  b : PGtkTreeIter;
	  user_data: lg.gpointer
	): lg.gint;
	END;

	GtkTreeModelFilterModifyFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model : PGtkTreeModel;
	  iter : PGtkTreeIter;
	  value : lo.PGValue;
	  column: lg.gint;
	  data : lg.gpointer
	);
	END;

	GtkTreeModelFilterVisibleFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model: PGtkTreeModel;
	  iter : PGtkTreeIter;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkTreeModelForeachFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model: PGtkTreeModel;
	  path : PGtkTreePath;
	  iter : PGtkTreeIter;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkTreeSelectionForeachFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model: PGtkTreeModel;
	  path : PGtkTreePath;
	  iter : PGtkTreeIter;
	  data : lg.gpointer
	);
	END;

	GtkTreeSelectionFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  selection : PGtkTreeSelection;
	  model : PGtkTreeModel;
	  path : PGtkTreePath;
	  path_currently_selected: lg.gboolean;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkTreeViewColumnDropFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  tree_view : PGtkTreeView;
	  column : PGtkTreeViewColumn;
	  prev_column: PGtkTreeViewColumn;
	  next_column: PGtkTreeViewColumn;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkTreeViewMappingFunc*  = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  tree_view: PGtkTreeView;
	  path : PGtkTreePath;
	  user_data: lg.gpointer
	);
	END;

	GtkTreeViewRowSeparatorFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model: PGtkTreeModel;
	  iter : PGtkTreeIter;
	  data : lg.gpointer
	): lg.gboolean;
	END;

	GtkTreeViewSearchEqualFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  model : PGtkTreeModel;
	  column : lg.gint;
	  key : lg.Pgchar;
	  iter : PGtkTreeIter;
	  search_data: lg.gpointer
	): lg.gboolean;
	END;

	GtkWindowKeysForeachFunc* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  window : PGtkWindow;
	  keyval : lg.guint;
	  modifiers : gdk.GdkModifierType;
	  is_mnemonic: lg.gboolean;
	  data : lg.gpointer
	);
	END;

	_gtk_reserved1* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  arg1: lg.void
	);
	END;

	_gtk_reserved2* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  arg1: lg.void
	);
	END;

	_gtk_reserved3* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  arg1: lg.void
	);
	END;

	_gtk_reserved4* = RECORD (Callback)
	callback*: PROCEDURE [ccall]
	(
	  arg1: lg.void
	);
	END;

(* ================================================================== STRUCTS *)
TYPE

(* GtkAccelGroupEntry struct *)

	GtkAccelGroupEntry* = RECORD [noalign]
	key* : GtkAccelKey;
	closure* : lo.PGClosure;
	accel_path_quark*: lg.GQuark;
	END;

(* Flags of GtkAccelKey struct *)

CONST
	GtkAccelKey_flag0_accel_flags_Mask* = {0..15};

TYPE

(* GtkAccelKey struct *)

	GtkAccelKey* = RECORD [noalign]
	accel_key* : lg.guint;
	accel_mods*: gdk.GdkModifierType;
	flag0* : SET;
	END;

(* GtkActionEntry struct *)

	GtkActionEntry* = RECORD [noalign]
	name* : lg.Pgchar;
	stock_id* : lg.Pgchar;
	label* : lg.Pgchar;
	accelerator*: lg.Pgchar;
	tooltip* : lg.Pgchar;
	callback* : lo.GCallback;
	END;

(* GtkArg struct *)

	GtkArg* = RECORD [noalign]
	type* : GtkType;
	name* : lg.Pgchar;
	char_data*: lg.gchar;
	END;

(* GtkBindingArg struct *)

	GtkBindingArg* = RECORD [noalign]
	arg_type* : lo.GType;
	long_data*: lg.glong;
	END;

(* Flags of GtkBindingEntry struct *)

CONST
	GtkBindingEntry_flag0_destroyed_Mask* = {0};
	GtkBindingEntry_flag0_in_emission_Mask* = {1};

TYPE

(* GtkBindingEntry struct *)

	GtkBindingEntry* = RECORD [noalign]
	keyval* : lg.guint;
	modifiers* : gdk.GdkModifierType;
	binding_set*: PGtkBindingSet;
	flag0* : SET;
	hash_next* : PGtkBindingEntry;
	signals* : PGtkBindingSignal;
	END;

(* Flags of GtkBindingSet struct *)

CONST
	GtkBindingSet_flag0_parsed_Mask* = {0};

TYPE

(* GtkBindingSet struct *)

	GtkBindingSet* = RECORD [noalign]
	set_name* : lg.Pgchar;
	priority* : lg.gint;
	widget_path_pspecs* : lg.PGSList;
	widget_class_pspecs*: lg.PGSList;
	class_branch_pspecs*: lg.PGSList;
	entries* : PGtkBindingEntry;
	current* : PGtkBindingEntry;
	flag0* : SET;
	END;

(* GtkBindingSignal struct *)

	GtkBindingSignal* = RECORD [noalign]
	next* : PGtkBindingSignal;
	signal_name*: lg.Pgchar;
	n_args* : lg.guint;
	args* : PGtkBindingArg;
	END;

(* Flags of GtkBoxChild struct *)

CONST
	GtkBoxChild_flag0_expand_Mask* = {0};
	GtkBoxChild_flag0_fill_Mask* = {1};
	GtkBoxChild_flag0_pack_Mask* = {2};
	GtkBoxChild_flag0_is_secondary_Mask* = {3};

TYPE

(* GtkBoxChild struct *)

	GtkBoxChild* = RECORD [noalign]
	widget* : PGtkWidget;
	padding*: lg.guint16;
	flag0* : SET;
	END;

(* GtkFileFilterInfo struct *)

	GtkFileFilterInfo* = RECORD [noalign]
	contains* : GtkFileFilterFlags;
	filename* : lg.Pgchar;
	uri* : lg.Pgchar;
	display_name*: lg.Pgchar;
	mime_type* : lg.Pgchar;
	END;

(* GtkFixedChild struct *)

	GtkFixedChild* = RECORD [noalign]
	widget*: PGtkWidget;
	x* : lg.gint;
	y* : lg.gint;
	END;

(* GtkIMContextInfo struct *)

	GtkIMContextInfo* = RECORD [noalign]
	context_id* : lg.Pgchar;
	context_name* : lg.Pgchar;
	domain* : lg.Pgchar;
	domain_dirname* : lg.Pgchar;
	default_locales*: lg.Pgchar;
	END;

(* GtkImageAnimationData struct *)

	GtkImageAnimationData* = RECORD [noalign]
	anim* : gpb.PGdkPixbufAnimation;
	iter* : gpb.PGdkPixbufAnimationIter;
	frame_timeout*: lg.guint;
	END;

(* GtkImageGIconData struct *)

	GtkImageGIconData* = RECORD
	icon: lg.PGIcon;
	pixbuf: gpb.PGdkPixbuf;
	theme_change_id: lg.guint;
	END;

(* GtkImageIconNameData struct *)

	GtkImageIconNameData* = RECORD [noalign]
	icon_name* : lg.Pgchar;
	pixbuf* : gpb.PGdkPixbuf;
	theme_change_id*: lg.guint;
	END;

(* GtkImageIconSetData struct *)

	GtkImageIconSetData* = RECORD [noalign]
	icon_set*: PGtkIconSet;
	END;

(* GtkImageImageData struct *)

	GtkImageImageData* = RECORD [noalign]
	image*: gdk.PGdkImage;
	END;

(* GtkImagePixbufData struct *)

	GtkImagePixbufData* = RECORD [noalign]
	pixbuf*: gpb.PGdkPixbuf;
	END;

(* GtkImagePixmapData struct *)

	GtkImagePixmapData* = RECORD [noalign]
	pixmap*: gdk.PGdkPixmap;
	END;

(* GtkImageStockData struct *)

	GtkImageStockData* = RECORD [noalign]
	stock_id*: lg.Pgchar;
	END;

(* GtkItemFactoryEntry struct *)

	GtkItemFactoryEntry* = RECORD [noalign]
	path* : lg.Pgchar;
	accelerator* : lg.Pgchar;
	callback* : GtkItemFactoryCallback;
	callback_action*: lg.guint;
	item_type* : lg.Pgchar;
	extra_data* : lg.gpointer;
	END;

(* GtkItemFactoryItem struct *)

	GtkItemFactoryItem* = RECORD [noalign]
	path* : lg.Pgchar;
	widgets*: lg.PGSList;
	END;

(* GtkKeyHash struct *)

	GtkKeyHash* = RECORD [noalign]
	END;

(* GtkLabelSelectionInfo struct *)

	GtkLabelSelectionInfo* = RECORD [noalign]
	END;

(* GtkMenuEntry struct *)

	GtkMenuEntry* = RECORD [noalign]
	path* : lg.Pgchar;
	accelerator* : lg.Pgchar;
	callback* : GtkMenuCallback;
	callback_data*: lg.gpointer;
	widget* : PGtkWidget;
	END;

(* GtkMnemonicHash struct *)

	GtkMnemonicHash* = RECORD [noalign]
	END;

(* GtkNotebookPage struct *)

	GtkNotebookPage* = RECORD [noalign]
	END;

(* GtkRadioActionEntry struct *)

	GtkRadioActionEntry* = RECORD [noalign]
	name* : lg.Pgchar;
	stock_id* : lg.Pgchar;
	label* : lg.Pgchar;
	accelerator*: lg.Pgchar;
	tooltip* : lg.Pgchar;
	value* : lg.gint;
	END;

(* GtkRangeLayout struct *)

	GtkRangeLayout* = RECORD [noalign]
	END;

(* GtkRangeStepTimer struct *)

	GtkRangeStepTimer* = RECORD [noalign]
	END;

(* GtkRcContext struct *)

	GtkRcContext* = RECORD [noalign]
	END;

(* GtkRcProperty struct *)

	GtkRcProperty* = RECORD [noalign]
	type_name* : lg.GQuark;
	property_name*: lg.GQuark;
	origin* : lg.Pgchar;
	value* : lo.GValue;
	END;

(* GtkRulerMetric struct *)

	GtkRulerMetric* = RECORD [noalign]
	metric_name* : lg.Pgchar;
	abbrev* : lg.Pgchar;
	pixels_per_unit*: lg.gdouble;
	ruler_scale* : lg.gdouble;
	subdivide* : lg.gint;
	END;

(* GtkSettingsPropertyValue struct *)

	GtkSettingsPropertyValue* = RECORD [noalign]
	END;

(* GtkSettingsValue struct *)

	GtkSettingsValue* = RECORD [noalign]
	origin*: lg.Pgchar;
	value* : lo.GValue;
	END;

(* GtkStockItem struct *)

	GtkStockItem* = RECORD [noalign]
	stock_id* : lg.Pgchar;
	label* : lg.Pgchar;
	modifier* : gdk.GdkModifierType;
	keyval* : lg.guint;
	translation_domain*: lg.Pgchar;
	END;

(* Flags of GtkTableChild struct *)

CONST
	GtkTableChild_flag0_xexpand_Mask* = {0};
	GtkTableChild_flag0_yexpand_Mask* = {1};
	GtkTableChild_flag0_xshrink_Mask* = {2};
	GtkTableChild_flag0_yshrink_Mask* = {3};
	GtkTableChild_flag0_xfill_Mask* = {4};
	GtkTableChild_flag0_yfill_Mask* = {5};

TYPE

(* GtkTableChild struct *)

	GtkTableChild* = RECORD [noalign]
	widget* : PGtkWidget;
	left_attach* : lg.guint16;
	right_attach* : lg.guint16;
	top_attach* : lg.guint16;
	bottom_attach*: lg.guint16;
	xpadding* : lg.guint16;
	ypadding* : lg.guint16;
	flag0* : SET;
	END;

(* Flags of GtkTableRowCol struct *)

CONST
	GtkTableRowCol_flag0_need_expand_Mask* = {0};
	GtkTableRowCol_flag0_need_shrink_Mask* = {1};
	GtkTableRowCol_flag0_expand_Mask* = {2};
	GtkTableRowCol_flag0_shrink_Mask* = {3};
	GtkTableRowCol_flag0_empty_Mask* = {4};

TYPE

(* GtkTableRowCol struct *)

	GtkTableRowCol* = RECORD [noalign]
	requisition*: lg.guint16;
	allocation* : lg.guint16;
	spacing* : lg.guint16;
	flag0* : SET;
	END;

(* GtkTargetEntry struct *)

	GtkTargetEntry* = RECORD [noalign]
	target*: lg.Pgchar;
	flags* : GtkTargetFlags;
	info* : lg.guint;
	END;

(* GtkTargetList struct *)

	GtkTargetList* = RECORD [noalign]
	list* : lg.PGList;
	ref_count*: lg.guint;
	END;

(* GtkTargetPair struct *)

	GtkTargetPair* = RECORD [noalign]
	target*: gdk.GdkAtom;
	flags* : lg.guint;
	info* : lg.guint;
	END;

(* Flags of GtkTextAppearance struct *)

CONST
	GtkTextAppearance_flag0_underline_Mask*  = {0..3};
	GtkTextAppearance_flag0_strikethrough_Mask* = {4};
	GtkTextAppearance_flag0_draw_bg_Mask* = {5};
	GtkTextAppearance_flag0_inside_selection_Mask* = {6};
	GtkTextAppearance_flag0_is_text_Mask* = {7};
	GtkTextAppearance_flag0_pad1_Mask* = {8};
	GtkTextAppearance_flag0_pad2_Mask* = {9};
	GtkTextAppearance_flag0_pad3_Mask* = {10};
	GtkTextAppearance_flag0_pad4_Mask* = {11};

TYPE

(* GtkTextAppearance struct *)

	GtkTextAppearance* = RECORD [noalign]
	bg_color* : gdk.GdkColor;
	fg_color* : gdk.GdkColor;
	bg_stipple*: gdk.PGdkBitmap;
	fg_stipple*: gdk.PGdkBitmap;
	rise* : lg.gint;
	padding1* : lg.gpointer;
	flag0* : SET;
	END;

(* GtkTextBTree struct *)

	GtkTextBTree* = RECORD [noalign]
	END;

(* GtkTextLogAttrCache struct *)

	GtkTextLogAttrCache* = RECORD [noalign]
	END;

(* GtkTextPendingScroll struct *)

	GtkTextPendingScroll* = RECORD [noalign]
	END;

(* GtkTextWindow struct *)

	GtkTextWindow* = RECORD [noalign]
	END;

(* GtkThemeEngine struct *)

	GtkThemeEngine* = RECORD [noalign]
	END;

(* GtkToggleActionEntry struct *)

	GtkToggleActionEntry* = RECORD [noalign]
	name* : lg.Pgchar;
	stock_id* : lg.Pgchar;
	label* : lg.Pgchar;
	accelerator*: lg.Pgchar;
	tooltip* : lg.Pgchar;
	callback* : lo.GCallback;
	is_active* : lg.gboolean;
	END;

(* GtkToolbarChild struct *)

	GtkToolbarChild* = RECORD [noalign]
	type* : GtkToolbarChildType;
	widget*: PGtkWidget;
	icon* : PGtkWidget;
	label* : PGtkWidget;
	END;

(* GtkTooltipsData struct *)

	GtkTooltipsData* = RECORD [noalign]
	tooltips* : PGtkTooltips;
	widget* : PGtkWidget;
	tip_text* : lg.Pgchar;
	tip_private*: lg.Pgchar;
	END;

(* GtkTypeInfo struct *)

	GtkTypeInfo* = RECORD [noalign]
	type_name* : lg.Pgchar;
	object_size* : lg.guint;
	class_size* : lg.guint;
	class_init_func* : GtkClassInitFunc;
	object_init_func* : GtkObjectInitFunc;
	reserved_1* : lg.gpointer;
	reserved_2* : lg.gpointer;
	base_class_init_func*: GtkClassInitFunc;
	END;

(* Flags of GtkWidgetAuxInfo struct *)

CONST
	GtkWidgetAuxInfo_flag0_x_set_Mask* = {0};
	GtkWidgetAuxInfo_flag0_y_set_Mask* = {1};

TYPE

(* GtkWidgetAuxInfo struct *)

	GtkWidgetAuxInfo* = RECORD [noalign]
	x* : lg.gint;
	y* : lg.gint;
	width* : lg.gint;
	height*: lg.gint;
	flag0* : SET;
	END;

(* GtkWidgetShapeInfo struct *)

	GtkWidgetShapeInfo* = RECORD [noalign]
	offset_x* : lg.gint16;
	offset_y* : lg.gint16;
	shape_mask*: gdk.PGdkBitmap;
	END;

(* GtkWindowGeometryInfo struct *)

	GtkWindowGeometryInfo* = RECORD [noalign]
	END;

(* =================================================================== BOXEDS *)
TYPE

(* GtkBorder boxed *)

	GtkBorder* = RECORD [noalign]
	left* : lg.gint;
	right* : lg.gint;
	top* : lg.gint;
	bottom*: lg.gint;
	END;

(* GtkIconInfo boxed *)

	GtkIconInfo* = RECORD [noalign]
	END;

(* GtkIconSet boxed *)

	GtkIconSet* = RECORD [noalign]
	END;

(* GtkIconSource boxed *)

	GtkIconSource* = RECORD [noalign]
	END;

(* GtkRequisition boxed *)

	GtkRequisition* = RECORD [noalign]
	width* : lg.gint;
	height*: lg.gint;
	END;

(* GtkSelectionData boxed *)

	GtkSelectionData* = RECORD [noalign]
	selection*: gdk.GdkAtom;
	target* : gdk.GdkAtom;
	type* : gdk.GdkAtom;
	format* : lg.gint;
	data* : lg.Pguchar;
	length* : lg.gint;
	display* : gdk.PGdkDisplay;
	END;

(* GtkTextAttributes boxed *)

	GtkTextAttributes* = RECORD [noalign]
	refcount* : lg.guint;
	appearance* : GtkTextAppearance;
	justification* : GtkJustification;
	direction* : GtkTextDirection;
	font* : pan.PPangoFontDescription;
	font_scale* : lg.gdouble;
	left_margin* : lg.gint;
	indent* : lg.gint;
	right_margin* : lg.gint;
	pixels_above_lines*: lg.gint;
	pixels_below_lines*: lg.gint;
	pixels_inside_wrap*: lg.gint;
	tabs* : pan.PPangoTabArray;
	wrap_mode* : GtkWrapMode;
	language* : pan.PPangoLanguage;
	pg_bg_color* : gdk.PGdkColor;
	invisible* : lg.guint;
	bg_full_height* : lg.guint;
	editable* : lg.guint;
	realized* : lg.guint;
	pad1* : lg.guint;
	pad2* : lg.guint;
	pad3* : lg.guint;
	pad4* : lg.guint;
	END;

(* GtkTextIter boxed *)

	GtkTextIter* = RECORD [noalign]
	dummy1* : lg.gpointer;
	dummy2* : lg.gpointer;
	dummy3* : lg.gint;
	dummy4* : lg.gint;
	dummy5* : lg.gint;
	dummy6* : lg.gint;
	dummy7* : lg.gint;
	dummy8* : lg.gint;
	dummy9* : lg.gpointer;
	dummy10*: lg.gpointer;
	dummy11*: lg.gint;
	dummy12*: lg.gint;
	dummy13*: lg.gint;
	dummy14*: lg.gpointer;
	END;

(* GtkTreeIter boxed *)

	GtkTreeIter* = RECORD [noalign]
	stamp* : lg.gint;
	user_data* : lg.gpointer;
	user_data2*: lg.gpointer;
	user_data3*: lg.gpointer;
	END;

(* GtkTreePath boxed *)

	GtkTreePath* = RECORD [noalign]
	END;

(* GtkTreeRowReference boxed *)

	GtkTreeRowReference* = RECORD [noalign]
	END;

(*  ================================================================= OBJECTS *)
TYPE

(* GtkObject object *)

	GtkObject* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	flags* : lg.guint32;
	END;

(* GtkIMContext object *)

	GtkIMContext* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* GtkAction object *)

	GtkAction* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	GtkAction_pd*: lg.gpointer (* private_data *);
	END;

(* GtkAccelGroup object *)

	GtkAccelGroup* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	lock_count* : lg.guint;
	modifier_mask* : gdk.GdkModifierType;
	acceleratables* : lg.PGSList;
	n_accels* : lg.guint;
	priv_accels* : PGtkAccelGroupEntry;
	END;

(* GtkAccelMap object *)

	GtkAccelMap* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* GtkAccessible object *)

	GtkAccessible* = EXTENSIBLE RECORD [noalign] (atk.AtkObject)
	widget* : PGtkWidget;
	END;

(* GtkActionGroup object *)

	GtkActionGroup* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	GtkActionGroup_pd*: lg.gpointer (* private_data *);
	END;

(* GtkClipboard object *)

	GtkClipboard* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	END;

(* GtkEntryCompletion object *)

	GtkEntryCompletion* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	GtkEntryCompletion_pd*: lg.gpointer (* private_data *);
	END;

(* GtkIconFactory object *)

	GtkIconFactory* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	icons* : lg.PGHashTable;
	END;

(* GtkIconTheme object *)

	GtkIconTheme* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	GtkIconTheme_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkListStore object *)

CONST
	GtkListStore_flag0_columns_dirty_Mask* = {0};

TYPE

(* GtkListStore object *)

	GtkListStore* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	stamp* : lg.gint;
	seq* : lg.gpointer;
	_gtk_reserved1* : lg.gpointer;
	sort_list* : lg.PGList;
	n_columns* : lg.gint;
	sort_column_id* : lg.gint;
	order* : GtkSortType;
	column_headers* : POINTER TO ARRAY [untagged] OF lo.GType;
	length* : lg.gint;
	default_sort_func* : GtkTreeIterCompareFunc;
	default_sort_data* : lg.gpointer;
	default_sort_destroy*: GtkDestroyNotify;
	GtkListStore_flag0* : SET;
	END;

(* Flags of GtkRcStyle object *)

CONST
	GtkRcStyle_flag0_engine_specified_Mask* = {0};

TYPE

(* GtkRcStyle object *)

	GtkRcStyle* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	name* : lg.Pgchar;
	bg_pixmap_name* : lg.Pgchar;
	font_desc* : pan.PPangoFontDescription;
	color_flags* : GtkRcFlags;
	fg* : gdk.GdkColor;
	bg* : gdk.GdkColor;
	text* : gdk.GdkColor;
	base* : gdk.GdkColor;
	xthickness* : lg.gint;
	ythickness* : lg.gint;
	rc_properties* : lg.PGArray;
	rc_style_lists* : lg.PGSList;
	icon_factories* : lg.PGSList;
	GtkRcStyle_flag0*: SET;
	END;

(* GtkSettings object *)

	GtkSettings* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	queued_settings* : lo.PGData;
	property_values* : PGtkSettingsPropertyValue;
	rc_context* : PGtkRcContext;
	screen* : gdk.PGdkScreen;
	END;

(* Flags of GtkSizeGroup object *)

CONST
	GtkSizeGroup_flag0_have_width_Mask* = {0};
	GtkSizeGroup_flag0_have_height_Mask* = {1};
	GtkSizeGroup_flag0_ignore_hidden_Mask* = {2};

TYPE

(* GtkSizeGroup object *)

	GtkSizeGroup* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	widgets* : lg.PGSList;
	mode* : lg.guint8;
	GtkSizeGroup_flag0*: SET;
	END;

(* GtkStyle object *)

	GtkStyle* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	fg* : gdk.GdkColor;
	bg* : gdk.GdkColor;
	light* : gdk.GdkColor;
	dark* : gdk.GdkColor;
	mid* : gdk.GdkColor;
	text* : gdk.GdkColor;
	base* : gdk.GdkColor;
	text_aa* : gdk.GdkColor;
	black* : gdk.GdkColor;
	white* : gdk.GdkColor;
	font_desc* : pan.PPangoFontDescription;
	xthickness* : lg.gint;
	ythickness* : lg.gint;
	fg_gc* : gdk.PGdkGC;
	bg_gc* : gdk.PGdkGC;
	light_gc* : gdk.PGdkGC;
	dark_gc* : gdk.PGdkGC;
	mid_gc* : gdk.PGdkGC;
	text_gc* : gdk.PGdkGC;
	base_gc* : gdk.PGdkGC;
	text_aa_gc* : gdk.PGdkGC;
	black_gc* : gdk.PGdkGC;
	white_gc* : gdk.PGdkGC;
	bg_pixmap* : gdk.PGdkPixmap;
	attach_count* : lg.gint;
	depth* : lg.gint;
	colormap* : gdk.PGdkColormap;
	private_font* : gdk.PGdkFont;
	private_font_desc*: pan.PPangoFontDescription;
	rc_style* : PGtkRcStyle;
	styles* : lg.PGSList;
	property_cache* : lg.PGArray;
	icon_factories* : lg.PGSList;
	END;

(* Flags of GtkTextBuffer object *)

CONST
	GtkTextBuffer_flag0_modified_Mask* = {0};

TYPE

(* GtkTextBuffer object *)

	GtkTextBuffer* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	tag_table* : PGtkTextTagTable;
	btree* : PGtkTextBTree;
	clipboard_contents_buffers*: lg.PGSList;
	selection_clipboards* : lg.PGSList;
	log_attr_cache* : PGtkTextLogAttrCache;
	user_action_count* : lg.guint;
	GtkTextBuffer_flag0* : SET;
	END;

(* GtkTextChildAnchor object *)

	GtkTextChildAnchor* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	segment* : lg.gpointer;
	END;

(* GtkTextMark object *)

	GtkTextMark* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	segment* : lg.gpointer;
	END;

(* Flags of GtkTextTag object *)

CONST
	GtkTextTag_flag0_bg_color_set_Mask*  = {0};
	GtkTextTag_flag0_bg_stipple_set_Mask* = {1};
	GtkTextTag_flag0_fg_color_set_Mask*  = {2};
	GtkTextTag_flag0_scale_set_Mask*  = {3};
	GtkTextTag_flag0_fg_stipple_set_Mask* = {4};
	GtkTextTag_flag0_justification_set_Mask* = {5};
	GtkTextTag_flag0_left_margin_set_Mask*  = {6};
	GtkTextTag_flag0_indent_set_Mask* = {7};
	GtkTextTag_flag0_rise_set_Mask* = {8};
	GtkTextTag_flag0_strikethrough_set_Mask* = {9};
	GtkTextTag_flag0_right_margin_set_Mask* = {10};
	GtkTextTag_flag0_pixels_above_lines_set_Mask* = {11};
	GtkTextTag_flag0_pixels_below_lines_set_Mask* = {12};
	GtkTextTag_flag0_pixels_inside_wrap_set_Mask* = {13};
	GtkTextTag_flag0_tabs_set_Mask* = {14};
	GtkTextTag_flag0_underline_set_Mask* = {15};
	GtkTextTag_flag0_wrap_mode_set_Mask* = {16};
	GtkTextTag_flag0_bg_full_height_set_Mask*  = {17};
	GtkTextTag_flag0_invisible_set_Mask* = {18};
	GtkTextTag_flag0_editable_set_Mask*  = {19};
	GtkTextTag_flag0_language_set_Mask*  = {20};
	GtkTextTag_flag0_pg_bg_color_set_Mask*  = {21};
	GtkTextTag_flag0_pad1_Mask* = {22};
	GtkTextTag_flag0_pad2_Mask* = {23};

TYPE

(* GtkTextTag object *)

	GtkTextTag* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	table* : PGtkTextTagTable;
	name* : lg.Pgchar;
	priority* : lg.int;
	values* : PGtkTextAttributes;
	GtkTextTag_flag0*: SET;
	END;

(* GtkTextTagTable object *)

	GtkTextTagTable* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	hash* : lg.PGHashTable;
	anonymous* : lg.PGSList;
	anon_count* : lg.gint;
	buffers* : lg.PGSList;
	END;

(* GtkTreeModelFilter object *)

	GtkTreeModelFilter* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	GtkTreeModelFilter_pd*: lg.gpointer (* private_data *);
	END;

(* GtkTreeModelSort object *)

	GtkTreeModelSort* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	root* : lg.gpointer;
	stamp* : lg.gint;
	child_flags* : lg.guint;
	child_model* : PGtkTreeModel;
	zero_ref_count* : lg.gint;
	sort_list* : lg.PGList;
	sort_column_id* : lg.gint;
	order* : GtkSortType;
	default_sort_func* : GtkTreeIterCompareFunc;
	default_sort_data* : lg.gpointer;
	default_sort_destroy* : GtkDestroyNotify;
	changed_id* : lg.guint;
	inserted_id* : lg.guint;
	has_child_toggled_id* : lg.guint;
	deleted_id* : lg.guint;
	reordered_id* : lg.guint;
	END;

(* GtkTreeSelection object *)

	GtkTreeSelection* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	tree_view* : PGtkTreeView;
	type* : GtkSelectionMode;
	user_func* : GtkTreeSelectionFunc;
	user_data* : lg.gpointer;
	destroy* : GtkDestroyNotify;
	END;

(* Flags of GtkTreeStore object *)

CONST
	GtkTreeStore_flag0_columns_dirty_Mask* = {0};

TYPE

(* GtkTreeStore object *)

	GtkTreeStore* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	stamp* : lg.gint;
	root* : lg.gpointer;
	last* : lg.gpointer;
	n_columns* : lg.gint;
	sort_column_id* : lg.gint;
	sort_list* : lg.PGList;
	order* : GtkSortType;
	column_headers* : POINTER TO ARRAY [untagged] OF lo.GType;
	default_sort_func* : GtkTreeIterCompareFunc;
	default_sort_data* : lg.gpointer;
	default_sort_destroy*: GtkDestroyNotify;
	GtkTreeStore_flag0* : SET;
	END;

(* GtkUIManager object *)

	GtkUIManager* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	GtkUIManager_pd*: lg.gpointer (* private_data *);
	END;

(* GtkWindowGroup object *)

	GtkWindowGroup* = EXTENSIBLE RECORD [noalign] (lo.GObject)
	grabs* : lg.PGSList;
	END;

(* GtkStock object *)

	GtkStock* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkToggleAction object *)

	GtkToggleAction* = EXTENSIBLE RECORD [noalign] (GtkAction)
	GtkToggleAction_pd*: lg.gpointer (* private_data *);
	END;

(* GtkRadioAction object *)

	GtkRadioAction* = EXTENSIBLE RECORD [noalign] (GtkToggleAction)
	GtkRadioAction_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkIMContextSimple object *)

CONST
	GtkIMContextSimple_flag0_in_hex_sequence_Mask* = {0};

TYPE

(* GtkIMContextSimple object *)

	GtkIMContextSimple* = EXTENSIBLE RECORD [noalign] (GtkIMContext)
	tables* : lg.PGSList;
	compose_buffer* : lg.guint;
	tentative_match* : lg.gunichar;
	tentative_match_len* : lg.gint;
	GtkIMContextSimple_flag0*: SET;
	END;

(* GtkIMMulticontext object *)

	GtkIMMulticontext* = EXTENSIBLE RECORD [noalign] (GtkIMContext)
	slave* : PGtkIMContext;
	GtkIMMulticontext_pd*: lg.gpointer (* private_data *);
	context_id* : lg.Pgchar;
	END;

(* GtkWidget object *)

	GtkWidget* = EXTENSIBLE RECORD [noalign] (GtkObject)
	private_flags* : lg.guint16;
	state* : lg.guint8;
	saved_state* : lg.guint8;
	name* : lg.Pgchar;
	style* : PGtkStyle;
	requisition* : GtkRequisition;
	allocation* : GtkAllocation;
	window* : gdk.PGdkWindow;
	parent* : PGtkWidget;
	END;

(* Flags of GtkCellRenderer object *)

CONST
	GtkCellRenderer_flag0_mode_Mask* = {0..1};
	GtkCellRenderer_flag0_visible_Mask* = {2};
	GtkCellRenderer_flag0_is_expander_Mask* = {3};
	GtkCellRenderer_flag0_is_expanded_Mask* = {4};
	GtkCellRenderer_flag0_cell_background_set_Mask* = {5};
	GtkCellRenderer_flag0_sensitive_Mask*  = {6};
	GtkCellRenderer_flag0_editing_Mask* = {7};

TYPE

(* GtkCellRenderer object *)

	GtkCellRenderer* = EXTENSIBLE RECORD [noalign] (GtkObject)
	xalign* : lg.gfloat;
	yalign* : lg.gfloat;
	width* : lg.gint;
	height* : lg.gint;
	xpad* : lg.guint16;
	ypad* : lg.guint16;
	GtkCellRenderer_flag0*: SET;
	END;

(* GtkAdjustment object *)

	GtkAdjustment* = EXTENSIBLE RECORD [noalign] (GtkObject)
	lower* : lg.gdouble;
	upper* : lg.gdouble;
	value* : lg.gdouble;
	step_increment* : lg.gdouble;
	page_increment* : lg.gdouble;
	page_size* : lg.gdouble;
	END;

(* GtkFileFilter object *)

	GtkFileFilter* = EXTENSIBLE RECORD [noalign] (GtkObject)
	END;

(* GtkItemFactory object *)

	GtkItemFactory* = EXTENSIBLE RECORD [noalign] (GtkObject)
	path* : lg.Pgchar;
	accel_group* : PGtkAccelGroup;
	widget* : PGtkWidget;
	items* : lg.PGSList;
	translate_func* : GtkTranslateFunc;
	translate_data* : lg.gpointer;
	translate_notify* : GtkDestroyNotify;
	END;

(* Flags of GtkTooltips object *)

CONST
	GtkTooltips_flag0_delay_Mask* = {0..29};
	GtkTooltips_flag0_enabled_Mask* = {30};
	GtkTooltips_flag0_have_grab_Mask*  = {31};
	GtkTooltips_flag1_use_sticky_delay_Mask* = {0};

TYPE

(* GtkTooltips object *)

	GtkTooltips* = EXTENSIBLE RECORD [noalign] (GtkObject)
	tip_window* : PGtkWidget;
	tip_label* : PGtkWidget;
	active_tips_data* : PGtkTooltipsData;
	tips_data_list* : lg.PGList;
	GtkTooltips_flag0*: SET;
	GtkTooltips_flag1*: SET;
	last_popdown* : lg.GTimeVal;
	END;

(* Flags of GtkTreeViewColumn object *)

CONST
	GtkTreeViewColumn_flag0_visible_Mask* = {0};
	GtkTreeViewColumn_flag0_resizable_Mask*  = {1};
	GtkTreeViewColumn_flag0_clickable_Mask*  = {2};
	GtkTreeViewColumn_flag0_dirty_Mask* = {3};
	GtkTreeViewColumn_flag0_show_sort_indicator_Mask* = {4};
	GtkTreeViewColumn_flag0_maybe_reordered_Mask*  = {5};
	GtkTreeViewColumn_flag0_reorderable_Mask* = {6};
	GtkTreeViewColumn_flag0_use_resized_width_Mask* = {7};
	GtkTreeViewColumn_flag0_expand_Mask*  = {8};

TYPE

(* GtkTreeViewColumn object *)

	GtkTreeViewColumn* = EXTENSIBLE RECORD [noalign] (GtkObject)
	tree_view* : PGtkWidget;
	button* : PGtkWidget;
	child* : PGtkWidget;
	arrow* : PGtkWidget;
	alignment* : PGtkWidget;
	window* : gdk.PGdkWindow;
	editable_widget* : PGtkCellEditable;
	xalign* : lg.gfloat;
	property_changed_signal* : lg.guint;
	spacing* : lg.gint;
	column_type* : GtkTreeViewColumnSizing;
	requested_width* : lg.gint;
	button_request* : lg.gint;
	resized_width* : lg.gint;
	width* : lg.gint;
	fixed_width* : lg.gint;
	min_width* : lg.gint;
	max_width* : lg.gint;
	drag_x* : lg.gint;
	drag_y* : lg.gint;
	title* : lg.Pgchar;
	cell_list* : lg.PGList;
	sort_clicked_signal* : lg.guint;
	sort_column_changed_signal*: lg.guint;
	sort_column_id* : lg.gint;
	sort_order* : GtkSortType;
	GtkTreeViewColumn_flag0* : SET;
	END;

(* Flags of GtkContainer object *)

CONST
	GtkContainer_flag0_border_width_Mask* = {0..15};
	GtkContainer_flag0_need_resize_Mask*  = {16};
	GtkContainer_flag0_resize_mode_Mask*  = {17..18};
	GtkContainer_flag0_reallocate_redraws_Mask* = {19};
	GtkContainer_flag0_has_focus_chain_Mask* = {20};

TYPE

(* GtkContainer object *)

	GtkContainer* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	focus_child* : PGtkWidget;
	GtkContainer_flag0*: SET;
	END;

(* GtkMisc object *)

	GtkMisc* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	xalign* : lg.gfloat;
	yalign* : lg.gfloat;
	xpad* : lg.guint16;
	ypad* : lg.guint16;
	END;

(* Flags of GtkRange object *)

CONST
	GtkRange_flag0_inverted_Mask* = {0};
	GtkRange_flag0_flippable_Mask* = {1};
	GtkRange_flag0_has_stepper_a_Mask*  = {2};
	GtkRange_flag0_has_stepper_b_Mask*  = {3};
	GtkRange_flag0_has_stepper_c_Mask*  = {4};
	GtkRange_flag0_has_stepper_d_Mask*  = {5};
	GtkRange_flag0_need_recalc_Mask* = {6};
	GtkRange_flag0_slider_size_fixed_Mask* = {7};

	GtkRange_flag1_trough_click_forward_Mask* = {0};
	GtkRange_flag1_update_pending_Mask* = {1};

TYPE

(* GtkRange object *)

	GtkRange* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	adjustment* : PGtkAdjustment;
	update_policy* : GtkUpdateType;
	GtkRange_flag0* : SET;
	orientation* : GtkOrientation;
	range_rect* : gdk.GdkRectangle;
	slider_start* : lg.gint;
	slider_end* : lg.gint;
	round_digits* : lg.gint;
	GtkRange_flag1* : SET;
	timer* : PGtkRangeStepTimer;
	slide_initial_slider_position*: lg.gint;
	slide_initial_coordinate* : lg.gint;
	update_timeout_id* : lg.guint;
	event_window* : gdk.PGdkWindow;
	END;

(* GtkRuler object *)

	GtkRuler* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	backing_store* : gdk.PGdkPixmap;
	non_gr_exp_gc* : gdk.PGdkGC;
	metric* : PGtkRulerMetric;
	xsrc* : lg.gint;
	ysrc* : lg.gint;
	slider_size* : lg.gint;
	lower* : lg.gdouble;
	upper* : lg.gdouble;
	position* : lg.gdouble;
	max_size* : lg.gdouble;
	END;

(* GtkSeparator object *)

	GtkSeparator* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	END;

(* GtkDrawingArea object *)

	GtkDrawingArea* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	draw_data* : lg.gpointer;
	END;

(* Flags of GtkEntry object *)

CONST
	GtkEntry_flag0_editable_Mask* = {0};
	GtkEntry_flag0_visible_Mask* = {1};
	GtkEntry_flag0_overwrite_mode_Mask* = {2};
	GtkEntry_flag0_in_drag_Mask* = {3};

	GtkEntry_flag1_cache_includes_preedit_Mask* = {0};
	GtkEntry_flag1_need_im_reset_Mask* = {1};
	GtkEntry_flag1_has_frame_Mask*  = {2};
	GtkEntry_flag1_activates_default_Mask* = {3};
	GtkEntry_flag1_cursor_visible_Mask* = {4};
	GtkEntry_flag1_in_click_Mask* = {5};
	GtkEntry_flag1_is_cell_renderer_Mask* = {6};
	GtkEntry_flag1_editing_canceled_Mask* = {7};
	GtkEntry_flag1_mouse_cursor_obscured_Mask*  = {8};
	GtkEntry_flag1_select_words_Mask*  = {9};
	GtkEntry_flag1_select_lines_Mask*  = {10};
	GtkEntry_flag1_resolved_dir_Mask*  = {11..14};

TYPE

(* GtkEntry object *)

	GtkEntry* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	text* : lg.Pgchar;
	GtkEntry_flag0* : SET;
	text_max_length*: lg.guint16;
	text_area* : gdk.PGdkWindow;
	im_context* : PGtkIMContext;
	popup_menu* : PGtkWidget;
	current_pos* : lg.gint;
	selection_bound*: lg.gint;
	cached_layout* : pan.PPangoLayout;
	GtkEntry_flag1* : SET;
	blink_timeout* : lg.guint;
	recompute_idle* : lg.guint;
	scroll_offset* : lg.gint;
	ascent* : lg.gint;
	descent* : lg.gint;
	text_size* : lg.guint16;
	n_bytes* : lg.guint16;
	preedit_length* : lg.guint16;
	preedit_cursor* : lg.guint16;
	dnd_position* : lg.gint;
	drag_start_x* : lg.gint;
	drag_start_y* : lg.gint;
	invisible_char* : lg.gunichar;
	width_chars* : lg.gint;
	END;

(* GtkCalendar object *)

	GtkCalendar* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	header_style* : PGtkStyle;
	label_style* : PGtkStyle;
	month* : lg.gint;
	year* : lg.gint;
	selected_day* : lg.gint;
	day_month* : lg.gint;
	day* : lg.gint;
	num_marked_dates* : lg.gint;
	marked_date* : lg.gint;
	display_flags* : GtkCalendarDisplayOptions;
	marked_date_color*: gdk.GdkColor;
	gc* : gdk.PGdkGC;
	xor_gc* : gdk.PGdkGC;
	focus_row* : lg.gint;
	focus_col* : lg.gint;
	highlight_row* : lg.gint;
	highlight_col* : lg.gint;
	GtkCalendar_pd*: lg.gpointer (* private_data *);
	grow_space* : lg.gchar;
	END;

(* GtkCellView object *)

	GtkCellView* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	GtkCellView_pd*: lg.gpointer (* private_data *);
	END;

(* GtkHSV object *)

	GtkHSV* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	GtkHSV_pd*: lg.gpointer (* private_data *);
	END;

(* GtkInvisible object *)

	GtkInvisible* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	has_user_ref_count*: lg.gboolean;
	screen* : gdk.PGdkScreen;
	END;

(* Flags of GtkProgressBar object *)

CONST
	GtkProgressBar_flag0_activity_dir_Mask* = {0};
	GtkProgressBar_flag0_ellipsize_Mask* = {1..3};

TYPE

(* GtkProgressBar object *)

	GtkProgressBar* = EXTENSIBLE RECORD [noalign] (GtkWidget)
	bar_style* : GtkProgressBarStyle;
	orientation* : GtkProgressBarOrientation;
	blocks* : lg.guint;
	in_block* : lg.gint;
	activity_pos* : lg.gint;
	activity_step* : lg.guint;
	activity_blocks* : lg.guint;
	pulse_fraction* : lg.gdouble;
	GtkProgressBar_flag0*: SET;
	END;

(* GtkBin object *)

	GtkBin* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	child* : PGtkWidget;
	END;

(* Flags of GtkBox object *)

CONST
	GtkBox_flag0_homogeneous_Mask* = {0};

TYPE

(* GtkBox object *)

	GtkBox* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	children* : lg.PGList;
	spacing* : lg.gint16;
	GtkBox_flag0*: SET;
	END;

(* Flags of GtkMenuShell object *)

CONST
	GtkMenuShell_flag0_active_Mask* = {0};
	GtkMenuShell_flag0_have_grab_Mask* = {1};
	GtkMenuShell_flag0_have_xgrab_Mask* = {2};
	GtkMenuShell_flag0_ignore_leave_Mask* = {3};
	GtkMenuShell_flag0_menu_flag_Mask* = {4};
	GtkMenuShell_flag0_ignore_enter_Mask* = {5};

TYPE

(* GtkMenuShell object *)

	GtkMenuShell* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	children* : lg.PGList;
	active_menu_item* : PGtkWidget;
	parent_menu_shell* : PGtkWidget;
	button* : lg.guint;
	activate_time* : lg.guint32;
	GtkMenuShell_flag0*: SET;
	END;

(* Flags of GtkPaned object *)

CONST
	GtkPaned_flag0_position_set_Mask*  = {0};
	GtkPaned_flag0_in_drag_Mask* = {1};
	GtkPaned_flag0_child1_shrink_Mask* = {2};
	GtkPaned_flag0_child1_resize_Mask* = {3};
	GtkPaned_flag0_child2_shrink_Mask* = {4};
	GtkPaned_flag0_child2_resize_Mask* = {5};
	GtkPaned_flag0_orientation_Mask* = {6};
	GtkPaned_flag0_in_recursion_Mask*  = {7};
	GtkPaned_flag0_handle_prelit_Mask* = {8};

TYPE

(* GtkPaned object *)

	GtkPaned* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	child1* : PGtkWidget;
	child2* : PGtkWidget;
	handle* : gdk.PGdkWindow;
	xor_gc* : gdk.PGdkGC;
	cursor_type* : gdk.GdkCursorType;
	handle_pos* : gdk.GdkRectangle;
	child1_size* : lg.gint;
	last_allocation* : lg.gint;
	min_position* : lg.gint;
	max_position* : lg.gint;
	GtkPaned_flag0* : SET;
	last_child2_focus*: PGtkWidget;
	GtkPaned_pd*: lg.gpointer (* private_data *);
	drag_pos* : lg.gint;
	original_position*: lg.gint;
	END;

(* GtkFixed object *)

	GtkFixed* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	children* : lg.PGList;
	END;

(* GtkIconView object *)

	GtkIconView* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	GtkIconView_pd*: lg.gpointer (* private_data *);
	END;

(* GtkLayout object *)

	GtkLayout* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	children* : lg.PGList;
	width* : lg.guint;
	height* : lg.guint;
	hadjustment* : PGtkAdjustment;
	vadjustment* : PGtkAdjustment;
	bin_window* : gdk.PGdkWindow;
	visibility* : gdk.GdkVisibilityState;
	scroll_x* : lg.gint;
	scroll_y* : lg.gint;
	freeze_count* : lg.guint;
	END;

(* Flags of GtkNotebook object *)

CONST
	GtkNotebook_flag0_show_tabs_Mask*  = {0};
	GtkNotebook_flag0_homogeneous_Mask* = {1};
	GtkNotebook_flag0_show_border_Mask* = {2};
	GtkNotebook_flag0_tab_pos_Mask* = {3..4};
	GtkNotebook_flag0_scrollable_Mask* = {5};
	GtkNotebook_flag0_in_child_Mask* = {6..8};
	GtkNotebook_flag0_click_child_Mask* = {9..11};
	GtkNotebook_flag0_button_Mask*  = {12..13};
	GtkNotebook_flag0_need_timer_Mask* = {14};
	GtkNotebook_flag0_child_has_focus_Mask*  = {15};
	GtkNotebook_flag0_have_visible_child_Mask*  = {16};
	GtkNotebook_flag0_focus_out_Mask*  = {17};
	GtkNotebook_flag0_has_before_previous_Mask* = {18};
	GtkNotebook_flag0_has_before_next_Mask*  = {19};
	GtkNotebook_flag0_has_after_previous_Mask*  = {20};
	GtkNotebook_flag0_has_after_next_Mask* = {21};

TYPE

(* GtkNotebook object *)

	GtkNotebook* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	cur_page* : PGtkNotebookPage;
	children* : lg.PGList;
	first_tab* : lg.PGList;
	focus_tab* : lg.PGList;
	menu* : PGtkWidget;
	event_window* : gdk.PGdkWindow;
	timer* : lg.guint32;
	tab_hborder* : lg.guint16;
	tab_vborder* : lg.guint16;
	GtkNotebook_flag0*: SET;
	END;

(* Flags of GtkSocket object *)

CONST
	GtkSocket_flag0_same_app_Mask*  = {0};
	GtkSocket_flag0_focus_in_Mask*  = {1};
	GtkSocket_flag0_have_size_Mask* = {2};
	GtkSocket_flag0_need_map_Mask*  = {3};
	GtkSocket_flag0_is_mapped_Mask* = {4};
	GtkSocket_flag0_active_Mask* = {5};

TYPE

(* GtkSocket object *)

	GtkSocket* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	request_width* : lg.guint16;
	request_height* : lg.guint16;
	current_width* : lg.guint16;
	current_height* : lg.guint16;
	plug_window* : gdk.PGdkWindow;
	plug_widget* : PGtkWidget;
	xembed_version* : lg.gshort;
	GtkSocket_flag0*: SET;
	toplevel* : PGtkWidget;
	END;

(* Flags of GtkTable object *)

CONST
	GtkTable_flag0_homogeneous_Mask* = {0};

TYPE

(* GtkTable object *)

	GtkTable* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	children* : lg.PGList;
	rows* : PGtkTableRowCol;
	cols* : PGtkTableRowCol;
	nrows* : lg.guint16;
	ncols* : lg.guint16;
	column_spacing*: lg.guint16;
	row_spacing* : lg.guint16;
	GtkTable_flag0*: SET;
	END;

(* Flags of GtkTextView object *)

CONST
	GtkTextView_flag0_editable_Mask*  = {0};
	GtkTextView_flag0_overwrite_mode_Mask*  = {1};
	GtkTextView_flag0_cursor_visible_Mask*  = {2};
	GtkTextView_flag0_need_im_reset_Mask* = {3};
	GtkTextView_flag0_accepts_tab_Mask*  = {4};
	GtkTextView_flag0_width_changed_Mask* = {5};
	GtkTextView_flag0_onscreen_validated_Mask* = {6};
	GtkTextView_flag0_mouse_cursor_obscured_Mask* = {7};

TYPE

(* GtkTextView object *)

	GtkTextView* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	layout* : lg.gpointer;
	buffer* : PGtkTextBuffer;
	selection_drag_handler* : lg.guint;
	scroll_timeout* : lg.guint;
	pixels_above_lines* : lg.gint;
	pixels_below_lines* : lg.gint;
	pixels_inside_wrap* : lg.gint;
	wrap_mode* : GtkWrapMode;
	justify* : GtkJustification;
	left_margin* : lg.gint;
	right_margin* : lg.gint;
	indent* : lg.gint;
	tabs* : pan.PPangoTabArray;
	GtkTextView_flag0* : SET;
	left_window* : PGtkTextWindow;
	right_window* : PGtkTextWindow;
	top_window* : PGtkTextWindow;
	bottom_window* : PGtkTextWindow;
	hadjustment* : PGtkAdjustment;
	vadjustment* : PGtkAdjustment;
	xoffset* : lg.gint;
	yoffset* : lg.gint;
	width* : lg.gint;
	height* : lg.gint;
	virtual_cursor_x* : lg.gint;
	virtual_cursor_y* : lg.gint;
	first_para_mark* : PGtkTextMark;
	first_para_pixels* : lg.gint;
	dnd_mark* : PGtkTextMark;
	blink_timeout* : lg.guint;
	first_validate_idle* : lg.guint;
	incremental_validate_idle* : lg.guint;
	im_context* : PGtkIMContext;
	popup_menu* : PGtkWidget;
	drag_start_x* : lg.gint;
	drag_start_y* : lg.gint;
	children* : lg.PGSList;
	pending_scroll* : PGtkTextPendingScroll;
	pending_place_cursor_button*: lg.gint;
	END;

(* Flags of GtkToolbar object *)

CONST
	GtkToolbar_flag0_style_set_Mask*  = {0};
	GtkToolbar_flag0_icon_size_set_Mask* = {1};

TYPE

(* GtkToolbar object *)

	GtkToolbar* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	num_children* : lg.gint;
	children* : lg.PGList;
	orientation* : GtkOrientation;
	style_* : GtkToolbarStyle;
	icon_size* : GtkIconSize;
	tooltips* : PGtkTooltips;
	button_maxw* : lg.gint;
	button_maxh* : lg.gint;
	_gtk_reserved1* : lg.guint;
	_gtk_reserved2* : lg.guint;
	GtkToolbar_flag0*: SET;
	END;

(* GtkTreeView object *)

	GtkTreeView* = EXTENSIBLE RECORD [noalign] (GtkContainer)
	GtkTreeView_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkButton object *)

CONST
	GtkButton_flag0_constructed_Mask* = {0};
	GtkButton_flag0_in_button_Mask*  = {1};
	GtkButton_flag0_button_down_Mask* = {2};
	GtkButton_flag0_relief_Mask*  = {3..4};
	GtkButton_flag0_use_underline_Mask* = {5};
	GtkButton_flag0_use_stock_Mask*  = {6};
	GtkButton_flag0_depressed_Mask*  = {7};
	GtkButton_flag0_depress_on_activate_Mask* = {8};
	GtkButton_flag0_focus_on_click_Mask* = {9};

TYPE

(* GtkButton object *)

	GtkButton* = EXTENSIBLE RECORD [noalign] (GtkBin)
	event_window* : gdk.PGdkWindow;
	label_text* : lg.Pgchar;
	activate_timeout*: lg.guint;
	GtkButton_flag0* : SET;
	END;

(* GtkToolItem object *)

	GtkToolItem* = EXTENSIBLE RECORD [noalign] (GtkBin)
	GtkToolItem_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkWindow object *)

CONST
	GtkWindow_flag0_allow_shrink_Mask*  = {0};
	GtkWindow_flag0_allow_grow_Mask* = {1};
	GtkWindow_flag0_configure_notify_received_Mask* = {2};
	GtkWindow_flag0_need_default_position_Mask*  = {3};
	GtkWindow_flag0_need_default_size_Mask* = {4};
	GtkWindow_flag0_position_Mask* = {5..7};
	GtkWindow_flag0_type_Mask* = {8..11};
	GtkWindow_flag0_has_user_ref_count_Mask*  = {12};
	GtkWindow_flag0_has_focus_Mask*  = {13};
	GtkWindow_flag0_modal_Mask* = {14};
	GtkWindow_flag0_destroy_with_parent_Mask* = {15};
	GtkWindow_flag0_has_frame_Mask*  = {16};
	GtkWindow_flag0_iconify_initially_Mask* = {17};
	GtkWindow_flag0_stick_initially_Mask*  = {18};
	GtkWindow_flag0_maximize_initially_Mask*  = {19};
	GtkWindow_flag0_decorated_Mask*  = {20};
	GtkWindow_flag0_type_hint_Mask*  = {21..23};
	GtkWindow_flag0_gravity_Mask* = {24..28};
	GtkWindow_flag0_is_active_Mask*  = {29};
	GtkWindow_flag0_has_toplevel_focus_Mask*  = {30};

TYPE

(* GtkWindow object *)

	GtkWindow* = EXTENSIBLE RECORD [noalign] (GtkBin)
	title* : lg.Pgchar;
	wmclass_name* : lg.Pgchar;
	wmclass_class* : lg.Pgchar;
	wm_role* : lg.Pgchar;
	focus_widget* : PGtkWidget;
	default_widget* : PGtkWidget;
	transient_parent* : PGtkWindow;
	geometry_info* : PGtkWindowGeometryInfo;
	frame* : gdk.PGdkWindow;
	group* : PGtkWindowGroup;
	configure_request_count*: lg.guint16;
	GtkWindow_flag0* : SET;
	frame_top* : lg.guint;
	frame_right* : lg.guint;
	frame_bottom* : lg.guint;
	keys_changed_handler* : lg.guint;
	mnemonic_modifier* : gdk.GdkModifierType;
	screen* : gdk.PGdkScreen;
	END;

(* GtkComboBox object *)

	GtkComboBox* = EXTENSIBLE RECORD [noalign] (GtkBin)
	GtkComboBox_pd*: lg.gpointer (* private_data *);
	END;

(* GtkFrame object *)

	GtkFrame* = EXTENSIBLE RECORD [noalign] (GtkBin)
	label_widget* : PGtkWidget;
	shadow_type* : lg.gint16;
	label_xalign* : lg.gfloat;
	label_yalign* : lg.gfloat;
	child_allocation*: GtkAllocation;
	END;

(* GtkItem object *)

	GtkItem* = EXTENSIBLE RECORD [noalign] (GtkBin)
	END;

(* GtkAlignment object *)

	GtkAlignment* = EXTENSIBLE RECORD [noalign] (GtkBin)
	xalign* : lg.gfloat;
	yalign* : lg.gfloat;
	xscale* : lg.gfloat;
	yscale* : lg.gfloat;
	END;

(* GtkEventBox object *)

	GtkEventBox* = EXTENSIBLE RECORD [noalign] (GtkBin)
	END;

(* GtkExpander object *)

	GtkExpander* = EXTENSIBLE RECORD [noalign] (GtkBin)
	GtkExpander_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkHandleBox object *)

CONST
	GtkHandleBox_flag0_handle_position_Mask*  = {0..1};
	GtkHandleBox_flag0_float_window_mapped_Mask* = {2};
	GtkHandleBox_flag0_child_detached_Mask* = {3};
	GtkHandleBox_flag0_in_drag_Mask* = {4};
	GtkHandleBox_flag0_shrink_on_detach_Mask* = {5};
	GtkHandleBox_flag0_snap_edge_Mask*  = {6..8};

TYPE

(* GtkHandleBox object *)

	GtkHandleBox* = EXTENSIBLE RECORD [noalign] (GtkBin)
	bin_window* : gdk.PGdkWindow;
	float_window* : gdk.PGdkWindow;
	shadow_type* : GtkShadowType;
	GtkHandleBox_flag0*: SET;
	deskoff_y* : lg.gint;
	attach_allocation* : GtkAllocation;
	float_allocation* : GtkAllocation;
	END;

(* Flags of GtkScrolledWindow object *)

CONST
	GtkScrolledWindow_flag0_hscrollbar_policy_Mask*  = {0..1};
	GtkScrolledWindow_flag0_vscrollbar_policy_Mask*  = {2..3};
	GtkScrolledWindow_flag0_hscrollbar_visible_Mask* = {4};
	GtkScrolledWindow_flag0_vscrollbar_visible_Mask* = {5};
	GtkScrolledWindow_flag0_window_placement_Mask* = {6..7};
	GtkScrolledWindow_flag0_focus_out_Mask* = {8};

TYPE

(* GtkScrolledWindow object *)

	GtkScrolledWindow* = EXTENSIBLE RECORD [noalign] (GtkBin)
	hscrollbar* : PGtkWidget;
	vscrollbar* : PGtkWidget;
	GtkScrolledWindow_flag0*: SET;
	END;

(* GtkViewport object *)

	GtkViewport* = EXTENSIBLE RECORD [noalign] (GtkBin)
	shadow_type* : GtkShadowType;
	view_window* : gdk.PGdkWindow;
	bin_window* : gdk.PGdkWindow;
	hadjustment* : PGtkAdjustment;
	vadjustment* : PGtkAdjustment;
	END;

(* Flags of GtkToggleButton object *)

CONST
	GtkToggleButton_flag0_active_Mask* = {0};
	GtkToggleButton_flag0_draw_indicator_Mask* = {1};
	GtkToggleButton_flag0_inconsistent_Mask* = {2};

TYPE

(* GtkToggleButton object *)

	GtkToggleButton* = EXTENSIBLE RECORD [noalign] (GtkButton)
	GtkToggleButton_flag0*: SET;
	END;

(* GtkColorButton object *)

	GtkColorButton* = EXTENSIBLE RECORD [noalign] (GtkButton)
	GtkColorButton_pd*: lg.gpointer (* private_data *);
	END;

(* GtkFontButton object *)

	GtkFontButton* = EXTENSIBLE RECORD [noalign] (GtkButton)
	GtkFontButton_pd*: lg.gpointer (* private_data *);
	END;

(* GtkOptionMenu object *)

	GtkOptionMenu* = EXTENSIBLE RECORD [noalign] (GtkButton)
	menu* : PGtkWidget;
	menu_item* : PGtkWidget;
	width* : lg.guint16;
	height* : lg.guint16;
	END;

(* GtkCheckButton object *)

	GtkCheckButton* = EXTENSIBLE RECORD [noalign] (GtkToggleButton)
	END;

(* GtkRadioButton object *)

	GtkRadioButton* = EXTENSIBLE RECORD [noalign] (GtkCheckButton)
	group* : lg.PGSList;
	END;

(* GtkToolButton object *)

	GtkToolButton* = EXTENSIBLE RECORD [noalign] (GtkToolItem)
	GtkToolButton_pd*: lg.gpointer (* private_data *);
	END;

(* GtkSeparatorToolItem object *)

	GtkSeparatorToolItem* = EXTENSIBLE RECORD [noalign] (GtkToolItem)
	GtkSeparatorToolItem_pd*: lg.gpointer (* private_data *);
	END;

(* GtkToggleToolButton object *)

	GtkToggleToolButton* = EXTENSIBLE RECORD [noalign] (GtkToolButton)
	GtkToggleToolButton_pd*: lg.gpointer (* private_data *);
	END;

(* GtkMenuToolButton object *)

	GtkMenuToolButton* = EXTENSIBLE RECORD [noalign] (GtkToolButton)
	GtkMenuToolButton_pd*: lg.gpointer (* private_data *);
	END;

(* GtkRadioToolButton object *)

	GtkRadioToolButton* = EXTENSIBLE RECORD [noalign] (GtkToggleToolButton)
	END;

(* GtkDialog object *)

	GtkDialog* = EXTENSIBLE RECORD [noalign] (GtkWindow)
	vbox* : PGtkVBox;
	action_area* : PGtkHButtonBox;
	separator* : PGtkWidget;
	END;

(* Flags of GtkPlug object *)

CONST
	GtkPlug_flag0_same_app_Mask* = {0};

TYPE

(* GtkPlug object *)

	GtkPlug* = EXTENSIBLE RECORD [noalign] (GtkWindow)
	socket_window* : gdk.PGdkWindow;
	modality_window*: PGtkWidget;
	modality_group* : PGtkWindowGroup;
	grabbed_keys* : lg.PGHashTable;
	GtkPlug_flag0* : SET;
	END;

(* GtkAboutDialog object *)

	GtkAboutDialog* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	GtkAboutDialog_pd*: lg.gpointer (* private_data *);
	END;

(* GtkColorSelectionDialog object *)

	GtkColorSelectionDialog* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	colorsel* : PGtkWidget;
	ok_button* : PGtkWidget;
	cancel_button* : PGtkWidget;
	help_button* : PGtkWidget;
	END;

(* GtkFileChooserDialog object *)

	GtkFileChooserDialog* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	GtkFileChooserDialog_pd*: lg.gpointer (* private_data *);
	END;

(* GtkFileSelection object *)

	GtkFileSelection* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	     dir_list* : PGtkTreeView;
	     file_list* : PGtkTreeView;
	     selection_entry* : PGtkEntry;
	     selection_text* : PGtkLabel;
	     main_vbox* : PGtkVBox;
	     ok_button* : PGtkButton;
	     cancel_button* : PGtkButton;
	     help_button* : PGtkButton;
	     history_pulldown* : PGtkOptionMenu;
	     history_menu* : PGtkMenu;
	     history_list* : lg.PGList;
	     fileop_dialog* : PGtkMessageDialog;
	     fileop_entry* : PGtkEntry;
	     fileop_file* : lg.Pgchar;
	     cmpl_state* : lg.gpointer;
	     fileop_c_dir* : PGtkButton;
	     fileop_del_file* : PGtkButton;
	     fileop_ren_file* : PGtkButton;
	     button_area* : PGtkHButtonBox;
	     action_area_* : PGtkHButtonBox;
	     selected_names* : lg.PGPtrArray;
	     last_selected* : lg.Pgchar;
	END;

(* GtkFontSelectionDialog object *)

	GtkFontSelectionDialog* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	fontsel* : PGtkWidget;
	main_vbox* : PGtkWidget;
	action_area_* : PGtkWidget;
	ok_button* : PGtkButton;
	apply_button* : PGtkButton;
	cancel_button* : PGtkButton;
	dialog_width* : lg.gint;
	auto_resize* : lg.gboolean;
	END;

(* GtkInputDialog object *)

	GtkInputDialog* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	axis_list* : PGtkWidget;
	axis_listbox* : PGtkWidget;
	mode_optionmenu* : PGtkWidget;
	close_button* : PGtkButton;
	save_button* : PGtkButton;
	axis_items* : PGtkWidget;
	current_device* : gdk.PGdkDevice;
	keys_list* : PGtkWidget;
	keys_listbox* : PGtkWidget;
	END;

(* GtkMessageDialog object *)

	GtkMessageDialog* = EXTENSIBLE RECORD [noalign] (GtkDialog)
	image* : PGtkWidget;
	label* : PGtkWidget;
	END;

(* GtkComboBoxEntry object *)

	GtkComboBoxEntry* = EXTENSIBLE RECORD [noalign] (GtkComboBox)
	GtkComboBoxEntry_pd*: lg.gpointer (* private_data *);
	END;

(* GtkAspectFrame object *)

	GtkAspectFrame* = EXTENSIBLE RECORD [noalign] (GtkFrame)
	xalign* : lg.gfloat;
	yalign* : lg.gfloat;
	ratio* : lg.gfloat;
	obey_child* : lg.gboolean;
	center_allocation* : GtkAllocation;
	END;

(* Flags of GtkMenuItem object *)

CONST
	GtkMenuItem_flag0_show_submenu_indicator_Mask* = {0};
	GtkMenuItem_flag0_submenu_placement_Mask* = {1};
	GtkMenuItem_flag0_submenu_direction_Mask* = {2};
	GtkMenuItem_flag0_right_justify_Mask* = {3};
	GtkMenuItem_flag0_timer_from_keypress_Mask* = {4};

TYPE

(* GtkMenuItem object *)

	GtkMenuItem* = EXTENSIBLE RECORD [noalign] (GtkItem)
	submenu* : PGtkWidget;
	event_window* : gdk.PGdkWindow;
	toggle_size* : lg.guint16;
	accelerator_width*: lg.guint16;
	accel_path* : lg.Pgchar;
	GtkMenuItem_flag0*: SET;
	END;

(* Flags of GtkCheckMenuItem object *)

CONST
	GtkCheckMenuItem_flag0_active_Mask* = {0};
	GtkCheckMenuItem_flag0_always_show_toggle_Mask* = {1};
	GtkCheckMenuItem_flag0_inconsistent_Mask* = {2};
	GtkCheckMenuItem_flag0_draw_as_radio_Mask* = {3};

TYPE

(* GtkCheckMenuItem object *)

	GtkCheckMenuItem* = EXTENSIBLE RECORD [noalign] (GtkMenuItem)
	GtkCheckMenuItem_flag0*: SET;
	END;

(* GtkImageMenuItem object *)

	GtkImageMenuItem* = EXTENSIBLE RECORD [noalign] (GtkMenuItem)
	image* : PGtkWidget;
	END;

(* GtkSeparatorMenuItem object *)

	GtkSeparatorMenuItem* = EXTENSIBLE RECORD [noalign] (GtkMenuItem)
	END;

(* Flags of GtkTearoffMenuItem object *)

CONST
	GtkTearoffMenuItem_flag0_torn_off_Mask* = {0};

TYPE

(* GtkTearoffMenuItem object *)

	GtkTearoffMenuItem* = EXTENSIBLE RECORD [noalign] (GtkMenuItem)
	GtkTearoffMenuItem_flag0*: SET;
	END;

(* GtkRadioMenuItem object *)

	GtkRadioMenuItem* = EXTENSIBLE RECORD [noalign] (GtkCheckMenuItem)
	group* : lg.PGSList;
	END;

(* GtkVBox object *)

	GtkVBox* = EXTENSIBLE RECORD [noalign] (GtkBox)
	END;

(* GtkHBox object *)

	GtkHBox* = EXTENSIBLE RECORD [noalign] (GtkBox)
	END;

(* GtkButtonBox object *)

	GtkButtonBox* = EXTENSIBLE RECORD [noalign] (GtkBox)
	child_min_width* : lg.gint;
	child_min_height* : lg.gint;
	child_ipad_x* : lg.gint;
	child_ipad_y* : lg.gint;
	layout_style* : GtkButtonBoxStyle;
	END;

(* GtkColorSelection object *)

	GtkColorSelection* = EXTENSIBLE RECORD [noalign] (GtkVBox)
	GtkColorSelection_pd*: lg.gpointer (* private_data *);
	END;

(* GtkFileChooserWidget object *)

	GtkFileChooserWidget* = EXTENSIBLE RECORD [noalign] (GtkVBox)
	GtkFileChooserWidget_pd*: lg.gpointer (* private_data *);
	END;

(* GtkFontSelection object *)

	GtkFontSelection* = EXTENSIBLE RECORD [noalign] (GtkVBox)
	font_entry* : PGtkWidget;
	family_list* : PGtkWidget;
	font_style_entry* : PGtkWidget;
	face_list* : PGtkWidget;
	size_entry* : PGtkWidget;
	size_list* : PGtkWidget;
	pixels_button* : PGtkWidget;
	points_button* : PGtkWidget;
	filter_button* : PGtkWidget;
	preview_entry* : PGtkWidget;
	family* : pan.PPangoFontFamily;
	face* : pan.PPangoFontFace;
	size* : lg.gint;
	font* : gdk.PGdkFont;
	END;

(* GtkGammaCurve object *)

	GtkGammaCurve* = EXTENSIBLE RECORD [noalign] (GtkVBox)
	table* : PGtkWidget;
	curve* : PGtkWidget;
	button* : PGtkWidget;
	gamma* : lg.gfloat;
	gamma_dialog* : PGtkWidget;
	gamma_text* : PGtkWidget;
	END;

(* Flags of GtkCombo object *)

CONST
	GtkCombo_flag0_value_in_list_Mask*  = {0};
	GtkCombo_flag0_ok_if_empty_Mask* = {1};
	GtkCombo_flag0_case_sensitive_Mask* = {2};
	GtkCombo_flag0_use_arrows_Mask*  = {3};
	GtkCombo_flag0_use_arrows_always_Mask* = {4};

TYPE

(* GtkCombo object *)

	GtkCombo* = EXTENSIBLE RECORD [noalign] (GtkHBox)
	entry* : PGtkEntry;
	button* : PGtkButton;
	popup* : PGtkWidget;
	popwin* : PGtkWidget;
	list* : PGtkWidget;
	entry_change_id*: lg.guint;
	list_change_id* : lg.guint;
	GtkCombo_flag0* : SET;
	activate_id* : lg.guint;
	END;

(* GtkFileChooserButton object *)

	GtkFileChooserButton* = EXTENSIBLE RECORD [noalign] (GtkHBox)
	GtkFileChooserButton_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkStatusbar object *)

CONST
	GtkStatusbar_flag0_has_resize_grip_Mask* = {0};

TYPE

(* GtkStatusbar object *)

	GtkStatusbar* = EXTENSIBLE RECORD [noalign] (GtkHBox)
	frame* : PGtkWidget;
	label* : PGtkWidget;
	messages* : lg.PGSList;
	keys* : lg.PGSList;
	seq_context_id* : lg.guint;
	seq_message_id* : lg.guint;
	grip_window* : gdk.PGdkWindow;
	GtkStatusbar_flag0*: SET;
	END;

(* GtkHButtonBox object *)

	GtkHButtonBox* = EXTENSIBLE RECORD [noalign] (GtkButtonBox)
	END;

(* GtkVButtonBox object *)

	GtkVButtonBox* = EXTENSIBLE RECORD [noalign] (GtkButtonBox)
	END;

(* Flags of GtkMenu object *)

CONST
	GtkMenu_flag0_needs_destruction_ref_count_Mask* = {0};
	GtkMenu_flag0_torn_off_Mask*  = {1};
	GtkMenu_flag0_tearoff_active_Mask*  = {2};
	GtkMenu_flag0_scroll_fast_Mask*  = {3};
	GtkMenu_flag0_upper_arrow_visible_Mask* = {4};
	GtkMenu_flag0_lower_arrow_visible_Mask* = {5};
	GtkMenu_flag0_upper_arrow_prelight_Mask*  = {6};
	GtkMenu_flag0_lower_arrow_prelight_Mask*  = {7};

TYPE

(* GtkMenu object *)

	GtkMenu* = EXTENSIBLE RECORD [noalign] (GtkMenuShell)
	parent_menu_item* : PGtkWidget;
	old_active_menu_item*: PGtkWidget;
	accel_group* : PGtkAccelGroup;
	accel_path* : lg.Pgchar;
	position_func* : GtkMenuPositionFunc;
	position_func_data* : lg.gpointer;
	toggle_size* : lg.guint;
	toplevel* : PGtkWidget;
	tearoff_window* : PGtkWidget;
	tearoff_hbox* : PGtkWidget;
	tearoff_scrollbar* : PGtkWidget;
	tearoff_adjustment* : PGtkAdjustment;
	view_window* : gdk.PGdkWindow;
	bin_window* : gdk.PGdkWindow;
	scroll_offset* : lg.gint;
	saved_scroll_offset* : lg.gint;
	scroll_step* : lg.gint;
	timeout_id* : lg.guint;
	navigation_region* : gdk.PGdkRegion;
	navigation_timeout* : lg.guint;
	GtkMenu_flag0* : SET;
	END;

(* GtkMenuBar object *)

	GtkMenuBar* = EXTENSIBLE RECORD [noalign] (GtkMenuShell)
	END;

(* GtkHPaned object *)

	GtkHPaned* = EXTENSIBLE RECORD [noalign] (GtkPaned)
	END;

(* GtkVPaned object *)

	GtkVPaned* = EXTENSIBLE RECORD [noalign] (GtkPaned)
	END;

(* Flags of GtkLabel object *)

CONST
	GtkLabel_flag0_jtype_Mask* = {0..1};
	GtkLabel_flag0_wrap_Mask* = {2};
	GtkLabel_flag0_use_underline_Mask* = {3};
	GtkLabel_flag0_use_markup_Mask* = {4};
	GtkLabel_flag0_ellipsize_Mask*  = {5..7};

TYPE

(* GtkLabel object *)

	GtkLabel* = EXTENSIBLE RECORD [noalign] (GtkMisc)
	label* : lg.Pgchar;
	GtkLabel_flag0* : SET;
	text* : lg.Pgchar;
	attrs* : pan.PPangoAttrList;
	effective_attrs*: pan.PPangoAttrList;
	layout* : pan.PPangoLayout;
	mnemonic_widget*: PGtkWidget;
	mnemonic_window*: PGtkWindow;
	select_info* : PGtkLabelSelectionInfo;
	END;

(* GtkArrow object *)

	GtkArrow* = EXTENSIBLE RECORD [noalign] (GtkMisc)
	arrow_type* : lg.gint16;
	shadow_type* : lg.gint16;
	END;

(* GtkImage object *)

	GtkImage* = EXTENSIBLE RECORD [noalign] (GtkMisc)
	storage_type* : GtkImageType;
	data* : RECORD [union]
	  pixmap : GtkImagePixmapData;
	  image : GtkImageImageData;
	  pixbuf : GtkImagePixbufData;
	  stock : GtkImageStockData;
	  icon_set: GtkImageIconSetData;
	  anim : GtkImageAnimationData;
	  name : GtkImageIconNameData;
	  gicon : GtkImageGIconData;
	END;
	END;

(* GtkAccelLabel object *)

	GtkAccelLabel* = EXTENSIBLE RECORD [noalign] (GtkLabel)
	gtk_reserved* : lg.guint;
	accel_padding* : lg.guint;
	accel_widget* : PGtkWidget;
	accel_closure* : lo.PGClosure;
	accel_group* : PGtkAccelGroup;
	accel_string* : lg.Pgchar;
	accel_string_width* : lg.guint16;
	END;

(* Flags of GtkScale object *)

CONST
	GtkScale_flag0_draw_value_Mask* = {0};
	GtkScale_flag0_value_pos_Mask*  = {1..2};

TYPE

(* GtkScale object *)

	GtkScale* = EXTENSIBLE RECORD [noalign] (GtkRange)
	digits* : lg.gint;
	GtkScale_flag0*: SET;
	END;

(* GtkScrollbar object *)

	GtkScrollbar* = EXTENSIBLE RECORD [noalign] (GtkRange)
	END;

(* GtkHScale object *)

	GtkHScale* = EXTENSIBLE RECORD [noalign] (GtkScale)
	END;

(* GtkVScale object *)

	GtkVScale* = EXTENSIBLE RECORD [noalign] (GtkScale)
	END;

(* GtkHScrollbar object *)

	GtkHScrollbar* = EXTENSIBLE RECORD [noalign] (GtkScrollbar)
	END;

(* GtkVScrollbar object *)

	GtkVScrollbar* = EXTENSIBLE RECORD [noalign] (GtkScrollbar)
	END;

(* GtkHRuler object *)

	GtkHRuler* = EXTENSIBLE RECORD [noalign] (GtkRuler)
	END;

(* GtkVRuler object *)

	GtkVRuler* = EXTENSIBLE RECORD [noalign] (GtkRuler)
	END;

(* GtkHSeparator object *)

	GtkHSeparator* = EXTENSIBLE RECORD [noalign] (GtkSeparator)
	END;

(* GtkVSeparator object *)

	GtkVSeparator* = EXTENSIBLE RECORD [noalign] (GtkSeparator)
	END;

(* GtkCurve object *)

	GtkCurve* = EXTENSIBLE RECORD [noalign] (GtkDrawingArea)
	cursor_type* : lg.gint;
	min_x* : lg.gfloat;
	max_x* : lg.gfloat;
	min_y* : lg.gfloat;
	max_y* : lg.gfloat;
	pixmap* : gdk.PGdkPixmap;
	curve_type* : GtkCurveType;
	height* : lg.gint;
	grab_point* : lg.gint;
	last* : lg.gint;
	num_points* : lg.gint;
	point* : gdk.PGdkPoint;
	num_ctlpoints* : lg.gint;
	ctrlpoint* : POINTER TO ARRAY [untagged] 2 OF lg.gfloat;
	END;

(* Flags of GtkSpinButton object *)

CONST
	GtkSpinButton_flag0_in_child_Mask* = {0..1};
	GtkSpinButton_flag0_click_child_Mask* = {2..3};
	GtkSpinButton_flag0_button_Mask*  = {4..5};
	GtkSpinButton_flag0_need_timer_Mask* = {6};
	GtkSpinButton_flag0_timer_calls_Mask* = {7..9};
	GtkSpinButton_flag0_digits_Mask*  = {10..19};
	GtkSpinButton_flag0_numeric_Mask* = {20};
	GtkSpinButton_flag0_wrap_Mask* = {21};
	GtkSpinButton_flag0_snap_to_ticks_Mask* = {22};

TYPE

(* GtkSpinButton object *)

	GtkSpinButton* = EXTENSIBLE RECORD [noalign] (GtkEntry)
	adjustment* : PGtkAdjustment;
	panel* : gdk.PGdkWindow;
	timer* : lg.guint32;
	climb_rate* : lg.gdouble;
	timer_step* : lg.gdouble;
	update_policy* : GtkSpinButtonUpdatePolicy;
	GtkSpinButton_flag0*: SET;
	END;

(* Flags of GtkCellRendererText object *)

CONST
	GtkCellRendererText_flag0_strikethrough_Mask*  = {0};
	GtkCellRendererText_flag0_editable_Mask* = {1};
	GtkCellRendererText_flag0_scale_set_Mask* = {2};
	GtkCellRendererText_flag0_foreground_set_Mask* = {3};
	GtkCellRendererText_flag0_background_set_Mask* = {4};
	GtkCellRendererText_flag0_underline_set_Mask*  = {5};
	GtkCellRendererText_flag0_rise_set_Mask* = {6};
	GtkCellRendererText_flag0_strikethrough_set_Mask* = {7};
	GtkCellRendererText_flag0_editable_set_Mask* = {8};
	GtkCellRendererText_flag0_calc_fixed_height_Mask* = {9};

TYPE

(* GtkCellRendererText object *)

	GtkCellRendererText* = EXTENSIBLE RECORD [noalign] (GtkCellRenderer)
	text* : lg.Pgchar;
	font* : pan.PPangoFontDescription;
	font_scale* : lg.gdouble;
	foreground* : pan.PangoColor;
	background* : pan.PangoColor;
	extra_attrs* : pan.PPangoAttrList;
	underline_style* : pan.PangoUnderline;
	rise* : lg.gint;
	fixed_height_rows* : lg.gint;
	GtkCellRendererText_flag0*: SET;
	END;

(* GtkCellRendererPixbuf object *)

	GtkCellRendererPixbuf* = EXTENSIBLE RECORD [noalign] (GtkCellRenderer)
	pixbuf* : gpb.PGdkPixbuf;
	pixbuf_expander_open* : gpb.PGdkPixbuf;
	pixbuf_expander_closed* : gpb.PGdkPixbuf;
	END;

(* GtkCellRendererProgress object *)

	GtkCellRendererProgress* = EXTENSIBLE RECORD [noalign] (GtkCellRenderer)
	GtkCellRendererProgress_pd*: lg.gpointer (* private_data *);
	END;

(* Flags of GtkCellRendererToggle object *)

CONST
	GtkCellRendererToggle_flag0_active_Mask* = {0};
	GtkCellRendererToggle_flag0_activatable_Mask* = {1};
	GtkCellRendererToggle_flag0_radio_Mask* = {2};

TYPE

(* GtkCellRendererToggle object *)

	GtkCellRendererToggle* = EXTENSIBLE RECORD [noalign] (GtkCellRenderer)
	GtkCellRendererToggle_flag0*: SET;
	END;

(* GtkCellRendererCombo object *)

	GtkCellRendererCombo* = EXTENSIBLE RECORD [noalign] (GtkCellRendererText)
	model* : PGtkTreeModel;
	text_column* : lg.gint;
	has_entry* : lg.gboolean;
	focus_out_id* : lg.guint;
	END;

(* ============================================================= INTERFACES *)
TYPE

(* GtkCellEditable interface *)

	GtkCellEditable* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkCellLayout interface *)

	GtkCellLayout* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkEditable interface *)

	GtkEditable* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkFileChooser interface *)

	GtkFileChooser* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkTreeDragDest interface *)

	GtkTreeDragDest* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkTreeDragSource interface *)

	GtkTreeDragSource* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkTreeModel interface *)

	GtkTreeModel* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkTreeSortable interface *)

	GtkTreeSortable* = EXTENSIBLE RECORD [noalign] 
	END;

(* GtkObject methods *)
	PROCEDURE [ccall] gtk_object_destroy*(object: PGtkObject);
	PROCEDURE [ccall] gtk_object_get_type*(): GtkType;
	PROCEDURE [ccall] gtk_object_sink*(object: PGtkObject);

(* GtkObject constructors *)

(* GtkIMContext methods *)
	PROCEDURE [ccall] gtk_im_context_delete_surrounding*(imcontext: PGtkIMContext; offset: lg.gint; n_chars: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_im_context_filter_keypress*(imcontext: PGtkIMContext; event: gdk.PGdkEventKey): lg.gboolean;
	PROCEDURE [ccall] gtk_im_context_focus_in*(imcontext: PGtkIMContext);
	PROCEDURE [ccall] gtk_im_context_focus_out*(imcontext: PGtkIMContext);
	PROCEDURE [ccall] gtk_im_context_get_preedit_string*(imcontext: PGtkIMContext; VAR str: lg.Pgchar; VAR attrs: pan.PPangoAttrList; cursor_pos: lg.Pgint);
	PROCEDURE [ccall] gtk_im_context_get_surrounding*(imcontext: PGtkIMContext; VAR text: lg.Pgchar; VAR cursor_index: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_im_context_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_im_context_reset*(imcontext: PGtkIMContext);
	PROCEDURE [ccall] gtk_im_context_set_client_window*(imcontext: PGtkIMContext; window: gdk.PGdkWindow);
	PROCEDURE [ccall] gtk_im_context_set_cursor_location*(imcontext: PGtkIMContext; area: gdk.PGdkRectangle);
	PROCEDURE [ccall] gtk_im_context_set_surrounding*(imcontext: PGtkIMContext; text: lg.Pgchar; len: lg.gint; cursor_index: lg.gint);
	PROCEDURE [ccall] gtk_im_context_set_use_preedit*(imcontext: PGtkIMContext; use_preedit: lg.gboolean);

(* GtkAction methods *)
	PROCEDURE [ccall] gtk_action_activate*(action: PGtkAction);
	PROCEDURE [ccall] gtk_action_block_activate_from*(action: PGtkAction; proxy: PGtkWidget);
	PROCEDURE [ccall] gtk_action_connect_accelerator*(action: PGtkAction);
	PROCEDURE [ccall] gtk_action_connect_proxy*(action: PGtkAction; proxy: PGtkWidget);
	PROCEDURE [ccall] gtk_action_create_icon*(action: PGtkAction; icon_size: GtkIconSize): PGtkWidget;
	PROCEDURE [ccall] gtk_action_create_menu_item*(action: PGtkAction): PGtkWidget;
	PROCEDURE [ccall] gtk_action_create_tool_item*(action: PGtkAction): PGtkWidget;
	PROCEDURE [ccall] gtk_action_disconnect_accelerator*(action: PGtkAction);
	PROCEDURE [ccall] gtk_action_disconnect_proxy*(action: PGtkAction; proxy: PGtkWidget);
	PROCEDURE [ccall] gtk_action_get_accel_closure*(action: PGtkAction): lo.PGClosure;
	PROCEDURE [ccall] gtk_action_get_accel_path*(action: PGtkAction): lg.Pgchar;
	PROCEDURE [ccall] gtk_action_get_name*(action: PGtkAction): lg.Pgchar;
	PROCEDURE [ccall] gtk_action_get_proxies*(action: PGtkAction): lg.PGSList;
	PROCEDURE [ccall] gtk_action_get_sensitive*(action: PGtkAction): lg.gboolean;
	PROCEDURE [ccall] gtk_action_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_action_get_visible*(action: PGtkAction): lg.gboolean;
	PROCEDURE [ccall] gtk_action_is_sensitive*(action: PGtkAction): lg.gboolean;
	PROCEDURE [ccall] gtk_action_is_visible*(action: PGtkAction): lg.gboolean;
	PROCEDURE [ccall] gtk_action_set_accel_group*(action: PGtkAction; accel_group: PGtkAccelGroup);
	PROCEDURE [ccall] gtk_action_set_accel_path*(action: PGtkAction; accel_path: lg.Pgchar);
	PROCEDURE [ccall] gtk_action_set_sensitive*(action: PGtkAction; sensitive: lg.gboolean);
	PROCEDURE [ccall] gtk_action_set_visible*(action: PGtkAction; visible: lg.gboolean);
	PROCEDURE [ccall] gtk_action_unblock_activate_from*(action: PGtkAction; proxy: PGtkWidget);

(* GtkAction constructors *)
	PROCEDURE [ccall] gtk_action_new*(name: lg.Pgchar; label: lg.Pgchar; tooltip: lg.Pgchar; stock_id: lg.Pgchar): PGtkAction;

(* GtkAccelGroup methods *)
	PROCEDURE [ccall] gtk_accel_group_activate*(accelgroup: PGtkAccelGroup; accel_quark: lg.GQuark; acceleratable: lo.PGObject; accel_key: lg.guint; accel_mods: gdk.GdkModifierType): lg.gboolean;
	PROCEDURE [ccall] gtk_accel_group_connect*(accelgroup: PGtkAccelGroup; accel_key: lg.guint; accel_mods: gdk.GdkModifierType; accel_flags: GtkAccelFlags; closure: lo.PGClosure);
	PROCEDURE [ccall] gtk_accel_group_connect_by_path*(accelgroup: PGtkAccelGroup; accel_path: lg.Pgchar; closure: lo.PGClosure);
	PROCEDURE [ccall] gtk_accel_group_disconnect*(accelgroup: PGtkAccelGroup; closure: lo.PGClosure): lg.gboolean;
	PROCEDURE [ccall] gtk_accel_group_disconnect_key*(accelgroup: PGtkAccelGroup; accel_key: lg.guint; accel_mods: gdk.GdkModifierType): lg.gboolean;
	PROCEDURE [ccall] gtk_accel_group_find*(accelgroup: PGtkAccelGroup; find_func: GtkAccelGroupFindFunc; data: lg.gpointer): PGtkAccelKey;
	PROCEDURE [ccall] gtk_accel_group_from_accel_closure*(closure: lo.PGClosure): PGtkAccelGroup;
	PROCEDURE [ccall] gtk_accel_group_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_accel_group_lock*(accelgroup: PGtkAccelGroup);
	PROCEDURE [ccall] gtk_accel_group_query*(accelgroup: PGtkAccelGroup; accel_key: lg.guint; accel_mods: gdk.GdkModifierType; VAR n_entries: lg.guint): PGtkAccelGroupEntry;
	PROCEDURE [ccall] gtk_accel_group_unlock*(accelgroup: PGtkAccelGroup);

(* GtkAccelGroup constructors *)
	PROCEDURE [ccall] gtk_accel_group_new*(): PGtkAccelGroup;

(* GtkAccelMap methods *)
	PROCEDURE [ccall] gtk_accel_map_add_entry*(accel_path: lg.Pgchar; accel_key: lg.guint; accel_mods: gdk.GdkModifierType);
	PROCEDURE [ccall] gtk_accel_map_add_filter*(filter_pattern: lg.Pgchar);
	PROCEDURE [ccall] gtk_accel_map_change_entry*(accel_path: lg.Pgchar; accel_key: lg.guint; accel_mods: gdk.GdkModifierType; replace: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_accel_map_foreach*(data: lg.gpointer; foreach_func: GtkAccelMapForeach);
	PROCEDURE [ccall] gtk_accel_map_foreach_unfiltered*(data: lg.gpointer; foreach_func: GtkAccelMapForeach);
	PROCEDURE [ccall] gtk_accel_map_get*(): PGtkAccelMap;
	PROCEDURE [ccall] gtk_accel_map_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_accel_map_load*(file_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_accel_map_load_fd*(fd: lg.gint);
	PROCEDURE [ccall] gtk_accel_map_load_scanner*(scanner: lg.PGScanner);
	PROCEDURE [ccall] gtk_accel_map_lock_path*(accel_path: lg.Pgchar);
	PROCEDURE [ccall] gtk_accel_map_lookup_entry*(accel_path: lg.Pgchar; key: PGtkAccelKey): lg.gboolean;
	PROCEDURE [ccall] gtk_accel_map_save*(file_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_accel_map_save_fd*(fd: lg.gint);
	PROCEDURE [ccall] gtk_accel_map_unlock_path*(accel_path: lg.Pgchar);

(* GtkAccessible methods *)
	PROCEDURE [ccall] gtk_accessible_connect_widget_destroyed*(accessible: PGtkAccessible);
	PROCEDURE [ccall] gtk_accessible_get_type*(): lo.GType;

(* GtkActionGroup methods *)
	PROCEDURE [ccall] gtk_action_group_add_action*(actiongroup: PGtkActionGroup; action: PGtkAction);
	PROCEDURE [ccall] gtk_action_group_add_action_with_accel*(actiongroup: PGtkActionGroup; action: PGtkAction; accelerator: lg.Pgchar);
	PROCEDURE [ccall] gtk_action_group_add_actions*(actiongroup: PGtkActionGroup; entries: PGtkActionEntry; n_entries: lg.guint; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_action_group_add_actions_full*(actiongroup: PGtkActionGroup; entries: PGtkActionEntry; n_entries: lg.guint; user_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_action_group_add_radio_actions*(actiongroup: PGtkActionGroup; entries: PGtkRadioActionEntry; n_entries: lg.guint; value: lg.gint; on_change: lo.GCallback; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_action_group_add_radio_actions_full*(actiongroup: PGtkActionGroup; entries: PGtkRadioActionEntry; n_entries: lg.guint; value: lg.gint; on_change: lo.GCallback; user_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_action_group_add_toggle_actions*(actiongroup: PGtkActionGroup; entries: PGtkToggleActionEntry; n_entries: lg.guint; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_action_group_add_toggle_actions_full*(actiongroup: PGtkActionGroup; entries: PGtkToggleActionEntry; n_entries: lg.guint; user_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_action_group_get_action*(actiongroup: PGtkActionGroup; action_name: lg.Pgchar): PGtkAction;
	PROCEDURE [ccall] gtk_action_group_get_name*(actiongroup: PGtkActionGroup): lg.Pgchar;
	PROCEDURE [ccall] gtk_action_group_get_sensitive*(actiongroup: PGtkActionGroup): lg.gboolean;
	PROCEDURE [ccall] gtk_action_group_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_action_group_get_visible*(actiongroup: PGtkActionGroup): lg.gboolean;
	PROCEDURE [ccall] gtk_action_group_list_actions*(actiongroup: PGtkActionGroup): lg.PGList;
	PROCEDURE [ccall] gtk_action_group_remove_action*(actiongroup: PGtkActionGroup; action: PGtkAction);
	PROCEDURE [ccall] gtk_action_group_set_sensitive*(actiongroup: PGtkActionGroup; sensitive: lg.gboolean);
	PROCEDURE [ccall] gtk_action_group_set_translate_func*(actiongroup: PGtkActionGroup; func: GtkTranslateFunc; data: lg.gpointer; notify: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_action_group_set_translation_domain*(actiongroup: PGtkActionGroup; domain: lg.Pgchar);
	PROCEDURE [ccall] gtk_action_group_set_visible*(actiongroup: PGtkActionGroup; visible: lg.gboolean);
	PROCEDURE [ccall] gtk_action_group_translate_string*(actiongroup: PGtkActionGroup; string: lg.Pgchar): lg.Pgchar;

(* GtkActionGroup constructors *)
	PROCEDURE [ccall] gtk_action_group_new*(name: lg.Pgchar): PGtkActionGroup;

(* GtkClipboard methods *)
	PROCEDURE [ccall] gtk_clipboard_clear*(clipboard: PGtkClipboard);
	PROCEDURE [ccall] gtk_clipboard_get*(selection: gdk.GdkAtom): PGtkClipboard;
	PROCEDURE [ccall] gtk_clipboard_get_display*(clipboard: PGtkClipboard): gdk.PGdkDisplay;
	PROCEDURE [ccall] gtk_clipboard_get_for_display*(display: gdk.PGdkDisplay; selection: gdk.GdkAtom): PGtkClipboard;
	PROCEDURE [ccall] gtk_clipboard_get_owner*(clipboard: PGtkClipboard): lo.PGObject;
	PROCEDURE [ccall] gtk_clipboard_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_clipboard_request_contents*(clipboard: PGtkClipboard; target: gdk.GdkAtom; callback: GtkClipboardReceivedFunc; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_clipboard_request_image*(clipboard: PGtkClipboard; callback: GtkClipboardImageReceivedFunc; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_clipboard_request_targets*(clipboard: PGtkClipboard; callback: GtkClipboardTargetsReceivedFunc; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_clipboard_request_text*(clipboard: PGtkClipboard; callback: GtkClipboardTextReceivedFunc; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_clipboard_set_can_store*(clipboard: PGtkClipboard; targets: PGtkTargetEntry; n_targets: lg.gint);
	PROCEDURE [ccall] gtk_clipboard_set_image*(clipboard: PGtkClipboard; pixbuf: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_clipboard_set_text*(clipboard: PGtkClipboard; text: lg.Pgchar; len: lg.gint);
	PROCEDURE [ccall] gtk_clipboard_set_with_data*(clipboard: PGtkClipboard; targets: PGtkTargetEntry; n_targets: lg.guint; get_func: GtkClipboardGetFunc; clear_func: GtkClipboardClearFunc; user_data: lg.gpointer): lg.gboolean;
	PROCEDURE [ccall] gtk_clipboard_set_with_owner*(clipboard: PGtkClipboard; targets: PGtkTargetEntry; n_targets: lg.guint; get_func: GtkClipboardGetFunc; clear_func: GtkClipboardClearFunc; owner: lo.PGObject): lg.gboolean;
	PROCEDURE [ccall] gtk_clipboard_store*(clipboard: PGtkClipboard);
	PROCEDURE [ccall] gtk_clipboard_wait_for_contents*(clipboard: PGtkClipboard; target: gdk.GdkAtom): PGtkSelectionData;
	PROCEDURE [ccall] gtk_clipboard_wait_for_image*(clipboard: PGtkClipboard): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_clipboard_wait_for_targets*(clipboard: PGtkClipboard; VAR targets: POINTER TO ARRAY [untagged] OF gdk.PGdkAtom; VAR n_targets: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_clipboard_wait_for_text*(clipboard: PGtkClipboard): lg.Pgchar;
	PROCEDURE [ccall] gtk_clipboard_wait_is_image_available*(clipboard: PGtkClipboard): lg.gboolean;
	PROCEDURE [ccall] gtk_clipboard_wait_is_target_available*(clipboard: PGtkClipboard; target: gdk.GdkAtom): lg.gboolean;
	PROCEDURE [ccall] gtk_clipboard_wait_is_text_available*(clipboard: PGtkClipboard): lg.gboolean;

(* GtkEntryCompletion methods *)
	PROCEDURE [ccall] gtk_entry_completion_complete*(entrycompletion: PGtkEntryCompletion);
	PROCEDURE [ccall] gtk_entry_completion_delete_action*(entrycompletion: PGtkEntryCompletion; index_: lg.gint);
	PROCEDURE [ccall] gtk_entry_completion_get_entry*(entrycompletion: PGtkEntryCompletion): PGtkWidget;
	PROCEDURE [ccall] gtk_entry_completion_get_inline_completion*(entrycompletion: PGtkEntryCompletion): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_completion_get_minimum_key_length*(entrycompletion: PGtkEntryCompletion): lg.gint;
	PROCEDURE [ccall] gtk_entry_completion_get_model*(entrycompletion: PGtkEntryCompletion): PGtkTreeModel;
	PROCEDURE [ccall] gtk_entry_completion_get_popup_completion*(entrycompletion: PGtkEntryCompletion): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_completion_get_popup_set_width*(entrycompletion: PGtkEntryCompletion): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_completion_get_popup_single_match*(entrycompletion: PGtkEntryCompletion): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_completion_get_text_column*(entrycompletion: PGtkEntryCompletion): lg.gint;
	PROCEDURE [ccall] gtk_entry_completion_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_entry_completion_insert_action_markup*(entrycompletion: PGtkEntryCompletion; index_: lg.gint; markup: lg.Pgchar);
	PROCEDURE [ccall] gtk_entry_completion_insert_action_text*(entrycompletion: PGtkEntryCompletion; index_: lg.gint; text: lg.Pgchar);
	PROCEDURE [ccall] gtk_entry_completion_insert_prefix*(entrycompletion: PGtkEntryCompletion);
	PROCEDURE [ccall] gtk_entry_completion_set_inline_completion*(entrycompletion: PGtkEntryCompletion; inline_completion: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_completion_set_match_func*(entrycompletion: PGtkEntryCompletion; func: GtkEntryCompletionMatchFunc; func_data: lg.gpointer; func_notify: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_entry_completion_set_minimum_key_length*(entrycompletion: PGtkEntryCompletion; length: lg.gint);
	PROCEDURE [ccall] gtk_entry_completion_set_model*(entrycompletion: PGtkEntryCompletion; model: PGtkTreeModel);
	PROCEDURE [ccall] gtk_entry_completion_set_popup_completion*(entrycompletion: PGtkEntryCompletion; popup_completion: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_completion_set_popup_set_width*(entrycompletion: PGtkEntryCompletion; popup_set_width: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_completion_set_popup_single_match*(entrycompletion: PGtkEntryCompletion; popup_single_match: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_completion_set_text_column*(entrycompletion: PGtkEntryCompletion; column: lg.gint);

(* GtkEntryCompletion constructors *)
	PROCEDURE [ccall] gtk_entry_completion_new*(): PGtkEntryCompletion;

(* GtkIconFactory methods *)
	PROCEDURE [ccall] gtk_icon_factory_add*(iconfactory: PGtkIconFactory; stock_id: lg.Pgchar; icon_set: PGtkIconSet);
	PROCEDURE [ccall] gtk_icon_factory_add_default*(iconfactory: PGtkIconFactory);
	PROCEDURE [ccall] gtk_icon_factory_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_icon_factory_lookup*(iconfactory: PGtkIconFactory; stock_id: lg.Pgchar): PGtkIconSet;
	PROCEDURE [ccall] gtk_icon_factory_lookup_default*(stock_id: lg.Pgchar): PGtkIconSet;
	PROCEDURE [ccall] gtk_icon_factory_remove_default*(iconfactory: PGtkIconFactory);

(* GtkIconFactory constructors *)
	PROCEDURE [ccall] gtk_icon_factory_new*(): PGtkIconFactory;

(* GtkIconTheme methods *)
	PROCEDURE [ccall] gtk_icon_theme_add_builtin_icon*(icon_name: lg.Pgchar; size: lg.gint; pixbuf: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_icon_theme_append_search_path*(icontheme: PGtkIconTheme; path: lg.Pgchar);
	PROCEDURE [ccall] gtk_icon_theme_error_quark*(): lg.GQuark;
	PROCEDURE [ccall] gtk_icon_theme_get_default*(): PGtkIconTheme;
	PROCEDURE [ccall] gtk_icon_theme_get_example_icon_name*(icontheme: PGtkIconTheme): lg.Pgchar;
	PROCEDURE [ccall] gtk_icon_theme_get_for_screen*(screen: gdk.PGdkScreen): PGtkIconTheme;
	PROCEDURE [ccall] gtk_icon_theme_get_icon_sizes*(icontheme: PGtkIconTheme; icon_name: lg.Pgchar): lg.Pgint;
	PROCEDURE [ccall] gtk_icon_theme_get_search_path*(icontheme: PGtkIconTheme; VAR path: lg.Pgchar; VAR n_elements: lg.gint);
	PROCEDURE [ccall] gtk_icon_theme_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_icon_theme_has_icon*(icontheme: PGtkIconTheme; icon_name: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_theme_list_icons*(icontheme: PGtkIconTheme; context: lg.Pgchar): lg.PGList;
	PROCEDURE [ccall] gtk_icon_theme_load_icon*(icontheme: PGtkIconTheme; icon_name: lg.Pgchar; size: lg.gint; flags: GtkIconLookupFlags; VAR error: lg.PGError): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_icon_theme_lookup_icon*(icontheme: PGtkIconTheme; icon_name: lg.Pgchar; size: lg.gint; flags: GtkIconLookupFlags): PGtkIconInfo;
	PROCEDURE [ccall] gtk_icon_theme_prepend_search_path*(icontheme: PGtkIconTheme; path: lg.Pgchar);
	PROCEDURE [ccall] gtk_icon_theme_rescan_if_needed*(icontheme: PGtkIconTheme): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_theme_set_custom_theme*(icontheme: PGtkIconTheme; theme_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_icon_theme_set_screen*(icontheme: PGtkIconTheme; screen: gdk.PGdkScreen);
	PROCEDURE [ccall] gtk_icon_theme_set_search_path*(icontheme: PGtkIconTheme; path: lg.Pgchar; n_elements: lg.gint);

(* GtkIconTheme constructors *)
	PROCEDURE [ccall] gtk_icon_theme_new*(): PGtkIconTheme;

(* GtkListStore methods *)
	PROCEDURE [ccall] gtk_list_store_append*(liststore: PGtkListStore; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_clear*(liststore: PGtkListStore);
	PROCEDURE [ccall] gtk_list_store_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_list_store_insert*(liststore: PGtkListStore; iter: PGtkTreeIter; position: lg.gint);
	PROCEDURE [ccall] gtk_list_store_insert_after*(liststore: PGtkListStore; iter: PGtkTreeIter; sibling: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_insert_before*(liststore: PGtkListStore; iter: PGtkTreeIter; sibling: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_insert_with_values*(liststore: PGtkListStore; iter: PGtkTreeIter; position: lg.gint);
	PROCEDURE [ccall] gtk_list_store_insert_with_valuesv*(liststore: PGtkListStore; iter: PGtkTreeIter; position: lg.gint; columns: lg.Pgint; values: lo.PGValue; n_values: lg.gint);
	PROCEDURE [ccall] gtk_list_store_iter_is_valid*(liststore: PGtkListStore; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_list_store_move_after*(liststore: PGtkListStore; iter: PGtkTreeIter; position: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_move_before*(liststore: PGtkListStore; iter: PGtkTreeIter; position: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_prepend*(liststore: PGtkListStore; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_remove*(liststore: PGtkListStore; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_list_store_reorder*(liststore: PGtkListStore; new_order: lg.Pgint);
	PROCEDURE [ccall] gtk_list_store_set*(liststore: PGtkListStore; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_set_column_types*(liststore: PGtkListStore; n_columns: lg.gint; VAR types: POINTER TO ARRAY [untagged] OF lo.GType);
	PROCEDURE [ccall] gtk_list_store_set_valist*(liststore: PGtkListStore; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_list_store_set_value*(liststore: PGtkListStore; iter: PGtkTreeIter; column: lg.gint; value: lo.PGValue);
	PROCEDURE [ccall] gtk_list_store_swap*(liststore: PGtkListStore; a: PGtkTreeIter; b: PGtkTreeIter);

(* GtkListStore constructors *)
	PROCEDURE [ccall] gtk_list_store_new*(n_columns: lg.gint): PGtkListStore;
	PROCEDURE [ccall] gtk_list_store_newv*(n_columns: lg.gint; VAR types: POINTER TO ARRAY [untagged] OF lo.GType): PGtkListStore;

(* GtkRcStyle methods *)
	PROCEDURE [ccall] gtk_rc_style_copy*(rcstyle: PGtkRcStyle): PGtkRcStyle;
	PROCEDURE [ccall] gtk_rc_style_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_rc_style_ref*(rcstyle: PGtkRcStyle);
	PROCEDURE [ccall] gtk_rc_style_unref*(rcstyle: PGtkRcStyle);

(* GtkRcStyle constructors *)
	PROCEDURE [ccall] gtk_rc_style_new*(): PGtkRcStyle;

(* GtkSettings methods *)
	PROCEDURE [ccall] gtk_settings_get_default*(): PGtkSettings;
	PROCEDURE [ccall] gtk_settings_get_for_screen*(screen: gdk.PGdkScreen): PGtkSettings;
	PROCEDURE [ccall] gtk_settings_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_settings_install_property*(pspec: lo.PGParamSpec);
	PROCEDURE [ccall] gtk_settings_install_property_parser*(pspec: lo.PGParamSpec; parser: GtkRcPropertyParser);
	PROCEDURE [ccall] gtk_settings_set_double_property*(settings: PGtkSettings; name: lg.Pgchar; v_double: lg.gdouble; origin: lg.Pgchar);
	PROCEDURE [ccall] gtk_settings_set_long_property*(settings: PGtkSettings; name: lg.Pgchar; v_long: lg.glong; origin: lg.Pgchar);
	PROCEDURE [ccall] gtk_settings_set_property_value*(settings: PGtkSettings; name: lg.Pgchar; svalue: PGtkSettingsValue);
	PROCEDURE [ccall] gtk_settings_set_string_property*(settings: PGtkSettings; name: lg.Pgchar; v_string: lg.Pgchar; origin: lg.Pgchar);

(* GtkSizeGroup methods *)
	PROCEDURE [ccall] gtk_size_group_add_widget*(sizegroup: PGtkSizeGroup; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_size_group_get_ignore_hidden*(sizegroup: PGtkSizeGroup): lg.gboolean;
	PROCEDURE [ccall] gtk_size_group_get_mode*(sizegroup: PGtkSizeGroup): GtkSizeGroupMode;
	PROCEDURE [ccall] gtk_size_group_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_size_group_remove_widget*(sizegroup: PGtkSizeGroup; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_size_group_set_ignore_hidden*(sizegroup: PGtkSizeGroup; ignore_hidden: lg.gboolean);
	PROCEDURE [ccall] gtk_size_group_set_mode*(sizegroup: PGtkSizeGroup; mode: GtkSizeGroupMode);

(* GtkSizeGroup constructors *)
	PROCEDURE [ccall] gtk_size_group_new*(mode: GtkSizeGroupMode): PGtkSizeGroup;

(* GtkStyle methods *)
	PROCEDURE [ccall] gtk_style_apply_default_background*(style: PGtkStyle; window: gdk.PGdkWindow; set_bg: lg.gboolean; state_type: GtkStateType; area: gdk.PGdkRectangle; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_style_attach*(style: PGtkStyle; window: gdk.PGdkWindow): PGtkStyle;
	PROCEDURE [ccall] gtk_style_copy*(style: PGtkStyle): PGtkStyle;
	PROCEDURE [ccall] gtk_style_detach*(style: PGtkStyle);
	PROCEDURE [ccall] gtk_style_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_style_lookup_icon_set*(style: PGtkStyle; stock_id: lg.Pgchar): PGtkIconSet;
	PROCEDURE [ccall] gtk_style_render_icon*(style: PGtkStyle; source: PGtkIconSource; direction: GtkTextDirection; state: GtkStateType; size: GtkIconSize; widget: PGtkWidget; detail: lg.Pgchar): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_style_set_background*(style: PGtkStyle; window: gdk.PGdkWindow; state_type: GtkStateType);
	PROCEDURE [ccall] gtk_paint_arrow*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; arrow_type: GtkArrowType; fill: lg.gboolean; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_box_gap*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; gap_side: GtkPositionType; gap_x: lg.gint; gap_width: lg.gint);
	PROCEDURE [ccall] gtk_paint_box*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_check*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_diamond*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_expander*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; expander_style: GtkExpanderStyle);
	PROCEDURE [ccall] gtk_paint_extension*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; gap_side: GtkPositionType);
	PROCEDURE [ccall] gtk_paint_flat_box*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_focus*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_handle*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; orientation: GtkOrientation);
	PROCEDURE [ccall] gtk_paint_hline*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x1: lg.gint; x2: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_paint_layout*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; use_text: lg.gboolean; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; layout: pan.PPangoLayout);
	PROCEDURE [ccall] gtk_paint_option*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_polygon*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; points: gdk.PGdkPoint; n_points: lg.gint; fill: lg.gboolean);
	PROCEDURE [ccall] gtk_paint_resize_grip*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; edge: gdk.GdkWindowEdge; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_shadow_gap*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; gap_side: GtkPositionType; gap_x: lg.gint; gap_width: lg.gint);
	PROCEDURE [ccall] gtk_paint_shadow*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_slider*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint; orientation: GtkOrientation);
	PROCEDURE [ccall] gtk_paint_tab*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; shadow_type: GtkShadowType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_paint_vline*(style: PGtkStyle; window: gdk.PGdkDrawable; state_type: GtkStateType; area: gdk.PGdkRectangle; widget: PGtkWidget; detail: lg.Pgchar; y1_: lg.gint; y2_: lg.gint; x: lg.gint);

(* GtkStyle constructors *)
	PROCEDURE [ccall] gtk_style_new*(): PGtkStyle;

(* GtkTextBuffer methods *)
	PROCEDURE [ccall] gtk_text_buffer_add_selection_clipboard*(textbuffer: PGtkTextBuffer; clipboard: PGtkClipboard);
	PROCEDURE [ccall] gtk_text_buffer_apply_tag*(textbuffer: PGtkTextBuffer; tag: PGtkTextTag; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_apply_tag_by_name*(textbuffer: PGtkTextBuffer; name: lg.Pgchar; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_backspace*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; interactive: lg.gboolean; default_editable: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_begin_user_action*(textbuffer: PGtkTextBuffer);
	PROCEDURE [ccall] gtk_text_buffer_copy_clipboard*(textbuffer: PGtkTextBuffer; clipboard: PGtkClipboard);
	PROCEDURE [ccall] gtk_text_buffer_create_child_anchor*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter): PGtkTextChildAnchor;
	PROCEDURE [ccall] gtk_text_buffer_create_mark*(textbuffer: PGtkTextBuffer; mark_name: lg.Pgchar; where: PGtkTextIter; left_gravity: lg.gboolean): PGtkTextMark;
	PROCEDURE [ccall] gtk_text_buffer_create_tag*(textbuffer: PGtkTextBuffer; tag_name: lg.Pgchar; first_property_name: lg.Pgchar): PGtkTextTag;
	PROCEDURE [ccall] gtk_text_buffer_cut_clipboard*(textbuffer: PGtkTextBuffer; clipboard: PGtkClipboard; default_editable: lg.gboolean);
	PROCEDURE [ccall] gtk_text_buffer_delete*(textbuffer: PGtkTextBuffer; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_delete_interactive*(textbuffer: PGtkTextBuffer; start_iter: PGtkTextIter; end_iter: PGtkTextIter; default_editable: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_delete_mark*(textbuffer: PGtkTextBuffer; mark: PGtkTextMark);
	PROCEDURE [ccall] gtk_text_buffer_delete_mark_by_name*(textbuffer: PGtkTextBuffer; name: lg.Pgchar);
	PROCEDURE [ccall] gtk_text_buffer_delete_selection*(textbuffer: PGtkTextBuffer; interactive: lg.gboolean; default_editable: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_end_user_action*(textbuffer: PGtkTextBuffer);
	PROCEDURE [ccall] gtk_text_buffer_get_bounds*(textbuffer: PGtkTextBuffer; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_get_char_count*(textbuffer: PGtkTextBuffer): lg.gint;
	PROCEDURE [ccall] gtk_text_buffer_get_end_iter*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_get_insert*(textbuffer: PGtkTextBuffer): PGtkTextMark;
	PROCEDURE [ccall] gtk_text_buffer_get_iter_at_child_anchor*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; anchor: PGtkTextChildAnchor);
	PROCEDURE [ccall] gtk_text_buffer_get_iter_at_line*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; line_number: lg.gint);
	PROCEDURE [ccall] gtk_text_buffer_get_iter_at_line_index*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; line_number: lg.gint; byte_index: lg.gint);
	PROCEDURE [ccall] gtk_text_buffer_get_iter_at_line_offset*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; line_number: lg.gint; char_offset: lg.gint);
	PROCEDURE [ccall] gtk_text_buffer_get_iter_at_mark*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; mark: PGtkTextMark);
	PROCEDURE [ccall] gtk_text_buffer_get_iter_at_offset*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; char_offset: lg.gint);
	PROCEDURE [ccall] gtk_text_buffer_get_line_count*(textbuffer: PGtkTextBuffer): lg.gint;
	PROCEDURE [ccall] gtk_text_buffer_get_mark*(textbuffer: PGtkTextBuffer; name: lg.Pgchar): PGtkTextMark;
	PROCEDURE [ccall] gtk_text_buffer_get_modified*(textbuffer: PGtkTextBuffer): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_get_selection_bound*(textbuffer: PGtkTextBuffer): PGtkTextMark;
	PROCEDURE [ccall] gtk_text_buffer_get_selection_bounds*(textbuffer: PGtkTextBuffer; start: PGtkTextIter; end: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_get_slice*(textbuffer: PGtkTextBuffer; start: PGtkTextIter; end: PGtkTextIter; include_hidden_chars: lg.gboolean): lg.Pgchar;
	PROCEDURE [ccall] gtk_text_buffer_get_start_iter*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_get_tag_table*(textbuffer: PGtkTextBuffer): PGtkTextTagTable;
	PROCEDURE [ccall] gtk_text_buffer_get_text*(textbuffer: PGtkTextBuffer; start: PGtkTextIter; end: PGtkTextIter; include_hidden_chars: lg.gboolean): lg.Pgchar;
	PROCEDURE [ccall] gtk_text_buffer_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_text_buffer_insert*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; text: lg.Pgchar; len: lg.gint);
	PROCEDURE [ccall] gtk_text_buffer_insert_at_cursor*(textbuffer: PGtkTextBuffer; text: lg.Pgchar; len: lg.gint);
	PROCEDURE [ccall] gtk_text_buffer_insert_child_anchor*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; anchor: PGtkTextChildAnchor);
	PROCEDURE [ccall] gtk_text_buffer_insert_interactive*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; text: lg.Pgchar; len: lg.gint; default_editable: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_insert_interactive_at_cursor*(textbuffer: PGtkTextBuffer; text: lg.Pgchar; len: lg.gint; default_editable: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_insert_pixbuf*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; pixbuf: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_text_buffer_insert_range*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_insert_range_interactive*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; start: PGtkTextIter; end: PGtkTextIter; default_editable: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_text_buffer_insert_with_tags*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; text: lg.Pgchar; len: lg.gint; first_tag: PGtkTextTag);
	PROCEDURE [ccall] gtk_text_buffer_insert_with_tags_by_name*(textbuffer: PGtkTextBuffer; iter: PGtkTextIter; text: lg.Pgchar; len: lg.gint; first_tag_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_text_buffer_move_mark*(textbuffer: PGtkTextBuffer; mark: PGtkTextMark; where: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_move_mark_by_name*(textbuffer: PGtkTextBuffer; name: lg.Pgchar; where: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_paste_clipboard*(textbuffer: PGtkTextBuffer; clipboard: PGtkClipboard; override_location: PGtkTextIter; default_editable: lg.gboolean);
	PROCEDURE [ccall] gtk_text_buffer_place_cursor*(textbuffer: PGtkTextBuffer; where: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_remove_all_tags*(textbuffer: PGtkTextBuffer; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_remove_selection_clipboard*(textbuffer: PGtkTextBuffer; clipboard: PGtkClipboard);
	PROCEDURE [ccall] gtk_text_buffer_remove_tag*(textbuffer: PGtkTextBuffer; tag: PGtkTextTag; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_remove_tag_by_name*(textbuffer: PGtkTextBuffer; name: lg.Pgchar; start: PGtkTextIter; end: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_select_range*(textbuffer: PGtkTextBuffer; ins: PGtkTextIter; bound: PGtkTextIter);
	PROCEDURE [ccall] gtk_text_buffer_set_modified*(textbuffer: PGtkTextBuffer; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_text_buffer_set_text*(textbuffer: PGtkTextBuffer; text: lg.Pgchar; len: lg.gint);

(* GtkTextBuffer constructors *)
	PROCEDURE [ccall] gtk_text_buffer_new*(table: PGtkTextTagTable): PGtkTextBuffer;

(* GtkTextChildAnchor methods *)
	PROCEDURE [ccall] gtk_text_child_anchor_get_deleted*(textchildanchor: PGtkTextChildAnchor): lg.gboolean;
	PROCEDURE [ccall] gtk_text_child_anchor_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_text_child_anchor_get_widgets*(textchildanchor: PGtkTextChildAnchor): lg.PGList;

(* GtkTextChildAnchor constructors *)
	PROCEDURE [ccall] gtk_text_child_anchor_new*(): PGtkTextChildAnchor;

(* GtkTextMark methods *)
	PROCEDURE [ccall] gtk_text_mark_get_buffer*(textmark: PGtkTextMark): PGtkTextBuffer;
	PROCEDURE [ccall] gtk_text_mark_get_deleted*(textmark: PGtkTextMark): lg.gboolean;
	PROCEDURE [ccall] gtk_text_mark_get_left_gravity*(textmark: PGtkTextMark): lg.gboolean;
	PROCEDURE [ccall] gtk_text_mark_get_name*(textmark: PGtkTextMark): lg.Pgchar;
	PROCEDURE [ccall] gtk_text_mark_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_text_mark_get_visible*(textmark: PGtkTextMark): lg.gboolean;
	PROCEDURE [ccall] gtk_text_mark_set_visible*(textmark: PGtkTextMark; setting: lg.gboolean);

(* GtkTextTag methods *)
	PROCEDURE [ccall] gtk_text_tag_event*(texttag: PGtkTextTag; event_object: lo.PGObject; event: gdk.PGdkEvent; iter: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_tag_get_priority*(texttag: PGtkTextTag): lg.gint;
	PROCEDURE [ccall] gtk_text_tag_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_text_tag_set_priority*(texttag: PGtkTextTag; priority: lg.gint);

(* GtkTextTag constructors *)
	PROCEDURE [ccall] gtk_text_tag_new*(name: lg.Pgchar): PGtkTextTag;

(* GtkTextTagTable methods *)
	PROCEDURE [ccall] gtk_text_tag_table_add*(texttagtable: PGtkTextTagTable; tag: PGtkTextTag);
	PROCEDURE [ccall] gtk_text_tag_table_foreach*(texttagtable: PGtkTextTagTable; func: GtkTextTagTableForeach; data: lg.gpointer);
	PROCEDURE [ccall] gtk_text_tag_table_get_size*(texttagtable: PGtkTextTagTable): lg.gint;
	PROCEDURE [ccall] gtk_text_tag_table_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_text_tag_table_lookup*(texttagtable: PGtkTextTagTable; name: lg.Pgchar): PGtkTextTag;
	PROCEDURE [ccall] gtk_text_tag_table_remove*(texttagtable: PGtkTextTagTable; tag: PGtkTextTag);

(* GtkTextTagTable constructors *)
	PROCEDURE [ccall] gtk_text_tag_table_new*(): PGtkTextTagTable;

(* GtkTreeModelFilter methods *)
	PROCEDURE [ccall] gtk_tree_model_filter_clear_cache*(treemodelfilter: PGtkTreeModelFilter);
	PROCEDURE [ccall] gtk_tree_model_filter_convert_child_iter_to_iter*(treemodelfilter: PGtkTreeModelFilter; filter_iter: PGtkTreeIter; child_iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_filter_convert_child_path_to_path*(treemodelfilter: PGtkTreeModelFilter; child_path: PGtkTreePath): PGtkTreePath;
	PROCEDURE [ccall] gtk_tree_model_filter_convert_iter_to_child_iter*(treemodelfilter: PGtkTreeModelFilter; child_iter: PGtkTreeIter; filter_iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_filter_convert_path_to_child_path*(treemodelfilter: PGtkTreeModelFilter; filter_path: PGtkTreePath): PGtkTreePath;
	PROCEDURE [ccall] gtk_tree_model_filter_get_model*(treemodelfilter: PGtkTreeModelFilter): PGtkTreeModel;
	PROCEDURE [ccall] gtk_tree_model_filter_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_model_filter_refilter*(treemodelfilter: PGtkTreeModelFilter);
	PROCEDURE [ccall] gtk_tree_model_filter_set_modify_func*(treemodelfilter: PGtkTreeModelFilter; n_columns: lg.gint; VAR types: POINTER TO ARRAY [untagged] OF lo.GType; func: GtkTreeModelFilterModifyFunc; data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_model_filter_set_visible_column*(treemodelfilter: PGtkTreeModelFilter; column: lg.gint);
	PROCEDURE [ccall] gtk_tree_model_filter_set_visible_func*(treemodelfilter: PGtkTreeModelFilter; func: GtkTreeModelFilterVisibleFunc; data: lg.gpointer; destroy: lo.GDestroyNotify);

(* GtkTreeModelFilter constructors *)
	PROCEDURE [ccall] gtk_tree_model_filter_new*(child_model: PGtkTreeModel; root: PGtkTreePath): PGtkTreeModelFilter;

(* GtkTreeModelSort methods *)
	PROCEDURE [ccall] gtk_tree_model_sort_clear_cache*(treemodelsort: PGtkTreeModelSort);
	PROCEDURE [ccall] gtk_tree_model_sort_convert_child_iter_to_iter*(treemodelsort: PGtkTreeModelSort; sort_iter: PGtkTreeIter; child_iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_sort_convert_child_path_to_path*(treemodelsort: PGtkTreeModelSort; child_path: PGtkTreePath): PGtkTreePath;
	PROCEDURE [ccall] gtk_tree_model_sort_convert_iter_to_child_iter*(treemodelsort: PGtkTreeModelSort; child_iter: PGtkTreeIter; sorted_iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_sort_convert_path_to_child_path*(treemodelsort: PGtkTreeModelSort; sorted_path: PGtkTreePath): PGtkTreePath;
	PROCEDURE [ccall] gtk_tree_model_sort_get_model*(treemodelsort: PGtkTreeModelSort): PGtkTreeModel;
	PROCEDURE [ccall] gtk_tree_model_sort_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_model_sort_iter_is_valid*(treemodelsort: PGtkTreeModelSort; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_sort_reset_default_sort_func*(treemodelsort: PGtkTreeModelSort);

(* GtkTreeModelSort constructors *)
	PROCEDURE [ccall] gtk_tree_model_sort_new_with_model*(child_model: PGtkTreeModel): PGtkTreeModelSort;

(* GtkTreeSelection methods *)
	PROCEDURE [ccall] gtk_tree_selection_count_selected_rows*(treeselection: PGtkTreeSelection): lg.gint;
	PROCEDURE [ccall] gtk_tree_selection_get_mode*(treeselection: PGtkTreeSelection): GtkSelectionMode;
	PROCEDURE [ccall] gtk_tree_selection_get_selected*(treeselection: PGtkTreeSelection; VAR [nil] model: POINTER TO ARRAY [untagged] OF GtkTreeModel; VAR [nil] iter: GtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_selection_get_selected_rows*(treeselection: PGtkTreeSelection; VAR [nil] model: POINTER TO ARRAY [untagged] OF GtkTreeModel): lg.PGList;
	PROCEDURE [ccall] gtk_tree_selection_get_tree_view*(treeselection: PGtkTreeSelection): PGtkTreeView;
	PROCEDURE [ccall] gtk_tree_selection_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_selection_get_user_data*(treeselection: PGtkTreeSelection): lg.gpointer;
	PROCEDURE [ccall] gtk_tree_selection_iter_is_selected*(treeselection: PGtkTreeSelection; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_selection_path_is_selected*(treeselection: PGtkTreeSelection; path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_selection_select_all*(treeselection: PGtkTreeSelection);
	PROCEDURE [ccall] gtk_tree_selection_select_iter*(treeselection: PGtkTreeSelection; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_selection_select_path*(treeselection: PGtkTreeSelection; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_tree_selection_select_range*(treeselection: PGtkTreeSelection; start_path: PGtkTreePath; end_path: PGtkTreePath);
	PROCEDURE [ccall] gtk_tree_selection_selected_foreach*(treeselection: PGtkTreeSelection; func: GtkTreeSelectionForeachFunc; data: lg.gpointer);
	PROCEDURE [ccall] gtk_tree_selection_set_mode*(treeselection: PGtkTreeSelection; type: GtkSelectionMode);
	PROCEDURE [ccall] gtk_tree_selection_set_select_function*(treeselection: PGtkTreeSelection; func: GtkTreeSelectionFunc; data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_selection_unselect_all*(treeselection: PGtkTreeSelection);
	PROCEDURE [ccall] gtk_tree_selection_unselect_iter*(treeselection: PGtkTreeSelection; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_selection_unselect_path*(treeselection: PGtkTreeSelection; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_tree_selection_unselect_range*(treeselection: PGtkTreeSelection; start_path: PGtkTreePath; end_path: PGtkTreePath);

(* GtkTreeStore methods *)
	PROCEDURE [ccall] gtk_tree_store_append*(treestore: PGtkTreeStore; iter: PGtkTreeIter; parent: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_clear*(treestore: PGtkTreeStore);
	PROCEDURE [ccall] gtk_tree_store_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_store_insert*(treestore: PGtkTreeStore; iter: PGtkTreeIter; parent: PGtkTreeIter; position: lg.gint);
	PROCEDURE [ccall] gtk_tree_store_insert_after*(treestore: PGtkTreeStore; iter: PGtkTreeIter; parent: PGtkTreeIter; sibling: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_insert_before*(treestore: PGtkTreeStore; iter: PGtkTreeIter; parent: PGtkTreeIter; sibling: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_is_ancestor*(treestore: PGtkTreeStore; iter: PGtkTreeIter; descendant: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_store_iter_depth*(treestore: PGtkTreeStore; iter: PGtkTreeIter): lg.gint;
	PROCEDURE [ccall] gtk_tree_store_iter_is_valid*(treestore: PGtkTreeStore; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_store_move_after*(treestore: PGtkTreeStore; iter: PGtkTreeIter; position: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_move_before*(treestore: PGtkTreeStore; iter: PGtkTreeIter; position: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_prepend*(treestore: PGtkTreeStore; iter: PGtkTreeIter; parent: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_remove*(treestore: PGtkTreeStore; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_store_reorder*(treestore: PGtkTreeStore; parent: PGtkTreeIter; new_order: lg.Pgint);
	PROCEDURE [ccall] gtk_tree_store_set*(treestore: PGtkTreeStore; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_set_column_types*(treestore: PGtkTreeStore; n_columns: lg.gint; VAR types: POINTER TO ARRAY [untagged] OF lo.GType);
	PROCEDURE [ccall] gtk_tree_store_set_valist*(treestore: PGtkTreeStore; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_store_set_value*(treestore: PGtkTreeStore; iter: PGtkTreeIter; column: lg.gint; value: lo.PGValue);
	PROCEDURE [ccall] gtk_tree_store_swap*(treestore: PGtkTreeStore; a: PGtkTreeIter; b: PGtkTreeIter);

(* GtkTreeStore constructors *)
	PROCEDURE [ccall] gtk_tree_store_new*(n_columns: lg.gint): PGtkTreeStore;
	PROCEDURE [ccall] gtk_tree_store_newv*(n_columns: lg.gint; VAR types: POINTER TO ARRAY [untagged] OF lo.GType): PGtkTreeStore;

(* GtkUIManager methods *)
	PROCEDURE [ccall] gtk_ui_manager_add_ui*(uimanager: PGtkUIManager; merge_id: lg.guint; path: lg.Pgchar; name: lg.Pgchar; action: lg.Pgchar; type: GtkUIManagerItemType; top: lg.gboolean);
	PROCEDURE [ccall] gtk_ui_manager_add_ui_from_file*(uimanager: PGtkUIManager; filename: lg.Pgchar; VAR error: lg.PGError): lg.guint;
	PROCEDURE [ccall] gtk_ui_manager_add_ui_from_string*(uimanager: PGtkUIManager; buffer: lg.Pgchar; length: lg.gssize; VAR error: lg.PGError): lg.guint;
	PROCEDURE [ccall] gtk_ui_manager_ensure_update*(uimanager: PGtkUIManager);
	PROCEDURE [ccall] gtk_ui_manager_get_accel_group*(uimanager: PGtkUIManager): PGtkAccelGroup;
	PROCEDURE [ccall] gtk_ui_manager_get_action*(uimanager: PGtkUIManager; path: lg.Pgchar): PGtkAction;
	PROCEDURE [ccall] gtk_ui_manager_get_action_groups*(uimanager: PGtkUIManager): lg.PGList;
	PROCEDURE [ccall] gtk_ui_manager_get_add_tearoffs*(uimanager: PGtkUIManager): lg.gboolean;
	PROCEDURE [ccall] gtk_ui_manager_get_toplevels*(uimanager: PGtkUIManager; types: GtkUIManagerItemType): lg.PGSList;
	PROCEDURE [ccall] gtk_ui_manager_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_ui_manager_get_ui*(uimanager: PGtkUIManager): lg.Pgchar;
	PROCEDURE [ccall] gtk_ui_manager_get_widget*(uimanager: PGtkUIManager; path: lg.Pgchar): PGtkWidget;
	PROCEDURE [ccall] gtk_ui_manager_insert_action_group*(uimanager: PGtkUIManager; action_group: PGtkActionGroup; pos: lg.gint);
	PROCEDURE [ccall] gtk_ui_manager_remove_action_group*(uimanager: PGtkUIManager; action_group: PGtkActionGroup);
	PROCEDURE [ccall] gtk_ui_manager_remove_ui*(uimanager: PGtkUIManager; merge_id: lg.guint);
	PROCEDURE [ccall] gtk_ui_manager_set_add_tearoffs*(uimanager: PGtkUIManager; add_tearoffs: lg.gboolean);

(* GtkUIManager constructors *)
	PROCEDURE [ccall] gtk_ui_manager_new*(): PGtkUIManager;
	PROCEDURE [ccall] gtk_ui_manager_new_merge_id*(self: PGtkUIManager): PGtkUIManager;

(* GtkWindowGroup methods *)
	PROCEDURE [ccall] gtk_window_group_add_window*(windowgroup: PGtkWindowGroup; window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_group_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_window_group_remove_window*(windowgroup: PGtkWindowGroup; window: PGtkWindow);

(* GtkWindowGroup constructors *)
	PROCEDURE [ccall] gtk_window_group_new*(): PGtkWindowGroup;

(* GtkToggleAction methods *)
	PROCEDURE [ccall] gtk_toggle_action_get_active*(toggleaction: PGtkToggleAction): lg.gboolean;
	PROCEDURE [ccall] gtk_toggle_action_get_draw_as_radio*(toggleaction: PGtkToggleAction): lg.gboolean;
	PROCEDURE [ccall] gtk_toggle_action_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_toggle_action_set_active*(toggleaction: PGtkToggleAction; is_active: lg.gboolean);
	PROCEDURE [ccall] gtk_toggle_action_set_draw_as_radio*(toggleaction: PGtkToggleAction; draw_as_radio: lg.gboolean);
	PROCEDURE [ccall] gtk_toggle_action_toggled*(toggleaction: PGtkToggleAction);

(* GtkToggleAction constructors *)
	PROCEDURE [ccall] gtk_toggle_action_new*(name: lg.Pgchar; label: lg.Pgchar; tooltip: lg.Pgchar; stock_id: lg.Pgchar): PGtkToggleAction;

(* GtkRadioAction methods *)
	PROCEDURE [ccall] gtk_radio_action_get_current_value*(radioaction: PGtkRadioAction): lg.gint;
	PROCEDURE [ccall] gtk_radio_action_get_group*(radioaction: PGtkRadioAction): lg.PGSList;
	PROCEDURE [ccall] gtk_radio_action_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_radio_action_set_group*(radioaction: PGtkRadioAction; group: lg.PGSList);

(* GtkRadioAction constructors *)
	PROCEDURE [ccall] gtk_radio_action_new*(name: lg.Pgchar; label: lg.Pgchar; tooltip: lg.Pgchar; stock_id: lg.Pgchar; value: lg.gint): PGtkRadioAction;

(* GtkIMContextSimple methods *)
	PROCEDURE [ccall] gtk_im_context_simple_add_table*(imcontextsimple: PGtkIMContextSimple; data: lg.Pguint16; max_seq_len: lg.gint; n_seqs: lg.gint);
	PROCEDURE [ccall] gtk_im_context_simple_get_type*(): lo.GType;

(* GtkIMContextSimple constructors *)
	PROCEDURE [ccall] gtk_im_context_simple_new*(): PGtkIMContextSimple;

(* GtkIMMulticontext methods *)
	PROCEDURE [ccall] gtk_im_multicontext_append_menuitems*(immulticontext: PGtkIMMulticontext; menushell: PGtkMenuShell);
	PROCEDURE [ccall] gtk_im_multicontext_get_type*(): lo.GType;

(* GtkIMMulticontext constructors *)
	PROCEDURE [ccall] gtk_im_multicontext_new*(): PGtkIMMulticontext;

(* GtkWidget methods *)
	PROCEDURE [ccall] gtk_widget_activate*(widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_add_accelerator*(widget: PGtkWidget; accel_signal: lg.Pgchar; accel_group: PGtkAccelGroup; accel_key: lg.guint; accel_mods: gdk.GdkModifierType; accel_flags: GtkAccelFlags);
	PROCEDURE [ccall] gtk_widget_add_events*(widget: PGtkWidget; events: lg.gint);
	PROCEDURE [ccall] gtk_widget_add_mnemonic_label*(widget: PGtkWidget; label: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_can_activate_accel*(widget: PGtkWidget; signal_id: lg.guint): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_child_focus*(widget: PGtkWidget; direction: GtkDirectionType): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_child_notify*(widget: PGtkWidget; child_property: lg.Pgchar);
	PROCEDURE [ccall] gtk_widget_create_pango_context*(widget: PGtkWidget): pan.PPangoContext;
	PROCEDURE [ccall] gtk_widget_create_pango_layout*(widget: PGtkWidget; text: lg.Pgchar): pan.PPangoLayout;
	PROCEDURE [ccall] gtk_widget_destroy*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_destroyed*(widget: PGtkWidget; VAR widget_pointer: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_ensure_style*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_event*(widget: PGtkWidget; event: gdk.PGdkEvent): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_freeze_child_notify*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_get_accessible*(widget: PGtkWidget): atk.PAtkObject;
	PROCEDURE [ccall] gtk_widget_get_ancestor*(widget: PGtkWidget; widget_type: lo.GType): PGtkWidget;
	PROCEDURE [ccall] gtk_widget_get_child_requisition*(widget: PGtkWidget; requisition: PGtkRequisition);
	PROCEDURE [ccall] gtk_widget_get_child_visible*(widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_get_clipboard*(widget: PGtkWidget; selection: gdk.GdkAtom): PGtkClipboard;
	PROCEDURE [ccall] gtk_widget_get_colormap*(widget: PGtkWidget): gdk.PGdkColormap;
	PROCEDURE [ccall] gtk_widget_get_composite_name*(widget: PGtkWidget): lg.Pgchar;
	PROCEDURE [ccall] gtk_widget_get_default_colormap*(): gdk.PGdkColormap;
	PROCEDURE [ccall] gtk_widget_get_default_direction*(): GtkTextDirection;
	PROCEDURE [ccall] gtk_widget_get_default_style*(): PGtkStyle;
	PROCEDURE [ccall] gtk_widget_get_default_visual*(): gdk.PGdkVisual;
	PROCEDURE [ccall] gtk_widget_get_direction*(widget: PGtkWidget): GtkTextDirection;
	PROCEDURE [ccall] gtk_widget_get_display*(widget: PGtkWidget): gdk.PGdkDisplay;
	PROCEDURE [ccall] gtk_widget_get_events*(widget: PGtkWidget): gdk.GdkEventMask;
	PROCEDURE [ccall] gtk_widget_get_extension_events*(widget: PGtkWidget): gdk.GdkExtensionMode;
	PROCEDURE [ccall] gtk_widget_get_modifier_style*(widget: PGtkWidget): PGtkRcStyle;
	PROCEDURE [ccall] gtk_widget_get_name*(widget: PGtkWidget): lg.Pgchar;
	PROCEDURE [ccall] gtk_widget_get_no_show_all*(widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_get_pango_context*(widget: PGtkWidget): pan.PPangoContext;
	PROCEDURE [ccall] gtk_widget_get_parent*(widget: PGtkWidget): PGtkWidget;
	PROCEDURE [ccall] gtk_widget_get_parent_window*(widget: PGtkWidget): gdk.PGdkWindow;
	PROCEDURE [ccall] gtk_widget_get_pointer*(widget: PGtkWidget; x: lg.Pgint; y: lg.Pgint);
	PROCEDURE [ccall] gtk_widget_get_root_window*(widget: PGtkWidget): gdk.PGdkWindow;
	PROCEDURE [ccall] gtk_widget_get_screen*(widget: PGtkWidget): gdk.PGdkScreen;
	PROCEDURE [ccall] gtk_widget_get_settings*(widget: PGtkWidget): PGtkSettings;
	PROCEDURE [ccall] gtk_widget_get_size_request*(widget: PGtkWidget; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_widget_get_style*(widget: PGtkWidget): PGtkStyle;
	PROCEDURE [ccall] gtk_widget_get_toplevel*(widget: PGtkWidget): PGtkWidget;
	PROCEDURE [ccall] gtk_widget_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_widget_get_visual*(widget: PGtkWidget): gdk.PGdkVisual;
	PROCEDURE [ccall] gtk_widget_grab_default*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_grab_focus*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_has_screen*(widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_hide*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_hide_all*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_hide_on_delete*(widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_intersect*(widget: PGtkWidget; area: gdk.PGdkRectangle; intersection: gdk.PGdkRectangle): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_is_ancestor*(widget: PGtkWidget; ancestor: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_is_focus*(widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_list_accel_closures*(widget: PGtkWidget): lg.PGList;
	PROCEDURE [ccall] gtk_widget_list_mnemonic_labels*(widget: PGtkWidget): lg.PGList;
	PROCEDURE [ccall] gtk_widget_map*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_mnemonic_activate*(widget: PGtkWidget; group_cycling: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_modify_base*(widget: PGtkWidget; state: GtkStateType; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_widget_modify_bg*(widget: PGtkWidget; state: GtkStateType; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_widget_modify_fg*(widget: PGtkWidget; state: GtkStateType; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_widget_modify_font*(widget: PGtkWidget; font_desc: pan.PPangoFontDescription);
	PROCEDURE [ccall] gtk_widget_modify_style*(widget: PGtkWidget; style: PGtkRcStyle);
	PROCEDURE [ccall] gtk_widget_modify_text*(widget: PGtkWidget; state: GtkStateType; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_widget_path*(widget: PGtkWidget; VAR [nil] path_length: lg.guint; VAR [nil] path: POINTER TO ARRAY [untagged] OF lg.gchar; VAR [nil] path_reversed: POINTER TO ARRAY [untagged] OF lg.gchar);
	PROCEDURE [ccall] gtk_widget_pop_colormap*();
	PROCEDURE [ccall] gtk_widget_pop_composite_child*();
	PROCEDURE [ccall] gtk_widget_push_colormap*(cmap: gdk.PGdkColormap);
	PROCEDURE [ccall] gtk_widget_push_composite_child*();
	PROCEDURE [ccall] gtk_widget_queue_draw*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_queue_draw_area*(widget: PGtkWidget; x: lg.gint; y: lg.gint; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_widget_queue_resize*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_queue_resize_no_redraw*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_realize*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_ref*(widget: PGtkWidget): PGtkWidget;
	PROCEDURE [ccall] gtk_widget_region_intersect*(widget: PGtkWidget; region: gdk.PGdkRegion): gdk.PGdkRegion;
	PROCEDURE [ccall] gtk_widget_remove_accelerator*(widget: PGtkWidget; accel_group: PGtkAccelGroup; accel_key: lg.guint; accel_mods: gdk.GdkModifierType): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_remove_mnemonic_label*(widget: PGtkWidget; label: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_render_icon*(widget: PGtkWidget; stock_id: lg.Pgchar; size: GtkIconSize; detail: lg.Pgchar): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_widget_reparent*(widget: PGtkWidget; new_parent: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_reset_rc_styles*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_reset_shapes*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_send_expose*(widget: PGtkWidget; event: gdk.PGdkEvent): lg.gint;
	PROCEDURE [ccall] gtk_widget_set_accel_path*(widget: PGtkWidget; accel_path: lg.Pgchar; accel_group: PGtkAccelGroup);
	PROCEDURE [ccall] gtk_widget_set_app_paintable*(widget: PGtkWidget; app_paintable: lg.gboolean);
	PROCEDURE [ccall] gtk_widget_set_child_visible*(widget: PGtkWidget; is_visible: lg.gboolean);
	PROCEDURE [ccall] gtk_widget_set_colormap*(widget: PGtkWidget; colormap: gdk.PGdkColormap);
	PROCEDURE [ccall] gtk_widget_set_composite_name*(widget: PGtkWidget; name: lg.Pgchar);
	PROCEDURE [ccall] gtk_widget_set_default_colormap*(colormap: gdk.PGdkColormap);
	PROCEDURE [ccall] gtk_widget_set_default_direction*(dir: GtkTextDirection);
	PROCEDURE [ccall] gtk_widget_set_direction*(widget: PGtkWidget; dir: GtkTextDirection);
	PROCEDURE [ccall] gtk_widget_set_double_buffered*(widget: PGtkWidget; double_buffered: lg.gboolean);
	PROCEDURE [ccall] gtk_widget_set_events*(widget: PGtkWidget; events: gdk.GdkEventMask);
	PROCEDURE [ccall] gtk_widget_set_extension_events*(widget: PGtkWidget; mode: gdk.GdkExtensionMode);
	PROCEDURE [ccall] gtk_widget_set_name*(widget: PGtkWidget; name: lg.Pgchar);
	PROCEDURE [ccall] gtk_widget_set_no_show_all*(widget: PGtkWidget; no_show_all: lg.gboolean);
	PROCEDURE [ccall] gtk_widget_set_parent*(widget: PGtkWidget; parent: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_set_parent_window*(widget: PGtkWidget; parent_window: gdk.PGdkWindow);
	PROCEDURE [ccall] gtk_widget_set_redraw_on_allocate*(widget: PGtkWidget; redraw_on_allocate: lg.gboolean);
	PROCEDURE [ccall] gtk_widget_set_scroll_adjustments*(widget: PGtkWidget; hadjustment: PGtkAdjustment; vadjustment: PGtkAdjustment): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_set_sensitive*(widget: PGtkWidget; sensitive: lg.gboolean);
	PROCEDURE [ccall] gtk_widget_set_size_request*(widget: PGtkWidget; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_widget_set_state*(widget: PGtkWidget; state: GtkStateType);
	PROCEDURE [ccall] gtk_widget_set_style*(widget: PGtkWidget; style: PGtkStyle);
	PROCEDURE [ccall] gtk_widget_shape_combine_mask*(widget: PGtkWidget; shape_mask: gdk.PGdkBitmap; offset_x: lg.gint; offset_y: lg.gint);
	PROCEDURE [ccall] gtk_widget_show*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_show_all*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_show_now*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_size_allocate*(widget: PGtkWidget; allocation: PGtkAllocation);
	PROCEDURE [ccall] gtk_widget_size_request*(widget: PGtkWidget; requisition: PGtkRequisition);
	PROCEDURE [ccall] gtk_widget_style_get*(widget: PGtkWidget; first_property_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_widget_style_get_property*(widget: PGtkWidget; property_name: lg.Pgchar; value: lo.PGValue);
	PROCEDURE [ccall] gtk_widget_style_get_valist*(widget: PGtkWidget; first_property_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_widget_thaw_child_notify*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_translate_coordinates*(widget: PGtkWidget; dest_widget: PGtkWidget; src_x: lg.gint; src_y: lg.gint; dest_x: lg.Pgint; dest_y: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gtk_widget_unmap*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_unparent*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_unrealize*(widget: PGtkWidget);
	PROCEDURE [ccall] gtk_widget_unref*(widget: PGtkWidget);

(* GtkWidget constructors *)
	PROCEDURE [ccall] gtk_widget_new*(type: lo.GType; first_property_name: lg.Pgchar): PGtkWidget;

(* GtkCellRenderer methods *)
	PROCEDURE [ccall] gtk_cell_renderer_activate*(cellrenderer: PGtkCellRenderer; event: gdk.PGdkEvent; widget: PGtkWidget; path: lg.Pgchar; background_area: gdk.PGdkRectangle; cell_area: gdk.PGdkRectangle; flags: GtkCellRendererState): lg.gboolean;
	PROCEDURE [ccall] gtk_cell_renderer_get_fixed_size*(cellrenderer: PGtkCellRenderer; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_cell_renderer_get_size*(cellrenderer: PGtkCellRenderer; widget: PGtkWidget; cell_area: gdk.PGdkRectangle; x_offset: lg.Pgint; y_offset: lg.Pgint; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_cell_renderer_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_cell_renderer_render*(cellrenderer: PGtkCellRenderer; window: gdk.PGdkWindow; widget: PGtkWidget; background_area: gdk.PGdkRectangle; cell_area: gdk.PGdkRectangle; expose_area: gdk.PGdkRectangle; flags: GtkCellRendererState);
	PROCEDURE [ccall] gtk_cell_renderer_set_fixed_size*(cellrenderer: PGtkCellRenderer; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_cell_renderer_start_editing*(cellrenderer: PGtkCellRenderer; event: gdk.PGdkEvent; widget: PGtkWidget; path: lg.Pgchar; background_area: gdk.PGdkRectangle; cell_area: gdk.PGdkRectangle; flags: GtkCellRendererState): PGtkCellEditable;
	PROCEDURE [ccall] gtk_cell_renderer_stop_editing*(cellrenderer: PGtkCellRenderer; canceled: lg.gboolean);

(* GtkAdjustment methods *)
	PROCEDURE [ccall] gtk_adjustment_changed*(adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_adjustment_clamp_page*(adjustment: PGtkAdjustment; lower: lg.gdouble; upper: lg.gdouble);
	PROCEDURE [ccall] gtk_adjustment_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_adjustment_get_value*(adjustment: PGtkAdjustment): lg.gdouble;
	PROCEDURE [ccall] gtk_adjustment_set_value*(adjustment: PGtkAdjustment; value: lg.gdouble);
	PROCEDURE [ccall] gtk_adjustment_value_changed*(adjustment: PGtkAdjustment);

(* GtkAdjustment constructors *)
	PROCEDURE [ccall] gtk_adjustment_new*(value: lg.gdouble; lower: lg.gdouble; upper: lg.gdouble; step_increment: lg.gdouble; page_increment: lg.gdouble; page_size: lg.gdouble): PGtkAdjustment;

(* GtkFileFilter methods *)
	PROCEDURE [ccall] gtk_file_filter_add_custom*(filefilter: PGtkFileFilter; needed: GtkFileFilterFlags; func: GtkFileFilterFunc; data: lg.gpointer; notify: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_file_filter_add_mime_type*(filefilter: PGtkFileFilter; mime_type: lg.Pgchar);
	PROCEDURE [ccall] gtk_file_filter_add_pattern*(filefilter: PGtkFileFilter; pattern: lg.Pgchar);
	PROCEDURE [ccall] gtk_file_filter_add_pixbuf_formats*(filefilter: PGtkFileFilter);
	PROCEDURE [ccall] gtk_file_filter_filter*(filefilter: PGtkFileFilter; filter_info: PGtkFileFilterInfo): lg.gboolean;
	PROCEDURE [ccall] gtk_file_filter_get_name*(filefilter: PGtkFileFilter): lg.Pgchar;
	PROCEDURE [ccall] gtk_file_filter_get_needed*(filefilter: PGtkFileFilter): GtkFileFilterFlags;
	PROCEDURE [ccall] gtk_file_filter_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_file_filter_set_name*(filefilter: PGtkFileFilter; name: lg.Pgchar);

(* GtkFileFilter constructors *)
	PROCEDURE [ccall] gtk_file_filter_new*(): PGtkFileFilter;

(* GtkItemFactory methods *)

(* GtkItemFactory constructors *)

(* GtkTooltips methods *)
	PROCEDURE [ccall] gtk_tooltips_disable*(tooltips: PGtkTooltips);
	PROCEDURE [ccall] gtk_tooltips_enable*(tooltips: PGtkTooltips);
	PROCEDURE [ccall] gtk_tooltips_force_window*(tooltips: PGtkTooltips);
	PROCEDURE [ccall] gtk_tooltips_get_info_from_tip_window*(tip_window: PGtkWindow; VAR [nil] tooltips: PGtkTooltips; VAR [nil] current_widget: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_tooltips_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tooltips_set_tip*(tooltips: PGtkTooltips; widget: PGtkWidget; tip_text: lg.Pgchar; tip_private: lg.Pgchar);

(* GtkTooltips constructors *)
	PROCEDURE [ccall] gtk_tooltips_new*(): PGtkTooltips;

(* GtkTreeViewColumn methods *)
	PROCEDURE [ccall] gtk_tree_view_column_add_attribute*(treeviewcolumn: PGtkTreeViewColumn; cell_renderer: PGtkCellRenderer; attribute: lg.Pgchar; column: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_column_cell_get_position*(treeviewcolumn: PGtkTreeViewColumn; cell_renderer: PGtkCellRenderer; start_pos: lg.Pgint; width: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_cell_get_size*(treeviewcolumn: PGtkTreeViewColumn; cell_area: gdk.PGdkRectangle; x_offset: lg.Pgint; y_offset: lg.Pgint; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_tree_view_column_cell_is_visible*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_cell_set_cell_data*(treeviewcolumn: PGtkTreeViewColumn; tree_model: PGtkTreeModel; iter: PGtkTreeIter; is_expander: lg.gboolean; is_expanded: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_clear*(treeviewcolumn: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_column_clear_attributes*(treeviewcolumn: PGtkTreeViewColumn; cell_renderer: PGtkCellRenderer);
	PROCEDURE [ccall] gtk_tree_view_column_clicked*(treeviewcolumn: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_column_focus_cell*(treeviewcolumn: PGtkTreeViewColumn; cell: PGtkCellRenderer);
	PROCEDURE [ccall] gtk_tree_view_column_get_alignment*(treeviewcolumn: PGtkTreeViewColumn): lg.gfloat;
	PROCEDURE [ccall] gtk_tree_view_column_get_cell_renderers*(treeviewcolumn: PGtkTreeViewColumn): lg.PGList;
	PROCEDURE [ccall] gtk_tree_view_column_get_clickable*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_get_expand*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_get_fixed_width*(treeviewcolumn: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_column_get_max_width*(treeviewcolumn: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_column_get_min_width*(treeviewcolumn: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_column_get_reorderable*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_get_resizable*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_get_sizing*(treeviewcolumn: PGtkTreeViewColumn): GtkTreeViewColumnSizing;
	PROCEDURE [ccall] gtk_tree_view_column_get_sort_column_id*(treeviewcolumn: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_column_get_sort_indicator*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_get_sort_order*(treeviewcolumn: PGtkTreeViewColumn): GtkSortType;
	PROCEDURE [ccall] gtk_tree_view_column_get_spacing*(treeviewcolumn: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_column_get_title*(treeviewcolumn: PGtkTreeViewColumn): lg.Pgchar;
	PROCEDURE [ccall] gtk_tree_view_column_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_view_column_get_visible*(treeviewcolumn: PGtkTreeViewColumn): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_column_get_widget*(treeviewcolumn: PGtkTreeViewColumn): PGtkWidget;
	PROCEDURE [ccall] gtk_tree_view_column_get_width*(treeviewcolumn: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_column_pack_end*(treeviewcolumn: PGtkTreeViewColumn; cell: PGtkCellRenderer; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_pack_start*(treeviewcolumn: PGtkTreeViewColumn; cell: PGtkCellRenderer; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_queue_resize*(treeviewcolumn: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_column_set_alignment*(treeviewcolumn: PGtkTreeViewColumn; xalign: lg.gfloat);
	PROCEDURE [ccall] gtk_tree_view_column_set_attributes*(treeviewcolumn: PGtkTreeViewColumn; cell_renderer: PGtkCellRenderer);
	PROCEDURE [ccall] gtk_tree_view_column_set_cell_data_func*(treeviewcolumn: PGtkTreeViewColumn; cell_renderer: PGtkCellRenderer; func: GtkTreeCellDataFunc; func_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_view_column_set_clickable*(treeviewcolumn: PGtkTreeViewColumn; clickable: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_set_expand*(treeviewcolumn: PGtkTreeViewColumn; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_set_fixed_width*(treeviewcolumn: PGtkTreeViewColumn; fixed_width: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_column_set_max_width*(treeviewcolumn: PGtkTreeViewColumn; max_width: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_column_set_min_width*(treeviewcolumn: PGtkTreeViewColumn; min_width: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_column_set_reorderable*(treeviewcolumn: PGtkTreeViewColumn; reorderable: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_set_resizable*(treeviewcolumn: PGtkTreeViewColumn; resizable: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_set_sizing*(treeviewcolumn: PGtkTreeViewColumn; type: GtkTreeViewColumnSizing);
	PROCEDURE [ccall] gtk_tree_view_column_set_sort_column_id*(treeviewcolumn: PGtkTreeViewColumn; sort_column_id: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_column_set_sort_indicator*(treeviewcolumn: PGtkTreeViewColumn; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_set_sort_order*(treeviewcolumn: PGtkTreeViewColumn; order: GtkSortType);
	PROCEDURE [ccall] gtk_tree_view_column_set_spacing*(treeviewcolumn: PGtkTreeViewColumn; spacing: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_column_set_title*(treeviewcolumn: PGtkTreeViewColumn; title: lg.Pgchar);
	PROCEDURE [ccall] gtk_tree_view_column_set_visible*(treeviewcolumn: PGtkTreeViewColumn; visible: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_column_set_widget*(treeviewcolumn: PGtkTreeViewColumn; widget: PGtkWidget);

(* GtkTreeViewColumn constructors *)
	PROCEDURE [ccall] gtk_tree_view_column_new*(): PGtkTreeViewColumn;
	PROCEDURE [ccall] gtk_tree_view_column_new_with_attributes*(title: lg.Pgchar; cell: PGtkCellRenderer): PGtkTreeViewColumn;

(* GtkContainer methods *)
	PROCEDURE [ccall] gtk_container_add*(container: PGtkContainer; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_container_add_with_properties*(container: PGtkContainer; widget: PGtkWidget; first_prop_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_container_check_resize*(container: PGtkContainer);
	PROCEDURE [ccall] gtk_container_child_get*(container: PGtkContainer; child: PGtkWidget; first_prop_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_container_child_get_property*(container: PGtkContainer; child: PGtkWidget; property_name: lg.Pgchar; value: lo.PGValue);
	PROCEDURE [ccall] gtk_container_child_get_valist*(container: PGtkContainer; child: PGtkWidget; first_property_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_container_child_set*(container: PGtkContainer; child: PGtkWidget; first_prop_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_container_child_set_property*(container: PGtkContainer; child: PGtkWidget; property_name: lg.Pgchar; value: lo.PGValue);
	PROCEDURE [ccall] gtk_container_child_set_valist*(container: PGtkContainer; child: PGtkWidget; first_property_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_container_child_type*(container: PGtkContainer): lo.GType;
	PROCEDURE [ccall] gtk_container_forall*(container: PGtkContainer; callback: GtkCallback; callback_data: lg.gpointer);
	PROCEDURE [ccall] gtk_container_foreach*(container: PGtkContainer; callback: GtkCallback; callback_data: lg.gpointer);
	PROCEDURE [ccall] gtk_container_get_border_width*(container: PGtkContainer): lg.guint;
	PROCEDURE [ccall] gtk_container_get_children*(container: PGtkContainer): lg.PGList;
	PROCEDURE [ccall] gtk_container_get_focus_chain*(container: PGtkContainer; VAR [nil] focusable_widgets: lg.PGList): lg.gboolean;
	PROCEDURE [ccall] gtk_container_get_focus_hadjustment*(container: PGtkContainer): PGtkAdjustment;
	PROCEDURE [ccall] gtk_container_get_focus_vadjustment*(container: PGtkContainer): PGtkAdjustment;
	PROCEDURE [ccall] gtk_container_get_resize_mode*(container: PGtkContainer): GtkResizeMode;
	PROCEDURE [ccall] gtk_container_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_container_propagate_expose*(container: PGtkContainer; child: PGtkWidget; event: gdk.PGdkEventExpose);
	PROCEDURE [ccall] gtk_container_remove*(container: PGtkContainer; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_container_resize_children*(container: PGtkContainer);
	PROCEDURE [ccall] gtk_container_set_border_width*(container: PGtkContainer; border_width: lg.guint);
	PROCEDURE [ccall] gtk_container_set_focus_chain*(container: PGtkContainer; focusable_widgets: lg.PGList);
	PROCEDURE [ccall] gtk_container_set_focus_child*(container: PGtkContainer; child: PGtkWidget);
	PROCEDURE [ccall] gtk_container_set_focus_hadjustment*(container: PGtkContainer; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_container_set_focus_vadjustment*(container: PGtkContainer; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_container_set_reallocate_redraws*(container: PGtkContainer; needs_redraws: lg.gboolean);
	PROCEDURE [ccall] gtk_container_set_resize_mode*(container: PGtkContainer; resize_mode: GtkResizeMode);
	PROCEDURE [ccall] gtk_container_unset_focus_chain*(container: PGtkContainer);

(* GtkMisc methods *)
	PROCEDURE [ccall] gtk_misc_get_alignment*(misc: PGtkMisc; VAR [nil] xalign: lg.gfloat; VAR [nil] yalign: lg.gfloat);
	PROCEDURE [ccall] gtk_misc_get_padding*(misc: PGtkMisc; xpad: lg.Pgint; ypad: lg.Pgint);
	PROCEDURE [ccall] gtk_misc_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_misc_set_alignment*(misc: PGtkMisc; xalign: lg.gfloat; yalign: lg.gfloat);
	PROCEDURE [ccall] gtk_misc_set_padding*(misc: PGtkMisc; xpad: lg.gint; ypad: lg.gint);

(* GtkRange methods *)
	PROCEDURE [ccall] gtk_range_get_adjustment*(range: PGtkRange): PGtkAdjustment;
	PROCEDURE [ccall] gtk_range_get_inverted*(range: PGtkRange): lg.gboolean;
	PROCEDURE [ccall] gtk_range_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_range_get_update_policy*(range: PGtkRange): GtkUpdateType;
	PROCEDURE [ccall] gtk_range_get_value*(range: PGtkRange): lg.gdouble;
	PROCEDURE [ccall] gtk_range_set_adjustment*(range: PGtkRange; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_range_set_increments*(range: PGtkRange; step: lg.gdouble; page: lg.gdouble);
	PROCEDURE [ccall] gtk_range_set_inverted*(range: PGtkRange; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_range_set_range*(range: PGtkRange; min: lg.gdouble; max: lg.gdouble);
	PROCEDURE [ccall] gtk_range_set_update_policy*(range: PGtkRange; policy: GtkUpdateType);
	PROCEDURE [ccall] gtk_range_set_value*(range: PGtkRange; value: lg.gdouble);

(* GtkRuler methods *)
	PROCEDURE [ccall] gtk_ruler_draw_pos*(ruler: PGtkRuler);
	PROCEDURE [ccall] gtk_ruler_draw_ticks*(ruler: PGtkRuler);
	PROCEDURE [ccall] gtk_ruler_get_metric*(ruler: PGtkRuler): GtkMetricType;
	PROCEDURE [ccall] gtk_ruler_get_range*(ruler: PGtkRuler; lower: lg.Pgdouble; upper: lg.Pgdouble; position: lg.Pgdouble; max_size: lg.Pgdouble);
	PROCEDURE [ccall] gtk_ruler_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_ruler_set_metric*(ruler: PGtkRuler; metric: GtkMetricType);
	PROCEDURE [ccall] gtk_ruler_set_range*(ruler: PGtkRuler; lower: lg.gdouble; upper: lg.gdouble; position: lg.gdouble; max_size: lg.gdouble);

(* GtkSeparator methods *)
	PROCEDURE [ccall] gtk_separator_get_type*(): lo.GType;

(* GtkDrawingArea methods *)
	PROCEDURE [ccall] gtk_drawing_area_get_type*(): lo.GType;

(* GtkDrawingArea constructors *)
	PROCEDURE [ccall] gtk_drawing_area_new*(): PGtkDrawingArea;

(* GtkEntry methods *)
	PROCEDURE [ccall] gtk_entry_get_activates_default*(entry: PGtkEntry): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_get_alignment*(entry: PGtkEntry): lg.gfloat;
	PROCEDURE [ccall] gtk_entry_get_completion*(entry: PGtkEntry): PGtkEntryCompletion;
	PROCEDURE [ccall] gtk_entry_get_has_frame*(entry: PGtkEntry): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_get_invisible_char*(entry: PGtkEntry): lg.gunichar;
	PROCEDURE [ccall] gtk_entry_get_layout*(entry: PGtkEntry): pan.PPangoLayout;
	PROCEDURE [ccall] gtk_entry_get_layout_offsets*(entry: PGtkEntry; x: lg.Pgint; y: lg.Pgint);
	PROCEDURE [ccall] gtk_entry_get_max_length*(entry: PGtkEntry): lg.gint;
	PROCEDURE [ccall] gtk_entry_get_text*(entry: PGtkEntry): lg.Pgchar;
	PROCEDURE [ccall] gtk_entry_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_entry_get_visibility*(entry: PGtkEntry): lg.gboolean;
	PROCEDURE [ccall] gtk_entry_get_width_chars*(entry: PGtkEntry): lg.gint;
	PROCEDURE [ccall] gtk_entry_layout_index_to_text_index*(entry: PGtkEntry; layout_index: lg.gint): lg.gint;
	PROCEDURE [ccall] gtk_entry_set_activates_default*(entry: PGtkEntry; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_set_alignment*(entry: PGtkEntry; xalign: lg.gfloat);
	PROCEDURE [ccall] gtk_entry_set_completion*(entry: PGtkEntry; completion: PGtkEntryCompletion);
	PROCEDURE [ccall] gtk_entry_set_has_frame*(entry: PGtkEntry; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_set_invisible_char*(entry: PGtkEntry; ch: lg.gunichar);
	PROCEDURE [ccall] gtk_entry_set_max_length*(entry: PGtkEntry; max: lg.gint);
	PROCEDURE [ccall] gtk_entry_set_text*(entry: PGtkEntry; text: lg.Pgchar);
	PROCEDURE [ccall] gtk_entry_set_visibility*(entry: PGtkEntry; visible: lg.gboolean);
	PROCEDURE [ccall] gtk_entry_set_width_chars*(entry: PGtkEntry; n_chars: lg.gint);
	PROCEDURE [ccall] gtk_entry_text_index_to_layout_index*(entry: PGtkEntry; text_index: lg.gint): lg.gint;

(* GtkEntry constructors *)
	PROCEDURE [ccall] gtk_entry_new*(): PGtkEntry;

(* GtkCalendar methods *)
	PROCEDURE [ccall] gtk_calendar_clear_marks*(calendar: PGtkCalendar);
	PROCEDURE [ccall] gtk_calendar_get_date*(calendar: PGtkCalendar; VAR [nil] year: lg.guint; VAR [nil] month: lg.guint; VAR [nil] day: lg.guint);
	PROCEDURE [ccall] gtk_calendar_get_display_options*(calendar: PGtkCalendar): GtkCalendarDisplayOptions;
	PROCEDURE [ccall] gtk_calendar_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_calendar_mark_day*(calendar: PGtkCalendar; day: lg.guint): lg.gboolean;
	PROCEDURE [ccall] gtk_calendar_select_day*(calendar: PGtkCalendar; day: lg.guint);
	PROCEDURE [ccall] gtk_calendar_select_month*(calendar: PGtkCalendar; month: lg.guint; year: lg.guint): lg.gboolean;
	PROCEDURE [ccall] gtk_calendar_set_display_options*(calendar: PGtkCalendar; flags: GtkCalendarDisplayOptions);
	PROCEDURE [ccall] gtk_calendar_unmark_day*(calendar: PGtkCalendar; day: lg.guint): lg.gboolean;

(* GtkCalendar constructors *)
	PROCEDURE [ccall] gtk_calendar_new*(): PGtkCalendar;

(* GtkCellView methods *)
	PROCEDURE [ccall] gtk_cell_view_get_cell_renderers*(cellview: PGtkCellView): lg.PGList;
	PROCEDURE [ccall] gtk_cell_view_get_displayed_row*(cellview: PGtkCellView): PGtkTreePath;
	PROCEDURE [ccall] gtk_cell_view_get_size_of_row*(cellview: PGtkCellView; path: PGtkTreePath; requisition: PGtkRequisition): lg.gboolean;
	PROCEDURE [ccall] gtk_cell_view_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_cell_view_set_background_color*(cellview: PGtkCellView; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_cell_view_set_displayed_row*(cellview: PGtkCellView; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_cell_view_set_model*(cellview: PGtkCellView; model: PGtkTreeModel);

(* GtkCellView constructors *)
	PROCEDURE [ccall] gtk_cell_view_new*(): PGtkCellView;
	PROCEDURE [ccall] gtk_cell_view_new_with_markup*(markup: lg.Pgchar): PGtkCellView;
	PROCEDURE [ccall] gtk_cell_view_new_with_pixbuf*(pixbuf: gpb.PGdkPixbuf): PGtkCellView;
	PROCEDURE [ccall] gtk_cell_view_new_with_text*(text: lg.Pgchar): PGtkCellView;

(* GtkHSV methods *)
	PROCEDURE [ccall] gtk_hsv_get_color*(hsv: PGtkHSV; h: lg.Pgdouble; s: lg.Pgdouble; v: lg.Pgdouble);
	PROCEDURE [ccall] gtk_hsv_get_metrics*(hsv: PGtkHSV; size: lg.Pgint; ring_width: lg.Pgint);
	PROCEDURE [ccall] gtk_hsv_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_hsv_is_adjusting*(hsv: PGtkHSV): lg.gboolean;
	PROCEDURE [ccall] gtk_hsv_set_color*(hsv: PGtkHSV; h: lg.gdouble; s: lg.gdouble; v: lg.gdouble);
	PROCEDURE [ccall] gtk_hsv_set_metrics*(hsv: PGtkHSV; size: lg.gint; ring_width: lg.gint);
	PROCEDURE [ccall] gtk_hsv_to_rgb*(h: lg.gdouble; s: lg.gdouble; v: lg.gdouble; r: lg.Pgdouble; g: lg.Pgdouble; b: lg.Pgdouble);

(* GtkHSV constructors *)
	PROCEDURE [ccall] gtk_hsv_new*(): PGtkHSV;

(* GtkInvisible methods *)
	PROCEDURE [ccall] gtk_invisible_get_screen*(invisible: PGtkInvisible): gdk.PGdkScreen;
	PROCEDURE [ccall] gtk_invisible_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_invisible_set_screen*(invisible: PGtkInvisible; screen: gdk.PGdkScreen);

(* GtkInvisible constructors *)
	PROCEDURE [ccall] gtk_invisible_new*(): PGtkInvisible;
	PROCEDURE [ccall] gtk_invisible_new_for_screen*(screen: gdk.PGdkScreen): PGtkInvisible;

(* GtkProgressBar methods *)
	PROCEDURE [ccall] gtk_progress_bar_get_ellipsize*(progressbar: PGtkProgressBar): pan.PangoEllipsizeMode;
	PROCEDURE [ccall] gtk_progress_bar_get_fraction*(progressbar: PGtkProgressBar): lg.gdouble;
	PROCEDURE [ccall] gtk_progress_bar_get_orientation*(progressbar: PGtkProgressBar): GtkProgressBarOrientation;
	PROCEDURE [ccall] gtk_progress_bar_get_pulse_step*(progressbar: PGtkProgressBar): lg.gdouble;
	PROCEDURE [ccall] gtk_progress_bar_get_text*(progressbar: PGtkProgressBar): lg.Pgchar;
	PROCEDURE [ccall] gtk_progress_bar_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_progress_bar_pulse*(progressbar: PGtkProgressBar);
	PROCEDURE [ccall] gtk_progress_bar_set_ellipsize*(progressbar: PGtkProgressBar; mode: pan.PangoEllipsizeMode);
	PROCEDURE [ccall] gtk_progress_bar_set_fraction*(progressbar: PGtkProgressBar; fraction: lg.gdouble);
	PROCEDURE [ccall] gtk_progress_bar_set_orientation*(progressbar: PGtkProgressBar; orientation: GtkProgressBarOrientation);
	PROCEDURE [ccall] gtk_progress_bar_set_pulse_step*(progressbar: PGtkProgressBar; fraction: lg.gdouble);
	PROCEDURE [ccall] gtk_progress_bar_set_text*(progressbar: PGtkProgressBar; text: lg.Pgchar);

(* GtkProgressBar constructors *)
	PROCEDURE [ccall] gtk_progress_bar_new*(): PGtkProgressBar;

(* GtkBin methods *)
	PROCEDURE [ccall] gtk_bin_get_child*(bin: PGtkBin): PGtkWidget;
	PROCEDURE [ccall] gtk_bin_get_type*(): lo.GType;

(* GtkBox methods *)
	PROCEDURE [ccall] gtk_box_get_homogeneous*(box: PGtkBox): lg.gboolean;
	PROCEDURE [ccall] gtk_box_get_spacing*(box: PGtkBox): lg.gint;
	PROCEDURE [ccall] gtk_box_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_box_pack_end*(box: PGtkBox; child: PGtkWidget; expand: lg.gboolean; fill: lg.gboolean; padding: lg.guint);
	PROCEDURE [ccall] gtk_box_pack_end_defaults*(box: PGtkBox; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_box_pack_start*(box: PGtkBox; child: PGtkWidget; expand: lg.gboolean; fill: lg.gboolean; padding: lg.guint);
	PROCEDURE [ccall] gtk_box_pack_start_defaults*(box: PGtkBox; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_box_query_child_packing*(box: PGtkBox; child: PGtkWidget; VAR expand: lg.gboolean; VAR fill: lg.gboolean; VAR padding: lg.guint; VAR pack_type: GtkPackType);
	PROCEDURE [ccall] gtk_box_reorder_child*(box: PGtkBox; child: PGtkWidget; position: lg.gint);
	PROCEDURE [ccall] gtk_box_set_child_packing*(box: PGtkBox; child: PGtkWidget; expand: lg.gboolean; fill: lg.gboolean; padding: lg.guint; pack_type: GtkPackType);
	PROCEDURE [ccall] gtk_box_set_homogeneous*(box: PGtkBox; homogeneous: lg.gboolean);
	PROCEDURE [ccall] gtk_box_set_spacing*(box: PGtkBox; spacing: lg.gint);

(* GtkMenuShell methods *)
	PROCEDURE [ccall] gtk_menu_shell_activate_item*(menushell: PGtkMenuShell; menu_item: PGtkWidget; force_deactivate: lg.gboolean);
	PROCEDURE [ccall] gtk_menu_shell_append*(menushell: PGtkMenuShell; child: PGtkWidget);
	PROCEDURE [ccall] gtk_menu_shell_cancel*(menushell: PGtkMenuShell);
	PROCEDURE [ccall] gtk_menu_shell_deactivate*(menushell: PGtkMenuShell);
	PROCEDURE [ccall] gtk_menu_shell_deselect*(menushell: PGtkMenuShell);
	PROCEDURE [ccall] gtk_menu_shell_get_take_focus*(menushell: PGtkMenuShell): lg.gboolean;
	PROCEDURE [ccall] gtk_menu_shell_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_menu_shell_insert*(menushell: PGtkMenuShell; child: PGtkWidget; position: lg.gint);
	PROCEDURE [ccall] gtk_menu_shell_prepend*(menushell: PGtkMenuShell; child: PGtkWidget);
	PROCEDURE [ccall] gtk_menu_shell_select_first*(menushell: PGtkMenuShell; search_sensitive: lg.gboolean);
	PROCEDURE [ccall] gtk_menu_shell_select_item*(menushell: PGtkMenuShell; menu_item: PGtkWidget);
	PROCEDURE [ccall] gtk_menu_shell_set_take_focus*(menushell: PGtkMenuShell; take_focus: lg.gboolean);

(* GtkPaned methods *)
	PROCEDURE [ccall] gtk_paned_add1*(paned: PGtkPaned; child: PGtkWidget);
	PROCEDURE [ccall] gtk_paned_add2*(paned: PGtkPaned; child: PGtkWidget);
	PROCEDURE [ccall] gtk_paned_get_child1*(paned: PGtkPaned): PGtkWidget;
	PROCEDURE [ccall] gtk_paned_get_child2*(paned: PGtkPaned): PGtkWidget;
	PROCEDURE [ccall] gtk_paned_get_position*(paned: PGtkPaned): lg.gint;
	PROCEDURE [ccall] gtk_paned_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_paned_pack1*(paned: PGtkPaned; child: PGtkWidget; resize: lg.gboolean; shrink: lg.gboolean);
	PROCEDURE [ccall] gtk_paned_pack2*(paned: PGtkPaned; child: PGtkWidget; resize: lg.gboolean; shrink: lg.gboolean);
	PROCEDURE [ccall] gtk_paned_set_position*(paned: PGtkPaned; position: lg.gint);

(* GtkFixed methods *)
	PROCEDURE [ccall] gtk_fixed_get_has_window*(fixed: PGtkFixed): lg.gboolean;
	PROCEDURE [ccall] gtk_fixed_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_fixed_move*(fixed: PGtkFixed; widget: PGtkWidget; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_fixed_put*(fixed: PGtkFixed; widget: PGtkWidget; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_fixed_set_has_window*(fixed: PGtkFixed; has_window: lg.gboolean);

(* GtkFixed constructors *)
	PROCEDURE [ccall] gtk_fixed_new*(): PGtkFixed;

(* GtkIconView methods *)
	PROCEDURE [ccall] gtk_icon_view_create_drag_icon*(iconview: PGtkIconView; path: PGtkTreePath): gdk.PGdkPixmap;
	PROCEDURE [ccall] gtk_icon_view_enable_model_drag_dest*(iconview: PGtkIconView; targets: PGtkTargetEntry; n_targets: lg.gint; actions: gdk.GdkDragAction);
	PROCEDURE [ccall] gtk_icon_view_enable_model_drag_source*(iconview: PGtkIconView; start_button_mask: gdk.GdkModifierType; targets: PGtkTargetEntry; n_targets: lg.gint; actions: gdk.GdkDragAction);
	PROCEDURE [ccall] gtk_icon_view_get_column_spacing*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_columns*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_cursor*(iconview: PGtkIconView; VAR [nil] path: PGtkTreePath; VAR [nil] cell: PGtkCellRenderer): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_view_get_dest_item_at_pos*(iconview: PGtkIconView; drag_x: lg.gint; drag_y: lg.gint; VAR [nil] path: PGtkTreePath; VAR [nil] pos: GtkIconViewDropPosition): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_view_get_drag_dest_item*(iconview: PGtkIconView; VAR [nil] path: PGtkTreePath; VAR [nil] pos: GtkIconViewDropPosition);
	PROCEDURE [ccall] gtk_icon_view_get_item_at_pos*(iconview: PGtkIconView; x: lg.gint; y: lg.gint; VAR [nil] path: PGtkTreePath; VAR [nil] cell: PGtkCellRenderer): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_view_get_item_width*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_margin*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_markup_column*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_model*(iconview: PGtkIconView): PGtkTreeModel;
	PROCEDURE [ccall] gtk_icon_view_get_orientation*(iconview: PGtkIconView): GtkOrientation;
	PROCEDURE [ccall] gtk_icon_view_get_path_at_pos*(iconview: PGtkIconView; x: lg.gint; y: lg.gint): PGtkTreePath;
	PROCEDURE [ccall] gtk_icon_view_get_pixbuf_column*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_reorderable*(iconview: PGtkIconView): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_view_get_row_spacing*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_selected_items*(iconview: PGtkIconView): lg.PGList;
	PROCEDURE [ccall] gtk_icon_view_get_selection_mode*(iconview: PGtkIconView): GtkSelectionMode;
	PROCEDURE [ccall] gtk_icon_view_get_spacing*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_text_column*(iconview: PGtkIconView): lg.gint;
	PROCEDURE [ccall] gtk_icon_view_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_icon_view_get_visible_range*(iconview: PGtkIconView; VAR [nil] start_path: PGtkTreePath; VAR [nil] end_path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_view_item_activated*(iconview: PGtkIconView; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_icon_view_path_is_selected*(iconview: PGtkIconView; path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_icon_view_scroll_to_path*(iconview: PGtkIconView; path: PGtkTreePath; use_align: lg.gboolean; row_align: lg.gfloat; col_align: lg.gfloat);
	PROCEDURE [ccall] gtk_icon_view_select_all*(iconview: PGtkIconView);
	PROCEDURE [ccall] gtk_icon_view_select_path*(iconview: PGtkIconView; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_icon_view_selected_foreach*(iconview: PGtkIconView; func: GtkIconViewForeachFunc; data: lg.gpointer);
	PROCEDURE [ccall] gtk_icon_view_set_column_spacing*(iconview: PGtkIconView; column_spacing: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_columns*(iconview: PGtkIconView; columns: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_cursor*(iconview: PGtkIconView; path: PGtkTreePath; cell: PGtkCellRenderer; start_editing: lg.gboolean);
	PROCEDURE [ccall] gtk_icon_view_set_drag_dest_item*(iconview: PGtkIconView; path: PGtkTreePath; pos: GtkIconViewDropPosition);
	PROCEDURE [ccall] gtk_icon_view_set_item_width*(iconview: PGtkIconView; item_width: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_margin*(iconview: PGtkIconView; margin: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_markup_column*(iconview: PGtkIconView; column: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_model*(iconview: PGtkIconView; model: PGtkTreeModel);
	PROCEDURE [ccall] gtk_icon_view_set_orientation*(iconview: PGtkIconView; orientation: GtkOrientation);
	PROCEDURE [ccall] gtk_icon_view_set_pixbuf_column*(iconview: PGtkIconView; column: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_reorderable*(iconview: PGtkIconView; reorderable: lg.gboolean);
	PROCEDURE [ccall] gtk_icon_view_set_row_spacing*(iconview: PGtkIconView; row_spacing: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_selection_mode*(iconview: PGtkIconView; mode: GtkSelectionMode);
	PROCEDURE [ccall] gtk_icon_view_set_spacing*(iconview: PGtkIconView; spacing: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_set_text_column*(iconview: PGtkIconView; column: lg.gint);
	PROCEDURE [ccall] gtk_icon_view_unselect_all*(iconview: PGtkIconView);
	PROCEDURE [ccall] gtk_icon_view_unselect_path*(iconview: PGtkIconView; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_icon_view_unset_model_drag_dest*(iconview: PGtkIconView);
	PROCEDURE [ccall] gtk_icon_view_unset_model_drag_source*(iconview: PGtkIconView);

(* GtkIconView constructors *)
	PROCEDURE [ccall] gtk_icon_view_new*(): PGtkIconView;
	PROCEDURE [ccall] gtk_icon_view_new_with_model*(model: PGtkTreeModel): PGtkIconView;

(* GtkLayout methods *)
	PROCEDURE [ccall] gtk_layout_get_hadjustment*(layout: PGtkLayout): PGtkAdjustment;
	PROCEDURE [ccall] gtk_layout_get_size*(layout: PGtkLayout; VAR [nil] width: lg.guint; VAR [nil] height: lg.guint);
	PROCEDURE [ccall] gtk_layout_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_layout_get_vadjustment*(layout: PGtkLayout): PGtkAdjustment;
	PROCEDURE [ccall] gtk_layout_move*(layout: PGtkLayout; child_widget: PGtkWidget; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_layout_put*(layout: PGtkLayout; child_widget: PGtkWidget; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_layout_set_hadjustment*(layout: PGtkLayout; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_layout_set_size*(layout: PGtkLayout; width: lg.guint; height: lg.guint);
	PROCEDURE [ccall] gtk_layout_set_vadjustment*(layout: PGtkLayout; adjustment: PGtkAdjustment);

(* GtkLayout constructors *)
	PROCEDURE [ccall] gtk_layout_new*(hadjustment: PGtkAdjustment; vadjustment: PGtkAdjustment): PGtkLayout;

(* GtkNotebook methods *)
	PROCEDURE [ccall] gtk_notebook_append_page*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget): lg.gint;
	PROCEDURE [ccall] gtk_notebook_append_page_menu*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget; menu_label: PGtkWidget): lg.gint;
	PROCEDURE [ccall] gtk_notebook_get_current_page*(notebook: PGtkNotebook): lg.gint;
	PROCEDURE [ccall] gtk_notebook_get_menu_label*(notebook: PGtkNotebook; child: PGtkWidget): PGtkWidget;
	PROCEDURE [ccall] gtk_notebook_get_menu_label_text*(notebook: PGtkNotebook; child: PGtkWidget): lg.Pgchar;
	PROCEDURE [ccall] gtk_notebook_get_n_pages*(notebook: PGtkNotebook): lg.gint;
	PROCEDURE [ccall] gtk_notebook_get_nth_page*(notebook: PGtkNotebook; page_num: lg.gint): PGtkWidget;
	PROCEDURE [ccall] gtk_notebook_get_scrollable*(notebook: PGtkNotebook): lg.gboolean;
	PROCEDURE [ccall] gtk_notebook_get_show_border*(notebook: PGtkNotebook): lg.gboolean;
	PROCEDURE [ccall] gtk_notebook_get_show_tabs*(notebook: PGtkNotebook): lg.gboolean;
	PROCEDURE [ccall] gtk_notebook_get_tab_label*(notebook: PGtkNotebook; child: PGtkWidget): PGtkWidget;
	PROCEDURE [ccall] gtk_notebook_get_tab_label_text*(notebook: PGtkNotebook; child: PGtkWidget): lg.Pgchar;
	PROCEDURE [ccall] gtk_notebook_get_tab_pos*(notebook: PGtkNotebook): GtkPositionType;
	PROCEDURE [ccall] gtk_notebook_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_notebook_insert_page*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget; position: lg.gint): lg.gint;
	PROCEDURE [ccall] gtk_notebook_insert_page_menu*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget; menu_label: PGtkWidget; position: lg.gint): lg.gint;
	PROCEDURE [ccall] gtk_notebook_next_page*(notebook: PGtkNotebook);
	PROCEDURE [ccall] gtk_notebook_popup_disable*(notebook: PGtkNotebook);
	PROCEDURE [ccall] gtk_notebook_popup_enable*(notebook: PGtkNotebook);
	PROCEDURE [ccall] gtk_notebook_prepend_page*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget): lg.gint;
	PROCEDURE [ccall] gtk_notebook_prepend_page_menu*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget; menu_label: PGtkWidget): lg.gint;
	PROCEDURE [ccall] gtk_notebook_prev_page*(notebook: PGtkNotebook);
	PROCEDURE [ccall] gtk_notebook_query_tab_label_packing*(notebook: PGtkNotebook; child: PGtkWidget; VAR [nil] expand: lg.gboolean; VAR [nil] fill: lg.gboolean; VAR [nil] pack_type: GtkPackType);
	PROCEDURE [ccall] gtk_notebook_remove_page*(notebook: PGtkNotebook; page_num: lg.gint);
	PROCEDURE [ccall] gtk_notebook_reorder_child*(notebook: PGtkNotebook; child: PGtkWidget; position: lg.gint);
	PROCEDURE [ccall] gtk_notebook_set_current_page*(notebook: PGtkNotebook; page_num: lg.gint);
	PROCEDURE [ccall] gtk_notebook_set_menu_label*(notebook: PGtkNotebook; child: PGtkWidget; menu_label: PGtkWidget);
	PROCEDURE [ccall] gtk_notebook_set_menu_label_text*(notebook: PGtkNotebook; child: PGtkWidget; menu_text: lg.Pgchar);
	PROCEDURE [ccall] gtk_notebook_set_scrollable*(notebook: PGtkNotebook; scrollable: lg.gboolean);
	PROCEDURE [ccall] gtk_notebook_set_show_border*(notebook: PGtkNotebook; show_border: lg.gboolean);
	PROCEDURE [ccall] gtk_notebook_set_show_tabs*(notebook: PGtkNotebook; show_tabs: lg.gboolean);
	PROCEDURE [ccall] gtk_notebook_set_tab_label*(notebook: PGtkNotebook; child: PGtkWidget; tab_label: PGtkWidget);
	PROCEDURE [ccall] gtk_notebook_set_tab_label_packing*(notebook: PGtkNotebook; child: PGtkWidget; expand: lg.gboolean; fill: lg.gboolean; pack_type: GtkPackType);
	PROCEDURE [ccall] gtk_notebook_set_tab_label_text*(notebook: PGtkNotebook; child: PGtkWidget; tab_text: lg.Pgchar);
	PROCEDURE [ccall] gtk_notebook_set_tab_pos*(notebook: PGtkNotebook; pos: GtkPositionType);

(* GtkNotebook constructors *)
	PROCEDURE [ccall] gtk_notebook_new*(): PGtkNotebook;

(* GtkSocket methods *)
	PROCEDURE [ccall] gtk_socket_add_id*(socket: PGtkSocket; window_id: gdk.GdkNativeWindow);
	PROCEDURE [ccall] gtk_socket_get_id*(socket: PGtkSocket): gdk.GdkNativeWindow;
	PROCEDURE [ccall] gtk_socket_get_type*(): lo.GType;

(* GtkSocket constructors *)
	PROCEDURE [ccall] gtk_socket_new*(): PGtkSocket;

(* GtkTable methods *)
	PROCEDURE [ccall] gtk_table_attach*(table: PGtkTable; child: PGtkWidget; left_attach: lg.guint; right_attach: lg.guint; top_attach: lg.guint; bottom_attach: lg.guint; xoptions: GtkAttachOptions; yoptions: GtkAttachOptions; xpadding: lg.guint; ypadding: lg.guint);
	PROCEDURE [ccall] gtk_table_attach_defaults*(table: PGtkTable; widget: PGtkWidget; left_attach: lg.guint; right_attach: lg.guint; top_attach: lg.guint; bottom_attach: lg.guint);
	PROCEDURE [ccall] gtk_table_get_col_spacing*(table: PGtkTable; column: lg.guint): lg.guint;
	PROCEDURE [ccall] gtk_table_get_default_col_spacing*(table: PGtkTable): lg.guint;
	PROCEDURE [ccall] gtk_table_get_default_row_spacing*(table: PGtkTable): lg.guint;
	PROCEDURE [ccall] gtk_table_get_homogeneous*(table: PGtkTable): lg.gboolean;
	PROCEDURE [ccall] gtk_table_get_row_spacing*(table: PGtkTable; row: lg.guint): lg.guint;
	PROCEDURE [ccall] gtk_table_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_table_resize*(table: PGtkTable; rows: lg.guint; columns: lg.guint);
	PROCEDURE [ccall] gtk_table_set_col_spacing*(table: PGtkTable; column: lg.guint; spacing: lg.guint);
	PROCEDURE [ccall] gtk_table_set_col_spacings*(table: PGtkTable; spacing: lg.guint);
	PROCEDURE [ccall] gtk_table_set_homogeneous*(table: PGtkTable; homogeneous: lg.gboolean);
	PROCEDURE [ccall] gtk_table_set_row_spacing*(table: PGtkTable; row: lg.guint; spacing: lg.guint);
	PROCEDURE [ccall] gtk_table_set_row_spacings*(table: PGtkTable; spacing: lg.guint);

(* GtkTable constructors *)
	PROCEDURE [ccall] gtk_table_new*(rows: lg.guint; columns: lg.guint; homogeneous: lg.gboolean): PGtkTable;

(* GtkTextView methods *)
	PROCEDURE [ccall] gtk_text_view_add_child_at_anchor*(textview: PGtkTextView; child: PGtkWidget; anchor: PGtkTextChildAnchor);
	PROCEDURE [ccall] gtk_text_view_add_child_in_window*(textview: PGtkTextView; child: PGtkWidget; which_window: GtkTextWindowType; xpos: lg.gint; ypos: lg.gint);
	PROCEDURE [ccall] gtk_text_view_backward_display_line*(textview: PGtkTextView; iter: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_backward_display_line_start*(textview: PGtkTextView; iter: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_buffer_to_window_coords*(textview: PGtkTextView; win: GtkTextWindowType; buffer_x: lg.gint; buffer_y: lg.gint; window_x: lg.Pgint; window_y: lg.Pgint);
	PROCEDURE [ccall] gtk_text_view_forward_display_line*(textview: PGtkTextView; iter: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_forward_display_line_end*(textview: PGtkTextView; iter: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_get_accepts_tab*(textview: PGtkTextView): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_get_border_window_size*(textview: PGtkTextView; type: GtkTextWindowType): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_buffer*(textview: PGtkTextView): PGtkTextBuffer;
	PROCEDURE [ccall] gtk_text_view_get_cursor_visible*(textview: PGtkTextView): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_get_default_attributes*(textview: PGtkTextView): PGtkTextAttributes;
	PROCEDURE [ccall] gtk_text_view_get_editable*(textview: PGtkTextView): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_get_indent*(textview: PGtkTextView): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_iter_at_location*(textview: PGtkTextView; iter: PGtkTextIter; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_text_view_get_iter_at_position*(textview: PGtkTextView; iter: PGtkTextIter; trailing: lg.Pgint; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_text_view_get_iter_location*(textview: PGtkTextView; iter: PGtkTextIter; location: gdk.PGdkRectangle);
	PROCEDURE [ccall] gtk_text_view_get_justification*(textview: PGtkTextView): GtkJustification;
	PROCEDURE [ccall] gtk_text_view_get_left_margin*(textview: PGtkTextView): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_line_at_y*(textview: PGtkTextView; target_iter: PGtkTextIter; y: lg.gint; line_top: lg.Pgint);
	PROCEDURE [ccall] gtk_text_view_get_line_yrange*(textview: PGtkTextView; iter: PGtkTextIter; y: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_text_view_get_overwrite*(textview: PGtkTextView): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_get_pixels_above_lines*(textview: PGtkTextView): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_pixels_below_lines*(textview: PGtkTextView): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_pixels_inside_wrap*(textview: PGtkTextView): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_right_margin*(textview: PGtkTextView): lg.gint;
	PROCEDURE [ccall] gtk_text_view_get_tabs*(textview: PGtkTextView): pan.PPangoTabArray;
	PROCEDURE [ccall] gtk_text_view_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_text_view_get_visible_rect*(textview: PGtkTextView; visible_rect: gdk.PGdkRectangle);
	PROCEDURE [ccall] gtk_text_view_get_window*(textview: PGtkTextView; win: GtkTextWindowType): gdk.PGdkWindow;
	PROCEDURE [ccall] gtk_text_view_get_window_type*(textview: PGtkTextView; window: gdk.PGdkWindow): GtkTextWindowType;
	PROCEDURE [ccall] gtk_text_view_get_wrap_mode*(textview: PGtkTextView): GtkWrapMode;
	PROCEDURE [ccall] gtk_text_view_move_child*(textview: PGtkTextView; child: PGtkWidget; xpos: lg.gint; ypos: lg.gint);
	PROCEDURE [ccall] gtk_text_view_move_mark_onscreen*(textview: PGtkTextView; mark: PGtkTextMark): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_move_visually*(textview: PGtkTextView; iter: PGtkTextIter; count: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_place_cursor_onscreen*(textview: PGtkTextView): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_scroll_mark_onscreen*(textview: PGtkTextView; mark: PGtkTextMark);
	PROCEDURE [ccall] gtk_text_view_scroll_to_iter*(textview: PGtkTextView; iter: PGtkTextIter; within_margin: lg.gdouble; use_align: lg.gboolean; xalign: lg.gdouble; yalign: lg.gdouble): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_scroll_to_mark*(textview: PGtkTextView; mark: PGtkTextMark; within_margin: lg.gdouble; use_align: lg.gboolean; xalign: lg.gdouble; yalign: lg.gdouble);
	PROCEDURE [ccall] gtk_text_view_set_accepts_tab*(textview: PGtkTextView; accepts_tab: lg.gboolean);
	PROCEDURE [ccall] gtk_text_view_set_border_window_size*(textview: PGtkTextView; type: GtkTextWindowType; size: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_buffer*(textview: PGtkTextView; buffer: PGtkTextBuffer);
	PROCEDURE [ccall] gtk_text_view_set_cursor_visible*(textview: PGtkTextView; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_text_view_set_editable*(textview: PGtkTextView; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_text_view_set_indent*(textview: PGtkTextView; indent: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_justification*(textview: PGtkTextView; justification: GtkJustification);
	PROCEDURE [ccall] gtk_text_view_set_left_margin*(textview: PGtkTextView; left_margin: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_overwrite*(textview: PGtkTextView; overwrite: lg.gboolean);
	PROCEDURE [ccall] gtk_text_view_set_pixels_above_lines*(textview: PGtkTextView; pixels_above_lines: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_pixels_below_lines*(textview: PGtkTextView; pixels_below_lines: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_pixels_inside_wrap*(textview: PGtkTextView; pixels_inside_wrap: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_right_margin*(textview: PGtkTextView; right_margin: lg.gint);
	PROCEDURE [ccall] gtk_text_view_set_tabs*(textview: PGtkTextView; tabs: pan.PPangoTabArray);
	PROCEDURE [ccall] gtk_text_view_set_wrap_mode*(textview: PGtkTextView; wrap_mode: GtkWrapMode);
	PROCEDURE [ccall] gtk_text_view_starts_display_line*(textview: PGtkTextView; iter: PGtkTextIter): lg.gboolean;
	PROCEDURE [ccall] gtk_text_view_window_to_buffer_coords*(textview: PGtkTextView; win: GtkTextWindowType; window_x: lg.gint; window_y: lg.gint; buffer_x: lg.Pgint; buffer_y: lg.Pgint);

(* GtkTextView constructors *)
	PROCEDURE [ccall] gtk_text_view_new*(): PGtkTextView;
	PROCEDURE [ccall] gtk_text_view_new_with_buffer*(buffer: PGtkTextBuffer): PGtkTextView;

(* GtkToolbar methods *)
	PROCEDURE [ccall] gtk_toolbar_get_drop_index*(toolbar: PGtkToolbar; x: lg.gint; y: lg.gint): lg.gint;
	PROCEDURE [ccall] gtk_toolbar_get_icon_size*(toolbar: PGtkToolbar): GtkIconSize;
	PROCEDURE [ccall] gtk_toolbar_get_item_index*(toolbar: PGtkToolbar; item: PGtkToolItem): lg.gint;
	PROCEDURE [ccall] gtk_toolbar_get_n_items*(toolbar: PGtkToolbar): lg.gint;
	PROCEDURE [ccall] gtk_toolbar_get_nth_item*(toolbar: PGtkToolbar; n: lg.gint): PGtkToolItem;
	PROCEDURE [ccall] gtk_toolbar_get_orientation*(toolbar: PGtkToolbar): GtkOrientation;
	PROCEDURE [ccall] gtk_toolbar_get_relief_style*(toolbar: PGtkToolbar): GtkReliefStyle;
	PROCEDURE [ccall] gtk_toolbar_get_show_arrow*(toolbar: PGtkToolbar): lg.gboolean;
	PROCEDURE [ccall] gtk_toolbar_get_style*(toolbar: PGtkToolbar): GtkToolbarStyle;
	PROCEDURE [ccall] gtk_toolbar_get_tooltips*(toolbar: PGtkToolbar): lg.gboolean;
	PROCEDURE [ccall] gtk_toolbar_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_toolbar_insert*(toolbar: PGtkToolbar; item: PGtkToolItem; pos: lg.gint);
	PROCEDURE [ccall] gtk_toolbar_set_drop_highlight_item*(toolbar: PGtkToolbar; tool_item: PGtkToolItem; index_: lg.gint);
	PROCEDURE [ccall] gtk_toolbar_set_orientation*(toolbar: PGtkToolbar; orientation: GtkOrientation);
	PROCEDURE [ccall] gtk_toolbar_set_show_arrow*(toolbar: PGtkToolbar; show_arrow: lg.gboolean);
	PROCEDURE [ccall] gtk_toolbar_set_style*(toolbar: PGtkToolbar; style: GtkToolbarStyle);
	PROCEDURE [ccall] gtk_toolbar_set_tooltips*(toolbar: PGtkToolbar; enable: lg.gboolean);
	PROCEDURE [ccall] gtk_toolbar_unset_style*(toolbar: PGtkToolbar);

(* GtkToolbar constructors *)
	PROCEDURE [ccall] gtk_toolbar_new*(): PGtkToolbar;

(* GtkTreeView methods *)
	PROCEDURE [ccall] gtk_tree_view_append_column*(treeview: PGtkTreeView; column: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_collapse_all*(treeview: PGtkTreeView);
	PROCEDURE [ccall] gtk_tree_view_collapse_row*(treeview: PGtkTreeView; path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_columns_autosize*(treeview: PGtkTreeView);
	PROCEDURE [ccall] gtk_tree_view_create_row_drag_icon*(treeview: PGtkTreeView; path: PGtkTreePath): gdk.PGdkPixmap;
	PROCEDURE [ccall] gtk_tree_view_enable_model_drag_dest*(treeview: PGtkTreeView; targets: PGtkTargetEntry; n_targets: lg.gint; actions: gdk.GdkDragAction);
	PROCEDURE [ccall] gtk_tree_view_enable_model_drag_source*(treeview: PGtkTreeView; start_button_mask: gdk.GdkModifierType; targets: PGtkTargetEntry; n_targets: lg.gint; actions: gdk.GdkDragAction);
	PROCEDURE [ccall] gtk_tree_view_expand_all*(treeview: PGtkTreeView);
	PROCEDURE [ccall] gtk_tree_view_expand_row*(treeview: PGtkTreeView; path: PGtkTreePath; open_all: lg.gboolean): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_expand_to_path*(treeview: PGtkTreeView; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_tree_view_get_background_area*(treeview: PGtkTreeView; path: PGtkTreePath; column: PGtkTreeViewColumn; rect: gdk.PGdkRectangle);
	PROCEDURE [ccall] gtk_tree_view_get_bin_window*(treeview: PGtkTreeView): gdk.PGdkWindow;
	PROCEDURE [ccall] gtk_tree_view_get_cell_area*(treeview: PGtkTreeView; path: PGtkTreePath; column: PGtkTreeViewColumn; rect: gdk.PGdkRectangle);
	PROCEDURE [ccall] gtk_tree_view_get_column*(treeview: PGtkTreeView; n: lg.gint): PGtkTreeViewColumn;
	PROCEDURE [ccall] gtk_tree_view_get_columns*(treeview: PGtkTreeView): lg.PGList;
	PROCEDURE [ccall] gtk_tree_view_get_cursor*(treeview: PGtkTreeView; VAR [nil] path: PGtkTreePath; VAR [nil] focus_column: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_get_dest_row_at_pos*(treeview: PGtkTreeView; drag_x: lg.gint; drag_y: lg.gint; VAR [nil] path: PGtkTreePath; VAR [nil] pos: GtkTreeViewDropPosition): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_drag_dest_row*(treeview: PGtkTreeView; VAR [nil] path: PGtkTreePath; VAR [nil] pos: GtkTreeViewDropPosition);
	PROCEDURE [ccall] gtk_tree_view_get_enable_search*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_expander_column*(treeview: PGtkTreeView): PGtkTreeViewColumn;
	PROCEDURE [ccall] gtk_tree_view_get_fixed_height_mode*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_hadjustment*(treeview: PGtkTreeView): PGtkAdjustment;
	PROCEDURE [ccall] gtk_tree_view_get_headers_visible*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_hover_expand*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_hover_selection*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_model*(treeview: PGtkTreeView): PGtkTreeModel;
	PROCEDURE [ccall] gtk_tree_view_get_path_at_pos*(treeview: PGtkTreeView; x: lg.gint; y: lg.gint; VAR [nil] path: PGtkTreePath; VAR [nil] column: PGtkTreeViewColumn; VAR [nil] cell_x: lg.gint; VAR [nil] cell_y: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_reorderable*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_row_separator_func*(treeview: PGtkTreeView): POINTER TO GtkTreeViewRowSeparatorFunc;
	PROCEDURE [ccall] gtk_tree_view_get_rules_hint*(treeview: PGtkTreeView): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_search_column*(treeview: PGtkTreeView): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_get_search_equal_func*(treeview: PGtkTreeView): POINTER TO GtkTreeViewSearchEqualFunc;
	PROCEDURE [ccall] gtk_tree_view_get_selection*(treeview: PGtkTreeView): PGtkTreeSelection;
	PROCEDURE [ccall] gtk_tree_view_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_view_get_vadjustment*(treeview: PGtkTreeView): PGtkAdjustment;
	PROCEDURE [ccall] gtk_tree_view_get_visible_range*(treeview: PGtkTreeView; VAR [nil] start_path: PGtkTreePath; VAR [nil] end_path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_get_visible_rect*(treeview: PGtkTreeView; visible_rect: gdk.PGdkRectangle);
	PROCEDURE [ccall] gtk_tree_view_insert_column*(treeview: PGtkTreeView; column: PGtkTreeViewColumn; position: lg.gint): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_insert_column_with_attributes*(treeview: PGtkTreeView; position: lg.gint; title: lg.Pgchar; cell: PGtkCellRenderer): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_insert_column_with_data_func*(treeview: PGtkTreeView; position: lg.gint; title: lg.Pgchar; cell: PGtkCellRenderer; func: GtkTreeCellDataFunc; data: lg.gpointer; dnotify: lo.GDestroyNotify): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_map_expanded_rows*(treeview: PGtkTreeView; func: GtkTreeViewMappingFunc; data: lg.gpointer);
	PROCEDURE [ccall] gtk_tree_view_mode_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_view_move_column_after*(treeview: PGtkTreeView; column: PGtkTreeViewColumn; base_column: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_remove_column*(treeview: PGtkTreeView; column: PGtkTreeViewColumn): lg.gint;
	PROCEDURE [ccall] gtk_tree_view_row_activated*(treeview: PGtkTreeView; path: PGtkTreePath; column: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_row_expanded*(treeview: PGtkTreeView; path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_view_scroll_to_cell*(treeview: PGtkTreeView; path: PGtkTreePath; column: PGtkTreeViewColumn; use_align: lg.gboolean; row_align: lg.gfloat; col_align: lg.gfloat);
	PROCEDURE [ccall] gtk_tree_view_scroll_to_point*(treeview: PGtkTreeView; tree_x: lg.gint; tree_y: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_set_column_drag_function*(treeview: PGtkTreeView; func: GtkTreeViewColumnDropFunc; user_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_view_set_cursor*(treeview: PGtkTreeView; path: PGtkTreePath; focus_column: PGtkTreeViewColumn; start_editing: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_cursor_on_cell*(treeview: PGtkTreeView; path: PGtkTreePath; focus_column: PGtkTreeViewColumn; focus_cell: PGtkCellRenderer; start_editing: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_destroy_count_func*(treeview: PGtkTreeView; func: GtkTreeDestroyCountFunc; data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_view_set_drag_dest_row*(treeview: PGtkTreeView; path: PGtkTreePath; pos: GtkTreeViewDropPosition);
	PROCEDURE [ccall] gtk_tree_view_set_enable_search*(treeview: PGtkTreeView; enable_search: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_expander_column*(treeview: PGtkTreeView; column: PGtkTreeViewColumn);
	PROCEDURE [ccall] gtk_tree_view_set_fixed_height_mode*(treeview: PGtkTreeView; enable: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_hadjustment*(treeview: PGtkTreeView; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_tree_view_set_headers_clickable*(treeview: PGtkTreeView; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_headers_visible*(treeview: PGtkTreeView; headers_visible: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_hover_expand*(treeview: PGtkTreeView; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_hover_selection*(treeview: PGtkTreeView; hover: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_model*(treeview: PGtkTreeView; model: PGtkTreeModel);
	PROCEDURE [ccall] gtk_tree_view_set_reorderable*(treeview: PGtkTreeView; reorderable: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_row_separator_func*(treeview: PGtkTreeView; func: GtkTreeViewRowSeparatorFunc; data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_view_set_rules_hint*(treeview: PGtkTreeView; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_tree_view_set_search_column*(treeview: PGtkTreeView; column: lg.gint);
	PROCEDURE [ccall] gtk_tree_view_set_search_equal_func*(treeview: PGtkTreeView; search_equal_func: GtkTreeViewSearchEqualFunc; search_user_data: lg.gpointer; search_destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_view_set_vadjustment*(treeview: PGtkTreeView; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_tree_view_tree_to_widget_coords*(treeview: PGtkTreeView; tx: lg.gint; ty: lg.gint; wx: lg.Pgint; wy: lg.Pgint);
	PROCEDURE [ccall] gtk_tree_view_unset_rows_drag_dest*(treeview: PGtkTreeView);
	PROCEDURE [ccall] gtk_tree_view_unset_rows_drag_source*(treeview: PGtkTreeView);
	PROCEDURE [ccall] gtk_tree_view_widget_to_tree_coords*(treeview: PGtkTreeView; wx: lg.gint; wy: lg.gint; tx: lg.Pgint; ty: lg.Pgint);

(* GtkTreeView constructors *)
	PROCEDURE [ccall] gtk_tree_view_new*(): PGtkTreeView;
	PROCEDURE [ccall] gtk_tree_view_new_with_model*(model: PGtkTreeModel): PGtkTreeView;

(* GtkButton methods *)
	PROCEDURE [ccall] gtk_button_action_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_button_clicked*(button: PGtkButton);
	PROCEDURE [ccall] gtk_button_enter*(button: PGtkButton);
	PROCEDURE [ccall] gtk_button_get_alignment*(button: PGtkButton; VAR [nil] xalign: lg.gfloat; VAR [nil] yalign: lg.gfloat);
	PROCEDURE [ccall] gtk_button_get_focus_on_click*(button: PGtkButton): lg.gboolean;
	PROCEDURE [ccall] gtk_button_get_image*(button: PGtkButton): PGtkWidget;
	PROCEDURE [ccall] gtk_button_get_label*(button: PGtkButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_button_get_relief*(button: PGtkButton): GtkReliefStyle;
	PROCEDURE [ccall] gtk_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_button_get_use_stock*(button: PGtkButton): lg.gboolean;
	PROCEDURE [ccall] gtk_button_get_use_underline*(button: PGtkButton): lg.gboolean;
	PROCEDURE [ccall] gtk_button_leave*(button: PGtkButton);
	PROCEDURE [ccall] gtk_button_pressed*(button: PGtkButton);
	PROCEDURE [ccall] gtk_button_released*(button: PGtkButton);
	PROCEDURE [ccall] gtk_button_set_alignment*(button: PGtkButton; xalign: lg.gfloat; yalign: lg.gfloat);
	PROCEDURE [ccall] gtk_button_set_focus_on_click*(button: PGtkButton; focus_on_click: lg.gboolean);
	PROCEDURE [ccall] gtk_button_set_image*(button: PGtkButton; image: PGtkWidget);
	PROCEDURE [ccall] gtk_button_set_label*(button: PGtkButton; label: lg.Pgchar);
	PROCEDURE [ccall] gtk_button_set_relief*(button: PGtkButton; newstyle: GtkReliefStyle);
	PROCEDURE [ccall] gtk_button_set_use_stock*(button: PGtkButton; use_stock: lg.gboolean);
	PROCEDURE [ccall] gtk_button_set_use_underline*(button: PGtkButton; use_underline: lg.gboolean);

(* GtkButton constructors *)
	PROCEDURE [ccall] gtk_button_new*(): PGtkButton;
	PROCEDURE [ccall] gtk_button_new_from_stock*(stock_id: lg.Pgchar): PGtkButton;
	PROCEDURE [ccall] gtk_button_new_with_label*(label: lg.Pgchar): PGtkButton;
	PROCEDURE [ccall] gtk_button_new_with_mnemonic*(label: lg.Pgchar): PGtkButton;

(* GtkToolItem methods *)
	PROCEDURE [ccall] gtk_tool_item_get_expand*(toolitem: PGtkToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_item_get_homogeneous*(toolitem: PGtkToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_item_get_icon_size*(toolitem: PGtkToolItem): GtkIconSize;
	PROCEDURE [ccall] gtk_tool_item_get_is_important*(toolitem: PGtkToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_item_get_orientation*(toolitem: PGtkToolItem): GtkOrientation;
	PROCEDURE [ccall] gtk_tool_item_get_proxy_menu_item*(toolitem: PGtkToolItem; menu_item_id: lg.Pgchar): PGtkWidget;
	PROCEDURE [ccall] gtk_tool_item_get_relief_style*(toolitem: PGtkToolItem): GtkReliefStyle;
	PROCEDURE [ccall] gtk_tool_item_get_toolbar_style*(toolitem: PGtkToolItem): GtkToolbarStyle;
	PROCEDURE [ccall] gtk_tool_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tool_item_get_use_drag_window*(toolitem: PGtkToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_item_get_visible_horizontal*(toolitem: PGtkToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_item_get_visible_vertical*(toolitem: PGtkToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_item_rebuild_menu*(toolitem: PGtkToolItem);
	PROCEDURE [ccall] gtk_tool_item_retrieve_proxy_menu_item*(toolitem: PGtkToolItem): PGtkWidget;
	PROCEDURE [ccall] gtk_tool_item_set_expand*(toolitem: PGtkToolItem; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_tool_item_set_homogeneous*(toolitem: PGtkToolItem; homogeneous: lg.gboolean);
	PROCEDURE [ccall] gtk_tool_item_set_is_important*(toolitem: PGtkToolItem; is_important: lg.gboolean);
	PROCEDURE [ccall] gtk_tool_item_set_proxy_menu_item*(toolitem: PGtkToolItem; menu_item_id: lg.Pgchar; menu_item: PGtkWidget);
	PROCEDURE [ccall] gtk_tool_item_set_tooltip*(toolitem: PGtkToolItem; tooltips: PGtkTooltips; tip_text: lg.Pgchar; tip_private: lg.Pgchar);
	PROCEDURE [ccall] gtk_tool_item_set_use_drag_window*(toolitem: PGtkToolItem; use_drag_window: lg.gboolean);
	PROCEDURE [ccall] gtk_tool_item_set_visible_horizontal*(toolitem: PGtkToolItem; visible_horizontal: lg.gboolean);
	PROCEDURE [ccall] gtk_tool_item_set_visible_vertical*(toolitem: PGtkToolItem; visible_vertical: lg.gboolean);

(* GtkToolItem constructors *)
	PROCEDURE [ccall] gtk_tool_item_new*(): PGtkToolItem;

(* GtkWindow methods *)
	PROCEDURE [ccall] gtk_window_activate_default*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_activate_focus*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_activate_key*(window: PGtkWindow; event: gdk.PGdkEventKey): lg.gboolean;
	PROCEDURE [ccall] gtk_window_add_accel_group*(window: PGtkWindow; accel_group: PGtkAccelGroup);
	PROCEDURE [ccall] gtk_window_add_embedded_xid*(window: PGtkWindow; xid: lg.guint);
	PROCEDURE [ccall] gtk_window_add_mnemonic*(window: PGtkWindow; keyval: lg.guint; target: PGtkWidget);
	PROCEDURE [ccall] gtk_window_begin_move_drag*(window: PGtkWindow; button: lg.gint; root_x: lg.gint; root_y: lg.gint; timestamp: lg.guint32);
	PROCEDURE [ccall] gtk_window_begin_resize_drag*(window: PGtkWindow; edge: gdk.GdkWindowEdge; button: lg.gint; root_x: lg.gint; root_y: lg.gint; timestamp: lg.guint32);
	PROCEDURE [ccall] gtk_window_deiconify*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_fullscreen*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_get_accept_focus*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_decorated*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_default_icon_list*(): lg.PGList;
	PROCEDURE [ccall] gtk_window_get_default_size*(window: PGtkWindow; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_window_get_destroy_with_parent*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_focus*(window: PGtkWindow): PGtkWidget;
	PROCEDURE [ccall] gtk_window_get_focus_on_map*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_frame_dimensions*(window: PGtkWindow; left: lg.Pgint; top: lg.Pgint; right: lg.Pgint; bottom: lg.Pgint);
	PROCEDURE [ccall] gtk_window_get_gravity*(window: PGtkWindow): gdk.GdkGravity;
	PROCEDURE [ccall] gtk_window_get_has_frame*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_icon*(window: PGtkWindow): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_window_get_icon_list*(window: PGtkWindow): lg.PGList;
	PROCEDURE [ccall] gtk_window_get_icon_name*(window: PGtkWindow): lg.Pgchar;
	PROCEDURE [ccall] gtk_window_get_mnemonic_modifier*(window: PGtkWindow): gdk.GdkModifierType;
	PROCEDURE [ccall] gtk_window_get_modal*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_position*(window: PGtkWindow; root_x: lg.Pgint; root_y: lg.Pgint);
	PROCEDURE [ccall] gtk_window_get_resizable*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_role*(window: PGtkWindow): lg.Pgchar;
	PROCEDURE [ccall] gtk_window_get_screen*(window: PGtkWindow): gdk.PGdkScreen;
	PROCEDURE [ccall] gtk_window_get_size*(window: PGtkWindow; width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] gtk_window_get_skip_pager_hint*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_skip_taskbar_hint*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_get_title*(window: PGtkWindow): lg.Pgchar;
	PROCEDURE [ccall] gtk_window_get_transient_for*(window: PGtkWindow): PGtkWindow;
	PROCEDURE [ccall] gtk_window_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_window_get_type_hint*(window: PGtkWindow): gdk.GdkWindowTypeHint;
	PROCEDURE [ccall] gtk_window_get_urgency_hint*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_has_toplevel_focus*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_iconify*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_is_active*(window: PGtkWindow): lg.gboolean;
	PROCEDURE [ccall] gtk_window_list_toplevels*(): lg.PGList;
	PROCEDURE [ccall] gtk_window_maximize*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_mnemonic_activate*(window: PGtkWindow; keyval: lg.guint; modifier: gdk.GdkModifierType): lg.gboolean;
	PROCEDURE [ccall] gtk_window_move*(window: PGtkWindow; x: lg.gint; y: lg.gint);
	PROCEDURE [ccall] gtk_window_parse_geometry*(window: PGtkWindow; geometry: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_window_present*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_present_with_time*(window: PGtkWindow; timestamp: lg.guint32);
	PROCEDURE [ccall] gtk_window_propagate_key_event*(window: PGtkWindow; event: gdk.PGdkEventKey): lg.gboolean;
	PROCEDURE [ccall] gtk_window_remove_accel_group*(window: PGtkWindow; accel_group: PGtkAccelGroup);
	PROCEDURE [ccall] gtk_window_remove_embedded_xid*(window: PGtkWindow; xid: lg.guint);
	PROCEDURE [ccall] gtk_window_remove_mnemonic*(window: PGtkWindow; keyval: lg.guint; target: PGtkWidget);
	PROCEDURE [ccall] gtk_window_reshow_with_initial_size*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_resize*(window: PGtkWindow; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_window_set_accept_focus*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_auto_startup_notification*(setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_decorated*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_default*(window: PGtkWindow; default_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_window_set_default_icon*(icon: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_window_set_default_icon_from_file*(filename: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
	PROCEDURE [ccall] gtk_window_set_default_icon_list*(list: lg.PGList);
	PROCEDURE [ccall] gtk_window_set_default_icon_name*(name: lg.Pgchar);
	PROCEDURE [ccall] gtk_window_set_default_size*(window: PGtkWindow; width: lg.gint; height: lg.gint);
	PROCEDURE [ccall] gtk_window_set_destroy_with_parent*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_focus*(window: PGtkWindow; focus: PGtkWidget);
	PROCEDURE [ccall] gtk_window_set_focus_on_map*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_frame_dimensions*(window: PGtkWindow; left: lg.gint; top: lg.gint; right: lg.gint; bottom: lg.gint);
	PROCEDURE [ccall] gtk_window_set_geometry_hints*(window: PGtkWindow; geometry_widget: PGtkWidget; geometry: gdk.PGdkGeometry; geom_mask: gdk.GdkWindowHints);
	PROCEDURE [ccall] gtk_window_set_gravity*(window: PGtkWindow; gravity: gdk.GdkGravity);
	PROCEDURE [ccall] gtk_window_set_has_frame*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_icon*(window: PGtkWindow; icon: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_window_set_icon_from_file*(window: PGtkWindow; filename: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
	PROCEDURE [ccall] gtk_window_set_icon_list*(window: PGtkWindow; list: lg.PGList);
	PROCEDURE [ccall] gtk_window_set_icon_name*(window: PGtkWindow; name: lg.Pgchar);
	PROCEDURE [ccall] gtk_window_set_keep_above*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_keep_below*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_mnemonic_modifier*(window: PGtkWindow; modifier: gdk.GdkModifierType);
	PROCEDURE [ccall] gtk_window_set_modal*(window: PGtkWindow; modal: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_position*(window: PGtkWindow; position: GtkWindowPosition);
	PROCEDURE [ccall] gtk_window_set_resizable*(window: PGtkWindow; resizable: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_role*(window: PGtkWindow; role: lg.Pgchar);
	PROCEDURE [ccall] gtk_window_set_screen*(window: PGtkWindow; screen: gdk.PGdkScreen);
	PROCEDURE [ccall] gtk_window_set_skip_pager_hint*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_skip_taskbar_hint*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_title*(window: PGtkWindow; title: lg.Pgchar);
	PROCEDURE [ccall] gtk_window_set_transient_for*(window: PGtkWindow; parent: PGtkWindow);
	PROCEDURE [ccall] gtk_window_set_type_hint*(window: PGtkWindow; hint: gdk.GdkWindowTypeHint);
	PROCEDURE [ccall] gtk_window_set_urgency_hint*(window: PGtkWindow; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_window_set_wmclass*(window: PGtkWindow; wmclass_name: lg.Pgchar; wmclass_class: lg.Pgchar);
	PROCEDURE [ccall] gtk_window_stick*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_unfullscreen*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_unmaximize*(window: PGtkWindow);
	PROCEDURE [ccall] gtk_window_unstick*(window: PGtkWindow);

(* GtkWindow constructors *)
	PROCEDURE [ccall] gtk_window_new*(type: GtkWindowType): PGtkWindow;

(* GtkComboBox methods *)
	PROCEDURE [ccall] gtk_combo_box_append_text*(combobox: PGtkComboBox; text: lg.Pgchar);
	PROCEDURE [ccall] gtk_combo_box_get_active*(combobox: PGtkComboBox): lg.gint;
	PROCEDURE [ccall] gtk_combo_box_get_active_iter*(combobox: PGtkComboBox; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_combo_box_get_active_text*(combobox: PGtkComboBox): lg.Pgchar;
	PROCEDURE [ccall] gtk_combo_box_get_add_tearoffs*(combobox: PGtkComboBox): lg.gboolean;
	PROCEDURE [ccall] gtk_combo_box_get_column_span_column*(combobox: PGtkComboBox): lg.gint;
	PROCEDURE [ccall] gtk_combo_box_get_focus_on_click*(combobox: PGtkComboBox): lg.gboolean;
	PROCEDURE [ccall] gtk_combo_box_get_model*(combobox: PGtkComboBox): PGtkTreeModel;
	PROCEDURE [ccall] gtk_combo_box_get_popup_accessible*(combobox: PGtkComboBox): atk.PAtkObject;
	PROCEDURE [ccall] gtk_combo_box_get_row_separator_func*(combobox: PGtkComboBox): POINTER TO GtkTreeViewRowSeparatorFunc;
	PROCEDURE [ccall] gtk_combo_box_get_row_span_column*(combobox: PGtkComboBox): lg.gint;
	PROCEDURE [ccall] gtk_combo_box_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_combo_box_get_wrap_width*(combobox: PGtkComboBox): lg.gint;
	PROCEDURE [ccall] gtk_combo_box_insert_text*(combobox: PGtkComboBox; position: lg.gint; text: lg.Pgchar);
	PROCEDURE [ccall] gtk_combo_box_popdown*(combobox: PGtkComboBox);
	PROCEDURE [ccall] gtk_combo_box_popup*(combobox: PGtkComboBox);
	PROCEDURE [ccall] gtk_combo_box_prepend_text*(combobox: PGtkComboBox; text: lg.Pgchar);
	PROCEDURE [ccall] gtk_combo_box_remove_text*(combobox: PGtkComboBox; position: lg.gint);
	PROCEDURE [ccall] gtk_combo_box_set_active*(combobox: PGtkComboBox; index_: lg.gint);
	PROCEDURE [ccall] gtk_combo_box_set_active_iter*(combobox: PGtkComboBox; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_combo_box_set_add_tearoffs*(combobox: PGtkComboBox; add_tearoffs: lg.gboolean);
	PROCEDURE [ccall] gtk_combo_box_set_column_span_column*(combobox: PGtkComboBox; column_span: lg.gint);
	PROCEDURE [ccall] gtk_combo_box_set_focus_on_click*(combobox: PGtkComboBox; focus_on_click: lg.gboolean);
	PROCEDURE [ccall] gtk_combo_box_set_model*(combobox: PGtkComboBox; model: PGtkTreeModel);
	PROCEDURE [ccall] gtk_combo_box_set_row_separator_func*(combobox: PGtkComboBox; func: GtkTreeViewRowSeparatorFunc; data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_combo_box_set_row_span_column*(combobox: PGtkComboBox; row_span: lg.gint);
	PROCEDURE [ccall] gtk_combo_box_set_wrap_width*(combobox: PGtkComboBox; width: lg.gint);

(* GtkComboBox constructors *)
	PROCEDURE [ccall] gtk_combo_box_new*(): PGtkComboBox;
	PROCEDURE [ccall] gtk_combo_box_new_text*(): PGtkComboBox;
	PROCEDURE [ccall] gtk_combo_box_new_with_model*(model: PGtkTreeModel): PGtkComboBox;

(* GtkFrame methods *)
	PROCEDURE [ccall] gtk_frame_get_label*(frame: PGtkFrame): lg.Pgchar;
	PROCEDURE [ccall] gtk_frame_get_label_align*(frame: PGtkFrame; VAR [nil] xalign: lg.gfloat; VAR [nil] yalign: lg.gfloat);
	PROCEDURE [ccall] gtk_frame_get_label_widget*(frame: PGtkFrame): PGtkWidget;
	PROCEDURE [ccall] gtk_frame_get_shadow_type*(frame: PGtkFrame): GtkShadowType;
	PROCEDURE [ccall] gtk_frame_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_frame_set_label*(frame: PGtkFrame; label: lg.Pgchar);
	PROCEDURE [ccall] gtk_frame_set_label_align*(frame: PGtkFrame; xalign: lg.gfloat; yalign: lg.gfloat);
	PROCEDURE [ccall] gtk_frame_set_label_widget*(frame: PGtkFrame; label_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_frame_set_shadow_type*(frame: PGtkFrame; type: GtkShadowType);

(* GtkFrame constructors *)
	PROCEDURE [ccall] gtk_frame_new*(label: lg.Pgchar): PGtkFrame;

(* GtkItem methods *)
	PROCEDURE [ccall] gtk_item_deselect*(item: PGtkItem);
	PROCEDURE [ccall] gtk_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_item_select*(item: PGtkItem);
	PROCEDURE [ccall] gtk_item_toggle*(item: PGtkItem);

(* GtkAlignment methods *)
	PROCEDURE [ccall] gtk_alignment_get_padding*(alignment: PGtkAlignment; VAR [nil] padding_top: lg.guint; VAR [nil] padding_bottom: lg.guint; VAR [nil] padding_left: lg.guint; VAR [nil] padding_right: lg.guint);
	PROCEDURE [ccall] gtk_alignment_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_alignment_set*(alignment: PGtkAlignment; xalign: lg.gfloat; yalign: lg.gfloat; xscale: lg.gfloat; yscale: lg.gfloat);
	PROCEDURE [ccall] gtk_alignment_set_padding*(alignment: PGtkAlignment; padding_top: lg.guint; padding_bottom: lg.guint; padding_left: lg.guint; padding_right: lg.guint);

(* GtkAlignment constructors *)
	PROCEDURE [ccall] gtk_alignment_new*(xalign: lg.gfloat; yalign: lg.gfloat; xscale: lg.gfloat; yscale: lg.gfloat): PGtkAlignment;

(* GtkEventBox methods *)
	PROCEDURE [ccall] gtk_event_box_get_above_child*(eventbox: PGtkEventBox): lg.gboolean;
	PROCEDURE [ccall] gtk_event_box_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_event_box_get_visible_window*(eventbox: PGtkEventBox): lg.gboolean;
	PROCEDURE [ccall] gtk_event_box_set_above_child*(eventbox: PGtkEventBox; above_child: lg.gboolean);
	PROCEDURE [ccall] gtk_event_box_set_visible_window*(eventbox: PGtkEventBox; visible_window: lg.gboolean);

(* GtkEventBox constructors *)
	PROCEDURE [ccall] gtk_event_box_new*(): PGtkEventBox;

(* GtkExpander methods *)
	PROCEDURE [ccall] gtk_expander_get_expanded*(expander: PGtkExpander): lg.gboolean;
	PROCEDURE [ccall] gtk_expander_get_label*(expander: PGtkExpander): lg.Pgchar;
	PROCEDURE [ccall] gtk_expander_get_label_widget*(expander: PGtkExpander): PGtkWidget;
	PROCEDURE [ccall] gtk_expander_get_spacing*(expander: PGtkExpander): lg.gint;
	PROCEDURE [ccall] gtk_expander_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_expander_get_use_markup*(expander: PGtkExpander): lg.gboolean;
	PROCEDURE [ccall] gtk_expander_get_use_underline*(expander: PGtkExpander): lg.gboolean;
	PROCEDURE [ccall] gtk_expander_set_expanded*(expander: PGtkExpander; expanded: lg.gboolean);
	PROCEDURE [ccall] gtk_expander_set_label*(expander: PGtkExpander; label: lg.Pgchar);
	PROCEDURE [ccall] gtk_expander_set_label_widget*(expander: PGtkExpander; label_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_expander_set_spacing*(expander: PGtkExpander; spacing: lg.gint);
	PROCEDURE [ccall] gtk_expander_set_use_markup*(expander: PGtkExpander; use_markup: lg.gboolean);
	PROCEDURE [ccall] gtk_expander_set_use_underline*(expander: PGtkExpander; use_underline: lg.gboolean);

(* GtkExpander constructors *)
	PROCEDURE [ccall] gtk_expander_new*(label: lg.Pgchar): PGtkExpander;
	PROCEDURE [ccall] gtk_expander_new_with_mnemonic*(label: lg.Pgchar): PGtkExpander;

(* GtkHandleBox methods *)
	PROCEDURE [ccall] gtk_handle_box_get_handle_position*(handlebox: PGtkHandleBox): GtkPositionType;
	PROCEDURE [ccall] gtk_handle_box_get_shadow_type*(handlebox: PGtkHandleBox): GtkShadowType;
	PROCEDURE [ccall] gtk_handle_box_get_snap_edge*(handlebox: PGtkHandleBox): GtkPositionType;
	PROCEDURE [ccall] gtk_handle_box_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_handle_box_set_handle_position*(handlebox: PGtkHandleBox; position: GtkPositionType);
	PROCEDURE [ccall] gtk_handle_box_set_shadow_type*(handlebox: PGtkHandleBox; type: GtkShadowType);
	PROCEDURE [ccall] gtk_handle_box_set_snap_edge*(handlebox: PGtkHandleBox; edge: GtkPositionType);

(* GtkHandleBox constructors *)
	PROCEDURE [ccall] gtk_handle_box_new*(): PGtkHandleBox;

(* GtkScrolledWindow methods *)
	PROCEDURE [ccall] gtk_scrolled_window_add_with_viewport*(scrolledwindow: PGtkScrolledWindow; child: PGtkWidget);
	PROCEDURE [ccall] gtk_scrolled_window_get_hadjustment*(scrolledwindow: PGtkScrolledWindow): PGtkAdjustment;
	PROCEDURE [ccall] gtk_scrolled_window_get_hscrollbar*(scrolledwindow: PGtkScrolledWindow): PGtkWidget;
	PROCEDURE [ccall] gtk_scrolled_window_get_placement*(scrolledwindow: PGtkScrolledWindow): GtkCornerType;
	PROCEDURE [ccall] gtk_scrolled_window_get_policy*(scrolledwindow: PGtkScrolledWindow; VAR [nil] hscrollbar_policy: GtkPolicyType; VAR [nil] vscrollbar_policy: GtkPolicyType);
	PROCEDURE [ccall] gtk_scrolled_window_get_shadow_type*(scrolledwindow: PGtkScrolledWindow): GtkShadowType;
	PROCEDURE [ccall] gtk_scrolled_window_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_scrolled_window_get_vadjustment*(scrolledwindow: PGtkScrolledWindow): PGtkAdjustment;
	PROCEDURE [ccall] gtk_scrolled_window_get_vscrollbar*(scrolledwindow: PGtkScrolledWindow): PGtkWidget;
	PROCEDURE [ccall] gtk_scrolled_window_set_hadjustment*(scrolledwindow: PGtkScrolledWindow; hadjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_scrolled_window_set_placement*(scrolledwindow: PGtkScrolledWindow; window_placement: GtkCornerType);
	PROCEDURE [ccall] gtk_scrolled_window_set_policy*(scrolledwindow: PGtkScrolledWindow; hscrollbar_policy: GtkPolicyType; vscrollbar_policy: GtkPolicyType);
	PROCEDURE [ccall] gtk_scrolled_window_set_shadow_type*(scrolledwindow: PGtkScrolledWindow; type: GtkShadowType);
	PROCEDURE [ccall] gtk_scrolled_window_set_vadjustment*(scrolledwindow: PGtkScrolledWindow; vadjustment: PGtkAdjustment);

(* GtkScrolledWindow constructors *)
	PROCEDURE [ccall] gtk_scrolled_window_new*(hadjustment: PGtkAdjustment; vadjustment: PGtkAdjustment): PGtkScrolledWindow;

(* GtkViewport methods *)
	PROCEDURE [ccall] gtk_viewport_get_hadjustment*(viewport: PGtkViewport): PGtkAdjustment;
	PROCEDURE [ccall] gtk_viewport_get_shadow_type*(viewport: PGtkViewport): GtkShadowType;
	PROCEDURE [ccall] gtk_viewport_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_viewport_get_vadjustment*(viewport: PGtkViewport): PGtkAdjustment;
	PROCEDURE [ccall] gtk_viewport_set_hadjustment*(viewport: PGtkViewport; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_viewport_set_shadow_type*(viewport: PGtkViewport; type: GtkShadowType);
	PROCEDURE [ccall] gtk_viewport_set_vadjustment*(viewport: PGtkViewport; adjustment: PGtkAdjustment);

(* GtkViewport constructors *)
	PROCEDURE [ccall] gtk_viewport_new*(hadjustment: PGtkAdjustment; vadjustment: PGtkAdjustment): PGtkViewport;

(* GtkToggleButton methods *)
	PROCEDURE [ccall] gtk_toggle_button_get_active*(togglebutton: PGtkToggleButton): lg.gboolean;
	PROCEDURE [ccall] gtk_toggle_button_get_inconsistent*(togglebutton: PGtkToggleButton): lg.gboolean;
	PROCEDURE [ccall] gtk_toggle_button_get_mode*(togglebutton: PGtkToggleButton): lg.gboolean;
	PROCEDURE [ccall] gtk_toggle_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_toggle_button_set_active*(togglebutton: PGtkToggleButton; is_active: lg.gboolean);
	PROCEDURE [ccall] gtk_toggle_button_set_inconsistent*(togglebutton: PGtkToggleButton; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_toggle_button_set_mode*(togglebutton: PGtkToggleButton; draw_indicator: lg.gboolean);
	PROCEDURE [ccall] gtk_toggle_button_toggled*(togglebutton: PGtkToggleButton);

(* GtkToggleButton constructors *)
	PROCEDURE [ccall] gtk_toggle_button_new*(): PGtkToggleButton;
	PROCEDURE [ccall] gtk_toggle_button_new_with_label*(label: lg.Pgchar): PGtkToggleButton;
	PROCEDURE [ccall] gtk_toggle_button_new_with_mnemonic*(label: lg.Pgchar): PGtkToggleButton;

(* GtkColorButton methods *)
	PROCEDURE [ccall] gtk_color_button_get_alpha*(colorbutton: PGtkColorButton): lg.guint16;
	PROCEDURE [ccall] gtk_color_button_get_color*(colorbutton: PGtkColorButton; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_color_button_get_title*(colorbutton: PGtkColorButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_color_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_color_button_get_use_alpha*(colorbutton: PGtkColorButton): lg.gboolean;
	PROCEDURE [ccall] gtk_color_button_set_alpha*(colorbutton: PGtkColorButton; alpha: lg.guint16);
	PROCEDURE [ccall] gtk_color_button_set_color*(colorbutton: PGtkColorButton; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_color_button_set_title*(colorbutton: PGtkColorButton; title: lg.Pgchar);
	PROCEDURE [ccall] gtk_color_button_set_use_alpha*(colorbutton: PGtkColorButton; use_alpha: lg.gboolean);

(* GtkColorButton constructors *)
	PROCEDURE [ccall] gtk_color_button_new*(): PGtkColorButton;
	PROCEDURE [ccall] gtk_color_button_new_with_color*(color: gdk.PGdkColor): PGtkColorButton;

(* GtkFontButton methods *)
	PROCEDURE [ccall] gtk_font_button_get_font_name*(fontbutton: PGtkFontButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_font_button_get_show_size*(fontbutton: PGtkFontButton): lg.gboolean;
	PROCEDURE [ccall] gtk_font_button_get_show_style*(fontbutton: PGtkFontButton): lg.gboolean;
	PROCEDURE [ccall] gtk_font_button_get_title*(fontbutton: PGtkFontButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_font_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_font_button_get_use_font*(fontbutton: PGtkFontButton): lg.gboolean;
	PROCEDURE [ccall] gtk_font_button_get_use_size*(fontbutton: PGtkFontButton): lg.gboolean;
	PROCEDURE [ccall] gtk_font_button_set_font_name*(fontbutton: PGtkFontButton; fontname: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_font_button_set_show_size*(fontbutton: PGtkFontButton; show_size: lg.gboolean);
	PROCEDURE [ccall] gtk_font_button_set_show_style*(fontbutton: PGtkFontButton; show_style: lg.gboolean);
	PROCEDURE [ccall] gtk_font_button_set_title*(fontbutton: PGtkFontButton; title: lg.Pgchar);
	PROCEDURE [ccall] gtk_font_button_set_use_font*(fontbutton: PGtkFontButton; use_font: lg.gboolean);
	PROCEDURE [ccall] gtk_font_button_set_use_size*(fontbutton: PGtkFontButton; use_size: lg.gboolean);

(* GtkFontButton constructors *)
	PROCEDURE [ccall] gtk_font_button_new*(): PGtkFontButton;
	PROCEDURE [ccall] gtk_font_button_new_with_font*(fontname: lg.Pgchar): PGtkFontButton;

(* GtkOptionMenu methods *)

(* GtkOptionMenu constructors *)

(* GtkCheckButton methods *)
	PROCEDURE [ccall] gtk_check_button_get_type*(): lo.GType;

(* GtkCheckButton constructors *)
	PROCEDURE [ccall] gtk_check_button_new*(): PGtkCheckButton;
	PROCEDURE [ccall] gtk_check_button_new_with_label*(label: lg.Pgchar): PGtkCheckButton;
	PROCEDURE [ccall] gtk_check_button_new_with_mnemonic*(label: lg.Pgchar): PGtkCheckButton;

(* GtkRadioButton methods *)
	PROCEDURE [ccall] gtk_radio_button_get_group*(radiobutton: PGtkRadioButton): lg.PGSList;
	PROCEDURE [ccall] gtk_radio_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_radio_button_set_group*(radiobutton: PGtkRadioButton; group: lg.PGSList);

(* GtkRadioButton constructors *)
	PROCEDURE [ccall] gtk_radio_button_new*(group: lg.PGSList): PGtkRadioButton;
	PROCEDURE [ccall] gtk_radio_button_new_from_widget*(group: PGtkRadioButton): PGtkRadioButton;
	PROCEDURE [ccall] gtk_radio_button_new_with_label*(group: lg.PGSList; label: lg.Pgchar): PGtkRadioButton;
	PROCEDURE [ccall] gtk_radio_button_new_with_label_from_widget*(group: PGtkRadioButton; label: lg.Pgchar): PGtkRadioButton;
	PROCEDURE [ccall] gtk_radio_button_new_with_mnemonic*(group: lg.PGSList; label: lg.Pgchar): PGtkRadioButton;
	PROCEDURE [ccall] gtk_radio_button_new_with_mnemonic_from_widget*(group: PGtkRadioButton; label: lg.Pgchar): PGtkRadioButton;

(* GtkToolButton methods *)
	PROCEDURE [ccall] gtk_tool_button_get_icon_name*(toolbutton: PGtkToolButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_tool_button_get_icon_widget*(toolbutton: PGtkToolButton): PGtkWidget;
	PROCEDURE [ccall] gtk_tool_button_get_label*(toolbutton: PGtkToolButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_tool_button_get_label_widget*(toolbutton: PGtkToolButton): PGtkWidget;
	PROCEDURE [ccall] gtk_tool_button_get_stock_id*(toolbutton: PGtkToolButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_tool_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tool_button_get_use_underline*(toolbutton: PGtkToolButton): lg.gboolean;
	PROCEDURE [ccall] gtk_tool_button_set_icon_name*(toolbutton: PGtkToolButton; icon_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_tool_button_set_icon_widget*(toolbutton: PGtkToolButton; icon_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_tool_button_set_label*(toolbutton: PGtkToolButton; label: lg.Pgchar);
	PROCEDURE [ccall] gtk_tool_button_set_label_widget*(toolbutton: PGtkToolButton; label_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_tool_button_set_stock_id*(toolbutton: PGtkToolButton; stock_id: lg.Pgchar);
	PROCEDURE [ccall] gtk_tool_button_set_use_underline*(toolbutton: PGtkToolButton; use_underline: lg.gboolean);

(* GtkToolButton constructors *)
	PROCEDURE [ccall] gtk_tool_button_new*(icon_widget: PGtkWidget; label: lg.Pgchar): PGtkToolButton;
	PROCEDURE [ccall] gtk_tool_button_new_from_stock*(stock_id: lg.Pgchar): PGtkToolButton;

(* GtkSeparatorToolItem methods *)
	PROCEDURE [ccall] gtk_separator_tool_item_get_draw*(separatortoolitem: PGtkSeparatorToolItem): lg.gboolean;
	PROCEDURE [ccall] gtk_separator_tool_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_separator_tool_item_set_draw*(separatortoolitem: PGtkSeparatorToolItem; draw: lg.gboolean);

(* GtkSeparatorToolItem constructors *)
	PROCEDURE [ccall] gtk_separator_tool_item_new*(): PGtkSeparatorToolItem;

(* GtkToggleToolButton methods *)
	PROCEDURE [ccall] gtk_toggle_tool_button_get_active*(toggletoolbutton: PGtkToggleToolButton): lg.gboolean;
	PROCEDURE [ccall] gtk_toggle_tool_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_toggle_tool_button_set_active*(toggletoolbutton: PGtkToggleToolButton; is_active: lg.gboolean);

(* GtkToggleToolButton constructors *)
	PROCEDURE [ccall] gtk_toggle_tool_button_new*(): PGtkToggleToolButton;
	PROCEDURE [ccall] gtk_toggle_tool_button_new_from_stock*(stock_id: lg.Pgchar): PGtkToggleToolButton;

(* GtkMenuToolButton methods *)
	PROCEDURE [ccall] gtk_menu_tool_button_get_menu*(menutoolbutton: PGtkMenuToolButton): PGtkWidget;
	PROCEDURE [ccall] gtk_menu_tool_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_menu_tool_button_set_arrow_tooltip*(menutoolbutton: PGtkMenuToolButton; tooltips: PGtkTooltips; tip_text: lg.Pgchar; tip_private: lg.Pgchar);
	PROCEDURE [ccall] gtk_menu_tool_button_set_menu*(menutoolbutton: PGtkMenuToolButton; menu: PGtkWidget);

(* GtkMenuToolButton constructors *)
	PROCEDURE [ccall] gtk_menu_tool_button_new*(icon_widget: PGtkWidget; label: lg.Pgchar): PGtkMenuToolButton;
	PROCEDURE [ccall] gtk_menu_tool_button_new_from_stock*(stock_id: lg.Pgchar): PGtkMenuToolButton;

(* GtkRadioToolButton methods *)
	PROCEDURE [ccall] gtk_radio_tool_button_get_group*(radiotoolbutton: PGtkRadioToolButton): lg.PGSList;
	PROCEDURE [ccall] gtk_radio_tool_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_radio_tool_button_set_group*(radiotoolbutton: PGtkRadioToolButton; group: lg.PGSList);

(* GtkRadioToolButton constructors *)
	PROCEDURE [ccall] gtk_radio_tool_button_new*(group: lg.PGSList): PGtkRadioToolButton;
	PROCEDURE [ccall] gtk_radio_tool_button_new_from_stock*(group: lg.PGSList; stock_id: lg.Pgchar): PGtkRadioToolButton;
	PROCEDURE [ccall] gtk_radio_tool_button_new_from_widget*(group: PGtkRadioToolButton): PGtkRadioToolButton;
	PROCEDURE [ccall] gtk_radio_tool_button_new_with_stock_from_widget*(group: PGtkRadioToolButton; stock_id: lg.Pgchar): PGtkRadioToolButton;

(* GtkDialog methods *)
	PROCEDURE [ccall] gtk_dialog_add_action_widget*(dialog: PGtkDialog; child: PGtkWidget; response_id: lg.gint);
	PROCEDURE [ccall] gtk_dialog_add_button*(dialog: PGtkDialog; button_text: lg.Pgchar; response_id: lg.gint): PGtkWidget;
	PROCEDURE [ccall] gtk_dialog_add_buttons*(dialog: PGtkDialog; first_button_text: lg.Pgchar);
	PROCEDURE [ccall] gtk_dialog_get_has_separator*(dialog: PGtkDialog): lg.gboolean;
	PROCEDURE [ccall] gtk_dialog_get_response_for_widget*(dialog: PGtkDialog; widget: PGtkWidget): lg.gint;
	PROCEDURE [ccall] gtk_dialog_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_dialog_response*(dialog: PGtkDialog; response_id: lg.gint);
	PROCEDURE [ccall] gtk_dialog_run*(dialog: PGtkDialog): lg.gint;
	PROCEDURE [ccall] gtk_dialog_set_alternative_button_order*(dialog: PGtkDialog; first_response_id: lg.gint);
	PROCEDURE [ccall] gtk_dialog_set_alternative_button_order_from_array*(dialog: PGtkDialog; n_params: lg.gint; new_order: lg.Pgint);
	PROCEDURE [ccall] gtk_dialog_set_default_response*(dialog: PGtkDialog; response_id: GtkResponseType);
	PROCEDURE [ccall] gtk_dialog_set_has_separator*(dialog: PGtkDialog; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_dialog_set_response_sensitive*(dialog: PGtkDialog; response_id: GtkResponseType; setting: lg.gboolean);

(* GtkDialog constructors *)
	PROCEDURE [ccall] gtk_dialog_new*(): PGtkDialog;
	PROCEDURE [ccall] gtk_dialog_new_with_buttons*(title: lg.Pgchar; parent: PGtkWindow; flags: GtkDialogFlags; first_button_text: lg.Pgchar): PGtkDialog;

(* GtkPlug methods *)
	PROCEDURE [ccall] gtk_plug_construct*(plug: PGtkPlug; socket_id: gdk.GdkNativeWindow);
	PROCEDURE [ccall] gtk_plug_construct_for_display*(plug: PGtkPlug; display: gdk.PGdkDisplay; socket_id: gdk.GdkNativeWindow);
	PROCEDURE [ccall] gtk_plug_get_id*(plug: PGtkPlug): gdk.GdkNativeWindow;
	PROCEDURE [ccall] gtk_plug_get_type*(): lo.GType;

(* GtkPlug constructors *)
	PROCEDURE [ccall] gtk_plug_new*(socket_id: gdk.GdkNativeWindow): PGtkPlug;
	PROCEDURE [ccall] gtk_plug_new_for_display*(display: gdk.PGdkDisplay; socket_id: gdk.GdkNativeWindow): PGtkPlug;

(* GtkAboutDialog methods *)
	PROCEDURE [ccall] gtk_about_dialog_get_artists*(aboutdialog: PGtkAboutDialog): POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_about_dialog_get_authors*(aboutdialog: PGtkAboutDialog): POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_about_dialog_get_comments*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_copyright*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_documenters*(aboutdialog: PGtkAboutDialog): POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_about_dialog_get_license*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_logo*(aboutdialog: PGtkAboutDialog): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_about_dialog_get_logo_icon_name*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_name*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_translator_credits*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_about_dialog_get_version*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_website*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_website_label*(aboutdialog: PGtkAboutDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_about_dialog_get_wrap_license*(aboutdialog: PGtkAboutDialog): lg.gboolean;
	PROCEDURE [ccall] gtk_about_dialog_set_artists*(aboutdialog: PGtkAboutDialog; artists: POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar);
	PROCEDURE [ccall] gtk_about_dialog_set_authors*(aboutdialog: PGtkAboutDialog; authors: POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar);
	PROCEDURE [ccall] gtk_about_dialog_set_comments*(aboutdialog: PGtkAboutDialog; comments: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_copyright*(aboutdialog: PGtkAboutDialog; copyright: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_documenters*(aboutdialog: PGtkAboutDialog; documenters: POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar);
	PROCEDURE [ccall] gtk_about_dialog_set_email_hook*(func: GtkAboutDialogActivateLinkFunc; data: lg.gpointer; destroy: lo.GDestroyNotify): POINTER TO GtkAboutDialogActivateLinkFunc;
	PROCEDURE [ccall] gtk_about_dialog_set_license*(aboutdialog: PGtkAboutDialog; license: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_logo*(aboutdialog: PGtkAboutDialog; logo: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_about_dialog_set_logo_icon_name*(aboutdialog: PGtkAboutDialog; icon_name: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_name*(aboutdialog: PGtkAboutDialog; name: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_translator_credits*(aboutdialog: PGtkAboutDialog; translator_credits: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_url_hook*(func: GtkAboutDialogActivateLinkFunc; data: lg.gpointer; destroy: lo.GDestroyNotify): POINTER TO GtkAboutDialogActivateLinkFunc;
	PROCEDURE [ccall] gtk_about_dialog_set_version*(aboutdialog: PGtkAboutDialog; version: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_website*(aboutdialog: PGtkAboutDialog; website: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_website_label*(aboutdialog: PGtkAboutDialog; website_label: lg.Pgchar);
	PROCEDURE [ccall] gtk_about_dialog_set_wrap_license*(aboutdialog: PGtkAboutDialog; wrap_license: lg.gboolean);

(* GtkAboutDialog constructors *)
	PROCEDURE [ccall] gtk_about_dialog_new*(): PGtkAboutDialog;

(* GtkColorSelectionDialog methods *)
	PROCEDURE [ccall] gtk_color_selection_dialog_get_type*(): lo.GType;

(* GtkColorSelectionDialog constructors *)
	PROCEDURE [ccall] gtk_color_selection_dialog_new*(title: lg.Pgchar): PGtkColorSelectionDialog;

(* GtkFileChooserDialog methods *)
	PROCEDURE [ccall] gtk_file_chooser_dialog_get_type*(): lo.GType;

(* GtkFileChooserDialog constructors *)
	PROCEDURE [ccall] gtk_file_chooser_dialog_new*(title: lg.Pgchar; parent: PGtkWindow; action: GtkFileChooserAction; first_button_text: lg.Pgchar): PGtkFileChooserDialog;
	PROCEDURE [ccall] gtk_file_chooser_dialog_new_with_backend*(title: lg.Pgchar; parent: PGtkWindow; action: GtkFileChooserAction; backend: lg.Pgchar; first_button_text: lg.Pgchar): PGtkFileChooserDialog;

(* GtkFileSelection methods *)
	PROCEDURE [ccall] gtk_file_selection_complete*(fileselection: PGtkFileSelection; pattern: lg.Pgchar);
	PROCEDURE [ccall] gtk_file_selection_get_filename*(fileselection: PGtkFileSelection): POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_file_selection_get_select_multiple*(fileselection: PGtkFileSelection): lg.gboolean;
	PROCEDURE [ccall] gtk_file_selection_get_selections*(fileselection: PGtkFileSelection): POINTER TO ARRAY OF POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_file_selection_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_file_selection_hide_fileop_buttons*(fileselection: PGtkFileSelection);
	PROCEDURE [ccall] gtk_file_selection_set_filename*(fileselection: PGtkFileSelection; filename: POINTER TO ARRAY [untagged] OF lg.gchar);
	PROCEDURE [ccall] gtk_file_selection_set_select_multiple*(fileselection: PGtkFileSelection; select_multiple: lg.gboolean);
	PROCEDURE [ccall] gtk_file_selection_show_fileop_buttons*(fileselection: PGtkFileSelection);

(* GtkFileSelection constructors *)
	PROCEDURE [ccall] gtk_file_selection_new*(title: lg.Pgchar): PGtkFileSelection;

(* GtkFontSelectionDialog methods *)
	PROCEDURE [ccall] gtk_font_selection_dialog_get_font_name*(fontselectiondialog: PGtkFontSelectionDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_font_selection_dialog_get_preview_text*(fontselectiondialog: PGtkFontSelectionDialog): lg.Pgchar;
	PROCEDURE [ccall] gtk_font_selection_dialog_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_font_selection_dialog_set_font_name*(fontselectiondialog: PGtkFontSelectionDialog; fontname: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_font_selection_dialog_set_preview_text*(fontselectiondialog: PGtkFontSelectionDialog; text: lg.Pgchar);

(* GtkFontSelectionDialog constructors *)
	PROCEDURE [ccall] gtk_font_selection_dialog_new*(title: lg.Pgchar): PGtkFontSelectionDialog;

(* GtkInputDialog methods *)
	PROCEDURE [ccall] gtk_input_dialog_get_type*(): lo.GType;

(* GtkInputDialog constructors *)
	PROCEDURE [ccall] gtk_input_dialog_new*(): PGtkInputDialog;

(* GtkMessageDialog methods *)
	PROCEDURE [ccall] gtk_message_dialog_format_secondary_markup*(messagedialog: PGtkMessageDialog; message_format: lg.Pgchar);
	PROCEDURE [ccall] gtk_message_dialog_format_secondary_text*(messagedialog: PGtkMessageDialog; message_format: lg.Pgchar);
	PROCEDURE [ccall] gtk_message_dialog_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_message_dialog_set_markup*(messagedialog: PGtkMessageDialog; str: lg.Pgchar);

(* GtkMessageDialog constructors *)
	PROCEDURE [ccall] gtk_message_dialog_new*(parent: PGtkWindow; flags: GtkDialogFlags; type: GtkMessageType; buttons: GtkButtonsType; message_format: lg.Pgchar): PGtkMessageDialog;
	PROCEDURE [ccall] gtk_message_dialog_new_with_markup*(parent: PGtkWindow; flags: GtkDialogFlags; type: GtkMessageType; buttons: GtkButtonsType; message_format: lg.Pgchar): PGtkMessageDialog;

(* GtkComboBoxEntry methods *)
	PROCEDURE [ccall] gtk_combo_box_entry_get_text_column*(comboboxentry: PGtkComboBoxEntry): lg.gint;
	PROCEDURE [ccall] gtk_combo_box_entry_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_combo_box_entry_set_text_column*(comboboxentry: PGtkComboBoxEntry; text_column: lg.gint);

(* GtkComboBoxEntry constructors *)
	PROCEDURE [ccall] gtk_combo_box_entry_new*(): PGtkComboBoxEntry;
	PROCEDURE [ccall] gtk_combo_box_entry_new_text*(): PGtkComboBoxEntry;
	PROCEDURE [ccall] gtk_combo_box_entry_new_with_model*(model: PGtkTreeModel; text_column: lg.gint): PGtkComboBoxEntry;

(* GtkAspectFrame methods *)
	PROCEDURE [ccall] gtk_aspect_frame_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_aspect_frame_set*(aspectframe: PGtkAspectFrame; xalign: lg.gfloat; yalign: lg.gfloat; ratio: lg.gfloat; obey_child: lg.gboolean);

(* GtkAspectFrame constructors *)
	PROCEDURE [ccall] gtk_aspect_frame_new*(label: lg.Pgchar; xalign: lg.gfloat; yalign: lg.gfloat; ratio: lg.gfloat; obey_child: lg.gboolean): PGtkAspectFrame;

(* GtkMenuItem methods *)
	PROCEDURE [ccall] gtk_menu_item_activate*(menuitem: PGtkMenuItem);
	PROCEDURE [ccall] gtk_menu_item_deselect*(menuitem: PGtkMenuItem);
	PROCEDURE [ccall] gtk_menu_item_get_right_justified*(menuitem: PGtkMenuItem): lg.gboolean;
	PROCEDURE [ccall] gtk_menu_item_get_submenu*(menuitem: PGtkMenuItem): PGtkWidget;
	PROCEDURE [ccall] gtk_menu_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_menu_item_remove_submenu*(menuitem: PGtkMenuItem);
	PROCEDURE [ccall] gtk_menu_item_select*(menuitem: PGtkMenuItem);
	PROCEDURE [ccall] gtk_menu_item_set_accel_path*(menuitem: PGtkMenuItem; accel_path: lg.Pgchar);
	PROCEDURE [ccall] gtk_menu_item_set_right_justified*(menuitem: PGtkMenuItem; right_justified: lg.gboolean);
	PROCEDURE [ccall] gtk_menu_item_set_submenu*(menuitem: PGtkMenuItem; submenu: PGtkWidget);
	PROCEDURE [ccall] gtk_menu_item_toggle_size_allocate*(menuitem: PGtkMenuItem; allocation: lg.gint);
	PROCEDURE [ccall] gtk_menu_item_toggle_size_request*(menuitem: PGtkMenuItem; requisition: lg.Pgint);

(* GtkMenuItem constructors *)
	PROCEDURE [ccall] gtk_menu_item_new*(): PGtkMenuItem;
	PROCEDURE [ccall] gtk_menu_item_new_with_label*(label: lg.Pgchar): PGtkMenuItem;
	PROCEDURE [ccall] gtk_menu_item_new_with_mnemonic*(label: lg.Pgchar): PGtkMenuItem;

(* GtkCheckMenuItem methods *)
	PROCEDURE [ccall] gtk_check_menu_item_get_active*(checkmenuitem: PGtkCheckMenuItem): lg.gboolean;
	PROCEDURE [ccall] gtk_check_menu_item_get_draw_as_radio*(checkmenuitem: PGtkCheckMenuItem): lg.gboolean;
	PROCEDURE [ccall] gtk_check_menu_item_get_inconsistent*(checkmenuitem: PGtkCheckMenuItem): lg.gboolean;
	PROCEDURE [ccall] gtk_check_menu_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_check_menu_item_set_active*(checkmenuitem: PGtkCheckMenuItem; is_active: lg.gboolean);
	PROCEDURE [ccall] gtk_check_menu_item_set_draw_as_radio*(checkmenuitem: PGtkCheckMenuItem; draw_as_radio: lg.gboolean);
	PROCEDURE [ccall] gtk_check_menu_item_set_inconsistent*(checkmenuitem: PGtkCheckMenuItem; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_check_menu_item_toggled*(checkmenuitem: PGtkCheckMenuItem);

(* GtkCheckMenuItem constructors *)
	PROCEDURE [ccall] gtk_check_menu_item_new*(): PGtkCheckMenuItem;
	PROCEDURE [ccall] gtk_check_menu_item_new_with_label*(label: lg.Pgchar): PGtkCheckMenuItem;
	PROCEDURE [ccall] gtk_check_menu_item_new_with_mnemonic*(label: lg.Pgchar): PGtkCheckMenuItem;

(* GtkImageMenuItem methods *)
	PROCEDURE [ccall] gtk_image_menu_item_get_image*(imagemenuitem: PGtkImageMenuItem): PGtkWidget;
	PROCEDURE [ccall] gtk_image_menu_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_image_menu_item_set_image*(imagemenuitem: PGtkImageMenuItem; image: PGtkWidget);

(* GtkImageMenuItem constructors *)
	PROCEDURE [ccall] gtk_image_menu_item_new*(): PGtkImageMenuItem;
	PROCEDURE [ccall] gtk_image_menu_item_new_from_stock*(stock_id: lg.Pgchar; accel_group: PGtkAccelGroup): PGtkImageMenuItem;
	PROCEDURE [ccall] gtk_image_menu_item_new_with_label*(label: lg.Pgchar): PGtkImageMenuItem;
	PROCEDURE [ccall] gtk_image_menu_item_new_with_mnemonic*(label: lg.Pgchar): PGtkImageMenuItem;

(* GtkSeparatorMenuItem methods *)
	PROCEDURE [ccall] gtk_separator_menu_item_get_type*(): lo.GType;

(* GtkSeparatorMenuItem constructors *)
	PROCEDURE [ccall] gtk_separator_menu_item_new*(): PGtkSeparatorMenuItem;

(* GtkTearoffMenuItem methods *)
	PROCEDURE [ccall] gtk_tearoff_menu_item_get_type*(): lo.GType;

(* GtkTearoffMenuItem constructors *)
	PROCEDURE [ccall] gtk_tearoff_menu_item_new*(): PGtkTearoffMenuItem;

(* GtkRadioMenuItem methods *)
	PROCEDURE [ccall] gtk_radio_menu_item_get_group*(radiomenuitem: PGtkRadioMenuItem): lg.PGSList;
	PROCEDURE [ccall] gtk_radio_menu_item_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_radio_menu_item_set_group*(radiomenuitem: PGtkRadioMenuItem; group: lg.PGSList);

(* GtkRadioMenuItem constructors *)
	PROCEDURE [ccall] gtk_radio_menu_item_new*(group: lg.PGSList): PGtkRadioMenuItem;
	PROCEDURE [ccall] gtk_radio_menu_item_new_from_widget*(group: PGtkRadioMenuItem): PGtkRadioMenuItem;
	PROCEDURE [ccall] gtk_radio_menu_item_new_with_label*(group: lg.PGSList; label: lg.Pgchar): PGtkRadioMenuItem;
	PROCEDURE [ccall] gtk_radio_menu_item_new_with_label_from_widget*(group: PGtkRadioMenuItem; label: lg.Pgchar): PGtkRadioMenuItem;
	PROCEDURE [ccall] gtk_radio_menu_item_new_with_mnemonic*(group: lg.PGSList; label: lg.Pgchar): PGtkRadioMenuItem;
	PROCEDURE [ccall] gtk_radio_menu_item_new_with_mnemonic_from_widget*(group: PGtkRadioMenuItem; label: lg.Pgchar): PGtkRadioMenuItem;

(* GtkVBox methods *)
	PROCEDURE [ccall] gtk_vbox_get_type*(): lo.GType;

(* GtkVBox constructors *)
	PROCEDURE [ccall] gtk_vbox_new*(homogeneous: lg.gboolean; spacing: lg.gint): PGtkVBox;

(* GtkHBox methods *)
	PROCEDURE [ccall] gtk_hbox_get_type*(): lo.GType;

(* GtkHBox constructors *)
	PROCEDURE [ccall] gtk_hbox_new*(homogeneous: lg.gboolean; spacing: lg.gint): PGtkHBox;

(* GtkButtonBox methods *)
	PROCEDURE [ccall] gtk_button_box_get_child_secondary*(buttonbox: PGtkButtonBox; child: PGtkWidget): lg.gboolean;
	PROCEDURE [ccall] gtk_button_box_get_layout*(buttonbox: PGtkButtonBox): GtkButtonBoxStyle;
	PROCEDURE [ccall] gtk_button_box_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_button_box_set_child_secondary*(buttonbox: PGtkButtonBox; child: PGtkWidget; is_secondary: lg.gboolean);
	PROCEDURE [ccall] gtk_button_box_set_layout*(buttonbox: PGtkButtonBox; layout_style: GtkButtonBoxStyle);

(* GtkColorSelection methods *)
	PROCEDURE [ccall] gtk_color_selection_get_current_alpha*(colorselection: PGtkColorSelection): lg.guint16;
	PROCEDURE [ccall] gtk_color_selection_get_current_color*(colorselection: PGtkColorSelection; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_color_selection_get_has_opacity_control*(colorselection: PGtkColorSelection): lg.gboolean;
	PROCEDURE [ccall] gtk_color_selection_get_has_palette*(colorselection: PGtkColorSelection): lg.gboolean;
	PROCEDURE [ccall] gtk_color_selection_get_previous_alpha*(colorselection: PGtkColorSelection): lg.guint16;
	PROCEDURE [ccall] gtk_color_selection_get_previous_color*(colorselection: PGtkColorSelection; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_color_selection_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_color_selection_is_adjusting*(colorselection: PGtkColorSelection): lg.gboolean;
	PROCEDURE [ccall] gtk_color_selection_palette_from_string*(str: lg.Pgchar; VAR colors: gdk.PGdkColor; VAR n_colors: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_color_selection_palette_to_string*(colors: gdk.PGdkColor; n_colors: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] gtk_color_selection_set_change_palette_with_screen_hook*(func: GtkColorSelectionChangePaletteWithScreenFunc): POINTER TO GtkColorSelectionChangePaletteWithScreenFunc;
	PROCEDURE [ccall] gtk_color_selection_set_current_alpha*(colorselection: PGtkColorSelection; alpha: lg.guint16);
	PROCEDURE [ccall] gtk_color_selection_set_current_color*(colorselection: PGtkColorSelection; color: gdk.PGdkColor);
	PROCEDURE [ccall] gtk_color_selection_set_has_opacity_control*(colorselection: PGtkColorSelection; has_opacity: lg.gboolean);
	PROCEDURE [ccall] gtk_color_selection_set_has_palette*(colorselection: PGtkColorSelection; has_palette: lg.gboolean);
	PROCEDURE [ccall] gtk_color_selection_set_previous_alpha*(colorselection: PGtkColorSelection; alpha: lg.guint16);
	PROCEDURE [ccall] gtk_color_selection_set_previous_color*(colorselection: PGtkColorSelection; color: gdk.PGdkColor);

(* GtkColorSelection constructors *)
	PROCEDURE [ccall] gtk_color_selection_new*(): PGtkColorSelection;

(* GtkFileChooserWidget methods *)
	PROCEDURE [ccall] gtk_file_chooser_widget_get_type*(): lo.GType;

(* GtkFileChooserWidget constructors *)
	PROCEDURE [ccall] gtk_file_chooser_widget_new*(action: GtkFileChooserAction): PGtkFileChooserWidget;
	PROCEDURE [ccall] gtk_file_chooser_widget_new_with_backend*(action: GtkFileChooserAction; backend: lg.Pgchar): PGtkFileChooserWidget;

(* GtkFontSelection methods *)
	PROCEDURE [ccall] gtk_font_selection_get_font_name*(fontselection: PGtkFontSelection): lg.Pgchar;
	PROCEDURE [ccall] gtk_font_selection_get_preview_text*(fontselection: PGtkFontSelection): lg.Pgchar;
	PROCEDURE [ccall] gtk_font_selection_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_font_selection_set_font_name*(fontselection: PGtkFontSelection; fontname: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_font_selection_set_preview_text*(fontselection: PGtkFontSelection; text: lg.Pgchar);

(* GtkFontSelection constructors *)
	PROCEDURE [ccall] gtk_font_selection_new*(): PGtkFontSelection;

(* GtkGammaCurve methods *)
	PROCEDURE [ccall] gtk_gamma_curve_get_type*(): lo.GType;

(* GtkGammaCurve constructors *)
	PROCEDURE [ccall] gtk_gamma_curve_new*(): PGtkGammaCurve;

(* GtkCombo methods *)

(* GtkCombo constructors *)

(* GtkFileChooserButton methods *)
	PROCEDURE [ccall] gtk_file_chooser_button_get_title*(filechooserbutton: PGtkFileChooserButton): lg.Pgchar;
	PROCEDURE [ccall] gtk_file_chooser_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_file_chooser_button_get_width_chars*(filechooserbutton: PGtkFileChooserButton): lg.gint;
	PROCEDURE [ccall] gtk_file_chooser_button_set_title*(filechooserbutton: PGtkFileChooserButton; title: lg.Pgchar);
	PROCEDURE [ccall] gtk_file_chooser_button_set_width_chars*(filechooserbutton: PGtkFileChooserButton; n_chars: lg.gint);

(* GtkFileChooserButton constructors *)
	PROCEDURE [ccall] gtk_file_chooser_button_new*(title: lg.Pgchar; action: GtkFileChooserAction): PGtkFileChooserButton;
	PROCEDURE [ccall] gtk_file_chooser_button_new_with_backend*(title: lg.Pgchar; action: GtkFileChooserAction; backend: lg.Pgchar): PGtkFileChooserButton;
	PROCEDURE [ccall] gtk_file_chooser_button_new_with_dialog*(dialog: PGtkWidget): PGtkFileChooserButton;

(* GtkStatusbar methods *)
	PROCEDURE [ccall] gtk_statusbar_get_context_id*(statusbar: PGtkStatusbar; context_description: lg.Pgchar): lg.guint;
	PROCEDURE [ccall] gtk_statusbar_get_has_resize_grip*(statusbar: PGtkStatusbar): lg.gboolean;
	PROCEDURE [ccall] gtk_statusbar_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_statusbar_pop*(statusbar: PGtkStatusbar; context_id: lg.guint);
	PROCEDURE [ccall] gtk_statusbar_push*(statusbar: PGtkStatusbar; context_id: lg.guint; text: lg.Pgchar): lg.guint;
	PROCEDURE [ccall] gtk_statusbar_remove*(statusbar: PGtkStatusbar; context_id: lg.guint; message_id: lg.guint);
	PROCEDURE [ccall] gtk_statusbar_set_has_resize_grip*(statusbar: PGtkStatusbar; setting: lg.gboolean);

(* GtkStatusbar constructors *)
	PROCEDURE [ccall] gtk_statusbar_new*(): PGtkStatusbar;

(* GtkHButtonBox methods *)
	PROCEDURE [ccall] gtk_hbutton_box_get_type*(): lo.GType;

(* GtkHButtonBox constructors *)
	PROCEDURE [ccall] gtk_hbutton_box_new*(): PGtkHButtonBox;

(* GtkVButtonBox methods *)
	PROCEDURE [ccall] gtk_vbutton_box_get_type*(): lo.GType;

(* GtkVButtonBox constructors *)
	PROCEDURE [ccall] gtk_vbutton_box_new*(): PGtkVButtonBox;

(* GtkMenu methods *)
	PROCEDURE [ccall] gtk_menu_attach*(menu: PGtkMenu; child: PGtkWidget; left_attach: lg.guint; right_attach: lg.guint; top_attach: lg.guint; bottom_attach: lg.guint);
	PROCEDURE [ccall] gtk_menu_attach_to_widget*(menu: PGtkMenu; attach_widget: PGtkWidget; detacher: GtkMenuDetachFunc);
	PROCEDURE [ccall] gtk_menu_detach*(menu: PGtkMenu);
	PROCEDURE [ccall] gtk_menu_get_accel_group*(menu: PGtkMenu): PGtkAccelGroup;
	PROCEDURE [ccall] gtk_menu_get_active*(menu: PGtkMenu): PGtkWidget;
	PROCEDURE [ccall] gtk_menu_get_attach_widget*(menu: PGtkMenu): PGtkWidget;
	PROCEDURE [ccall] gtk_menu_get_for_attach_widget*(widget: PGtkWidget): lg.PGList;
	PROCEDURE [ccall] gtk_menu_get_tearoff_state*(menu: PGtkMenu): lg.gboolean;
	PROCEDURE [ccall] gtk_menu_get_title*(menu: PGtkMenu): lg.Pgchar;
	PROCEDURE [ccall] gtk_menu_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_menu_popdown*(menu: PGtkMenu);
	PROCEDURE [ccall] gtk_menu_popup*(menu: PGtkMenu; parent_menu_shell: PGtkWidget; parent_menu_item: PGtkWidget; func: GtkMenuPositionFunc; data: lg.gpointer; button: lg.guint; activate_time: lg.guint32);
	PROCEDURE [ccall] gtk_menu_reorder_child*(menu: PGtkMenu; child: PGtkWidget; position: lg.gint);
	PROCEDURE [ccall] gtk_menu_reposition*(menu: PGtkMenu);
	PROCEDURE [ccall] gtk_menu_set_accel_group*(menu: PGtkMenu; accel_group: PGtkAccelGroup);
	PROCEDURE [ccall] gtk_menu_set_accel_path*(menu: PGtkMenu; accel_path: lg.Pgchar);
	PROCEDURE [ccall] gtk_menu_set_active*(menu: PGtkMenu; index_: lg.guint);
	PROCEDURE [ccall] gtk_menu_set_monitor*(menu: PGtkMenu; monitor_num: lg.gint);
	PROCEDURE [ccall] gtk_menu_set_screen*(menu: PGtkMenu; screen: gdk.PGdkScreen);
	PROCEDURE [ccall] gtk_menu_set_tearoff_state*(menu: PGtkMenu; torn_off: lg.gboolean);
	PROCEDURE [ccall] gtk_menu_set_title*(menu: PGtkMenu; title: lg.Pgchar);

(* GtkMenu constructors *)
	PROCEDURE [ccall] gtk_menu_new*(): PGtkMenu;

(* GtkMenuBar methods *)
	PROCEDURE [ccall] gtk_menu_bar_get_child_pack_direction*(menubar: PGtkMenuBar): GtkPackDirection;
	PROCEDURE [ccall] gtk_menu_bar_get_pack_direction*(menubar: PGtkMenuBar): GtkPackDirection;
	PROCEDURE [ccall] gtk_menu_bar_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_menu_bar_set_child_pack_direction*(menubar: PGtkMenuBar; child_pack_dir: GtkPackDirection);
	PROCEDURE [ccall] gtk_menu_bar_set_pack_direction*(menubar: PGtkMenuBar; pack_dir: GtkPackDirection);

(* GtkMenuBar constructors *)
	PROCEDURE [ccall] gtk_menu_bar_new*(): PGtkMenuBar;

(* GtkHPaned methods *)
	PROCEDURE [ccall] gtk_hpaned_get_type*(): lo.GType;

(* GtkHPaned constructors *)
	PROCEDURE [ccall] gtk_hpaned_new*(): PGtkHPaned;

(* GtkVPaned methods *)
	PROCEDURE [ccall] gtk_vpaned_get_type*(): lo.GType;

(* GtkVPaned constructors *)
	PROCEDURE [ccall] gtk_vpaned_new*(): PGtkVPaned;

(* GtkLabel methods *)
	PROCEDURE [ccall] gtk_label_get_angle*(label: PGtkLabel): lg.gdouble;
	PROCEDURE [ccall] gtk_label_get_attributes*(label: PGtkLabel): pan.PPangoAttrList;
	PROCEDURE [ccall] gtk_label_get_ellipsize*(label: PGtkLabel): pan.PangoEllipsizeMode;
	PROCEDURE [ccall] gtk_label_get_justify*(label: PGtkLabel): GtkJustification;
	PROCEDURE [ccall] gtk_label_get_label*(label: PGtkLabel): lg.Pgchar;
	PROCEDURE [ccall] gtk_label_get_layout*(label: PGtkLabel): pan.PPangoLayout;
	PROCEDURE [ccall] gtk_label_get_layout_offsets*(label: PGtkLabel; x: lg.Pgint; y: lg.Pgint);
	PROCEDURE [ccall] gtk_label_get_line_wrap*(label: PGtkLabel): lg.gboolean;
	PROCEDURE [ccall] gtk_label_get_max_width_chars*(label: PGtkLabel): lg.gint;
	PROCEDURE [ccall] gtk_label_get_mnemonic_keyval*(label: PGtkLabel): lg.guint;
	PROCEDURE [ccall] gtk_label_get_mnemonic_widget*(label: PGtkLabel): PGtkWidget;
	PROCEDURE [ccall] gtk_label_get_selectable*(label: PGtkLabel): lg.gboolean;
	PROCEDURE [ccall] gtk_label_get_selection_bounds*(label: PGtkLabel; start: lg.Pgint; end: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gtk_label_get_single_line_mode*(label: PGtkLabel): lg.gboolean;
	PROCEDURE [ccall] gtk_label_get_text*(label: PGtkLabel): lg.Pgchar;
	PROCEDURE [ccall] gtk_label_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_label_get_use_markup*(label: PGtkLabel): lg.gboolean;
	PROCEDURE [ccall] gtk_label_get_use_underline*(label: PGtkLabel): lg.gboolean;
	PROCEDURE [ccall] gtk_label_get_width_chars*(label: PGtkLabel): lg.gint;
	PROCEDURE [ccall] gtk_label_select_region*(label: PGtkLabel; start_offset: lg.gint; end_offset: lg.gint);
	PROCEDURE [ccall] gtk_label_set_angle*(label: PGtkLabel; angle: lg.gdouble);
	PROCEDURE [ccall] gtk_label_set_attributes*(label: PGtkLabel; attrs: pan.PPangoAttrList);
	PROCEDURE [ccall] gtk_label_set_ellipsize*(label: PGtkLabel; mode: pan.PangoEllipsizeMode);
	PROCEDURE [ccall] gtk_label_set_justify*(label: PGtkLabel; jtype: GtkJustification);
	PROCEDURE [ccall] gtk_label_set_label*(label: PGtkLabel; str: lg.Pgchar);
	PROCEDURE [ccall] gtk_label_set_line_wrap*(label: PGtkLabel; wrap: lg.gboolean);
	PROCEDURE [ccall] gtk_label_set_markup*(label: PGtkLabel; str: lg.Pgchar);
	PROCEDURE [ccall] gtk_label_set_markup_with_mnemonic*(label: PGtkLabel; str: lg.Pgchar);
	PROCEDURE [ccall] gtk_label_set_max_width_chars*(label: PGtkLabel; n_chars: lg.gint);
	PROCEDURE [ccall] gtk_label_set_mnemonic_widget*(label: PGtkLabel; widget: PGtkWidget);
	PROCEDURE [ccall] gtk_label_set_pattern*(label: PGtkLabel; pattern: lg.Pgchar);
	PROCEDURE [ccall] gtk_label_set_selectable*(label: PGtkLabel; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_label_set_single_line_mode*(label: PGtkLabel; single_line_mode: lg.gboolean);
	PROCEDURE [ccall] gtk_label_set_text*(label: PGtkLabel; str: lg.Pgchar);
	PROCEDURE [ccall] gtk_label_set_text_with_mnemonic*(label: PGtkLabel; str: lg.Pgchar);
	PROCEDURE [ccall] gtk_label_set_use_markup*(label: PGtkLabel; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_label_set_use_underline*(label: PGtkLabel; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_label_set_width_chars*(label: PGtkLabel; n_chars: lg.gint);

(* GtkLabel constructors *)
	PROCEDURE [ccall] gtk_label_new*(str: lg.Pgchar): PGtkLabel;
	PROCEDURE [ccall] gtk_label_new_with_mnemonic*(str: lg.Pgchar): PGtkLabel;

(* GtkArrow methods *)
	PROCEDURE [ccall] gtk_arrow_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_arrow_set*(arrow: PGtkArrow; arrow_type: GtkArrowType; shadow_type: GtkShadowType);

(* GtkArrow constructors *)
	PROCEDURE [ccall] gtk_arrow_new*(arrow_type: GtkArrowType; shadow_type: GtkShadowType): PGtkArrow;

(* GtkImage methods *)
	PROCEDURE [ccall] gtk_image_clear*(image: PGtkImage);
	PROCEDURE [ccall] gtk_image_get_animation*(image: PGtkImage): gpb.PGdkPixbufAnimation;
	PROCEDURE [ccall] gtk_image_get_icon_name*(image: PGtkImage; VAR icon_name: lg.Pgchar; VAR size: GtkIconSize);
	PROCEDURE [ccall] gtk_image_get_icon_set*(image: PGtkImage; VAR icon_set: PGtkIconSet; VAR size: GtkIconSize);
	PROCEDURE [ccall] gtk_image_get_image*(image: PGtkImage; VAR gdk_image: gdk.PGdkImage; VAR mask: gdk.PGdkBitmap);
	PROCEDURE [ccall] gtk_image_get_pixbuf*(image: PGtkImage): gpb.PGdkPixbuf;
	PROCEDURE [ccall] gtk_image_get_pixel_size*(image: PGtkImage): lg.gint;
	PROCEDURE [ccall] gtk_image_get_pixmap*(image: PGtkImage; VAR [nil] pixmap: gdk.PGdkPixmap; VAR [nil] mask: gdk.PGdkBitmap);
	PROCEDURE [ccall] gtk_image_get_stock*(image: PGtkImage; VAR stock_id: lg.Pgchar; VAR size: GtkIconSize);
	PROCEDURE [ccall] gtk_image_get_storage_type*(image: PGtkImage): GtkImageType;
	PROCEDURE [ccall] gtk_image_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_image_set_from_animation*(image: PGtkImage; animation: gpb.PGdkPixbufAnimation);
	PROCEDURE [ccall] gtk_image_set_from_file*(image: PGtkImage; filename: lg.Pgchar);
	PROCEDURE [ccall] gtk_image_set_from_icon_name*(image: PGtkImage; icon_name: lg.Pgchar; size: GtkIconSize);
	PROCEDURE [ccall] gtk_image_set_from_icon_set*(image: PGtkImage; icon_set: PGtkIconSet; size: GtkIconSize);
	PROCEDURE [ccall] gtk_image_set_from_image*(image: PGtkImage; gdk_image: gdk.PGdkImage; mask: gdk.PGdkBitmap);
	PROCEDURE [ccall] gtk_image_set_from_pixbuf*(image: PGtkImage; pixbuf: gpb.PGdkPixbuf);
	PROCEDURE [ccall] gtk_image_set_from_pixmap*(image: PGtkImage; pixmap: gdk.PGdkPixmap; mask: gdk.PGdkBitmap);
	PROCEDURE [ccall] gtk_image_set_from_stock*(image: PGtkImage; stock_id: lg.Pgchar; size: GtkIconSize);
	PROCEDURE [ccall] gtk_image_set_pixel_size*(image: PGtkImage; pixel_size: lg.gint);

(* GtkImage constructors *)
	PROCEDURE [ccall] gtk_image_new*(): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_animation*(animation: gpb.PGdkPixbufAnimation): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_file*(filename: lg.Pgchar): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_icon_name*(icon_name: lg.Pgchar; size: GtkIconSize): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_icon_set*(icon_set: PGtkIconSet; size: GtkIconSize): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_image*(image: gdk.PGdkImage; mask: gdk.PGdkBitmap): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_pixbuf*(pixbuf: gpb.PGdkPixbuf): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_pixmap*(pixmap: gdk.PGdkPixmap; mask: gdk.PGdkBitmap): PGtkImage;
	PROCEDURE [ccall] gtk_image_new_from_stock*(stock_id: lg.Pgchar; size: GtkIconSize): PGtkImage;

(* GtkAccelLabel methods *)
	PROCEDURE [ccall] gtk_accel_label_get_accel_widget*(accellabel: PGtkAccelLabel): PGtkWidget;
	PROCEDURE [ccall] gtk_accel_label_get_accel_width*(accellabel: PGtkAccelLabel): lg.guint;
	PROCEDURE [ccall] gtk_accel_label_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_accel_label_refetch*(accellabel: PGtkAccelLabel): lg.gboolean;
	PROCEDURE [ccall] gtk_accel_label_set_accel_closure*(accellabel: PGtkAccelLabel; accel_closure: lo.PGClosure);
	PROCEDURE [ccall] gtk_accel_label_set_accel_widget*(accellabel: PGtkAccelLabel; accel_widget: PGtkWidget);

(* GtkAccelLabel constructors *)
	PROCEDURE [ccall] gtk_accel_label_new*(string: lg.Pgchar): PGtkAccelLabel;

(* GtkScale methods *)
	PROCEDURE [ccall] gtk_scale_get_digits*(scale: PGtkScale): lg.gint;
	PROCEDURE [ccall] gtk_scale_get_draw_value*(scale: PGtkScale): lg.gboolean;
	PROCEDURE [ccall] gtk_scale_get_layout*(scale: PGtkScale): pan.PPangoLayout;
	PROCEDURE [ccall] gtk_scale_get_layout_offsets*(scale: PGtkScale; x: lg.Pgint; y: lg.Pgint);
	PROCEDURE [ccall] gtk_scale_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_scale_get_value_pos*(scale: PGtkScale): GtkPositionType;
	PROCEDURE [ccall] gtk_scale_set_digits*(scale: PGtkScale; digits: lg.gint);
	PROCEDURE [ccall] gtk_scale_set_draw_value*(scale: PGtkScale; draw_value: lg.gboolean);
	PROCEDURE [ccall] gtk_scale_set_value_pos*(scale: PGtkScale; pos: GtkPositionType);

(* GtkScrollbar methods *)
	PROCEDURE [ccall] gtk_scrollbar_get_type*(): lo.GType;

(* GtkHScale methods *)
	PROCEDURE [ccall] gtk_hscale_get_type*(): lo.GType;

(* GtkHScale constructors *)
	PROCEDURE [ccall] gtk_hscale_new*(adjustment: PGtkAdjustment): PGtkHScale;
	PROCEDURE [ccall] gtk_hscale_new_with_range*(min: lg.gdouble; max: lg.gdouble; step: lg.gdouble): PGtkHScale;

(* GtkVScale methods *)
	PROCEDURE [ccall] gtk_vscale_get_type*(): lo.GType;

(* GtkVScale constructors *)
	PROCEDURE [ccall] gtk_vscale_new*(adjustment: PGtkAdjustment): PGtkVScale;
	PROCEDURE [ccall] gtk_vscale_new_with_range*(min: lg.gdouble; max: lg.gdouble; step: lg.gdouble): PGtkVScale;

(* GtkHScrollbar methods *)
	PROCEDURE [ccall] gtk_hscrollbar_get_type*(): lo.GType;

(* GtkHScrollbar constructors *)
	PROCEDURE [ccall] gtk_hscrollbar_new*(adjustment: PGtkAdjustment): PGtkHScrollbar;

(* GtkVScrollbar methods *)
	PROCEDURE [ccall] gtk_vscrollbar_get_type*(): lo.GType;

(* GtkVScrollbar constructors *)
	PROCEDURE [ccall] gtk_vscrollbar_new*(adjustment: PGtkAdjustment): PGtkVScrollbar;

(* GtkHRuler methods *)
	PROCEDURE [ccall] gtk_hruler_get_type*(): lo.GType;

(* GtkHRuler constructors *)
	PROCEDURE [ccall] gtk_hruler_new*(): PGtkHRuler;

(* GtkVRuler methods *)
	PROCEDURE [ccall] gtk_vruler_get_type*(): lo.GType;

(* GtkVRuler constructors *)
	PROCEDURE [ccall] gtk_vruler_new*(): PGtkVRuler;

(* GtkHSeparator methods *)
	PROCEDURE [ccall] gtk_hseparator_get_type*(): lo.GType;

(* GtkHSeparator constructors *)
	PROCEDURE [ccall] gtk_hseparator_new*(): PGtkHSeparator;

(* GtkVSeparator methods *)
	PROCEDURE [ccall] gtk_vseparator_get_type*(): lo.GType;

(* GtkVSeparator constructors *)
	PROCEDURE [ccall] gtk_vseparator_new*(): PGtkVSeparator;

(* GtkCurve methods *)
	PROCEDURE [ccall] gtk_curve_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_curve_get_vector*(curve: PGtkCurve; veclen: lg.int; vector: lg.gfloat);
	PROCEDURE [ccall] gtk_curve_reset*(curve: PGtkCurve);
	PROCEDURE [ccall] gtk_curve_set_curve_type*(curve: PGtkCurve; type: GtkCurveType);
	PROCEDURE [ccall] gtk_curve_set_gamma*(curve: PGtkCurve; gamma_: lg.gfloat);
	PROCEDURE [ccall] gtk_curve_set_range*(curve: PGtkCurve; min_x: lg.gfloat; max_x: lg.gfloat; min_y: lg.gfloat; max_y: lg.gfloat);
	PROCEDURE [ccall] gtk_curve_set_vector*(curve: PGtkCurve; veclen: lg.int; vector: lg.gfloat);

(* GtkCurve constructors *)
	PROCEDURE [ccall] gtk_curve_new*(): PGtkCurve;

(* GtkSpinButton methods *)
	PROCEDURE [ccall] gtk_spin_button_configure*(spinbutton: PGtkSpinButton; adjustment: PGtkAdjustment; climb_rate: lg.gdouble; digits: lg.guint);
	PROCEDURE [ccall] gtk_spin_button_get_adjustment*(spinbutton: PGtkSpinButton): PGtkAdjustment;
	PROCEDURE [ccall] gtk_spin_button_get_digits*(spinbutton: PGtkSpinButton): lg.guint;
	PROCEDURE [ccall] gtk_spin_button_get_increments*(spinbutton: PGtkSpinButton; step: lg.Pgdouble; page: lg.Pgdouble);
	PROCEDURE [ccall] gtk_spin_button_get_numeric*(spinbutton: PGtkSpinButton): lg.gboolean;
	PROCEDURE [ccall] gtk_spin_button_get_range*(spinbutton: PGtkSpinButton; min: lg.Pgdouble; max: lg.Pgdouble);
	PROCEDURE [ccall] gtk_spin_button_get_snap_to_ticks*(spinbutton: PGtkSpinButton): lg.gboolean;
	PROCEDURE [ccall] gtk_spin_button_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_spin_button_get_update_policy*(spinbutton: PGtkSpinButton): GtkSpinButtonUpdatePolicy;
	PROCEDURE [ccall] gtk_spin_button_get_value*(spinbutton: PGtkSpinButton): lg.gdouble;
	PROCEDURE [ccall] gtk_spin_button_get_value_as_int*(spinbutton: PGtkSpinButton): lg.gint;
	PROCEDURE [ccall] gtk_spin_button_get_wrap*(spinbutton: PGtkSpinButton): lg.gboolean;
	PROCEDURE [ccall] gtk_spin_button_set_adjustment*(spinbutton: PGtkSpinButton; adjustment: PGtkAdjustment);
	PROCEDURE [ccall] gtk_spin_button_set_digits*(spinbutton: PGtkSpinButton; digits: lg.guint);
	PROCEDURE [ccall] gtk_spin_button_set_increments*(spinbutton: PGtkSpinButton; step: lg.gdouble; page: lg.gdouble);
	PROCEDURE [ccall] gtk_spin_button_set_numeric*(spinbutton: PGtkSpinButton; numeric: lg.gboolean);
	PROCEDURE [ccall] gtk_spin_button_set_range*(spinbutton: PGtkSpinButton; min: lg.gdouble; max: lg.gdouble);
	PROCEDURE [ccall] gtk_spin_button_set_snap_to_ticks*(spinbutton: PGtkSpinButton; snap_to_ticks: lg.gboolean);
	PROCEDURE [ccall] gtk_spin_button_set_update_policy*(spinbutton: PGtkSpinButton; policy: GtkSpinButtonUpdatePolicy);
	PROCEDURE [ccall] gtk_spin_button_set_value*(spinbutton: PGtkSpinButton; value: lg.gdouble);
	PROCEDURE [ccall] gtk_spin_button_set_wrap*(spinbutton: PGtkSpinButton; wrap: lg.gboolean);
	PROCEDURE [ccall] gtk_spin_button_spin*(spinbutton: PGtkSpinButton; direction: GtkSpinType; increment: lg.gdouble);
	PROCEDURE [ccall] gtk_spin_button_update*(spinbutton: PGtkSpinButton);

(* GtkSpinButton constructors *)
	PROCEDURE [ccall] gtk_spin_button_new*(adjustment: PGtkAdjustment; climb_rate: lg.gdouble; digits: lg.guint): PGtkSpinButton;
	PROCEDURE [ccall] gtk_spin_button_new_with_range*(min: lg.gdouble; max: lg.gdouble; step: lg.gdouble): PGtkSpinButton;

(* GtkCellRendererText methods *)
	PROCEDURE [ccall] gtk_cell_renderer_text_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_cell_renderer_text_set_fixed_height_from_font*(cellrenderertext: PGtkCellRendererText; number_of_rows: lg.gint);

(* GtkCellRendererText constructors *)
	PROCEDURE [ccall] gtk_cell_renderer_text_new*(): PGtkCellRendererText;

(* GtkCellRendererPixbuf methods *)
	PROCEDURE [ccall] gtk_cell_renderer_pixbuf_get_type*(): lo.GType;

(* GtkCellRendererPixbuf constructors *)
	PROCEDURE [ccall] gtk_cell_renderer_pixbuf_new*(): PGtkCellRendererPixbuf;

(* GtkCellRendererProgress methods *)
	PROCEDURE [ccall] gtk_cell_renderer_progress_get_type*(): lo.GType;

(* GtkCellRendererProgress constructors *)
	PROCEDURE [ccall] gtk_cell_renderer_progress_new*(): PGtkCellRendererProgress;

(* GtkCellRendererToggle methods *)
	PROCEDURE [ccall] gtk_cell_renderer_toggle_get_active*(cellrenderertoggle: PGtkCellRendererToggle): lg.gboolean;
	PROCEDURE [ccall] gtk_cell_renderer_toggle_get_radio*(cellrenderertoggle: PGtkCellRendererToggle): lg.gboolean;
	PROCEDURE [ccall] gtk_cell_renderer_toggle_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_cell_renderer_toggle_set_active*(cellrenderertoggle: PGtkCellRendererToggle; setting: lg.gboolean);
	PROCEDURE [ccall] gtk_cell_renderer_toggle_set_radio*(cellrenderertoggle: PGtkCellRendererToggle; radio: lg.gboolean);

(* GtkCellRendererToggle constructors *)
	PROCEDURE [ccall] gtk_cell_renderer_toggle_new*(): PGtkCellRendererToggle;

(* GtkCellRendererCombo methods *)
	PROCEDURE [ccall] gtk_cell_renderer_combo_get_type*(): lo.GType;

(* GtkCellRendererCombo constructors *)
	PROCEDURE [ccall] gtk_cell_renderer_combo_new*(): PGtkCellRendererCombo;

(* GtkCellEditable methods *)
	PROCEDURE [ccall] gtk_cell_editable_editing_done*(celleditable: PGtkCellEditable);
	PROCEDURE [ccall] gtk_cell_editable_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_cell_editable_remove_widget*(celleditable: PGtkCellEditable);
	PROCEDURE [ccall] gtk_cell_editable_start_editing*(celleditable: PGtkCellEditable; event: gdk.PGdkEvent);

(* GtkCellLayout methods *)
	PROCEDURE [ccall] gtk_cell_layout_add_attribute*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer; attribute: lg.Pgchar; column: lg.gint);
	PROCEDURE [ccall] gtk_cell_layout_clear*(celllayout: PGtkCellLayout);
	PROCEDURE [ccall] gtk_cell_layout_clear_attributes*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer);
	PROCEDURE [ccall] gtk_cell_layout_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_cell_layout_pack_end*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_cell_layout_pack_start*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer; expand: lg.gboolean);
	PROCEDURE [ccall] gtk_cell_layout_reorder*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer; position: lg.gint);
	PROCEDURE [ccall] gtk_cell_layout_set_attributes*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer);
	PROCEDURE [ccall] gtk_cell_layout_set_cell_data_func*(celllayout: PGtkCellLayout; cell: PGtkCellRenderer; func: GtkCellLayoutDataFunc; func_data: lg.gpointer; destroy: lo.GDestroyNotify);

(* GtkEditable methods *)
	PROCEDURE [ccall] gtk_editable_copy_clipboard*(editable: PGtkEditable);
	PROCEDURE [ccall] gtk_editable_cut_clipboard*(editable: PGtkEditable);
	PROCEDURE [ccall] gtk_editable_delete_selection*(editable: PGtkEditable);
	PROCEDURE [ccall] gtk_editable_delete_text*(editable: PGtkEditable; start_pos: lg.gint; end_pos: lg.gint);
	PROCEDURE [ccall] gtk_editable_get_chars*(editable: PGtkEditable; start_pos: lg.gint; end_pos: lg.gint): lg.Pgchar;
	PROCEDURE [ccall] gtk_editable_get_editable*(editable: PGtkEditable): lg.gboolean;
	PROCEDURE [ccall] gtk_editable_get_position*(editable: PGtkEditable): lg.gint;
	PROCEDURE [ccall] gtk_editable_get_selection_bounds*(editable: PGtkEditable; start: lg.Pgint; end: lg.Pgint): lg.gboolean;
	PROCEDURE [ccall] gtk_editable_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_editable_insert_text*(editable: PGtkEditable; new_text: lg.Pgchar; new_text_length: lg.gint; position: lg.Pgint);
	PROCEDURE [ccall] gtk_editable_paste_clipboard*(editable: PGtkEditable);
	PROCEDURE [ccall] gtk_editable_select_region*(editable: PGtkEditable; start: lg.gint; end: lg.gint);
	PROCEDURE [ccall] gtk_editable_set_editable*(editable: PGtkEditable; is_editable: lg.gboolean);
	PROCEDURE [ccall] gtk_editable_set_position*(editable: PGtkEditable; position: lg.gint);

(* GtkFileChooser methods *)
	PROCEDURE [ccall] gtk_file_chooser_add_filter*(filechooser: PGtkFileChooser; filter: PGtkFileFilter);
	PROCEDURE [ccall] gtk_file_chooser_add_shortcut_folder*(filechooser: PGtkFileChooser; folder: POINTER TO ARRAY [untagged] OF lg.gchar; VAR error: lg.PGError): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_add_shortcut_folder_uri*(filechooser: PGtkFileChooser; uri: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_error_quark*(): lg.GQuark;
	PROCEDURE [ccall] gtk_file_chooser_get_action*(filechooser: PGtkFileChooser): GtkFileChooserAction;
	PROCEDURE [ccall] gtk_file_chooser_get_current_folder*(filechooser: PGtkFileChooser): POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_file_chooser_get_current_folder_uri*(filechooser: PGtkFileChooser): lg.Pgchar;
	PROCEDURE [ccall] gtk_file_chooser_get_do_overwrite_confirmation*(filechooser: PGtkFileChooser): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_get_extra_widget*(filechooser: PGtkFileChooser): PGtkWidget;
	PROCEDURE [ccall] gtk_file_chooser_get_filename*(filechooser: PGtkFileChooser): POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_file_chooser_get_filenames*(filechooser: PGtkFileChooser): lg.PGSList;
	PROCEDURE [ccall] gtk_file_chooser_get_filter*(filechooser: PGtkFileChooser): PGtkFileFilter;
	PROCEDURE [ccall] gtk_file_chooser_get_local_only*(filechooser: PGtkFileChooser): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_get_preview_filename*(filechooser: PGtkFileChooser): POINTER TO ARRAY [untagged] OF lg.gchar;
	PROCEDURE [ccall] gtk_file_chooser_get_preview_uri*(filechooser: PGtkFileChooser): lg.Pgchar;
	PROCEDURE [ccall] gtk_file_chooser_get_preview_widget*(filechooser: PGtkFileChooser): PGtkWidget;
	PROCEDURE [ccall] gtk_file_chooser_get_preview_widget_active*(filechooser: PGtkFileChooser): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_get_select_multiple*(filechooser: PGtkFileChooser): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_get_show_hidden*(filechooser: PGtkFileChooser): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_file_chooser_get_uri*(filechooser: PGtkFileChooser): lg.Pgchar;
	PROCEDURE [ccall] gtk_file_chooser_get_uris*(filechooser: PGtkFileChooser): lg.PGSList;
	PROCEDURE [ccall] gtk_file_chooser_get_use_preview_label*(filechooser: PGtkFileChooser): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_list_filters*(filechooser: PGtkFileChooser): lg.PGSList;
	PROCEDURE [ccall] gtk_file_chooser_list_shortcut_folder_uris*(filechooser: PGtkFileChooser): lg.PGSList;
	PROCEDURE [ccall] gtk_file_chooser_list_shortcut_folders*(filechooser: PGtkFileChooser): lg.PGSList;
	PROCEDURE [ccall] gtk_file_chooser_remove_filter*(filechooser: PGtkFileChooser; filter: PGtkFileFilter);
	PROCEDURE [ccall] gtk_file_chooser_remove_shortcut_folder*(filechooser: PGtkFileChooser; folder: POINTER TO ARRAY [untagged] OF lg.gchar; VAR error: lg.PGError): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_remove_shortcut_folder_uri*(filechooser: PGtkFileChooser; uri: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_select_all*(filechooser: PGtkFileChooser);
	PROCEDURE [ccall] gtk_file_chooser_select_filename*(filechooser: PGtkFileChooser; filename: POINTER TO ARRAY [untagged] OF lg.gchar): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_select_uri*(filechooser: PGtkFileChooser; uri: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_set_action*(filechooser: PGtkFileChooser; action: GtkFileChooserAction);
	PROCEDURE [ccall] gtk_file_chooser_set_current_folder*(filechooser: PGtkFileChooser; filename: POINTER TO ARRAY [untagged] OF lg.gchar): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_set_current_folder_uri*(filechooser: PGtkFileChooser; uri: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_set_current_name*(filechooser: PGtkFileChooser; name: lg.Pgchar);
	PROCEDURE [ccall] gtk_file_chooser_set_do_overwrite_confirmation*(filechooser: PGtkFileChooser; do_overwrite_confirmation: lg.gboolean);
	PROCEDURE [ccall] gtk_file_chooser_set_extra_widget*(filechooser: PGtkFileChooser; extra_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_file_chooser_set_filename*(filechooser: PGtkFileChooser; filename: POINTER TO ARRAY [untagged] OF lg.gchar): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_set_filter*(filechooser: PGtkFileChooser; filter: PGtkFileFilter);
	PROCEDURE [ccall] gtk_file_chooser_set_local_only*(filechooser: PGtkFileChooser; local_only: lg.gboolean);
	PROCEDURE [ccall] gtk_file_chooser_set_preview_widget*(filechooser: PGtkFileChooser; preview_widget: PGtkWidget);
	PROCEDURE [ccall] gtk_file_chooser_set_preview_widget_active*(filechooser: PGtkFileChooser; active: lg.gboolean);
	PROCEDURE [ccall] gtk_file_chooser_set_select_multiple*(filechooser: PGtkFileChooser; select_multiple: lg.gboolean);
	PROCEDURE [ccall] gtk_file_chooser_set_show_hidden*(filechooser: PGtkFileChooser; show_hidden: lg.gboolean);
	PROCEDURE [ccall] gtk_file_chooser_set_uri*(filechooser: PGtkFileChooser; uri: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_file_chooser_set_use_preview_label*(filechooser: PGtkFileChooser; use_label: lg.gboolean);
	PROCEDURE [ccall] gtk_file_chooser_unselect_all*(filechooser: PGtkFileChooser);
	PROCEDURE [ccall] gtk_file_chooser_unselect_filename*(filechooser: PGtkFileChooser; filename: POINTER TO ARRAY [untagged] OF lg.gchar);
	PROCEDURE [ccall] gtk_file_chooser_unselect_uri*(filechooser: PGtkFileChooser; uri: lg.Pgchar);

(* GtkTreeDragDest methods *)
	PROCEDURE [ccall] gtk_tree_drag_dest_drag_data_received*(treedragdest: PGtkTreeDragDest; dest: PGtkTreePath; selection_data: PGtkSelectionData): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_drag_dest_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_drag_dest_row_drop_possible*(treedragdest: PGtkTreeDragDest; dest_path: PGtkTreePath; selection_data: PGtkSelectionData): lg.gboolean;

(* GtkTreeDragSource methods *)
	PROCEDURE [ccall] gtk_tree_drag_source_drag_data_delete*(treedragsource: PGtkTreeDragSource; path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_drag_source_drag_data_get*(treedragsource: PGtkTreeDragSource; path: PGtkTreePath; selection_data: PGtkSelectionData): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_drag_source_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_drag_source_row_draggable*(treedragsource: PGtkTreeDragSource; path: PGtkTreePath): lg.gboolean;

(* GtkTreeModel methods *)
	PROCEDURE [ccall] gtk_tree_model_foreach*(treemodel: PGtkTreeModel; func: GtkTreeModelForeachFunc; user_data: lg.gpointer);
	PROCEDURE [ccall] gtk_tree_model_get*(treemodel: PGtkTreeModel; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_get_column_type*(treemodel: PGtkTreeModel; index_: lg.gint): lo.GType;
	PROCEDURE [ccall] gtk_tree_model_get_flags*(treemodel: PGtkTreeModel): GtkTreeModelFlags;
	PROCEDURE [ccall] gtk_tree_model_get_iter*(treemodel: PGtkTreeModel; iter: PGtkTreeIter; path: PGtkTreePath): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_get_iter_first*(treemodel: PGtkTreeModel; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_get_iter_from_string*(treemodel: PGtkTreeModel; iter: PGtkTreeIter; path_string: lg.Pgchar): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_get_n_columns*(treemodel: PGtkTreeModel): lg.gint;
	PROCEDURE [ccall] gtk_tree_model_get_path*(treemodel: PGtkTreeModel; iter: PGtkTreeIter): PGtkTreePath;
	PROCEDURE [ccall] gtk_tree_model_get_string_from_iter*(treemodel: PGtkTreeModel; iter: PGtkTreeIter): lg.Pgchar;
	PROCEDURE [ccall] gtk_tree_model_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_model_get_valist*(treemodel: PGtkTreeModel; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_get_value*(treemodel: PGtkTreeModel; iter: PGtkTreeIter; column: lg.gint; value: lo.PGValue);
	PROCEDURE [ccall] gtk_tree_model_iter_children*(treemodel: PGtkTreeModel; iter: PGtkTreeIter; parent: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_iter_has_child*(treemodel: PGtkTreeModel; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_iter_n_children*(treemodel: PGtkTreeModel; iter: PGtkTreeIter): lg.gint;
	PROCEDURE [ccall] gtk_tree_model_iter_next*(treemodel: PGtkTreeModel; iter: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_iter_nth_child*(treemodel: PGtkTreeModel; iter: PGtkTreeIter; parent: PGtkTreeIter; n: lg.gint): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_iter_parent*(treemodel: PGtkTreeModel; iter: PGtkTreeIter; child: PGtkTreeIter): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_model_ref_node*(treemodel: PGtkTreeModel; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_row_changed*(treemodel: PGtkTreeModel; path: PGtkTreePath; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_row_deleted*(treemodel: PGtkTreeModel; path: PGtkTreePath);
	PROCEDURE [ccall] gtk_tree_model_row_has_child_toggled*(treemodel: PGtkTreeModel; path: PGtkTreePath; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_row_inserted*(treemodel: PGtkTreeModel; path: PGtkTreePath; iter: PGtkTreeIter);
	PROCEDURE [ccall] gtk_tree_model_rows_reordered*(treemodel: PGtkTreeModel; path: PGtkTreePath; iter: PGtkTreeIter; new_order: lg.Pgint);
	PROCEDURE [ccall] gtk_tree_model_unref_node*(treemodel: PGtkTreeModel; iter: PGtkTreeIter);

(* GtkTreeSortable methods *)
	PROCEDURE [ccall] gtk_tree_sortable_get_sort_column_id*(treesortable: PGtkTreeSortable; VAR sort_column_id: lg.gint; VAR order: GtkSortType): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_sortable_get_type*(): lo.GType;
	PROCEDURE [ccall] gtk_tree_sortable_has_default_sort_func*(treesortable: PGtkTreeSortable): lg.gboolean;
	PROCEDURE [ccall] gtk_tree_sortable_set_default_sort_func*(treesortable: PGtkTreeSortable; sort_func: GtkTreeIterCompareFunc; user_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_sortable_set_sort_column_id*(treesortable: PGtkTreeSortable; sort_column_id: lg.gint; order: GtkSortType);
	PROCEDURE [ccall] gtk_tree_sortable_set_sort_func*(treesortable: PGtkTreeSortable; sort_column_id: lg.gint; sort_func: GtkTreeIterCompareFunc; user_data: lg.gpointer; destroy: lo.GDestroyNotify);
	PROCEDURE [ccall] gtk_tree_sortable_sort_column_changed*(treesortable: PGtkTreeSortable);

	PROCEDURE [ccall] gtk_init*(argc: PInt; argv: PStrList);
	PROCEDURE [ccall] gtk_main*();
	PROCEDURE [ccall] gtk_main_quit*();
END LibsGtk.
