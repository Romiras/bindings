MODULE LibsGObject ["libgobject-2.0-0.dll"];
(*MODULE LibsGObject ["libgobject-2.0.so"];*)

	IMPORT SYSTEM, lg := LibsGlib;

	CONST
		G_TYPE_DEBUG_NONE* = {};
		G_TYPE_DEBUG_OBJECTS* = {0};
		G_TYPE_DEBUG_SIGNALS* = {1};
		G_TYPE_DEBUG_MASK* = {0, 1};
		
		G_PARAM_READABLE* = {0};
		G_PARAM_WRITABLE* = {1};
		G_PARAM_CONSTRUCT* = {2};
		G_PARAM_CONSTRUCT_ONLY* = {3};
		G_PARAM_LAX_VALIDATION* = {4};
		G_PARAM_STATIC_NAME* = {5};
		G_PARAM_PRIVATE* = G_PARAM_STATIC_NAME;
		G_PARAM_STATIC_NICK* = {6};
		G_PARAM_STATIC_BLURB* = {7};

		G_CONNECT_AFTER* = {0};
		G_CONNECT_SWAPPED* = {1};

		G_SIGNAL_MATCH_ID* = {0};
		G_SIGNAL_MATCH_DETAIL* = {1};
		G_SIGNAL_MATCH_CLOSURE* = {2};
		G_SIGNAL_MATCH_FUNC* = {3};
		G_SIGNAL_MATCH_DATA* = {4};
		G_SIGNAL_MATCH_UNBLOCKED* = {5};

		G_SIGNAL_MATCH_MASK* = {0..5};

	TYPE
		GType* = lg.gsize;

		PGTypeClass* = POINTER TO GTypeClass;
		GTypeClass* = EXTENSIBLE RECORD
			g_type*: GType;
		END;

		PGTypeInstance* = POINTER TO GTypeInstance;
		GTypeInstance* = EXTENSIBLE RECORD (*lg.BaseRecord*)
			g_class*: PGTypeClass;
		END;

		PGTypeInterface* = POINTER TO GTypeInterface;
		GTypeInterface* = EXTENSIBLE RECORD(GTypeClass)
			g_instance_type*: GType;
		END;

		GTypeQuery* = EXTENSIBLE RECORD(GTypeClass)
			type_name*: lg.Pgchar;
			class_size*: lg.guint;
			instance_size*: lg.guint;
		END;

		PGData* = POINTER TO GData;
		GData* = RECORD END;

		GTypeDebugFlags* = SET;

		GBaseInitFunc* = PROCEDURE [ccall] (g_class: lg.gpointer);
		GBaseFinalizeFunc* = PROCEDURE [ccall] (g_class: lg.gpointer);

		PGObject* = POINTER TO GObject;
		GObject* = EXTENSIBLE RECORD(GTypeInstance)
			ref_count*: lg.guint;
			qdata*: PGData;
		END;

		PGValue* = POINTER TO GValue;
		GValue* = RECORD[noalign](GTypeClass)
			data*: ARRAY 2 OF
			RECORD[union]
				v_int: lg.gint;
				v_uint: lg.guint;
				v_long: lg.glong;
				v_ulong: lg.gulong;
				v_int64: lg.gint64;
				v_uint64: lg.guint64;
				v_float: lg.gfloat;
				v_double: lg.gdouble;
				v_pointer: lg.gpointer;
			END;
		END;

		GParamFlags* = SET;

		PGParamSpec* = POINTER TO GParamSpec;
		GParamSpec* = RECORD[noalign](GTypeInstance)
			name*: lg.Pgchar;
			flags*: GParamFlags;
			value_type*: GType;
			owner_type*: GType; (* class or interface using this property *)
			_nick*: lg.Pgchar;
			_blurb*: lg.Pgchar;
			qdata*: GData;
			ref_count*: lg.guint;
			param_id*: lg.guint; (* sort-criteria *)
		END;

		GObjectConstructParam* = RECORD
			pspec*: PGParamSpec;
			value*: PGValue;
		END;

		PGFlagsValue* = POINTER TO GFlagsValue;
		GFlagsValue* = RECORD
			value*: SET;
			value_name-: lg.gchar;
			value_nick-: lg.gchar;
		END;

		PGEnumValue* = POINTER TO GEnumValue;
		GEnumValue* = RECORD
			value*: SET;
			value_name-: lg.gchar;
			value_nick-: lg.gchar;
		END;

		PGObjectClass* = POINTER TO GObjectClass;
		GObjectClass* = RECORD(GTypeClass)
			construct_properties*: lg.PGSList;
			constructor*: PROCEDURE [ccall] (
				type: GType;
				n_construct_properties: lg.guint;
				construct_properties: POINTER TO ARRAY[untagged] OF GObjectConstructParam
				): PGObject;

			(* overridable methods *)

			set_property*: PROCEDURE [ccall] (
				object: PGObject;
				property_id: lg.guint;
				value: PGValue;
				pspec: PGParamSpec
				);

			get_property*: PROCEDURE [ccall] (
				object: PGObject;
				property_id: lg.guint;
				value: PGValue;
				pspec: PGParamSpec
				);

			dispose*: PROCEDURE [ccall] (object: PGObject);

			finalize*: PROCEDURE [ccall] (object: PGObject);

			dispatch_properties_changed*: PROCEDURE [ccall] (
				object: PGObject;
				n_pspecs: lg.guint;
				VAR pspecs: PGParamSpec
				);

			notify*: PROCEDURE [ccall] (
				object: PGObject;
				pspec: PGParamSpec
				);

			constructed*: PROCEDURE [ccall] (object: GObject);
			pdummy: ARRAY [untagged] 7 OF lg.gpointer;
		END;


		PGInitiallyUnowned* = POINTER TO GInitiallyUnowned;
		GInitiallyUnowned* = GObject;

		PGInitiallyUnownedClass* = POINTER TO GInitiallyUnownedClass;
		GInitiallyUnownedClass* = GObjectClass;

		GClassInitFunc* = PROCEDURE [ccall] (g_class: lg.gpointer; class_data: lg.gpointer);

		GClassFinalizeFunc* = PROCEDURE [ccall] (g_class: lg.gpointer; class_data: lg.gpointer);

		GInstanceInitFunc* = PROCEDURE [ccall] (instance: PGTypeInstance; g_class: lg.gpointer);

		GInterfaceInitFunc* = PROCEDURE [ccall] (g_iface: lg.gpointer; iface_data: lg.gpointer);

		GInterfaceFinalizeFunc* = PROCEDURE [ccall] (g_iface: lg.gpointer; iface_data: lg.gpointer);

		GTypeClassCacheFunc* = PROCEDURE [ccall] (cache_data: lg.gpointer;
			g_class: GTypeClass): lg.gboolean;

		GTypeInterfaceCheckFunc* = PROCEDURE [ccall] (check_data: lg.gpointer; g_iface: lg.gpointer);

		PGClosure* = POINTER TO GClosure;
		PGClosureNotifyData* = POINTER TO GClosureNotifyData;
		GClosureNotify* = PROCEDURE [ccall] (data: lg.gpointer; closure: PGClosure);

		GClosure* = RECORD[noalign]
			flag0*: SET;
			marshal*: PROCEDURE [ccall] (
				closure: PGClosure;
				VAR return_value: GValue;
				n_param_values: lg.guint;
				param_values: POINTER TO ARRAY [untagged] OF GValue;
				invocation_hint: lg.gpointer;
				marshal_data: lg.gpointer
				);
			data*: lg.gpointer;
			notifiers*: POINTER TO ARRAY [untagged] OF PGClosureNotifyData;
		END;

		PGCallback* = POINTER TO GCallback;
		GCallback* = EXTENSIBLE RECORD END;

		GCallback1* = INTEGER;

		GClosureMarshal* = RECORD (GCallback)
			callback*: PROCEDURE [ccall] (
				closure: PGClosure;
				VAR return_value: GValue;
				n_param_values: lg.guint;
				param_values: POINTER TO ARRAY[untagged] OF GValue;
				invocation_hint: lg.gpointer;
				marshal_data: lg.gpointer
				);
		END;

		GDestroyNotify* = RECORD(GCallback)
			callback: PROCEDURE [ccall] (data: lg.gpointer);
		END;

		GClosureNotifyData* = RECORD[noalign]
			data*: lg.gpointer;
			notify*: GClosureNotify;
		END;

		PGCClosure* = POINTER TO GCClosure;
		GCClosure* = RECORD
			closure*: GClosure;
			callback*: lg.gpointer;
		END;

		GSignalCMarshaller* = GClosureMarshal;

		GConnectFlags* = SET;

		GSignalMatchType* = SET;

		PROCEDURE [ccall] g_signal_connect_data* (
			instance : PGObject;
			detailed_signal : lg.Pgchar;
			c_handler : INTEGER;
			data : lg.gpointer;
			destroy_data : GClosureNotify;
			connect_flags : GConnectFlags
		);
		PROCEDURE [ccall] g_object_ref* (object: lg.gpointer): lg.gpointer;
		PROCEDURE [ccall] g_object_unref* (object: lg.gpointer);

END LibsGObject.