MODULE LibsGObjectStuff;

	IMPORT
		SYSTEM,
		lg := LibsGlib,
		lo := LibsGObject;

	PROCEDURE g_signal_connect*(
	  instance        : lo.PGObject;
	  detailed_signal : lg.Pgchar;
	  c_handler       : INTEGER;
	  data            : lg.gpointer
	  );
	BEGIN
		lo.g_signal_connect_data
		(
		  instance,
		  detailed_signal,
		  c_handler,
		  data, NIL, 
		  {}
    );
	END g_signal_connect;

	PROCEDURE g_signal_connect_swapped*(
  	instance        : lo.PGObject;
	  detailed_signal : lg.Pgchar;
	  c_handler       : INTEGER;
	  data            : lg.gpointer
	);
	BEGIN
		lo.g_signal_connect_data
		(
		  instance,
		  detailed_signal,
		  c_handler,
		  data, 
		  NIL, 
		  lo.G_CONNECT_SWAPPED
		);
	END g_signal_connect_swapped;
	
END LibsGObjectStuff.