MODULE LibsGdkPixbuf ["libgdk_pixbuf-2.0-0.dll"];
(*MODULE LibsGdkPixbuf ["libgdk_pixbuf-2.0.so"];*)

IMPORT
  SYSTEM,
  lg := LibsGlib,
  lo := LibsGObject;

(* ==================================================================== ENUMS *)
TYPE
  GdkColorspace* = lg.gint32;
CONST
  GDK_COLORSPACE_RGB* = 0;

TYPE
  GdkInterpType* = lg.gint32;
CONST
  GDK_INTERP_NEAREST*  = 0;
  GDK_INTERP_TILES*    = 1;
  GDK_INTERP_BILINEAR* = 2;
  GDK_INTERP_HYPER*    = 3;

TYPE
  GdkPixbufAlphaMode* = lg.gint32;
CONST
  GDK_PIXBUF_ALPHA_BILEVEL* = 0;
  GDK_PIXBUF_ALPHA_FULL*    = 1;

TYPE
  GdkPixbufError* = lg.gint32;
CONST
  GDK_PIXBUF_ERROR_CORRUPT_IMAGE*         = 0;
  GDK_PIXBUF_ERROR_INSUFFICIENT_MEMORY*   = 1;
  GDK_PIXBUF_ERROR_BAD_OPTION*            = 2;
  GDK_PIXBUF_ERROR_UNKNOWN_TYPE*          = 3;
  GDK_PIXBUF_ERROR_UNSUPPORTED_OPERATION* = 4;
  GDK_PIXBUF_ERROR_FAILED*                = 5;

TYPE
  GdkPixbufFrameAction* = lg.gint32;
CONST
  GDK_PIXBUF_FRAME_RETAIN*  = 0;
  GDK_PIXBUF_FRAME_DISPOSE* = 1;
  GDK_PIXBUF_FRAME_REVERT*  = 2;

TYPE
  GdkPixbufRotation* = lg.gint32;
CONST
  GDK_PIXBUF_ROTATE_NONE*             = 0;
  GDK_PIXBUF_ROTATE_COUNTERCLOCKWISE* = 90;
  GDK_PIXBUF_ROTATE_UPSIDEDOWN*       = 180;
  GDK_PIXBUF_ROTATE_CLOCKWISE*        = 270;

TYPE
  GdkPixdataDumpType* = SET;
CONST
  GDK_PIXDATA_DUMP_PIXDATA_STREAM* = {};
  GDK_PIXDATA_DUMP_PIXDATA_STRUCT* = 1;
  GDK_PIXDATA_DUMP_MACROS*         = 2;
  GDK_PIXDATA_DUMP_GTYPES*         = {};
  GDK_PIXDATA_DUMP_CTYPES*         = {8};
  GDK_PIXDATA_DUMP_STATIC*         = {9};
  GDK_PIXDATA_DUMP_CONST*          = {10};
  GDK_PIXDATA_DUMP_RLE_DECODER*    = {16};

TYPE
  GdkPixdataType* = SET;
CONST
  GDK_PIXDATA_COLOR_TYPE_RGB*    = {0};
  GDK_PIXDATA_COLOR_TYPE_RGBA*   = {1};
  GDK_PIXDATA_COLOR_TYPE_MASK*   = {0..7};
  GDK_PIXDATA_SAMPLE_WIDTH_8*    = {16};
  GDK_PIXDATA_SAMPLE_WIDTH_MASK* = {17..20};
  GDK_PIXDATA_ENCODING_RAW*      = {24};
  GDK_PIXDATA_ENCODING_RLE*      = {25};
  GDK_PIXDATA_ENCODING_MASK*     = {24..27};

(* ====================================================== POINTERS TO OBJECTS *)
TYPE
  PGdkPixbuf*              = POINTER TO GdkPixbuf;
  PGdkPixbufAnimation*     = POINTER TO GdkPixbufAnimation;
  PGdkPixbufAnimationIter* = POINTER TO GdkPixbufAnimationIter;
  PGdkPixbufAniAnim*       = POINTER TO GdkPixbufAniAnim;
  PGdkPixbufAniAnimIter*   = POINTER TO GdkPixbufAniAnimIter;
  PGdkPixbufGifAnim*       = POINTER TO GdkPixbufGifAnim;
  PGdkPixbufGifAnimIter*   = POINTER TO GdkPixbufGifAnimIter;
  PGdkPixbufLoader*        = POINTER TO GdkPixbufLoader;
  PGdkPixbufSimpleAnim*    = POINTER TO GdkPixbufSimpleAnim;

(* ====================================================== POINTERS TO STRUCTS *)
TYPE
  PGdkPixbufFormat* = POINTER TO GdkPixbufFormat;
  PGdkPixbufFrame*  = POINTER TO GdkPixbufFrame;
  PGdkPixdata*      = POINTER TO GdkPixdata;

(* ================================================================ CALLBACKS *)
TYPE
  Callback* = lo.GCallback;

  GdkPixbufDestroyNotify* = RECORD (Callback)
    callback*: PROCEDURE [ccall]
    (
      pixels: lg.Pguchar;
      data  : lg.gpointer
    );
  END;

  GdkPixbufSaveFunc*      = RECORD (Callback)
    callback*: PROCEDURE [ccall]
    (
      buf  : lg.Pgchar;
      count: lg.gsize;
      VAR error: lg.PGError;
      data : lg.gpointer
    ): lg.gboolean;
  END;

(* ================================================================== STRUCTS *)
TYPE

(* GdkPixbufFormat struct *)

  GdkPixbufFormat* = RECORD [noalign]
  END;

(* GdkPixbufFrame struct *)

  GdkPixbufFrame* = RECORD [noalign]
    pixbuf*          : PGdkPixbuf;
    x_offset*        : lg.int;
    y_offset*        : lg.int;
    delay_time*      : lg.int;
    elapsed*         : lg.int;
    action*          : GdkPixbufFrameAction;
    need_recomposite*: lg.gboolean;
    bg_transparent*  : lg.gboolean;
    composited*      : PGdkPixbuf;
    revert*          : PGdkPixbuf;
  END;

(* GdkPixdata struct *)

  GdkPixdata* = RECORD [noalign]
    magic*       : lg.guint32;
    length*      : lg.gint32;
    pixdata_type*: lg.guint32;
    rowstride*   : lg.guint32;
    width*       : lg.guint32;
    height*      : lg.guint32;
    pixel_data*  : lg.Pguint8;
  END;

(*  ================================================================= OBJECTS *)
TYPE

(* GdkPixbufAnimation object *)

  GdkPixbufAnimation* = EXTENSIBLE RECORD [noalign] (lo.GObject)
  END;

(* GdkPixbufAnimationIter object *)

  GdkPixbufAnimationIter* = EXTENSIBLE RECORD [noalign] (lo.GObject)
  END;

(* GdkPixbuf object *)

  GdkPixbuf* = EXTENSIBLE RECORD [noalign] (lo.GObject)
  END;

(* GdkPixbufLoader object *)

  GdkPixbufLoader* = EXTENSIBLE RECORD [noalign] (lo.GObject)
    GdkPixbufLoader_pd*: lg.gpointer (* private_data *);
  END;

(* GdkPixbufAniAnim object *)

  GdkPixbufAniAnim* = EXTENSIBLE RECORD [noalign] (GdkPixbufAnimation)
    total_time*            : lg.int;
    n_frames*              : lg.int;
    n_pixbufs*             : lg.int;
    pixbufs*               : POINTER TO ARRAY [untagged] OF PGdkPixbuf;
    sequence*              : lg.Pint;
    delay*                 : lg.Pint;
    width*                 : lg.int;
    height*                : lg.int;
  END;

(* GdkPixbufGifAnim object *)

  GdkPixbufGifAnim* = EXTENSIBLE RECORD [noalign] (GdkPixbufAnimation)
    n_frames*              : lg.int;
    total_time*            : lg.int;
    frames*                : lg.PGList;
    width*                 : lg.int;
    height*                : lg.int;
    bg_red*                : lg.guchar;
    bg_green*              : lg.guchar;
    bg_blue*               : lg.guchar;
    loop*                  : lg.int;
    loading*               : lg.gboolean;
  END;

(* GdkPixbufSimpleAnim object *)

  GdkPixbufSimpleAnim* = EXTENSIBLE RECORD [noalign] (GdkPixbufAnimation)
  END;

(* GdkPixbufAniAnimIter object *)

  GdkPixbufAniAnimIter* = EXTENSIBLE RECORD [noalign] (GdkPixbufAnimationIter)
    ani_anim*                  : PGdkPixbufAniAnim;
    start_time*                : lg.GTimeVal;
    current_time*              : lg.GTimeVal;
    position*                  : lg.gint;
    current_frame*             : lg.gint;
    elapsed*                   : lg.gint;
  END;

(* GdkPixbufGifAnimIter object *)

  GdkPixbufGifAnimIter* = EXTENSIBLE RECORD [noalign] (GdkPixbufAnimationIter)
    gif_anim*                  : PGdkPixbufGifAnim;
    start_time*                : lg.GTimeVal;
    current_time*              : lg.GTimeVal;
    position*                  : lg.gint;
    current_frame*             : lg.PGList;
    first_loop_slowness*       : lg.gint;
  END;

(* GdkPixbufAnimation methods *)
  PROCEDURE [ccall] gdk_pixbuf_animation_get_height*(pixbufanimation: PGdkPixbufAnimation): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_animation_get_iter*(pixbufanimation: PGdkPixbufAnimation; start_time: lg.PGTimeVal): PGdkPixbufAnimationIter;
  PROCEDURE [ccall] gdk_pixbuf_animation_get_static_image*(pixbufanimation: PGdkPixbufAnimation): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_animation_get_type*(): lo.GType;
  PROCEDURE [ccall] gdk_pixbuf_animation_get_width*(pixbufanimation: PGdkPixbufAnimation): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_animation_is_static_image*(pixbufanimation: PGdkPixbufAnimation): lg.gboolean;

(* GdkPixbufAnimation constructors *)
  PROCEDURE [ccall] gdk_pixbuf_animation_new_from_file*(filename: lg.Pgchar; VAR error: lg.PGError): PGdkPixbufAnimation;

(* GdkPixbufAnimationIter methods *)
  PROCEDURE [ccall] gdk_pixbuf_animation_iter_advance*(pixbufanimationiter: PGdkPixbufAnimationIter; current_time: lg.PGTimeVal): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_animation_iter_get_delay_time*(pixbufanimationiter: PGdkPixbufAnimationIter): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_animation_iter_get_pixbuf*(pixbufanimationiter: PGdkPixbufAnimationIter): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_animation_iter_get_type*(): lo.GType;
  PROCEDURE [ccall] gdk_pixbuf_animation_iter_on_currently_loading_frame*(pixbufanimationiter: PGdkPixbufAnimationIter): lg.gboolean;

(* GdkPixbuf methods *)
  PROCEDURE [ccall] gdk_pixbuf_add_alpha*(pixbuf: PGdkPixbuf; substitute_color: lg.gboolean; r: lg.guchar; g: lg.guchar; b: lg.guchar): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_composite*(pixbuf: PGdkPixbuf; dest: PGdkPixbuf; dest_x: lg.int; dest_y: lg.int; dest_width: lg.int; dest_height: lg.int; offset_x: lg.gdouble; offset_y: lg.gdouble; scale_x: lg.gdouble; scale_y: lg.gdouble; interp_type: GdkInterpType; overall_alpha: lg.int);
  PROCEDURE [ccall] gdk_pixbuf_composite_color*(pixbuf: PGdkPixbuf; dest: PGdkPixbuf; dest_x: lg.int; dest_y: lg.int; dest_width: lg.int; dest_height: lg.int; offset_x: lg.gdouble; offset_y: lg.gdouble; scale_x: lg.gdouble; scale_y: lg.gdouble; interp_type: GdkInterpType; overall_alpha: lg.int; check_x: lg.int; check_y: lg.int; check_size: lg.int; color1: lg.guint32; color2: lg.guint32);
  PROCEDURE [ccall] gdk_pixbuf_composite_color_simple*(pixbuf: PGdkPixbuf; dest_width: lg.int; dest_height: lg.int; interp_type: GdkInterpType; overall_alpha: lg.int; check_size: lg.int; color1: lg.guint32; color2: lg.guint32): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_copy*(pixbuf: PGdkPixbuf): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_copy_area*(pixbuf: PGdkPixbuf; src_x: lg.int; src_y: lg.int; width: lg.int; height: lg.int; dest_pixbuf: PGdkPixbuf; dest_x: lg.int; dest_y: lg.int);
  PROCEDURE [ccall] gdk_pixbuf_error_quark*(): lg.GQuark;
  PROCEDURE [ccall] gdk_pixbuf_fill*(pixbuf: PGdkPixbuf; pixel: lg.guint32);
  PROCEDURE [ccall] gdk_pixbuf_flip*(pixbuf: PGdkPixbuf; horizontal: lg.gboolean): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_from_pixdata*(pixdata: PGdkPixdata; copy_pixels: lg.gboolean; VAR error: lg.PGError): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_get_bits_per_sample*(pixbuf: PGdkPixbuf): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_get_colorspace*(pixbuf: PGdkPixbuf): GdkColorspace;
  PROCEDURE [ccall] gdk_pixbuf_get_file_info*(filename: lg.Pgchar; width: lg.Pgint; height: lg.Pgint): PGdkPixbufFormat;
  PROCEDURE [ccall] gdk_pixbuf_get_formats*(): lg.PGSList;
  PROCEDURE [ccall] gdk_pixbuf_get_has_alpha*(pixbuf: PGdkPixbuf): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_get_height*(pixbuf: PGdkPixbuf): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_get_n_channels*(pixbuf: PGdkPixbuf): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_get_option*(pixbuf: PGdkPixbuf; key: lg.Pgchar): lg.Pgchar;
  PROCEDURE [ccall] gdk_pixbuf_get_pixels*(pixbuf: PGdkPixbuf): lg.Pguchar;
  PROCEDURE [ccall] gdk_pixbuf_get_rowstride*(pixbuf: PGdkPixbuf): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_get_type*(): lo.GType;
  PROCEDURE [ccall] gdk_pixbuf_get_width*(pixbuf: PGdkPixbuf): lg.int;
  PROCEDURE [ccall] gdk_pixbuf_rotate_simple*(pixbuf: PGdkPixbuf; angle: GdkPixbufRotation): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_saturate_and_pixelate*(pixbuf: PGdkPixbuf; dest: PGdkPixbuf; saturation: lg.gfloat; pixelate: lg.gboolean);
  PROCEDURE [ccall] gdk_pixbuf_save*(pixbuf: PGdkPixbuf; filename: lg.Pgchar; type: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_save_to_buffer*(pixbuf: PGdkPixbuf; VAR buffer: lg.Pgchar; VAR buffer_size: lg.gsize; type: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_save_to_bufferv*(pixbuf: PGdkPixbuf; VAR buffer: lg.Pgchar; VAR buffer_size: lg.gsize; type: lg.Pgchar; VAR option_keys: lg.Pgchar; VAR option_values: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_save_to_callback*(pixbuf: PGdkPixbuf; save_func: GdkPixbufSaveFunc; user_data: lg.gpointer; type: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_save_to_callbackv*(pixbuf: PGdkPixbuf; save_func: GdkPixbufSaveFunc; user_data: lg.gpointer; type: lg.Pgchar; VAR option_keys: lg.Pgchar; VAR option_values: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_savev*(pixbuf: PGdkPixbuf; filename: lg.Pgchar; type: lg.Pgchar; VAR option_keys: lg.Pgchar; VAR option_values: lg.Pgchar; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_scale*(pixbuf: PGdkPixbuf; dest: PGdkPixbuf; dest_x: lg.int; dest_y: lg.int; dest_width: lg.int; dest_height: lg.int; offset_x: lg.gdouble; offset_y: lg.gdouble; scale_x: lg.gdouble; scale_y: lg.gdouble; interp_type: GdkInterpType);
  PROCEDURE [ccall] gdk_pixbuf_scale_simple*(pixbuf: PGdkPixbuf; dest_width: lg.int; dest_height: lg.int; interp_type: GdkInterpType): PGdkPixbuf;

(* GdkPixbuf constructors *)
  PROCEDURE [ccall] gdk_pixbuf_new*(colorspace: GdkColorspace; has_alpha: lg.gboolean; bits_per_sample: lg.int; width: lg.int; height: lg.int): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_from_data*(data: lg.Pguchar; colorspace: GdkColorspace; has_alpha: lg.gboolean; bits_per_sample: lg.int; width: lg.int; height: lg.int; rowstride: lg.int; destroy_fn: GdkPixbufDestroyNotify; destroy_fn_data: lg.gpointer): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_from_file*(filename: lg.Pgchar; VAR error: lg.PGError): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_from_file_at_scale*(filename: lg.Pgchar; width: lg.int; height: lg.int; preserve_aspect_ratio: lg.gboolean; VAR error: lg.PGError): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_from_file_at_size*(filename: lg.Pgchar; width: lg.int; height: lg.int; VAR error: lg.PGError): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_from_inline*(data_length: lg.gint; data: lg.Pguint8; copy_pixels: lg.gboolean; VAR error: lg.PGError): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_from_xpm_data*(VAR data: lg.Pgchar): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_new_subpixbuf*(src_pixbuf: PGdkPixbuf; src_x: lg.int; src_y: lg.int; width: lg.int; height: lg.int): PGdkPixbuf;

(* GdkPixbufLoader methods *)
  PROCEDURE [ccall] gdk_pixbuf_loader_close*(pixbufloader: PGdkPixbufLoader; VAR error: lg.PGError): lg.gboolean;
  PROCEDURE [ccall] gdk_pixbuf_loader_get_animation*(pixbufloader: PGdkPixbufLoader): PGdkPixbufAnimation;
  PROCEDURE [ccall] gdk_pixbuf_loader_get_format*(pixbufloader: PGdkPixbufLoader): PGdkPixbufFormat;
  PROCEDURE [ccall] gdk_pixbuf_loader_get_pixbuf*(pixbufloader: PGdkPixbufLoader): PGdkPixbuf;
  PROCEDURE [ccall] gdk_pixbuf_loader_get_type*(): lo.GType;
  PROCEDURE [ccall] gdk_pixbuf_loader_set_size*(pixbufloader: PGdkPixbufLoader; width: lg.int; height: lg.int);
  PROCEDURE [ccall] gdk_pixbuf_loader_write*(pixbufloader: PGdkPixbufLoader; buf: lg.Pguchar; count: lg.gsize; VAR error: lg.PGError): lg.gboolean;

(* GdkPixbufLoader constructors *)
  PROCEDURE [ccall] gdk_pixbuf_loader_new*(): PGdkPixbufLoader;
  PROCEDURE [ccall] gdk_pixbuf_loader_new_with_mime_type*(mime_type: lg.Pgchar; VAR error: lg.PGError): PGdkPixbufLoader;
  PROCEDURE [ccall] gdk_pixbuf_loader_new_with_type*(image_type: lg.Pgchar; VAR error: lg.PGError): PGdkPixbufLoader;

(* GdkPixbufAniAnim methods *)
  PROCEDURE [ccall] gdk_pixbuf_ani_anim_get_type*(): lo.GType;

(* GdkPixbufGifAnim methods *)
  PROCEDURE [ccall] gdk_pixbuf_gif_anim_frame_composite*(pixbufgifanim: PGdkPixbufGifAnim; frame: PGdkPixbufFrame);
  PROCEDURE [ccall] gdk_pixbuf_gif_anim_get_type*(): lo.GType;

(* GdkPixbufSimpleAnim methods *)
  PROCEDURE [ccall] gdk_pixbuf_simple_anim_add_frame*(pixbufsimpleanim: PGdkPixbufSimpleAnim; pixbuf: PGdkPixbuf);
  PROCEDURE [ccall] gdk_pixbuf_simple_anim_get_type*(): lo.GType;
  PROCEDURE [ccall] gdk_pixbuf_simple_anim_iter_get_type*(): lo.GType;

(* GdkPixbufSimpleAnim constructors *)
  PROCEDURE [ccall] gdk_pixbuf_simple_anim_new*(width: lg.gint; height: lg.gint; rate: lg.gfloat): PGdkPixbufSimpleAnim;

(* GdkPixbufAniAnimIter methods *)
  PROCEDURE [ccall] gdk_pixbuf_ani_anim_iter_get_type*(): lo.GType;

(* GdkPixbufGifAnimIter methods *)
  PROCEDURE [ccall] gdk_pixbuf_gif_anim_iter_get_type*(): lo.GType;
END LibsGdkPixbuf.
