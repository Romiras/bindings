MODULE LibsPangoWin32 ["libpangowin32-1.0-0.dll"];

	IMPORT
		glib := LibsGlib,
		Lib := LibsPango,
		WinApi;
	
	PROCEDURE [ccall] pango_win32_get_context* () : Lib.PPangoContext;
	PROCEDURE [ccall] pango_win32_render* (hdc : WinApi.HDC; font : Lib.PPangoFont; glyphs : Lib.PPangoGlyphString; x : glib.gint; y : glib.gint);
	PROCEDURE [ccall] pango_win32_render_layout_line* (hdc : WinApi.HDC; line : Lib.PPangoLayoutLine; x : INTEGER; y : INTEGER);
	PROCEDURE [ccall] pango_win32_render_layout* (hdc : WinApi.HDC; layout : Lib.PPangoLayout; x : INTEGER; y : INTEGER);
	PROCEDURE [ccall] pango_win32_render_transformed* (hdc : WinApi.HDC; matrix : Lib.PPangoMatrix; font : Lib.PPangoFont; glyphs : Lib.PPangoGlyphString; x : INTEGER; 
				y : INTEGER);

	(* For shape engines *)
	PROCEDURE [ccall] pango_win32_get_unknown_glyph* (font : Lib.PPangoFont; wc : glib.gunichar) : Lib.PangoGlyph;
	PROCEDURE [ccall] pango_win32_font_get_glyph_index* (font : Lib.PPangoFont; wc : glib.gunichar) : glib.gint;
	PROCEDURE [ccall] pango_win32_get_dc* () : WinApi.HDC;
	PROCEDURE [ccall] pango_win32_get_debug_flag* () : glib.gboolean;

	PROCEDURE [ccall] pango_win32_font_cache_new* () : Lib.PPangoWin32FontCache;
	PROCEDURE [ccall] pango_win32_font_cache_free* (cache : Lib.PPangoWin32FontCache);
	PROCEDURE [ccall] pango_win32_font_cache_loadw* (cache : Lib.PPangoWin32FontCache; logfont : WinApi.PtrLOGFONTW) : WinApi.HFONT;
	PROCEDURE [ccall] pango_win32_font_cache_unload* (cache : Lib.PPangoWin32FontCache; hfont : WinApi.HFONT);
	PROCEDURE [ccall] pango_win32_font_map_for_display* () : Lib.PPangoFontMap;
	PROCEDURE [ccall] pango_win32_shutdown_display*;
	PROCEDURE [ccall] pango_win32_font_map_get_font_cache* (font_map : Lib.PPangoFontMap) : Lib.PPangoWin32FontCache;
	PROCEDURE [ccall] pango_win32_font_logfonwt* (font : Lib.PPangoFont) : WinApi.PtrLOGFONTW;

END LibsPangoWin32.