MODULE LibsPango ["libpango-1.0-0.dll"];
(*MODULE LibsPango ["libpango-1.0.so"];*)

	IMPORT
		SYSTEM,
		lg := LibsGlib,
		lo := LibsGObject;

	(* ==================================================================== ENUMS *)
	CONST
        PANGO_SCALE* = 1024;
    
		PANGO_ALIGN_LEFT* = 0;
		PANGO_ALIGN_CENTER* = 1;
		PANGO_ALIGN_RIGHT* = 2;
		
		PANGO_ATTR_INVALID* = 0;
		PANGO_ATTR_LANGUAGE* = 1;
		PANGO_ATTR_FAMILY* = 2;
		PANGO_ATTR_STYLE* = 3;
		PANGO_ATTR_WEIGHT* = 4;
		PANGO_ATTR_VARIANT* = 5;
		PANGO_ATTR_STRETCH* = 6;
		PANGO_ATTR_SIZE* = 7;
		PANGO_ATTR_FONT_DESC* = 8;
		PANGO_ATTR_FOREGROUND* = 9;
		PANGO_ATTR_BACKGROUND* = 10;
		PANGO_ATTR_UNDERLINE* = 11;
		PANGO_ATTR_STRIKETHROUGH* = 12;
		PANGO_ATTR_RISE* = 13;
		PANGO_ATTR_SHAPE* = 14;
		PANGO_ATTR_SCALE* = 15;
		PANGO_ATTR_FALLBACK* = 16;
		PANGO_ATTR_LETTER_SPACING* = 17;
		PANGO_ATTR_UNDERLINE_COLOR* = 18;
		PANGO_ATTR_STRIKETHROUGH_COLOR* = 19;
		PANGO_ATTR_ABSOLUTE_SIZE* = 20;

		PANGO_DIRECTION_LTR* = 0;
		PANGO_DIRECTION_RTL* = 1;
		PANGO_DIRECTION_TTB_LTR* = 2;
		PANGO_DIRECTION_TTB_RTL* = 3;
		PANGO_DIRECTION_WEAK_LTR* = 4;
		PANGO_DIRECTION_WEAK_RTL* = 5;
		PANGO_DIRECTION_NEUTRAL* = 6;

		PANGO_ELLIPSIZE_NONE* = 0;
		PANGO_ELLIPSIZE_START* = 1;
		PANGO_ELLIPSIZE_MIDDLE* = 2;
		PANGO_ELLIPSIZE_END* = 3;

		PANGO_FONT_MASK_FAMILY* = {0};
		PANGO_FONT_MASK_STYLE* = {1};
		PANGO_FONT_MASK_VARIANT* = {2};
		PANGO_FONT_MASK_WEIGHT* = {3};
		PANGO_FONT_MASK_STRETCH* = {4};
		PANGO_FONT_MASK_SIZE* = {5};
		
		PANGO_RENDER_PART_FOREGROUND* = 0;
		PANGO_RENDER_PART_BACKGROUND* = 1;
		PANGO_RENDER_PART_UNDERLINE* = 2;
		PANGO_RENDER_PART_STRIKETHROUGH* = 3;

		PANGO_COVERAGE_NONE* = 0;
		PANGO_COVERAGE_FALLBACK* = 1;
		PANGO_COVERAGE_APPROXIMATE* = 2;
		PANGO_COVERAGE_EXACT* = 3;

		PANGO_SCRIPT_INVALID_CODE* = -1;
		PANGO_SCRIPT_COMMON* = -1+1;
		PANGO_SCRIPT_INHERITED* = -1+2;
		PANGO_SCRIPT_ARABIC* = -1+3;
		PANGO_SCRIPT_ARMENIAN* = -1+4;
		PANGO_SCRIPT_BENGALI* = -1+5;
		PANGO_SCRIPT_BOPOMOFO* = -1+6;
		PANGO_SCRIPT_CHEROKEE* = -1+7;
		PANGO_SCRIPT_COPTIC* = -1+8;
		PANGO_SCRIPT_CYRILLIC* = -1+9;
		PANGO_SCRIPT_DESERET* = -1+10;
		PANGO_SCRIPT_DEVANAGARI* = -1+11;
		PANGO_SCRIPT_ETHIOPIC* = -1+12;
		PANGO_SCRIPT_GEORGIAN* = -1+13;
		PANGO_SCRIPT_GOTHIC* = -1+14;
		PANGO_SCRIPT_GREEK* = -1+15;
		PANGO_SCRIPT_GUJARATI* = -1+16;
		PANGO_SCRIPT_GURMUKHI* = -1+17;
		PANGO_SCRIPT_HAN* = -1+18;
		PANGO_SCRIPT_HANGUL* = -1+19;
		PANGO_SCRIPT_HEBREW* = -1+20;
		PANGO_SCRIPT_HIRAGANA* = -1+21;
		PANGO_SCRIPT_KANNADA* = -1+22;
		PANGO_SCRIPT_KATAKANA* = -1+23;
		PANGO_SCRIPT_KHMER* = -1+24;
		PANGO_SCRIPT_LAO* = -1+25;
		PANGO_SCRIPT_LATIN* = -1+26;
		PANGO_SCRIPT_MALAYALAM* = -1+27;
		PANGO_SCRIPT_MONGOLIAN* = -1+28;
		PANGO_SCRIPT_MYANMAR* = -1+29;
		PANGO_SCRIPT_OGHAM* = -1+30;
		PANGO_SCRIPT_OLD_ITALIC* = -1+31;
		PANGO_SCRIPT_ORIYA* = -1+32;
		PANGO_SCRIPT_RUNIC* = -1+33;
		PANGO_SCRIPT_SINHALA* = -1+34;
		PANGO_SCRIPT_SYRIAC* = -1+35;
		PANGO_SCRIPT_TAMIL* = -1+36;
		PANGO_SCRIPT_TELUGU* = -1+37;
		PANGO_SCRIPT_THAANA* = -1+38;
		PANGO_SCRIPT_THAI* = -1+39;
		PANGO_SCRIPT_TIBETAN* = -1+40;
		PANGO_SCRIPT_CANADIAN_ABORIGINAL* = -1+41;
		PANGO_SCRIPT_YI* = -1+42;
		PANGO_SCRIPT_TAGALOG* = -1+43;
		PANGO_SCRIPT_HANUNOO* = -1+44;
		PANGO_SCRIPT_BUHID* = -1+45;
		PANGO_SCRIPT_TAGBANWA* = -1+46;
		PANGO_SCRIPT_BRAILLE* = -1+47;
		PANGO_SCRIPT_CYPRIOT* = -1+48;
		PANGO_SCRIPT_LIMBU* = -1+49;
		PANGO_SCRIPT_OSMANYA* = -1+50;
		PANGO_SCRIPT_SHAVIAN* = -1+51;
		PANGO_SCRIPT_LINEAR_B* = -1+52;
		PANGO_SCRIPT_TAI_LE* = -1+53;
		PANGO_SCRIPT_UGARITIC* = -1+54;
		PANGO_SCRIPT_NEW_TAI_LUE* = -1+55;
		PANGO_SCRIPT_BUGINESE* = -1+56;
		PANGO_SCRIPT_GLAGOLITIC* = -1+57;
		PANGO_SCRIPT_TIFINAGH* = -1+58;
		PANGO_SCRIPT_SYLOTI_NAGRI* = -1+59;
		PANGO_SCRIPT_OLD_PERSIAN* = -1+60;
		PANGO_SCRIPT_KHAROSHTHI* = -1+61;

		PANGO_STRETCH_ULTRA_CONDENSED* = 0;
		PANGO_STRETCH_EXTRA_CONDENSED* = 1;
		PANGO_STRETCH_CONDENSED* = 2;
		PANGO_STRETCH_SEMI_CONDENSED* = 3;
		PANGO_STRETCH_NORMAL* = 4;
		PANGO_STRETCH_SEMI_EXPANDED* = 5;
		PANGO_STRETCH_EXPANDED* = 6;
		PANGO_STRETCH_EXTRA_EXPANDED* = 7;
		PANGO_STRETCH_ULTRA_EXPANDED* = 8;

		PANGO_STYLE_NORMAL* = 0;
		PANGO_STYLE_OBLIQUE* = 1;
		PANGO_STYLE_ITALIC* = 2;

		PANGO_TAB_LEFT* = 0;

		PANGO_UNDERLINE_NONE* = 0;
		PANGO_UNDERLINE_SINGLE* = 1;
		PANGO_UNDERLINE_DOUBLE* = 2;
		PANGO_UNDERLINE_LOW* = 3;
		PANGO_UNDERLINE_ERROR* = 4;

		PANGO_VARIANT_NORMAL* = 0;
		PANGO_VARIANT_SMALL_CAPS* = 1;

		PANGO_WEIGHT_ULTRALIGHT* = 200;
		PANGO_WEIGHT_LIGHT* = 300;
		PANGO_WEIGHT_NORMAL* = 400;
		PANGO_WEIGHT_SEMIBOLD* = 600;
		PANGO_WEIGHT_BOLD* = 700;
		PANGO_WEIGHT_ULTRABOLD* = 800;
		PANGO_WEIGHT_HEAVY* = 900;
		PANGO_WRAP_WORD* = 0;
		PANGO_WRAP_CHAR* = 1;
		PANGO_WRAP_WORD_CHAR* = 2;
		
		(* Flags of PangoAttrSize struct *)
		PangoAttrSize_flag0_absolute_Mask* = {0};
		
		(* Flags of PangoLogAttr struct *)
		PangoLogAttr_flag0_is_line_break_Mask* = {0};
		PangoLogAttr_flag0_is_mandatory_break_Mask* = {1};
		PangoLogAttr_flag0_is_char_break_Mask* = {2};
		PangoLogAttr_flag0_is_white_Mask* = {3};
		PangoLogAttr_flag0_is_cursor_position_Mask* = {4};
		PangoLogAttr_flag0_is_word_start_Mask* = {5};
		PangoLogAttr_flag0_is_word_end_Mask* = {6};
		PangoLogAttr_flag0_is_sentence_boundary_Mask* = {7};
		PangoLogAttr_flag0_is_sentence_start_Mask* = {8};
		PangoLogAttr_flag0_is_sentence_end_Mask* = {9};
		PangoLogAttr_flag0_backspace_deletes_character_Mask* = {10};

		(* Flags of PangoGlyphVisAttr struct *)
		PangoGlyphVisAttr_flag0_is_cluster_start_Mask* = {0};
	
	TYPE
		PangoAlignment* = lg.gint32;
		PangoAttrType* = lg.gint32;
		PangoCoverageLevel* = lg.gint32;
		PangoDirection* = lg.gint32;
		PangoEllipsizeMode* = lg.gint32;
		PangoFontMask* = SET;
		PangoRenderPart* = lg.gint32;
		PangoScript* = lg.gint32;
		PangoStretch* = lg.gint32;
		PangoStyle* = lg.gint32;
		PangoTabAlign* = lg.gint32;
		PangoUnderline* = lg.gint32;
		PangoVariant* = lg.gint32;
		PangoWeight* = lg.gint32;
		PangoWrapMode* = lg.gint32;

	(* ====================================================== POINTERS TO OBJECTS *)
		PPangoContext* = POINTER TO PangoContext;
		PPangoFont* = POINTER TO PangoFont;
		PPangoFontset* = POINTER TO PangoFontset;
		PPangoFontFace* = POINTER TO PangoFontFace;
		PPangoFontFamily* = POINTER TO PangoFontFamily;
		PPangoFontMap* = POINTER TO PangoFontMap;
		PPangoFT2FontMap* = POINTER TO PangoFT2FontMap;
		PPangoLayout* = POINTER TO PangoLayout;
		PPangoRenderer* = POINTER TO PangoRenderer;

	(* ====================================================== POINTERS TO STRUCTS *)
		PPangoAnalysis* = POINTER TO PangoAnalysis;
		PPangoAttrClass* = POINTER TO PangoAttrClass;
		PPangoAttrColor* = POINTER TO PangoAttrColor;
		PPangoAttrFloat* = POINTER TO PangoAttrFloat;
		PPangoAttrFontDesc* = POINTER TO PangoAttrFontDesc;
		PPangoAttrInt* = POINTER TO PangoAttrInt;
		PPangoAttrIterator* = POINTER TO PangoAttrIterator;
		PPangoAttrLanguage* = POINTER TO PangoAttrLanguage;
		PPangoAttrShape* = POINTER TO PangoAttrShape;
		PPangoAttrSize* = POINTER TO PangoAttrSize;
		PPangoAttrString* = POINTER TO PangoAttrString;
		PPangoAttribute* = POINTER TO PangoAttribute;
		PPangoCoverage* = POINTER TO PangoCoverage;
		PPangoEngineLang* = POINTER TO PangoEngineLang;
		PPangoEngineShape* = POINTER TO PangoEngineShape;
		PPangoGlyphGeometry* = POINTER TO PangoGlyphGeometry;
		PPangoGlyphInfo* = POINTER TO PangoGlyphInfo;
		PPangoGlyphItem* = POINTER TO PangoGlyphItem;
		PPangoGlyphVisAttr* = POINTER TO PangoGlyphVisAttr;
		PPangoLayoutRun* = POINTER TO PangoLayoutRun;
		PPangoLogAttr* = POINTER TO PangoLogAttr;
		PPangoRectangle* = POINTER TO PangoRectangle;
		PPangoScriptIter* = POINTER TO PangoScriptIter;
		PPangoWin32FontCache* = POINTER TO PangoWin32FontCache;

	(* ======================================================= POINTERS TO BOXEDS *)
		PPangoAttrList* = POINTER TO PangoAttrList;
		PPangoColor* = POINTER TO PangoColor;
		PPangoFontDescription* = POINTER TO PangoFontDescription;
		PPangoFontMetrics* = POINTER TO PangoFontMetrics;
		PPangoGlyphString* = POINTER TO PangoGlyphString;
		PPangoItem* = POINTER TO PangoItem;
		PPangoLanguage* = POINTER TO PangoLanguage;
		PPangoLayoutIter* = POINTER TO PangoLayoutIter;
		PPangoLayoutLine* = POINTER TO PangoLayoutLine;
		PPangoMatrix* = POINTER TO PangoMatrix;
		PPangoTabArray* = POINTER TO PangoTabArray;

	(* ================================================================== ALIASES *)
		PangoGlyph* = lg.guint32;
		PangoGlyphUnit* = lg.gint32;
		PFcPattern* = ANYPTR;
		PangoFcFontMap* = EXTENSIBLE RECORD END; (* Pango defines this as abstract record *)

	(* ================================================================ CALLBACKS *)
		Callback* = lo.GCallback;

		PangoAttrDataCopyFunc* = RECORD (Callback)
			callback*: PROCEDURE [ccall]
			(
				data: lg.gpointer
			): lg.gpointer;
		END;

		PangoAttrFilterFunc* = RECORD (Callback)
			callback*: PROCEDURE [ccall]
			(
				attribute: PPangoAttribute;
				data : lg.gpointer
			): lg.gboolean;
		END;

		PangoFT2SubstituteFunc* = RECORD (Callback)
			callback*: PROCEDURE [ccall]
			(
				pattern: PFcPattern;
				data : lg.gpointer
			);
		END;

		PangoFontsetForeachFunc* = RECORD (Callback)
			callback*: PROCEDURE [ccall]
			(
				fontset: PPangoFontset;
				font : PPangoFont;
				data : lg.gpointer
			): lg.gboolean;
		END;

	(* ================================================================== STRUCTS *)

	(* PangoAnalysis struct *)

		PangoAnalysis* = RECORD [noalign]
			shape_engine*: PPangoEngineShape;
			lang_engine* : PPangoEngineLang;
			font* : PPangoFont;
			level* : lg.guint8;
			language* : PPangoLanguage;
			extra_attrs* : lg.PGSList;
		END;

	(* PangoAttrClass struct *)

		PangoAttrClass* = RECORD [noalign]
			type*: PangoAttrType;
			copy* : PROCEDURE [ccall] (attr: PPangoAttribute): PPangoAttribute;
			destroy*: PROCEDURE [ccall] (attr: PPangoAttribute);
			equal* : PROCEDURE [ccall] (attr1, attr2: PPangoAttribute): lg.gboolean;
		END;

	(* PangoAttrColor struct *)

		PangoAttrColor* = RECORD [noalign]
			attr* : PangoAttribute;
			color*: PangoColor;
		END;

	(* PangoAttrFloat struct *)

		PangoAttrFloat* = RECORD [noalign]
			attr* : PangoAttribute;
			value*: lg.gdouble;
		END;

	(* PangoAttrFontDesc struct *)

		PangoAttrFontDesc* = RECORD [noalign]
			attr*: PangoAttribute;
			desc*: PPangoFontDescription;
		END;

	(* PangoAttrInt struct *)

		PangoAttrInt* = RECORD [noalign]
			attr* : PangoAttribute;
			value*: lg.int;
		END;

	(* PangoAttrIterator struct *)

		PangoAttrIterator* = RECORD [noalign]
		END;

	(* PangoAttrLanguage struct *)

		PangoAttrLanguage* = RECORD [noalign]
			attr* : PangoAttribute;
			value*: PPangoLanguage;
		END;

	(* PangoAttrShape struct *)

		PangoAttrShape* = RECORD [noalign]
			attr* : PangoAttribute;
			ink_rect* : PangoRectangle;
			logical_rect*: PangoRectangle;
			data* : lg.gpointer;
			copy_func* : PangoAttrDataCopyFunc;
			destroy_func*: lo.GDestroyNotify;
		END;


	(* PangoAttrSize struct *)

		PangoAttrSize* = RECORD [noalign]
			attr* : PangoAttribute;
			size* : lg.int;
			flag0*: SET;
		END;

	(* PangoAttrString struct *)

		PangoAttrString* = RECORD [noalign]
			attr* : PangoAttribute;
			value*: lg.Pgchar;
		END;

	(* PangoAttribute struct *)

		PangoAttribute* = RECORD [noalign]
			klass* : PPangoAttrClass;
			start_index*: lg.guint;
			end_index* : lg.guint;
		END;

	(* PangoCoverage struct *)

		PangoCoverage* = RECORD [noalign]
		END;

	(* PangoEngineLang struct *)

		PangoEngineLang* = RECORD [noalign]
		END;

	(* PangoEngineShape struct *)

		PangoEngineShape* = RECORD [noalign]
		END;

	(* PangoGlyphGeometry struct *)

		PangoGlyphGeometry* = RECORD [noalign]
			width* : PangoGlyphUnit;
			x_offset*: PangoGlyphUnit;
			y_offset*: PangoGlyphUnit;
		END;

	(* PangoGlyphInfo struct *)

		PangoGlyphInfo* = RECORD [noalign]
			glyph* : PangoGlyph;
			geometry*: PangoGlyphGeometry;
			attr* : PangoGlyphVisAttr;
		END;

	(* PangoGlyphItem struct *)

		PangoGlyphItem* = RECORD [noalign]
			item* : PPangoItem;
			glyphs*: PPangoGlyphString;
		END;

	(* PangoGlyphVisAttr struct *)

		PangoGlyphVisAttr* = RECORD [noalign]
			flag0*: SET;
		END;

	(* PangoLayoutRun struct *)

		PangoLayoutRun* = RECORD [noalign]
			item* : PPangoItem;
			glyphs*: PPangoGlyphString;
		END;

	(* PangoLogAttr struct *)

		PangoLogAttr* = RECORD [noalign]
			flag0*: SET;
		END;

	(* PangoRectangle struct *)

		PangoRectangle* = RECORD [noalign]
			x* : lg.int;
			y* : lg.int;
			width* : lg.int;
			height*: lg.int;
		END;

	(* PangoScriptIter struct *)

		PangoScriptIter* = RECORD [noalign]
		END;

	(* PangoWin32FontCache struct *)

		PangoWin32FontCache* = RECORD [noalign]
		END;

	(* =================================================================== BOXEDS *)

	(* PangoAttrList boxed *)

		PangoAttrList* = RECORD [noalign]
		END;

	(* PangoColor boxed *)

		PangoColor* = RECORD [noalign]
			red* : lg.guint16;
			green*: lg.guint16;
			blue* : lg.guint16;
		END;

	(* PangoFontDescription boxed *)

		PangoFontDescription* = RECORD [noalign]
		END;

	(* PangoFontMetrics boxed *)

		PangoFontMetrics* = RECORD [noalign]
			ref_count-: lg.guint;

			ascent-,
			descent-,
			approximate_char_width-,
			approximate_digit_width-,
			underline_position-,
			underline_thickness-,
			strikethrough_position-,
			strikethrough_thickness-: lg.gint
		END;

	(* PangoGlyphString boxed *)

		PangoGlyphString* = RECORD [noalign]
			num_glyphs* : lg.gint;
			glyphs* : PPangoGlyphInfo;
			log_clusters*: lg.Pgint;
			space* : lg.gint;
		END;

	(* PangoItem boxed *)

		PangoItem* = RECORD [noalign]
			offset* : lg.gint;
			length* : lg.gint;
			num_chars*: lg.gint;
			analysis* : PangoAnalysis;
		END;

	(* PangoLanguage boxed *)

		PangoLanguage* = RECORD [noalign]
		END;

	(* PangoLayoutIter boxed *)

		PangoLayoutIter* = RECORD [noalign]
		END;

	(* PangoLayoutLine boxed *)

		PangoLayoutLine* = RECORD [noalign]
			layout* : PPangoLayout;
			start_index* : lg.gint;
			length* : lg.gint;
			runs* : lg.PGSList;
			is_paragraph_start*: lg.guint;
			resolved_dir* : lg.guint;
		END;

	(* PangoMatrix boxed *)

		PangoMatrix* = RECORD [noalign]
			xx*: lg.gdouble;
			xy*: lg.gdouble;
			yx*: lg.gdouble;
			yy*: lg.gdouble;
			x0*: lg.gdouble;
			y0*: lg.gdouble;
		END;

	(* PangoTabArray boxed *)

		PangoTabArray* = RECORD [noalign]
		END;

	(* ================================================================= OBJECTS *)

	(* PangoContext object *)

		PangoContext* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoFont object *)

		PangoFont* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoFontset object *)

		PangoFontset* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoFontFace object *)

		PangoFontFace* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoFontFamily object *)

		PangoFontFamily* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoFontMap object *)

		PangoFontMap* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoFT2FontMap object *)

		PangoFT2FontMap* = EXTENSIBLE RECORD [noalign] (PangoFcFontMap)
		END;

	(* PangoLayout object *)

		PangoLayout* = EXTENSIBLE RECORD [noalign] (lo.GObject)
		END;

	(* PangoRenderer object *)

		PangoRenderer* = EXTENSIBLE RECORD [noalign] (lo.GObject)
			underline* : PangoUnderline;
			strikethrough* : lg.gboolean;
			active_count* : lg.int;
			matrix* : PPangoMatrix;
			PangoRenderer_pd*: lg.gpointer (* private_data *);
		END;

(* PangoContext methods *)
	PROCEDURE [ccall] pango_context_get_base_dir*(context: PPangoContext): PangoDirection;
	PROCEDURE [ccall] pango_context_get_font_description*(context: PPangoContext): PPangoFontDescription;
	PROCEDURE [ccall] pango_context_get_font_map*(context: PPangoContext): PPangoFontMap;
	PROCEDURE [ccall] pango_context_get_language*(context: PPangoContext): PPangoLanguage;
	PROCEDURE [ccall] pango_context_get_matrix*(context: PPangoContext): PPangoMatrix;
	PROCEDURE [ccall] pango_context_get_metrics*(context: PPangoContext; desc: PPangoFontDescription; language: PPangoLanguage): PPangoFontMetrics;
	PROCEDURE [ccall] pango_context_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_context_list_families*(context: PPangoContext; VAR families: POINTER [untagged] TO ARRAY OF PPangoFontFamily; n_families: lg.Pint);
	PROCEDURE [ccall] pango_context_load_font*(context: PPangoContext; desc: PPangoFontDescription): PPangoFont;
	PROCEDURE [ccall] pango_context_load_fontset*(context: PPangoContext; desc: PPangoFontDescription; language: PPangoLanguage): PPangoFontset;
	PROCEDURE [ccall] pango_context_set_base_dir*(context: PPangoContext; direction: PangoDirection);
	PROCEDURE [ccall] pango_context_set_font_description*(context: PPangoContext; desc: PPangoFontDescription);
	PROCEDURE [ccall] pango_context_set_language*(context: PPangoContext; language: PPangoLanguage);
	PROCEDURE [ccall] pango_context_set_matrix*(context: PPangoContext; matrix: PPangoMatrix);

(* PangoFont methods *)
	PROCEDURE [ccall] pango_font_describe*(font: PPangoFont): PPangoFontDescription;
	
	PROCEDURE [ccall] pango_font_description_new*(VAR descs: PPangoFontDescription);
	PROCEDURE [ccall] pango_font_description_from_string* (IN str: ARRAY [untagged] OF SHORTCHAR): PPangoFontDescription;
	PROCEDURE [ccall] pango_font_description_free*(descs: PPangoFontDescription);
	PROCEDURE [ccall] pango_font_descriptions_free*(VAR descs: POINTER [untagged] TO ARRAY OF PangoFontDescription; n_descs: lg.int);
	PROCEDURE [ccall] pango_font_description_set_family*(descs: PPangoFontDescription;
		IN family: ARRAY [untagged] OF SHORTCHAR);
	PROCEDURE [ccall] pango_font_description_set_style*(descs: PPangoFontDescription;
		style: PangoStyle);
	PROCEDURE [ccall] pango_font_description_set_weight*(descs: PPangoFontDescription;
		weight: PangoWeight);
	PROCEDURE [ccall] pango_font_description_set_size*(descs: PPangoFontDescription;
		size: lg.gint);
	
	PROCEDURE [ccall] pango_font_find_shaper*(font: PPangoFont; language: PPangoLanguage; ch: lg.guint32): PPangoEngineShape;
	PROCEDURE [ccall] pango_font_get_coverage*(font: PPangoFont; language: PPangoLanguage): PPangoCoverage;
	PROCEDURE [ccall] pango_font_get_font_map*(font: PPangoFont): PPangoFontMap;
	PROCEDURE [ccall] pango_font_get_glyph_extents*(font: PPangoFont; glyph: PangoGlyph; ink_rect: PPangoRectangle; logical_rect: PPangoRectangle);
	PROCEDURE [ccall] pango_font_get_metrics*(font: PPangoFont; language: PPangoLanguage): PPangoFontMetrics;
	PROCEDURE [ccall] pango_font_get_type*(): lo.GType;

(* PangoFontset methods *)
	PROCEDURE [ccall] pango_fontset_foreach*(fontset: PPangoFontset; func: PangoFontsetForeachFunc; data: lg.gpointer);
	PROCEDURE [ccall] pango_fontset_get_font*(fontset: PPangoFontset; wc: lg.guint): PPangoFont;
	PROCEDURE [ccall] pango_fontset_get_metrics*(fontset: PPangoFontset): PPangoFontMetrics;
	PROCEDURE [ccall] pango_fontset_get_type*(): lo.GType;

(* PangoFontFace methods *)
	PROCEDURE [ccall] pango_font_face_describe*(fontface: PPangoFontFace): PPangoFontDescription;
	PROCEDURE [ccall] pango_font_face_get_face_name*(fontface: PPangoFontFace): lg.Pgchar;
	PROCEDURE [ccall] pango_font_face_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_font_face_list_sizes*(fontface: PPangoFontFace; VAR sizes: POINTER [untagged] TO ARRAY OF lg.int; n_sizes: lg.Pint);

(* PangoFontFamily methods *)
	PROCEDURE [ccall] pango_font_family_get_name*(fontfamily: PPangoFontFamily): lg.Pgchar;
	PROCEDURE [ccall] pango_font_family_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_font_family_is_monospace*(fontfamily: PPangoFontFamily): lg.gboolean;
	PROCEDURE [ccall] pango_font_family_list_faces*(fontfamily: PPangoFontFamily; VAR faces: POINTER [untagged] TO ARRAY OF PPangoFontFace; n_faces: lg.Pint);

(* PangoFontMap methods *)
	PROCEDURE [ccall] pango_font_map_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_font_map_list_families*(fontmap: PPangoFontMap; VAR families: POINTER [untagged] TO ARRAY OF PPangoFontFamily; n_families: lg.Pint);
	PROCEDURE [ccall] pango_font_map_load_font*(fontmap: PPangoFontMap; context: PPangoContext; desc: PPangoFontDescription): PPangoFont;
	PROCEDURE [ccall] pango_font_map_load_fontset*(fontmap: PPangoFontMap; context: PPangoContext; desc: PPangoFontDescription; language: PPangoLanguage): PPangoFontset;

(* PangoFT2FontMap methods *)
	PROCEDURE [ccall] pango_ft2_font_map_create_context*(ft2fontmap: PPangoFT2FontMap): PPangoContext;
	PROCEDURE [ccall] pango_ft2_font_map_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_ft2_font_map_set_default_substitute*(ft2fontmap: PPangoFT2FontMap; func: PangoFT2SubstituteFunc; data: lg.gpointer; notify: lo.GDestroyNotify);
	PROCEDURE [ccall] pango_ft2_font_map_set_resolution*(ft2fontmap: PPangoFT2FontMap; dpi_x: lg.gdouble; dpi_y: lg.gdouble);
	PROCEDURE [ccall] pango_ft2_font_map_substitute_changed*(ft2fontmap: PPangoFT2FontMap);

(* PangoFT2FontMap constructors *)
	PROCEDURE [ccall] pango_ft2_font_map_new*(): PPangoFT2FontMap;

(* PangoLayout methods *)
	PROCEDURE [ccall] pango_layout_context_changed*(layout: PPangoLayout);
	PROCEDURE [ccall] pango_layout_copy*(layout: PPangoLayout): PPangoLayout;
	PROCEDURE [ccall] pango_layout_get_alignment*(layout: PPangoLayout): PangoAlignment;
	PROCEDURE [ccall] pango_layout_get_attributes*(layout: PPangoLayout): PPangoAttrList;
	PROCEDURE [ccall] pango_layout_get_auto_dir*(layout: PPangoLayout): lg.gboolean;
	PROCEDURE [ccall] pango_layout_get_context*(layout: PPangoLayout): PPangoContext;
	PROCEDURE [ccall] pango_layout_get_cursor_pos*(layout: PPangoLayout; index_: lg.int; strong_pos: PPangoRectangle; weak_pos: PPangoRectangle);
	PROCEDURE [ccall] pango_layout_get_ellipsize*(layout: PPangoLayout): PangoEllipsizeMode;
	PROCEDURE [ccall] pango_layout_get_extents*(layout: PPangoLayout; ink_rect: PPangoRectangle; logical_rect: PPangoRectangle);
	PROCEDURE [ccall] pango_layout_get_font_description*(layout: PPangoLayout): PPangoFontDescription;
	PROCEDURE [ccall] pango_layout_get_indent*(layout: PPangoLayout): lg.int;
	PROCEDURE [ccall] pango_layout_get_iter*(layout: PPangoLayout): PPangoLayoutIter;
	PROCEDURE [ccall] pango_layout_get_justify*(layout: PPangoLayout): lg.gboolean;
	PROCEDURE [ccall] pango_layout_get_line*(layout: PPangoLayout; line: lg.int): PPangoLayoutLine;
	PROCEDURE [ccall] pango_layout_get_line_count*(layout: PPangoLayout): lg.int;
	PROCEDURE [ccall] pango_layout_get_lines*(layout: PPangoLayout): lg.PGSList;
	PROCEDURE [ccall] pango_layout_get_log_attrs*(layout: PPangoLayout; VAR attrs: POINTER [untagged] TO ARRAY OF PangoLogAttr; n_attrs: lg.Pgint);
	PROCEDURE [ccall] pango_layout_get_pixel_extents*(layout: PPangoLayout; ink_rect: PPangoRectangle; logical_rect: PPangoRectangle);
	PROCEDURE [ccall] pango_layout_get_pixel_size*(layout: PPangoLayout; VAR width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] pango_layout_get_single_paragraph_mode*(layout: PPangoLayout): lg.gboolean;
	PROCEDURE [ccall] pango_layout_get_size*(layout: PPangoLayout; VAR width: lg.Pgint; height: lg.Pgint);
	PROCEDURE [ccall] pango_layout_get_spacing*(layout: PPangoLayout): lg.int;
	PROCEDURE [ccall] pango_layout_get_tabs*(layout: PPangoLayout): PPangoTabArray;
	PROCEDURE [ccall] pango_layout_get_text*(layout: PPangoLayout): lg.Pgchar;
	PROCEDURE [ccall] pango_layout_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_layout_get_width*(layout: PPangoLayout): lg.int;
	PROCEDURE [ccall] pango_layout_get_wrap*(layout: PPangoLayout): PangoWrapMode;
	PROCEDURE [ccall] pango_layout_index_to_pos*(layout: PPangoLayout; index_: lg.int; pos: PPangoRectangle);
	PROCEDURE [ccall] pango_layout_move_cursor_visually*(layout: PPangoLayout; strong: lg.gboolean; old_index: lg.int; old_trailing: lg.int; direction: lg.int; new_index: lg.Pint; new_trailing: lg.Pint);
	PROCEDURE [ccall] pango_layout_set_alignment*(layout: PPangoLayout; alignment: PangoAlignment);
	PROCEDURE [ccall] pango_layout_set_attributes*(layout: PPangoLayout; attrs: PPangoAttrList);
	PROCEDURE [ccall] pango_layout_set_auto_dir*(layout: PPangoLayout; auto_dir: lg.gboolean);
	PROCEDURE [ccall] pango_layout_set_ellipsize*(layout: PPangoLayout; ellipsize: PangoEllipsizeMode);
	PROCEDURE [ccall] pango_layout_set_font_description*(layout: PPangoLayout; desc: PPangoFontDescription);
	PROCEDURE [ccall] pango_layout_set_indent*(layout: PPangoLayout; indent: lg.int);
	PROCEDURE [ccall] pango_layout_set_justify*(layout: PPangoLayout; justify: lg.gboolean);
	PROCEDURE [ccall] pango_layout_set_markup*(layout: PPangoLayout; markup: lg.Pgchar; length: lg.int);
	PROCEDURE [ccall] pango_layout_set_markup_with_accel*(layout: PPangoLayout; markup: lg.Pgchar; length: lg.int; accel_marker: lg.gunichar; accel_char: lg.Pgunichar);
	PROCEDURE [ccall] pango_layout_set_single_paragraph_mode*(layout: PPangoLayout; setting: lg.gboolean);
	PROCEDURE [ccall] pango_layout_set_spacing*(layout: PPangoLayout; spacing: lg.int);
	PROCEDURE [ccall] pango_layout_set_tabs*(layout: PPangoLayout; tabs: PPangoTabArray);
	PROCEDURE [ccall] pango_layout_set_text*(layout: PPangoLayout; text: lg.Pgchar; length: lg.int);
	PROCEDURE [ccall] pango_layout_set_width*(layout: PPangoLayout; width: lg.int);
	PROCEDURE [ccall] pango_layout_set_wrap*(layout: PPangoLayout; wrap: PangoWrapMode);
	PROCEDURE [ccall] pango_layout_xy_to_index*(layout: PPangoLayout; x: lg.int; y: lg.int; index_: lg.Pint; trailing: lg.Pint): lg.gboolean;

(* PangoLayout constructors *)
	PROCEDURE [ccall] pango_layout_new*(context: PPangoContext): PPangoLayout;

(* PangoRenderer methods *)
	PROCEDURE [ccall] pango_renderer_activate*(renderer: PPangoRenderer);
	PROCEDURE [ccall] pango_renderer_deactivate*(renderer: PPangoRenderer);
	PROCEDURE [ccall] pango_renderer_draw_error_underline*(renderer: PPangoRenderer; x: lg.int; y: lg.int; width: lg.int; height: lg.int);
	PROCEDURE [ccall] pango_renderer_draw_glyph*(renderer: PPangoRenderer; font: PPangoFont; glyph: PangoGlyph; x: lg.gdouble; y: lg.gdouble);
	PROCEDURE [ccall] pango_renderer_draw_glyphs*(renderer: PPangoRenderer; font: PPangoFont; glyphs: PPangoGlyphString; x: lg.int; y: lg.int);
	PROCEDURE [ccall] pango_renderer_draw_layout*(renderer: PPangoRenderer; layout: PPangoLayout; x: lg.int; y: lg.int);
	PROCEDURE [ccall] pango_renderer_draw_layout_line*(renderer: PPangoRenderer; line: PPangoLayoutLine; x: lg.int; y: lg.int);
	PROCEDURE [ccall] pango_renderer_draw_rectangle*(renderer: PPangoRenderer; part: PangoRenderPart; x: lg.int; y: lg.int; width: lg.int; height: lg.int);
	PROCEDURE [ccall] pango_renderer_draw_trapezoid*(renderer: PPangoRenderer; part: PangoRenderPart; y1_: lg.gdouble; x11: lg.gdouble; x21: lg.gdouble; y2: lg.gdouble; x12: lg.gdouble; x22: lg.gdouble);
	PROCEDURE [ccall] pango_renderer_get_color*(renderer: PPangoRenderer; part: PangoRenderPart): PPangoColor;
	PROCEDURE [ccall] pango_renderer_get_matrix*(renderer: PPangoRenderer): PPangoMatrix;
	PROCEDURE [ccall] pango_renderer_get_type*(): lo.GType;
	PROCEDURE [ccall] pango_renderer_part_changed*(renderer: PPangoRenderer; part: PangoRenderPart);
	PROCEDURE [ccall] pango_renderer_set_color*(renderer: PPangoRenderer; part: PangoRenderPart; color: PPangoColor);
	PROCEDURE [ccall] pango_renderer_set_matrix*(renderer: PPangoRenderer; matrix: PPangoMatrix);

END LibsPango.
