MODULE LinLibcStuff;

	IMPORT Libc := LinLibc;

	PROCEDURE errno* (): INTEGER;
		VAR errAddr: Libc.IntPtr;
	BEGIN
		errAddr := Libc.__errno_location();
		RETURN errAddr[0]
	END errno;

END LinLibcStuff.
