MODULE LinNet ["libc.so.6"];

	(*
		A. V. Shiryaev, 2012.11

		GNU/Linux
		32-bit
	*)

	IMPORT Libc := LinLibc;

	CONST
		INVALID_SOCKET* = -1;
		SOCKET_ERROR* = -1;

		(* /usr/include/i386-linux-gnu/bits/socket.h *)
			SHUT_RD* = 0;
			SHUT_WR* = 1;
			SHUT_RDWR* = 2;
			SOCK_STREAM* = 1;
			SOCK_DGRAM* = 2;
			SOCK_RAW* = 3;
			SOCK_RDM* = 4;
			SOCK_SEQPACKET* = 5;
			SOCK_DCCP* = 6;
			SOCK_PACKET* = 10;

			PF_UNSPEC* = 0; (* Unspecified. *)
			PF_LOCAL* = 1; (* Local to host (pipes and file-domain). *)
			PF_UNIX* = PF_LOCAL; (* POSIX name for PF_LOCAL. *)
			PF_FILE* = PF_LOCAL; (* Another non-standard name for PF_LOCAL. *)
			PF_INET* = 2; (* IP protocol family. *)
			PF_AX25* = 3; (* Amateur Radio AX.25. *)
			PF_IPX* = 4; (* Novell Internet Protocol. *)
			PF_APPLETALK* = 5; (* Appletalk DDP. *)
			PF_NETROM* = 6; (* Amateur radio NetROM. *)
			PF_BRIDGE* = 7; (* Multiprotocol bridge. *)
			PF_ATMPVC* = 8; (* ATM PVCs. *)
			PF_X25* = 9; (* Reserved for X.25 project. *)
			PF_INET6* = 10; (* IP version 6. *)
			PF_ROSE* = 11; (* Amateur Radio X.25 PLP. *)
			PF_DECnet* = 12; (* Reserved for DECnet project. *)
			PF_NETBEUI* = 13; (* Reserved for 802.2LLC project. *)
			PF_SECURITY* = 14; (* Security callback pseudo AF. *)
			PF_KEY* = 15; (* PF_KEY key management API. *)
			PF_NETLINK* = 16;
			PF_ROUTE* = PF_NETLINK; (* Alias to emulate 4.4BSD. *)
			PF_PACKET* = 17; (* Packet family. *)
			PF_ASH* = 18; (* Ash. *)
			PF_ECONET* = 19; (* Acorn Econet. *)
			PF_ATMSVC* = 20; (* ATM SVCs. *)
			PF_RDS* = 21; (* RDS sockets. *)
			PF_SNA* = 22; (* Linux SNA Project *)
			PF_IRDA* = 23; (* IRDA sockets. *)
			PF_PPPOX* = 24; (* PPPoX sockets. *)
			PF_WANPIPE* = 25; (* Wanpipe API sockets. *)
			PF_LLC* = 26; (* Linux LLC. *)
			PF_CAN* = 29; (* Controller Area Network. *)
			PF_TIPC* = 30; (* TIPC sockets. *)
			PF_BLUETOOTH* = 31; (* Bluetooth sockets. *)
			PF_IUCV* = 32; (* IUCV sockets. *)
			PF_RXRPC* = 33; (* RxRPC sockets. *)
			PF_ISDN* = 34; (* mISDN sockets. *)
			PF_PHONET* = 35; (* Phonet sockets. *)
			PF_IEEE802154* = 36; (* IEEE 802.15.4 sockets. *)
			PF_CAIF* = 37; (* CAIF sockets. *)
			PF_ALG* = 38; (* Algorithm sockets. *)
			PF_NFC* = 39; (* NFC sockets. *)
			PF_MAX* = 40; (* For now.. *)
			SOMAXCONN* = 128;

		(* /usr/include/asm-generic/socket.h *)
			SO_DEBUG* = {0};
			SO_REUSEADDR* = {1};
			SO_TYPE* = {0,1};
			SO_ERROR* = {2};
			SO_DONTROUTE* = {0,2};
			SO_BROADCAST* = {1,2};
			SO_SNDBUF* = {0..2};
			SO_RCVBUF* = {3};
			SO_SNDBUFFORCE* = {5};
			SO_RCVBUFFORCE* = {0,5};
			SO_KEEPALIVE* = {0,3};
			SO_OOBINLINE* = {1,3};
			SO_NO_CHECK* = {0,1,3};
			SO_PRIORITY* = {2,3};
			SO_LINGER* = {0,2,3};
			SO_BSDCOMPAT* = {1..3};
			SO_SECURITY_AUTHENTICATION* = {1,2,4};
			SO_SECURITY_ENCRYPTION_TRANSPORT* = {0..2,4};
			SO_SECURITY_ENCRYPTION_NETWORK* = {3,4};
			SO_BINDTODEVICE* = {0,3,4};
			SO_ATTACH_FILTER* = {1,3,4};
			SO_DETACH_FILTER* = {0,1,3,4};
			SO_PEERNAME* = {2..4};
			SO_TIMESTAMP* = {0,2..4};
			SO_ACCEPTCONN* = {1..4};
			SO_PEERSEC* = {0..4};
			SO_PASSSEC* = {1,5};
			SO_TIMESTAMPNS* = {0,1,5};
			SO_MARK* = {2,5};
			SO_TIMESTAMPING* = {0,2,5};
			SO_PROTOCOL* = {1,2,5};
			SO_DOMAIN* = {0..2,5};
			SO_RXQ_OVFL* = {3,5};

		(* /usr/include/netinet/in.h *)
			INADDR_NONE* = -1;
			IPPROTO_IP = 0; (* Dummy protocol for TCP. *)
			IPPROTO_HOPOPTS* = 0; (* IPv6 Hop-by-Hop options. *)
			IPPROTO_ICMP* = 1; (* Internet Control Message Protocol. *)
			IPPROTO_IGMP* = 2; (* Internet Group Management Protocol. *)
			IPPROTO_IPIP* = 4; (* IPIP tunnels (older KA9Q tunnels use 94). *)
			IPPROTO_TCP* = 6; (* Transmission Control Protocol. *)
			IPPROTO_EGP* = 8; (* Exterior Gateway Protocol. *)
			IPPROTO_PUP* = 12; (* PUP protocol. *)
			IPPROTO_UDP* = 17; (* User Datagram Protocol. *)
			IPPROTO_IDP* = 22; (* XNS IDP protocol. *)
			IPPROTO_TP* = 29; (* SO Transport Protocol Class 4. *)
			IPPROTO_DCCP* = 33; (* Datagram Congestion Control Protocol. *)
			IPPROTO_IPV6* = 41; (* IPv6 header. *)
			IPPROTO_ROUTING* = 43; (* IPv6 routing header. *)
			IPPROTO_FRAGMENT* = 44; (* IPv6 fragmentation header. *)
			IPPROTO_RSVP* = 46; (* Reservation Protocol. *)
			IPPROTO_GRE* = 47; (* General Routing Encapsulation. *)
			IPPROTO_ESP* = 50; (* encapsulating security payload. *)
			IPPROTO_AH* = 51; (* authentication header. *)
			IPPROTO_ICMPV6* = 58; (* ICMPv6. *)
			IPPROTO_NONE* = 59; (* IPv6 no next header. *)
			IPPROTO_DSTOPTS* = 60; (* IPv6 destination options. *)
			IPPROTO_MTP* = 92; (* Multicast Transport Protocol. *)
			IPPROTO_ENCAP* = 98; (* Encapsulation Header. *)
			IPPROTO_PIM* = 103; (* Protocol Independent Multicast. *)
			IPPROTO_COMP* = 108; (* Compression Header Protocol. *)
			IPPROTO_SCTP* = 132; (* Stream Control Transmission Protocol. *)
			IPPROTO_UDPLITE* = 136; (* UDP-Lite protocol. *)
			IPPROTO_RAW* = 255; (* Raw IP packets. *)

		(* /usr/include/asm-generic/param.h *)
			MAXHOSTNAMELEN* = 64; (* max length of hostname *)

		(* /usr/include/netdb.h *)
			NETDB_INTERNAL* = -1; (* See errno. *)
			NETDB_SUCCESS* = 0; (* No problem. *)
			HOST_NOT_FOUND* = 1; (* Authoritative Answer Host not found. *)
			TRY_AGAIN* = 2; (* Non-Authoritative Host not found, or SERVERFAIL. *)
			NO_RECOVERY* = 3; (* Non recoverable errors, FORMERR, REFUSED, NOTIMP. *)
			NO_DATA* = 4; (* Valid name, no data record of requested type. *)

		(* /usr/include/i386-linux-gnu/bits/typesizes.h *)
			__FD_SETSIZE = 1024;

	TYPE
		SOCKET* = INTEGER;

		(* /usr/include/i386-linux-gnu/bits/types.h *)
			socklen_t* = INTEGER;
		(* /usr/include/i386-linux-gnu/bits/sockaddr.h *)
			sa_family_t* = SHORTINT; (* unsigned short int *)
		(* /usr/include/netinet/in.h *)
			in_addr_t* = INTEGER;
			in_port_t* = SHORTINT;

		(* /usr/include/i386-linux-gnu/bits/socket.h *)
			sockaddr* = RECORD [untagged]
				sa_family: sa_family_t;
				sa_data: ARRAY [untagged] 14 OF SHORTCHAR;
			END;

		(* /usr/include/linux/in.h *)
(*
			in_addr* = INTEGER;
*)
			in_addr* = RECORD [untagged]
				S_un*: RECORD [union]
					S_un_b*: RECORD [untagged]
						s_b1*: SHORTCHAR;
						s_b2*: SHORTCHAR;
						s_b3*: SHORTCHAR;
						s_b4*: SHORTCHAR;
					END;
					S_un_w*: RECORD [untagged]
						s_w1*: SHORTINT;
						s_w2*: SHORTINT;
					END;
					S_addr*: in_addr_t;
				END;
			END;

		(* /usr/include/linux/in.h *)
			sockaddr_in* = RECORD [untagged]
				sin_family*: sa_family_t; (* address family *)
				sin_port*: SHORTINT; (* port number *)
				sin_addr*: in_addr; (* internet address *)
				__pad: ARRAY [untagged] 8 OF SHORTCHAR;
			END;

		(* /usr/include/netdb.h *)
			Ptrhostent* = POINTER TO hostent;
			hostent* = RECORD [untagged]
				h_name*: Libc.PtrSTR; (* official name of host *)
				h_aliases*: POINTER TO ARRAY [untagged] OF Libc.PtrSTR; (* alias list *)
				h_addrtype*: INTEGER; (* host address type *)
				h_length*: INTEGER; (* length of address *)
				h_addr_list*: POINTER TO ARRAY [untagged] OF POINTER TO ARRAY [untagged] OF in_addr; (* list of addresses from name server *)
			END;

		(* /usr/include/linux/time.h *)
		(* /usr/include/asm-generic/posix_types.h *)
			timeval* = RECORD [untagged]
				tv_sec*: INTEGER; (* seconds *)
				tv_usec*: INTEGER; (* microseconds *)
			END;

		(* /usr/include/i386-linux-gnu/sys/select.h *)
			__fd_mask = SET;
			fd_set* = ARRAY [untagged] __FD_SETSIZE DIV (SIZE(__fd_mask) * 8) OF __fd_mask;

	VAR
		h_errno*: INTEGER;

	PROCEDURE [ccall] socket* (domain: INTEGER; type: INTEGER; protocol: INTEGER): SOCKET;
	PROCEDURE [ccall] accept* (sockfd: SOCKET; VAR addr: sockaddr; VAR addrlen: socklen_t): SOCKET;
	PROCEDURE [ccall] bind* (sockfd: SOCKET; VAR addr: sockaddr; addrlen: socklen_t): INTEGER;
	PROCEDURE [ccall] connect* (sockfd: SOCKET; VAR addr: sockaddr; addrlen: socklen_t): INTEGER;
	PROCEDURE [ccall] listen* (sockfd: SOCKET; backlog: INTEGER): INTEGER;
	PROCEDURE [ccall] recv* (soskfd: SOCKET; buf: Libc.PtrVoid; len: Libc.size_t; flags: SET): Libc.ssize_t;
	PROCEDURE [ccall] send* (sockfd: SOCKET; buf: Libc.PtrVoid; len: Libc.size_t; flags: SET): Libc.ssize_t;
	PROCEDURE [ccall] shutdown* (sockfd: SOCKET; how: INTEGER): INTEGER;
	PROCEDURE [ccall] getsockopt* (sockfd: SOCKET; level: INTEGER; optname: SET; optval: Libc.PtrVoid; VAR optlen: socklen_t): INTEGER;
	PROCEDURE [ccall] setsockopt* (sockfd: SOCKET; level: INTEGER; optname: SET; optval: Libc.PtrVoid; optlen: socklen_t): INTEGER;

	PROCEDURE [ccall] htons* (hostshort: SHORTINT): SHORTINT;

	PROCEDURE [ccall] gethostbyname* (name: Libc.PtrSTR): Ptrhostent;
	PROCEDURE [ccall] inet_addr* (cp: Libc.PtrSTR): in_addr_t;

	PROCEDURE [ccall] getsockname* (sockfd: SOCKET; VAR addr: sockaddr; VAR addrlen: socklen_t): INTEGER;

	PROCEDURE [ccall] hstrerror* (err: INTEGER): Libc.PtrSTR;

(*
	PROCEDURE FD_ZERO (VAR set: Net.fd_set);
		VAR i: INTEGER;
	BEGIN
		i := LEN(set); REPEAT DEC(i); set[i] := {} UNTIL i = 0
	END FD_ZERO;

	PROCEDURE FD_SET (fd: Net.SOCKET; VAR set: Net.fd_set);
	BEGIN
		INCL(set[fd DIV 32], fd MOD 32)
	END FD_SET;
*)

	PROCEDURE [ccall] select* (nfds: INTEGER; VAR [nil] readfds: fd_set; VAR [nil] writefds: fd_set; VAR [nil] exceptfds: fd_set; VAR timeout: timeval): INTEGER;

END LinNet.
