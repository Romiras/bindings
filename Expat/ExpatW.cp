MODULE ExpatW ["libexpatw.so"]; (* UNICODE  Information is UTF-16 encoded. *)
(*MODULE ExpatW ["libexpatw.dll"]; (* UNICODE *) *)

TYPE
  TXMLLChar = WideChar;
  PXMLLChar = PWideChar;
  WideChar* = CHAR;
  PWideChar* = POINTER TO ARRAY [untagged] OF WideChar;

END ExpatW.