MODULE Expat ["libexpat.so"];
(*MODULE Expat ["libexpat.dll"];*)

(* Information is UTF-8 encoded. *)

IMPORT SYSTEM;

CONST
    XML_STATUS_ERROR* = 0;
    XML_STATUS_OK* = 1;
    XML_STATUS_SUSPENDED* = 2;

    XML_CTYPE_ILLEGAL =  0; (* dummy *)
    XML_CTYPE_EMPTY* = 1;
    XML_CTYPE_ANY* = 2;
    XML_CTYPE_MIXED* = 3;
    XML_CTYPE_NAME* = 4;
    XML_CTYPE_CHOICE* = 5;
    XML_CTYPE_SEQ* = 6;

    XML_CQUANT_NONE* = 0;
    XML_CQUANT_OPT* = 1;
    XML_CQUANT_REP* = 2;
    XML_CQUANT_PLUS* =  3;

    XML_ERROR_NONE* = 0;
    XML_ERROR_NO_MEMORY* = 1;
    XML_ERROR_SYNTAX* = 2;
    XML_ERROR_NO_ELEMENTS* = 3;
    XML_ERROR_INVALID_TOKEN* = 4;
    XML_ERROR_UNCLOSED_TOKEN* = 5;
    XML_ERROR_PARTIAL_CHAR* = 6;
    XML_ERROR_TAG_MISMATCH* = 7;
    XML_ERROR_DUPLICATE_ATTRIBUTE* = 8;
    XML_ERROR_JUNK_AFTER_DOC_ELEMENT* = 9;
    XML_ERROR_PARAM_ENTITY_REF* = 10;
    XML_ERROR_UNDEFINED_ENTITY* = 11;
    XML_ERROR_RECURSIVE_ENTITY_REF* = 12;
    XML_ERROR_ASYNC_ENTITY* = 13;
    XML_ERROR_BAD_CHAR_REF* = 14;
    XML_ERROR_BINARY_ENTITY_REF* = 15;
    XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF* = 16;
    XML_ERROR_MISPLACED_XML_PI* = 17;
    XML_ERROR_UNKNOWN_ENCODING* = 18;
    XML_ERROR_INCORRECT_ENCODING* = 19;
    XML_ERROR_UNCLOSED_CDATA_SECTION* = 20;
    XML_ERROR_EXTERNAL_ENTITY_HANDLING* = 21;
    XML_ERROR_NOT_STANDALONE* = 22;
    XML_ERROR_UNEXPECTED_STATE* = 23;

    XML_FEATURE_END* = 0;
    XML_FEATURE_UNICODE* = 1;
    XML_FEATURE_UNICODE_WCHAR_T* = 2;
    XML_FEATURE_DTD* = 3;
    XML_FEATURE_CONTEXT_BYTES* = 4;
    XML_FEATURE_MIN_SIZE* = 5;
    XML_FEATURE_SIZEOF_XML_CHAR* = 6;
    XML_FEATURE_SIZEOF_XML_LCHAR* = 7;

    XML_PARAM_ENTITY_PARSING_NEVER* = 0;
    XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE* = 1;
    XML_PARAM_ENTITY_PARSING_ALWAYS* = 2;

TYPE
  ADDRESS = (*SYSTEM.PTR;*) POINTER TO RECORD [untagged] END;

  XMLParser* = ADDRESS;

  PtrXMLExpatVersion* = POINTER TO TXMLExpatVersion;
  TXMLExpatVersion = RECORD [noalign]
    Major* : INTEGER;
    Minor* : INTEGER;
    Micro* : INTEGER;
  END;

(*  The XMLStatus enum gives the possible return values for several API functions.  *)
  XMLStatus* = INTEGER;

  XMLChar* = SHORTCHAR;
  ByteBool* = BOOLEAN;
  PtrXMLChar* = POINTER TO ARRAY [untagged] OF SHORTCHAR;
  PtrXMLLChar* = POINTER TO ARRAY [untagged] OF CHAR;

  XMLError* = INTEGER;

  XMLContentType* = INTEGER;

  XMLContentQuant* = INTEGER;

  (* If type == XML_CTYPE_EMPTY or XML_CTYPE_ANY, then quant will be  *)
  (* -   XML_CQUANT_NONE, and the other fields will be zero or NULL.  *)
  (* -   If type == XML_CTYPE_MIXED, then quant will be NONE or REP and  *)
  (* -   numchildren will contain number of elements that may be mixed in  *)
  (* -   and children point to an array of XMLContent cells that will be  *)
  (* -   all of XML_CTYPE_NAME type with no quantification.  *)

  (* -   If type == XML_CTYPE_NAME, then the name points to the name, and  *)
  (* -   the numchildren field will be zero and children will be NULL. The  *)
  (* -   quant fields indicates any quantifiers placed on the name.  *)

  (* -   CHOICE and SEQ will have name NULL, the number of children in  *)
  (* -   numchildren and children will point, recursively, to an array  *)
  (* -   of XMLContent cells.  *)

  (* -   The EMPTY, ANY, and MIXED types will only occur at top level.  *)

  PtrXMLContent* = POINTER TO XMLContent;
  PtrXMLContents* = POINTER TO XMLContents;
  XMLCp* = RECORD [noalign]
    Type_* :      XMLContentType;
    Quant* :      XMLContentQuant;
    Name* :       PtrXMLChar;
    NumChildren* : INTEGER;
    Children* :   PtrXMLContents;
  END;
  XMLContent* = XMLCp;
  XMLContents* = (*packed array*) ARRAY [untagged] 100 OF XMLContent; (* length is MAX(INTEGER) DIV SIZE(XMLContent) *)


  (* This is called for an element declaration. See above for  *)
  (* -   description of the model argument. It's the caller's responsibility  *)
  (* -   to free model when finished with it.  *)

  XMLElementDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Name: PtrXMLChar;
    Model: PtrXMLContent);

  TMallocFcn* = PROCEDURE [ccall] (Size: INTEGER): ADDRESS;
  TReallocFcn* = PROCEDURE [ccall] (Ptr: ADDRESS; Size: INTEGER): ADDRESS;
  TFreeFcn* = PROCEDURE [ccall] (Ptr: ADDRESS);

  PtrXMLMemoryHandlingSuite* = POINTER TO XMLMemoryHandlingSuite;
  XMLMemoryHandlingSuite* = RECORD [noalign]
    MallocFcn* : TMallocFcn;
    ReallocFcn* : TReallocFcn;
    FreeFcn* : TFreeFcn;
  END;

  XMLFeature* = INTEGER;

  PtrXMLFeatureInfo* = POINTER TO XMLFeatureInfo;
  XMLFeatureInfo* = RECORD [noalign]
    Feature* : XMLFeature;
    Name* : PtrXMLLChar;
    Value* : INTEGER
  END;

  PtrXMLFeatureList* = POINTER TO XMLFeatureList;
  XMLFeatureList* = (*packed array*) ARRAY [untagged] (MAX(INTEGER) DIV SIZE(XMLFeatureInfo))
    OF XMLFeatureInfo;

  (* -  The Attlist declaration handler is called for*each* attribute. So  *)
  (* -  a single Attlist declaration with multiple attributes declared will  *)
  (* -  generate multiple calls to this handler. The "default" parameter  *)
  (*    may be NULL in the case of the "#IMPLIED" or "#REQUIRED" keyword.  *)
  (* -  The "isrequired" parameter will be true and the default value will  *)
  (*    be NULL in the case of "#REQUIRED". If "isrequired" is true and  *)
  (*    default is non-NULL, then this is a "#FIXED" default.  *)

  XMLAttlistDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN ElName: PtrXMLChar;
    (*const*) IN Attname, AttType, Dflt: PtrXMLChar;
    IsRequired: INTEGER);

  (* The XML declaration handler is called for*both* XML declarations and  *)
  (* -   text declarations. The way to distinguish is that the version parameter  *)
  (* -   will be null for text declarations. The encoding parameter may be null  *)
  (* -   for XML declarations. The standalone parameter will be -1, 0, or 1  *)
  (* -   indicating respectively that there was no standalone parameter in  *)
  (* -   the declaration, that it was given as no, or that it was given as yes.  *)

  XMLDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Version, Encoding: PtrXMLChar;
    Standalone: INTEGER);

  (* attrs is array of name/value pairs, terminated by 0;  *)
  (* =      names and values are 0 terminated.  *)
  TAttrs = ARRAY (MAX(INTEGER) DIV SIZE(PtrXMLChar)) OF PtrXMLChar;
  PAttrs = POINTER TO TAttrs;

  XMLStartElementHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Name: PtrXMLChar;
    (*const*) IN Atts: TAttrs);

  XMLEndElementHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Name: PtrXMLChar);

  (* s is not 0 terminated.*/  *)
  XMLCharacterDataHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN S: PtrXMLChar;
    Len: INTEGER);

  (* target and data are 0 terminated*/  *)
  XMLProcessingInstructionHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Target: PtrXMLChar;
    (*const*) IN Data: PtrXMLChar);

  (* data is 0 terminated*/  *)
  XMLCommentHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Data: PtrXMLChar);

  XMLStartCdataSectionHandler* = PROCEDURE [ccall] (UserData: ADDRESS);
  XMLEndCdataSectionHandler* = PROCEDURE [ccall] (UserData: ADDRESS);

  (* This is called for any characters in the XML document for  *)
  (* -   which there is no applicable handler. This includes both  *)
  (* -   characters that are part of markup which is of a kind that is  *)
  (* -   not reported (comments, markup declarations), or characters  *)
  (* -   that are part of a construct which could be reported but  *)
  (* -   for which no handler has been supplied. The characters are passed  *)
  (* -   exactly as they were in the XML document except that  *)
  (* -   they will be encoded in UTF-8. Line boundaries are not normalized.  *)
  (* -   Note that a byte order mark character is not passed to the default handler.  *)
  (* -   There are no guarantees about how characters are divided between calls  *)
  (* -   to the default handler: for example, a comment might be split between  *)
  (* =      multiple calls.  *)
  XMLDefaultHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN S: PtrXMLChar;
    Len: INTEGER);

  (* This is called for the start of the DOCTYPE declaration, before  *)
  (** =      any DTD or internal subset is parsed.  *)
  XMLStartDoctypeDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN DoctypeName: PtrXMLChar;
    (*const*) IN SysId: PtrXMLChar;
    (*const*) IN PubId: PtrXMLChar;
    HasInternalSubset: INTEGER);

  (* This is called for the start of the DOCTYPE declaration when the  *)
  (** =      closing > is encountered, but after processing any external subset.  *)
  XMLEndDoctypeDeclHandler* = PROCEDURE [ccall] (UserData: ADDRESS);

  (* This is called for entity declarations. The is_parameter_entity  *)
  (* -   argument will be non-zero if the entity is a parameter entity, zero  *)
  (* -   otherwise.  *)

  (* -   For internal entities (<!ENTITY foo "bar">), value will  *)
  (* -   be non-null and systemId, publicID, and notationName will be null.  *)
  (* -   The value string is NOT null terminated; the length is provided in  *)
  (* -   the value_length argument. Since it is legal to have zero-length  *)
  (* -   values, do not use this argument to test for internal entities.  *)

  (* -   For external entities, value will be null and systemId will be non-null.  *)
  (* -   The publicId argument will be null unless a public identifier was  *)
  (* -   provided. The notationName argument will have a non-null value only  *)
  (* -   for unparsed entity declarations.  *)

  XMLEntityDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN EntityName: PtrXMLChar;
    IsParameterEntity: INTEGER;
    (*const*) IN Value: PtrXMLChar;
    ValueLength: INTEGER;
		(*const*) IN Base: PtrXMLChar;
    (*const*) IN SystemId: PtrXMLChar;
    (*const*) IN PublicId: PtrXMLChar;
    (*const*) IN NotationName: PtrXMLChar);

  XMLParamEntityParsing* = INTEGER;

  (* OBSOLETE -- OBSOLETE -- OBSOLETE  *)
  (* -   This handler has been superceded by the EntityDeclHandler above.  *)
  (* -   It is provided here for backward compatibility.  *)
  (* -   This is called for a declaration of an unparsed (NDATA)  *)
  (* -   entity. The base argument is whatever was set by XMLSetBase.  *)
  (* -   The entityName, systemId and notationName arguments will never be null.  *)
  (* =      The other arguments may be.  *)
  XMLUnparsedEntityDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN EntityName: PtrXMLChar;
    (*const*) IN Base: PtrXMLChar;
    (*const*) IN SystemId: PtrXMLChar;
    (*const*) IN PublicId: PtrXMLChar;
    (*const*) IN NotationName: PtrXMLChar);

  (* This is called for a declaration of notation.  *)
  (* -   The base argument is whatever was set by XMLSetBase.  *)
  (** =      The notationName will never be null. The other arguments can be.  *)
  XMLNotationDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN NotationName: PtrXMLChar;
    (*const*) IN Base: PtrXMLChar;
    (*const*) IN SystemId: PtrXMLChar;
    (*const*) IN PublicId: PtrXMLChar);

  (* When namespace processing is enabled, these are called once for  *)
  (* -   each namespace declaration. The call to the start and END element  *)
  (* -   handlers occur between the calls to the start and END namespace  *)
  (* -   declaration handlers. For an xmlns attribute, prefix will be null.  *)
  (** =      For an xmlns="" attribute, uri will be null.  *)
  XMLStartNamespaceDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Prefix: PtrXMLChar;
    (*const*) IN Uri: PtrXMLChar);

  XMLEndNamespaceDeclHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN Prefix: PtrXMLChar);

  (* This is called if the document is not standalone (it has an  *)
  (* -   external subset or a reference to a parameter entity, but does not  *)
  (* -   have standalone="yes"). If this handler returns 0, then processing  *)
  (* -   will not continue, and the Parser will return a  *)
  (** =      XML_ERROR_NOT_STANDALONE error.  *)
  XMLNotStandaloneHandler* = PROCEDURE [ccall] (UserData: ADDRESS): INTEGER;

  (* This is called for a reference to an external parsed general entity.  *)
  (* -   The referenced entity is not automatically parsed.  *)
  (* -   The application can parse it immediately or later using  *)
  (* -   XMLExternalEntityParserCreate.  *)
  (* -   The Parser argument is the Parser parsing the entity containing the reference;  *)
  (* -   it can be passed as the Parser argument to XMLExternalEntityParserCreate.  *)
  (* -   The systemId argument is the system identifier as specified in the entity declaration;  *)
  (* -   it will not be null.  *)
  (* -   The base argument is the system identifier that should be used as the base for  *)
  (* -   resolving systemId if systemId was relative; this is set by XMLSetBase;  *)
  (* -   it may be null.  *)
  (* -   The publicId argument is the public identifier as specified in the entity declaration,  *)
  (* -   or null if none was specified; the whitespace in the public identifier  *)
  (* -   will have been normalized as required by the XML spec.  *)
  (* -   The context argument specifies the parsing context in the format  *)
  (* -   expected by the context argument to  *)
  (* -   XMLExternalEntityParserCreate; context is valid only until the handler  *)
  (* -   returns, so if the referenced entity is to be parsed later, it must be copied.  *)
  (* -   The handler should return 0 if processing should not continue because of  *)
  (* -   a fatal error in the handling of the external entity.  *)
  (* -   In this case the calling Parser will return an XML_ERROR_EXTERNAL_ENTITY_HANDLING  *)
  (* -   error.  *)
  (** =      Note that unlike other handlers the first argument is the Parser, not UserData.  *)
  XMLExternalEntityRefHandler* = PROCEDURE [ccall] (
    Parser: XMLParser;
    (*const*) IN Context: PtrXMLChar;
    (*const*) IN Base: PtrXMLChar;
    (*const*) IN SystemId: PtrXMLChar;
    (*const*) IN PublicId: PtrXMLChar): INTEGER;

  (* This is called in two situations:  *)
  (* -   1) An entity reference is encountered for which no declaration  *)
  (* -      has been read *and* this is not an error.  *)
  (* -   2) An internal entity reference is read, but not expanded, because  *)
  (** =      XML_SetDefaultHandler has been called.  *)
  XMLSkippedEntityHandler* = PROCEDURE [ccall] (
    UserData: ADDRESS;
    (*const*) IN EntityName: PtrXMLChar;
    IsParameterEntity: INTEGER);

  (* This structure is filled in by the XMLUnknownEncodingHandler  *)
  (* -   to provide information to the Parser about encodings that are unknown  *)
  (* -   to the Parser.  *)
  (* -   The map[b] member gives information about byte sequences  *)
  (* -   whose first byte is b.  *)
  (* -   If map[b] is c where c is >= 0, then b by itself encodes the Unicode scalar value c.  *)
  (* -   If map[b] is -1, then the byte sequence is malformed.  *)
  (* -   If map[b] is -n, where n >= 2, then b is the first byte of an n-byte  *)
  (* -   sequence that encodes a single Unicode scalar value.  *)
  (* -   The data member will be passed as the first argument to the convert function .  *)
  (* -   The convert function is used to convert multibyte sequences;  *)
  (* -   s will point to a n-byte sequence where map[(unsigned char)*s] == -n.  *)
  (* -   The convert function must return the Unicode scalar value  *)
  (* -   represented by this byte sequence or -1 if the byte sequence is malformed.  *)
  (* -   The convert function may be null if the encoding is a single-byte encoding,  *)
  (* -   that is if map[b] >= -1 for all bytes b.  *)
  (* -   When the Parser is finished with the encoding, then if release is not null,  *)
  (* -   it will call release passing it the data member;  *)
  (* -   once release has been called, the convert function will not be called again.  *)

  (* -   Expat places certain restrictions on the encodings that are supported  *)
  (* -   using this mechanism.  *)

  (* -   1. Every ASCII character that can appear in a well-formed XML document,  *)
  (* -   other than the characters  *)

  (*   $@\^`(*  *)~ *)

  (* -   must be represented by a single byte, and that byte must be the  *)
  (* -   same byte that represents that character in ASCII.  *)

  (* -   2. No character may require more than 4 bytes to encode.  *)

  (* -   3. All characters encoded must have Unicode scalar values <= 0xFFFF,  *)
  (* -   (ie characters that would be encoded by surrogates in UTF-16  *)
  (* -   are not allowed). Note that this restriction doesn't apply to  *)
  (* -   the built-in support for UTF-8 and UTF-16.  *)

  (* -   4. No Unicode character may be encoded by more than one distinct sequence  *)
  (* =   of bytes.  *)
  TConvertEncoding* = PROCEDURE [ccall] (Data: ADDRESS; S: PtrXMLChar): INTEGER;
  TReleaseEncoding* = PROCEDURE [ccall] (Data: ADDRESS);

  PtrXMLEncoding* = POINTER TO XMLEncoding;
  XMLEncoding* = RECORD [noalign]
    Map* : ARRAY 256 OF INTEGER;
    Data* : ADDRESS;
    Convert* : TConvertEncoding;
    Release* : TReleaseEncoding;
  END;

  (* This is called for an encoding that is unknown to the Parser.  *)
  (* -   The encodingHandlerData argument is that which was passed as the  *)
  (* -   second argument to XMLSetUnknownEncodingHandler.  *)
  (* -   The name argument gives the name of the encoding as specified in  *)
  (* -   the encoding declaration.  *)
  (* -   If the callback can provide information about the encoding,  *)
  (* -   it must fill in the XMLEncoding structure, and return 1.  *)
  (* -   Otherwise it must return 0.  *)
  (* -   If info does not describe a suitable encoding,  *)
  (* =   then the Parser will return an XML_UNKNOWN_ENCODING error.  *)
  XMLUnknownEncodingHandler* = PROCEDURE [ccall] (
    EncodingHandlerData: ADDRESS;
    (*const*) IN Name: PtrXMLChar;
    Info: PtrXMLEncoding): INTEGER;


PROCEDURE [ccall] SetElementDeclhandler* ["XML_SetElementDeclhandler"] (
  Parser: XMLParser;
  ElDecl: XMLElementDeclHandler);

PROCEDURE [ccall] SetAttlistDeclHandler* ["XML_SetAttlistDeclHandler"] (
  Parser: XMLParser;
  AttDecl: XMLAttlistDeclHandler);

PROCEDURE [ccall] SetXmlDeclHandler* ["XML_SetXmlDeclHandler"] (
  Parser: XMLParser;
  XmlDecl: XMLDeclHandler);

(* Constructs a new Parser; encoding is the encoding specified by the external  *)
(* =      protocol or null if there is none specified.  *)
PROCEDURE [ccall] ParserCreate* ["XML_ParserCreate"] ((*const*) IN Encoding: PtrXMLChar): XMLParser;

(* Constructs a new Parser and namespace processor. Element type names  *)
(* -   and attribute names that belong to a namespace will be expanded;  *)
(* -   unprefixed attribute names are never expanded; unprefixed element type  *)
(* -   names are expanded only if there is a default namespace. The expanded  *)
(* -   name is the concatenation of the namespace URI, the namespace separator character,  *)
(* -   and the local part of the name. If the namespace separator is '\0' then  *)
(* -   the namespace URI and the local part will be concatenated without any  *)
(* -   separator. When a namespace is not declared, the name and prefix will be  *)
(* =      passed through without expansion.  *)
PROCEDURE [ccall] ParserCreateNS* ["XML_ParserCreateNS"] (
  (*const*) IN Encoding: PtrXMLChar;
  NamespaceSeparator: XMLChar): XMLParser;

(* Constructs a new Parser using the memory management suit referred to  *)
(* -   by memsuite. If memsuite is NULL, then use the standard library memory  *)
(* -   suite. If namespaceSeparator is non-NULL it creates a Parser with  *)
(* -   namespace processing as described above. The character pointed at  *)
(* -   will serve as the namespace separator.  *)

(* -   All further memory operations used for the created Parser will come from  *)
(* -   the given suite.  *)
PROCEDURE [ccall] ParserCreateMM* ["XML_ParserCreateMM"] (
  (*const*) IN Encoding: PtrXMLChar;
  (*const*) IN MemSuite: PtrXMLMemoryHandlingSuite;
  (*const*) IN NamespaceSeparator: PtrXMLChar): XMLParser;

(* Prepare a parser object to be re-used.  This is particularly  *)
(* -   valuable when memory allocation overhead is disproportionatly high,  *)
(* -   such as when a large number of small documnents need to be parsed.  *)
(* -   All handlers are cleared from the parser, except for the  *)
(* -   unknownEncodingHandler. The parser's external state is re-initialized  *)
(* =   except for the values of ns, ns_triplets and useForeignDTD.  *)
PROCEDURE [ccall] ParserReset* ["XML_ParserReset"] (
  Parser: XMLParser;
  (*const*) IN Encoding: PtrXMLChar): INTEGER;

PROCEDURE [ccall] SetEntityDeclHandler* ["XML_SetEntityDeclHandler"] (
  Parser: XMLParser;
  Handler: XMLEntityDeclHandler);

PROCEDURE [ccall] SetElementHandler* ["XML_SetElementHandler"] (
  Parser: XMLParser;
  Start: XMLStartElementHandler;
  End: XMLEndElementHandler);

PROCEDURE [ccall] SetStartElementhandler* ["XML_SetStartElementhandler"] (
  Parser: XMLParser;
  Handler: XMLStartElementHandler);

PROCEDURE [ccall] SetEndElementHandler* ["XML_SetEndElementHandler"] (
  Parser: XMLParser;
  Handler: XMLEndElementHandler);

PROCEDURE [ccall] SetCharacterDataHandler* ["XML_SetCharacterDataHandler"] (
  Parser: XMLParser;
  Handler: XMLCharacterDataHandler);

PROCEDURE [ccall] SetProcessingInstructionHandler* ["XML_SetProcessingInstructionHandler"] (
  Parser: XMLParser;
  Handler: XMLProcessingInstructionHandler);

PROCEDURE [ccall] SetCommentHandler* ["XML_SetCommentHandler"] (
  Parser: XMLParser;
  Handler: XMLCommentHandler);

PROCEDURE [ccall] SetCdataSectionHandler* ["XML_SetCdataSectionHandler"] (
  Parser: XMLParser;
  Start: XMLStartCdataSectionHandler;
  End: XMLEndCdataSectionHandler);

PROCEDURE [ccall] SetStartCdataSectionHandler* ["XML_SetStartCdataSectionHandler"] (
  Parser: XMLParser;
  Start: XMLStartCdataSectionHandler);

PROCEDURE [ccall] SetEndCdataSectionHandler* ["XML_SetEndCdataSectionHandler"] (
  Parser: XMLParser;
  End: XMLEndCdataSectionHandler);

(* This sets the default handler and also inhibits expansion of internal entities.  *)
(* -   The entity references will be passed to the default handler, or to the*)
(* =   skipped entity handler, if one is set.  *)
PROCEDURE [ccall] SetDefaultHandler* ["XML_SetDefaultHandler"] (
  Parser: XMLParser;
  Handler: XMLDefaultHandler);

(* This sets the default handler but does not inhibit expansion of internal entities.  *)
(* =   The entity reference will not be passed to the default handler.  *)
PROCEDURE [ccall] SetDefaultHandlerExpand* ["XML_SetDefaultHandlerExpand"] (
  Parser: XMLParser;
  Handler: XMLDefaultHandler);

PROCEDURE [ccall] SetDoctypeDeclHandler* ["XML_SetDoctypeDeclHandler"] (
  Parser: XMLParser;
  Start: XMLStartDoctypeDeclHandler;
  End: XMLEndDoctypeDeclHandler);

PROCEDURE [ccall] SetStartDoctypeDeclHandler* ["XML_SetStartDoctypeDeclHandler"] (
  Parser: XMLParser;
  Start: XMLStartDoctypeDeclHandler);

PROCEDURE [ccall] SetEndDoctypeDeclHandler* ["XML_SetEndDoctypeDeclHandler"] (
  Parser: XMLParser;
  End: XMLEndDoctypeDeclHandler);

PROCEDURE [ccall] SetUnparsedEntityDeclHandler* ["XML_SetUnparsedEntityDeclHandler"] (
  Parser: XMLParser;
  Handler: XMLUnparsedEntityDeclHandler);

PROCEDURE [ccall] SetNotationDeclHandler* ["XML_SetNotationDeclHandler"] (
  Parser: XMLParser;
  Handler: XMLNotationDeclHandler);

PROCEDURE [ccall] SetNameSpaceDeclHandler* ["XML_SetNameSpaceDeclHandler"] (
  Parser: XMLParser;
    Start: XMLStartNamespaceDeclHandler;
    End: XMLEndNamespaceDeclHandler);

PROCEDURE [ccall] SetStartNameSpaceDeclHandler* ["XML_SetStartNameSpaceDeclHandler"] (
  Parser: XMLParser;
  Start: XMLStartNamespaceDeclHandler);

PROCEDURE [ccall] SetEndNameSpaceDeclHandler* ["XML_SetEndNameSpaceDeclHandler"] (
  Parser: XMLParser;
  End: XMLEndNamespaceDeclHandler);

PROCEDURE [ccall] SetNotstandaloneHandler* ["XML_SetNotstandaloneHandler"] (
  Parser: XMLParser;
  Handler: XMLNotStandaloneHandler);

PROCEDURE [ccall] SetExternalEntityRefHandler* ["XML_SetExternalEntityRefHandler"] (
  Parser: XMLParser;
  Handler: XMLExternalEntityRefHandler);

(* If a non-null value for arg is specified here, then it will be passed  *)
(* -   as the first argument to the external entity ref handler instead  *)
(* =   of the Parser object.  *)
PROCEDURE [ccall] SetExternalEntityRefHandlerArg* ["XML_SetExternalEntityRefHandlerArg"] (
  Parser: XMLParser;
  Arg: ADDRESS);

PROCEDURE [ccall] SetSkippedEntityHandler* ["XML_SetSkippedEntityHandler"] (
  Parser: XMLParser;
  Handler: XMLSkippedEntityHandler);

(*  EncodingHandlerData works like UserData  *)
PROCEDURE [ccall] SetUnknownEncodingHandler* ["XML_SetUnknownEncodingHandler"] (
  Parser: XMLParser;
  Handler: XMLUnknownEncodingHandler;
  EncodingHandlerData: ADDRESS);

(* This can be called within a handler for a start element, END element,  *)
(* -   processing instruction or character data. It causes the corresponding  *)
(* =   markup to be passed to the default handler.  *)
PROCEDURE [ccall] DefaultCurrent* ["XML_DefaultCurrent"] (Parser: XMLParser);

(* If do_nst is non-zero, and namespace processing is in effect, and  *)
(* -   a name has a prefix (i.e. an explicit namespace qualifier) then  *)
(* -   that name is returned as a triplet in a single  *)
(* -   string separated by the separator character specified when the Parser  *)
(* -   was created: URI + sep + local_name + sep + prefix.  *)

(* -   If do_nst is zero, then namespace information is returned in the  *)
(* -   default manner (URI + sep + local_name) whether or not the names  *)
(* -   has a prefix.  *)

PROCEDURE [ccall] SetReturnNSTriplet* ["XML_SetReturnNSTriplet"] (
  Parser: XMLParser;
  DoNst: INTEGER);

(* This value is passed as the UserData argument to callbacks.*/  *)
PROCEDURE [ccall] SetUserData* ["XML_SetUserData"] (
  Parser: XMLParser;
  UserData: ADDRESS);

(* Returns the last value set by XMLSetUserData or null.*/  *)
(*     #define XMLGetUserData(Parser) (*(void **)(Parser))  *)
PROCEDURE [ccall] GetUserData* ["XML_GetUserData"] (Parser: XMLParser): ADDRESS;

(* This is equivalent to supplying an encoding argument  *)
(* -   to XMLParserCreate. It must not be called after XMLParse  *)
(* =   or XMLParseBuffer.  *)
PROCEDURE [ccall] SetEncoding* ["XML_SetEncoding"] (
  Parser: XMLParser;
  (*const*) IN Encoding: PtrXMLChar): INTEGER;

(* If this function is called, then the Parser will be passed  *)
(* -   as the first argument to callbacks instead of UserData.  *)
(* =   The UserData will still be accessible using XMLGetUserData.  *)
PROCEDURE [ccall] UseParserAsHandlerArg* ["XML_UseParserAsHandlerArg"] (Parser: XMLParser);

(* If useDTD == XML_TRUE is passed to this function , then the parser  *)
(* -   will assume that there is an external subset, even if none is  *)
(* -   specified in the document. In such a case the parser will call the  *)
(* -   externalEntityRefHandler with a value of NULL for the systemId  *)
(* -   argument (the publicId and context arguments will be NULL as well).  *)
(* -   Note: If this function is called, then this must be done before  *)
(* -     the first call to XML_Parse or XML_ParseBuffer, since it will  *)
(* -     have no effect after that.  Returns  *)
(* -     XML_ERROR_CANT_CHANGE_FEATURE_ONCE_PARSING.  *)
(* -   Note: If the document does not have a DOCTYPE declaration at all,  *)
(* -     then startDoctypeDeclHandler and endDoctypeDeclHandler will not  *)
(* -     be called, despite an external subset being parsed.  *)
(* -   Note: If XML_DTD is not defined when Expat is compiled, returns  *)
(* =     XML_ERROR_FEATURE_REQUIRES_XML_DTD.  *)
PROCEDURE [ccall] UseForeignDTD* ["XML_UseForeignDTD"] (
  Parser: XMLParser;
  useDTD: ByteBool);

(* Sets the base to be used for resolving relative URIs in system identifiers in  *)
(* -   declarations. Resolving relative identifiers is left to the application:  *)
(* -   this value will be passed through as the base argument to the  *)
(* -   XMLExternalEntityRefHandler, XMLNotationDeclHandler  *)
(* -   and XMLUnparsedEntityDeclHandler. The base argument will be copied.  *)
(* =   Returns zero if out of memory, non-zero otherwise.  *)
PROCEDURE [ccall] SetBase* ["XML_SetBase"] (
  Parser: XMLParser;
  (*const*) IN Base: PtrXMLChar): INTEGER;

PROCEDURE [ccall] GetBase* ["XML_GetBase"] (Parser: XMLParser): PtrXMLChar;

(* Returns the number of the attribute/value pairs passed in last call  *)
(* -   to the XMLStartElementHandler that were specified in the start-tag  *)
(* -   rather than defaulted. Each attribute/value pair counts as 2; thus  *)
(* -   this correspondds to an index into the atts ARRAY passed to the  *)
(* =   XMLStartElementHandler.  *)
PROCEDURE [ccall] GetSpecifiedAttributeCount* ["XML_GetSpecifiedAttributeCount"] (Parser: XMLParser): INTEGER;

(* Returns the index of the ID attribute passed in the last call to  *)
(* -   XMLStartElementHandler, or -1 if there is no ID attribute. Each  *)
(* -   attribute/value pair counts as 2; thus this correspondds to an index  *)
(* =   into the atts ARRAY passed to the XMLStartElementHandler.  *)
PROCEDURE [ccall] GetIdAttributeIndex* ["XML_GetIdAttributeIndex"] (Parser: XMLParser): INTEGER;

(* Parses some input. Returns 0 if a fatal error is detected.  *)
(* -   The last call to XMLParse must have isFinal true;  *)
(* =   len may be zero for this call (or any other).  *)
PROCEDURE [ccall] Parse* ["XML_Parse"] (
  Parser: XMLParser;
  (*const*) IN S: PtrXMLChar;
  Len: INTEGER;
  IsFinal: INTEGER): XMLStatus;

PROCEDURE [ccall] GetBuffer* ["XML_GetBuffer"] (
  Parser: XMLParser;
  Len: INTEGER): ADDRESS;

PROCEDURE [ccall] ParseBuffer* ["XML_ParseBuffer"] (
  Parser: XMLParser;
  Len: INTEGER;
  IsFinal: INTEGER): INTEGER;

(* Creates an XMLParser object that can parse an external general entity;  *)
(* -   context is a '\0'-terminated string specifying the parse context;  *)
(* -   encoding is a '\0'-terminated string giving the name of the externally specified encoding,  *)
(* -   or null if there is no externally specified encoding.  *)
(* -   The context string consists of a sequence of tokens separated by formfeeds (\f);  *)
(* -   a token consisting of a name specifies that the general entity of the name  *)
(* -   is open; a token of the form prefix=uri specifies the namespace for a particular  *)
(* -   prefix; a token of the form =uri specifies the default namespace.  *)
(* -   This can be called at any point after the first call to an ExternalEntityRefHandler  *)
(* -   so longer as the Parser has not yet been freed.  *)
(* -   The new Parser is completely independent and may safely be used in a separate thread.  *)
(* -   The handlers and UserData are initialized from the Parser argument.  *)
(* =   Returns 0 if out of memory. Otherwise returns a new XMLParser object.  *)
PROCEDURE [ccall] ExternalEntityParserCreate* ["XML_ExternalEntityParserCreate"] (
  Parser: XMLParser;
  (*const*) IN Context: PtrXMLChar;
  (*const*) IN Encoding: PtrXMLChar): XMLParser;

(* Controls parsing of parameter entities (including the external DTD  *)
(* -   subset). If parsing of parameter entities is enabled, then references  *)
(* -   to external parameter entities (including the external DTD subset)  *)
(* -   will be passed to the handler set with  *)
(* -   XMLSetExternalEntityRefHandler. The context passed will be 0.  *)
(* -   Unlike external general entities, external parameter entities can only  *)
(* -   be parsed synchronously. If the external parameter entity is to be  *)
(* -   parsed, it must be parsed during the call to the external entity ref  *)
(* -   handler: the complete sequence of XMLExternalEntityParserCreate,  *)
(* -   XMLParse/XMLParseBuffer and XMLParserFree calls must be made during  *)
(* -   this call. After XMLExternalEntityParserCreate has been called to  *)
(* -   create the Parser for the external parameter entity (context must be 0  *)
(* -   for this call), it is illegal to make any calls on the old Parser  *)
(* -   until XMLParserFree has been called on the newly created Parser. If  *)
(* -   the library has been compiled without support for parameter entity  *)
(* -   parsing (ie without XML_DTD being defined), then  *)
(* -   XMLSetParamEntityParsing will return 0 if parsing of parameter  *)
(* =   entities is requested; otherwise it will return non-zero.  *)
PROCEDURE [ccall] SetParamEntityParsing* ["XML_SetParamEntityParsing"] (
  Parser: XMLParser;
  Parsing: XMLParamEntityParsing): INTEGER;

(* If XMLParse or XMLParseBuffer have returned 0, then XMLGetErrorCode  *)
(* =   returns information about the error.  *)
PROCEDURE [ccall] GetErrorCode* ["XML_GetErrorCode"] (Parser: XMLParser): XMLError;

(* These functions return information about the current parse location.  *)
(* -   They may be called when XMLParse or XMLParseBuffer return 0;  *)
(* -   in this case the location is the location of the character at which  *)
(* -   the error was detected.  *)
(* -   They may also be called from any other callback called to report  *)
(* -   some parse event; in this the location is the location of the first  *)
(* =   of the sequence of characters that generated the event.  *)
PROCEDURE [ccall] GetCurrentLineNumber* ["XML_GetCurrentLineNumber"] (Parser: XMLParser): INTEGER;
PROCEDURE [ccall] GetCurrentColumnNumber* ["XML_GetCurrentColumnNumber"] (Parser: XMLParser): INTEGER;
PROCEDURE [ccall] GetCurrentByteIndex* ["XML_GetCurrentByteIndex"] (Parser: XMLParser): INTEGER;

(* Return the number of bytes in the current event.  *)
(* =   Returns 0 if the event is in an internal entity.  *)
PROCEDURE [ccall] GetCurrentByteCount* ["XML_GetCurrentByteCount"] (Parser: XMLParser): INTEGER;

(* If XML_CONTEXT_BYTES is defined, returns the input buffer, sets  *)
(* -   the INTEGER pointed to by offset to the offset within this buffer  *)
(* -   of the current parse position, and sets the INTEGER pointed to by size  *)
(* -   to the size of this buffer (the number of input bytes). Otherwise  *)
(* -   returns a null ADDRESS. Also returns a null ADDRESS if a parse isn't active.  *)

(* -   NOTE: The character ADDRESS returned should not be used outside  *)
(* =   the handler that makes the call.  *)
PROCEDURE [ccall] GetInputContext* ["XML_GetInputContext"] (
  Parser: XMLParser;
  VAR Offset: INTEGER;
  VAR Size: INTEGER): PtrXMLChar;

(* Frees memory used by the Parser.  *)
PROCEDURE [ccall] ParserFree* ["XML_ParserFree"] (Parser: XMLParser);

(* Returns a string describing the error.  *)
PROCEDURE [ccall] ErrorString* ["XML_ErrorString"] (Code: INTEGER): PtrXMLLChar;

(* Return a string containing the version number of this expat  *)
PROCEDURE [ccall] ExpatVersion* ["XML_ExpatVersion"] () : PtrXMLLChar;

(* Return a XMLExpatVersion record with version details  *)
PROCEDURE [ccall] ExpatVersionInfo* ["XML_ExpatVersionInfo"] () : PtrXMLExpatVersion;

(* Return a list feature macro descriptions  *)
PROCEDURE [ccall] GetFeatureList* ["XML_GetFeatureList"] () : PtrXMLFeatureList;

(* Frees the content model passed to the element declaration handler  *)
PROCEDURE [ccall] FreeContentModel* ["XML_FreeContentModel"] (Parser: XMLParser; Model: PtrXMLContent);

(* Exposing the memory handling functions used in Expat  *)
PROCEDURE [ccall] MemMalloc* ["XML_MemMalloc"] (
  Parser: XMLParser;
  Size: INTEGER): ADDRESS;
PROCEDURE [ccall] MemRealloc* ["XML_MemRealloc"] (
  Parser: XMLParser;
  Ptr: ADDRESS;
  Size: INTEGER): ADDRESS;
PROCEDURE [ccall] MemFree* ["XML_MemFree"] (
  Parser: XMLParser;
  Ptr: ADDRESS);

END Expat.