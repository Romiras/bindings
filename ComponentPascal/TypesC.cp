MODULE LibsTypesC;

	IMPORT SYSTEM;

	TYPE
		BOOL* = INTEGER;
		UInt8* = BYTE;
		UInt32* = INTEGER;
		SInt32* = INTEGER;
		DWORD* = UInt32;
		
		String* = POINTER TO ARRAY [untagged] OF SHORTCHAR;

END LibsTypesC.