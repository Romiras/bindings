(*
	incomplete interface to FreeType2 library
	
	2010, Romiras
*)

MODULE LibsFreeType ["freetype6"];

	IMPORT SYSTEM;
	
	TYPE
		Pointer* = POINTER TO RECORD [untagged] END;
		
		FT_Byte* = SHORTCHAR;
		FT_Short* = SHORTINT;
		FT_UShort* = SHORTINT;
		FT_Int* = INTEGER;
		FT_UInt* = INTEGER;
		FT_Int32* = INTEGER;
		FT_Long* = INTEGER;
		FT_ULong* = INTEGER;
		FT_Fixed* = INTEGER;
		FT_Pos* = INTEGER;
		FT_Error* = INTEGER;
		FT_F26Dot6* = INTEGER;
		FT_String* = POINTER TO ARRAY [untagged] OF SHORTCHAR;
		
		FT_Byte_ptr*  = POINTER TO ARRAY [untagged] OF FT_Byte;
		FT_Short_ptr* = POINTER TO ARRAY [untagged] OF FT_Short;
		
		FT_Render_Mode* = FT_Int;
		
		FT_Encoding* = ARRAY 4 OF SHORTCHAR;
		
		FT_BBox* = RECORD [noalign]
			xMin ,
			yMin ,
			xMax ,
			yMax : FT_Pos;
		END;
		FT_BBox_ptr* = POINTER TO FT_BBox;
		
		FT_Bitmap_Size* = RECORD
			height ,
			width  : FT_Short;
		END;
		FT_Bitmap_Size_ptr* = POINTER TO ARRAY [untagged] 1024 OF FT_Bitmap_Size;
		
		FT_Generic_Finalizer* = PROCEDURE [ccall] (AnObject : Pointer);
		FT_Generic* = RECORD [noalign]
			data      : Pointer;
			finalizer : FT_Generic_Finalizer;
		END;
		
		FT_Charmap* = RECORD [noalign]
			face     : FT_Face_ptr;
			encoding : FT_Encoding;
			platform_id ,
			encoding_id : FT_UShort;
		END;
		FT_CharMap_ptr*     = POINTER TO ARRAY [untagged] OF FT_Charmap;
		FT_CharMap_ptr_ptr* = POINTER TO ARRAY [untagged] OF ARRAY OF FT_CharMap_ptr;
		
		FT_Library* = RECORD [untagged] END;
		FT_Library_ptr* = POINTER TO FT_Library;
		FT_Library_ptr_ptr* = POINTER TO ARRAY [untagged] OF FT_Library_ptr;
		
		FT_Vector* = RECORD [noalign]
			x ,
			y : FT_Pos;
		END;
		FT_Vector_ptr* = POINTER TO FT_Vector;
		
		FT_Bitmap* = RECORD [noalign]
			rows   ,
			width  ,
			pitch  : FT_Int;

			buffer : Pointer;

			num_grays    : FT_Short;
			pixel_mode   ,
			palette_mode : SHORTCHAR;

			palette : Pointer;
		END;
		FT_Bitmap_ptr* = POINTER TO FT_Bitmap;
		
		FT_Outline* = RECORD [noalign]
			n_contours ,
			n_points   : FT_Short;

			points : FT_Vector_ptr;
			tags   : FT_String;

			contours : FT_Short_ptr;
			flags    : FT_Int;
		END;
		FT_Outline_ptr* = POINTER TO FT_Outline;
		
		FT_SubGlyph* = RECORD [untagged] (* TODO *) END;
		FT_SubGlyph_ptr* = POINTER TO FT_SubGlyph; 
		
		FT_Glyph_Metrics* = RECORD [noalign]
			width  ,
			height ,
			horiBearingX ,
			horiBearingY ,
			horiAdvance  ,
			vertBearingX ,
			vertBearingY ,
			vertAdvance  : FT_Pos;
		END;
		
		FT_GlyphSlot* = RECORD [noalign]
			alibrary : FT_Library_ptr;

			face  : FT_Face_ptr;
			next  : FT_GlyphSlot_ptr;
			flags : FT_UInt;

			generic : FT_Generic;
			metrics : FT_Glyph_Metrics;

			linearHoriAdvance ,
			linearVertAdvance : FT_Fixed;

			advance : FT_Vector;
			format  : FT_ULong;
			bitmap  : FT_Bitmap;

			bitmap_left ,
			bitmap_top  : FT_Int;

			outline : FT_Outline;

			num_subglyphs : FT_UInt;
			subglyphs     : FT_SubGlyph_ptr;
			control_data  : Pointer;
			control_len   : INTEGER;

			other : Pointer;
		END;
		FT_GlyphSlot_ptr* = POINTER TO FT_GlyphSlot;
		
		FT_Size_Metrics* = RECORD
			x_ppem  ,
			y_ppem  : FT_UShort;
			x_scale ,
			y_scale : FT_Fixed;

			ascender    ,
			descender   ,
			height      ,
			max_advance : FT_Pos;
		END;
		
		FT_Size* = RECORD [noalign]
			face    : FT_Face_ptr;
			generic : FT_Generic;
			metrics : FT_Size_Metrics;
			(* internal : FT_Size_Internal; *)
		END;
		FT_Size_ptr* = POINTER TO FT_Size;
		
		FT_Face* = RECORD [noalign]
			num_faces-   ,
			face_index-  ,
			face_flags-  ,
			style_flags- ,
			num_glyphs-  : FT_Long;
			family_name- ,
			style_name-  : FT_String;

			num_fixed_sizes- : FT_Int;
			available_sizes- : FT_Bitmap_Size_ptr; (* is array *)

			num_charmaps- : FT_Int;
			charmaps-     : FT_CharMap_ptr;    (* is array *)

			generic- : FT_Generic;
			bbox-    : FT_BBox;

			units_per_EM- : FT_UShort;

			ascender-  ,
			descender- ,
			height-    ,

			max_advance_width-   ,
			max_advance_height-  ,
			underline_position-  ,
			underline_thickness- : FT_Short;

			glyph-   : FT_GlyphSlot_ptr;
			size-    : FT_Size_ptr;
			charmap- : FT_CharMap_ptr;
		END;
		FT_Face_ptr* = POINTER TO FT_Face;
	
	CONST
		(* fterrdef.h *)
		FT_Err_Ok* = 00H;
		FT_Err_Cannot_Open_Resource* = 01H;
		FT_Err_Unknown_File_Format* = 02H;
		FT_Err_Invalid_File_Format* = 03H;
		FT_Err_Invalid_Version* = 04H;
		FT_Err_Lower_Module_Version* = 05H;
		FT_Err_Invalid_Argument* = 06H;
		FT_Err_Unimplemented_Feature* = 07H;
		FT_Err_Invalid_Table* = 08H;
		FT_Err_Invalid_Offset* = 09H;
		FT_Err_Array_Too_Large* = 0AH;
		FT_Err_Out_Of_Memory* = 040H;
		FT_Err_Unlisted_Object* = 041H;
		FT_Err_Invalid_Glyph_Index* = 010H;
		FT_Err_Invalid_Character_Code* = 011H;
(*		FT_Err_* = ;*)
		
		FT_CURVE_TAG_ON*    = 1;
		FT_CURVE_TAG_CONIC* = 0;
		FT_CURVE_TAG_CUBIC* = 2;
		
		FT_FACE_FLAG_SCALABLE* = {0};
		FT_FACE_FLAG_FIXED_SIZES*  = {1};
		FT_FACE_FLAG_FIXED_WIDTH* = {2};
		FT_FACE_FLAG_SFNT* = {3};
		FT_FACE_FLAG_HORIZONTAL* = {4};
		FT_FACE_FLAG_VERTICAL* = {5};
		FT_FACE_FLAG_KERNING* = {6};
		FT_FACE_FLAG_FAST_GLYPHS* = {7};
		FT_FACE_FLAG_MULTIPLE_MASTERS* = {8};
		FT_FACE_FLAG_GLYPH_NAMES* = {9};
		FT_FACE_FLAG_EXTERNAL_STREAM* = {10};

		FT_LOAD_DEFAULT*        = 0000H;
		FT_LOAD_NO_HINTING*     = 0002H;
		FT_LOAD_FORCE_AUTOHINT* = 0020H;

		FT_RENDER_MODE_NORMAL* = 0;
		FT_RENDER_MODE_LIGHT*  = FT_RENDER_MODE_NORMAL + 1;
		FT_RENDER_MODE_MONO*   = FT_RENDER_MODE_LIGHT + 1;
		FT_RENDER_MODE_LCD*    = FT_RENDER_MODE_MONO + 1;
		FT_RENDER_MODE_LCD_V*  = FT_RENDER_MODE_LCD + 1;
		FT_RENDER_MODE_MAX*    = FT_RENDER_MODE_LCD_V + 1;

		FT_KERNING_DEFAULT*  = 0;
		FT_KERNING_UNFITTED* = 1;
		FT_KERNING_UNSCALED* = 2;

		FT_STYLE_FLAG_ITALIC* = {0};
		FT_STYLE_FLAG_BOLD*   = {1};

		(* FT_ENCODING_NONE* : FT_Encoding = (0X ,0X , 0X, 0X); *)
		

PROCEDURE [ccall] FT_Init_FreeType* (VAR alibrary : FT_Library_ptr ) : FT_Error;

PROCEDURE [ccall] FT_Done_FreeType* (alibrary : FT_Library_ptr ) : FT_Error;

PROCEDURE [ccall] FT_Attach_File* (VAR face : FT_Face_ptr; filepathname : FT_String ) : FT_Error;

PROCEDURE [ccall] FT_New_Memory_Face* (
            library_ : FT_Library_ptr;
            file_base : FT_Byte_ptr;
            file_size ,
            face_index : FT_Long;
            VAR aface : FT_Face_ptr ) : FT_Error;

PROCEDURE [ccall] FT_New_Face* (
            library_ : FT_Library_ptr;
            filepathname : FT_String;
            face_index : FT_Long;
            VAR aface : FT_Face_ptr ) : FT_Error;

PROCEDURE [ccall] FT_Done_Face* (face : FT_Face_ptr ) : FT_Error;

PROCEDURE [ccall] FT_Select_Charmap* (face : FT_Face_ptr; encoding : FT_Encoding ) : FT_Error;

PROCEDURE [ccall] FT_Get_Char_Index* (face : FT_Face_ptr; charcode : FT_ULong ) : FT_UInt;

PROCEDURE [ccall] FT_Load_Glyph* (
            VAR face : FT_Face_ptr;
            glyph_index : FT_UInt ;
            load_flags : FT_Int32 ) : FT_Error;

PROCEDURE [ccall] FT_Render_Glyph* (slot : FT_GlyphSlot_ptr; render_mode : FT_Render_Mode ) : FT_Error;

PROCEDURE [ccall] FT_Get_Kerning* (
            face : FT_Face_ptr;
            left_glyph ,right_glyph ,kern_mode : FT_UInt;
            akerning : FT_Vector_ptr ) : FT_Error;

PROCEDURE [ccall] FT_Set_Char_Size* (
            face : FT_Face_ptr;
            char_width ,char_height : FT_F26Dot6;
            horz_res ,vert_res : FT_UInt) : FT_Error;

PROCEDURE [ccall] FT_Set_Pixel_Sizes* (
            VAR face : FT_Face_ptr;
            pixel_width ,pixel_height : FT_UInt ) : FT_Error;
	
		
END LibsFreeType.